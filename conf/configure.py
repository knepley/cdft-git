#!/usr/bin/env python
if __name__ == '__main__':
  import sys
  sys.path.insert(0, '/home/dynamics/petsc/petsc-dev/config')
  import configure
  configure_options = ['--configModules=PETSc.Configure', '--download-mpich=yes', '--with-shared=0', '--with-scalar-type=complex', '--download-f-blas-lapack=yes', '-PETSC_ARCH=linux-gnu-cxx-fftw', '--with-clanguage=C++', '--download-fftw']
  configure.petsc_configure(configure_options)
