#!/usr/bin/env python
from pylab import *
import numpy

plotType = 3

if plotType == 1:
  # Step function convergence
  x1D = numpy.array([4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192])
  y1D = [0.347623, 0.278399, 0.192507, 0.134194, 0.0941318, 0.0662799, 0.0467651, 0.0330315, 0.0233439, 0.016502, 0.0116671, 0.00824929]
  y1DModel = 0.75/sqrt(x1D)

  loglog(x1D, y1D, 'b-', x1D, y1DModel, 'g-')
  show()
  plot(x1D, log(y1D - y1DModel))
  show()
elif plotType == 2:
  # Step function convergence
  x3D = [16, 32, 64, 128]
  y3D = [0.257016, 0.260585, 0.262994, 0.264182]
  #y3DModel = 0.75*pow(x, -0.5)

  loglog(x3D, y3D, 'b-')
  #loglog(x3D, y3D, 'b-', x3D, y3DModel, 'g-')
  show()
  #plot(x3D, log(y3D - y3DModel))
  #show()
elif plotType == 3:
  # Convolution convergence
  x1D = numpy.array([32, 64, 128, 256, 512, 1024, 2048, 4096, 8192])
  y1D = [0.085494, 0.0614772, 0.0438326, 0.0311222, 0.0220519, 0.015609, 0.0110429, 0.0078105, 0.00552357]
  y1DModel = 0.5/sqrt(x1D)

  loglog(x1D, y1D, 'b-', x1D, y1DModel, 'g-')
  show()
  plot(x1D, log(abs(y1D - y1DModel)))
  show()
