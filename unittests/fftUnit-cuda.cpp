#include <petscda.h>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/extensions/HelperMacros.h>

typedef struct _Options {
  int debug;    // The debugging level
  int iters;    // The number of test repetitions
  bool cuda;    // Use the CUDA kernels
  PetscScalar *coords; // Coordinates of the point for calculating the phase
  PetscScalar (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *);
  PetscScalar * R; //the screening radius(assumed constant)
} Options;

static PetscReal epsilon = 1.0e-10;

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
static PetscErrorCode ProcessOptions(MPI_Comm comm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  options->debug = 0;
  options->iters = 100;
  options->cuda = FALSE;
  options->R = 1./4.;

  ierr = PetscOptionsBegin(comm, "", "Options for fft memory test", "DFT");CHKERRQ(ierr);
    ierr = PetscOptionsInt("-debug", "The debugging level", "DFT", options->debug, &options->debug, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-iterations", "The number of test repetitions", "DFT", options->iters, &options->iters, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsTruth("-cuda", "Run the pseudoconvolution test using CUDA", options->cuda, &options->cuda, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-R", "The (constant) screening radius)", options->R, &options->R, PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenter"
PetscErrorCode VecViewCenter(DA da, Vec v, PetscViewer viewer, const char name[])
{
  DALocalInfo    info;
  MPI_Comm       comm;
  Vec            c;
  PetscScalar ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) da, &comm);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Viewing %s\n", name);
  ierr = DAGetLocalInfo(da, &info);CHKERRQ(ierr);
  if (info.dim == 1) {
    ierr = VecView(v, viewer);CHKERRQ(ierr);
  } else {
    ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
    ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions(c);CHKERRQ(ierr);
    ierr = DAVecGetArray(da, v, &a);CHKERRQ(ierr);
    ierr = VecGetArray(c, &b);CHKERRQ(ierr);
    for(PetscInt k = 0, i = info.mx/2, j = info.my/2; k < info.mz; ++k) {
      b[k] = a[k][j][i];
    }
    ierr = DAVecRestoreArray(da, v, &a);CHKERRQ(ierr);
    ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);
    ierr = VecView(c, viewer);
    ierr = VecDestroy(c);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateGeometry"
PetscErrorCode CalculateGeometry(DA da, PetscInt dim[], PetscReal L[], PetscReal h[], Options *options) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetInfo(da, 0, &dim[0],&dim[1],&dim[2],0,0,0,0,0,0,0);CHKERRQ(ierr);
  if (options->debug > 1) {std::cout << "CalculateGeometry" << std::endl;}
  for(PetscInt d = 0; d < 3; ++d) {
    h[d] = 1.0/dim[d];
    L[d] = h[d]*(dim[d] - 1);
    if (options->debug > 1) {std::cout << "  L["<<d<<"]: " << L[d] << " h["<<d<<"]: " << h[d] << std::endl;}
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateK"
// Calculate the corresponding k wavevector from an x position
PetscErrorCode CalculateK(const PetscScalar x[], const PetscReal h[], const PetscInt dim[], PetscReal k[], Options *options) {
  PetscReal indices[3] = {PetscRealPart(x[0])/h[0], PetscRealPart(x[1])/h[1], PetscRealPart(x[2])/h[2]};

  PetscFunctionBegin;
  for(PetscInt d = 0; d < 3; ++d){
    if (indices[d] <= dim[d]/2.0) {
      if (options->debug > 1) {std::cout << "x["<<d<<"]: " << x[d] << " index: " << indices[d] << " denom: " << dim[d]*h[d] << std::endl;}
      k[d] =  2.0*M_PI*indices[d]/(dim[d]*h[d]);
    } else {
      if (options->debug > 1) {std::cout << "x["<<d<<"]: " << x[d] << " index: " << dim[d]-indices[d] << " denom: " << dim[d]*h[d] << std::endl;}
      k[d] = -2.0*M_PI*(dim[d] - indices[d])/(dim[d]*h[d]);
    }
  }
  PetscFunctionReturn(0);
};

#undef __FUNCT__
#define __FUNCT__ "step1D"
/*
  This is the step function \theta(|x - 0.375| - 0.125)
*/
PetscScalar step1D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  if (PetscRealPart(x[0]) >= 0.25 && PetscRealPart(x[0]) <= 0.5) {
    return 1.0;
  }
  return 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "step1DHat"
/*
  This is the Fourier transform of the step function \theta(|x - 0.375| - 0.125):

    \frac{e^{-ikx/2} - e^{-ikx/4}}{-ik}

  The limit as k goes to 0 is 0.25.
*/
PetscScalar step1DHat(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3];
  PetscScalar    X[3] = {x[0], 0.0, 0.0};
  PetscErrorCode ierr;

  ierr = CalculateK(X, h, dim, k, options);CHKERRXX(ierr);
  if (fabs(k[0]) < epsilon) {
    return 0.25;
  }
  return (PetscExpScalar(-PETSC_i*k[0]*0.5) - PetscExpScalar(-PETSC_i*k[0]*0.25))/(-PETSC_i*k[0]);
}

#undef __FUNCT__
#define __FUNCT__ "cos1D"
/*
  This is the signal function \cos(2\pi x/L) = \cos(2\pi x/(M h))
*/
PetscScalar cos1D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  return cos(2.0*M_PI*x[0]/(dim[0]*h[0]));
}

#undef __FUNCT__
#define __FUNCT__ "cos1DHat"
/*
  This is the Fourier transform of the signal function \cos(2\pi x/L):

    \frac{1}{2} \delta(k - 2\pi/L) + \frac{1}{2} \delta(k + 2\pi/L)
*/
PetscScalar cos1DHat(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3];
  PetscScalar    X[3] = {x[0], 0.0, 0.0};
  PetscErrorCode ierr;

  ierr = CalculateK(X, h, dim, k, options);CHKERRXX(ierr);
  if (fabs(k[0] - 2.0*M_PI) < epsilon) {
    return 0.5;
  } else if (fabs(k[0] + 2.0*M_PI) < epsilon) {
    return 0.5;
  }
  return 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "conv1D"
/*
  This is the convolution function \int^L_0 \cos(2\pi x/L)
*/
PetscScalar conv1D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal L = dim[0]*h[0];
  PetscReal c = 2.0*M_PI/L;

  return (-1.0/c)*(sin(c*(x[0] - 0.5)) - sin(c*(x[0] - 0.25)));
}

#undef __FUNCT__
#define __FUNCT__ "phase1DHat"
/*
  This is the phase for the FT method:

    e^{ikx}
*/
PetscScalar phase1D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3];
  PetscScalar    X[3] = {x[0], 0.0, 0.0};
  PetscErrorCode ierr;

  ierr = CalculateK(X, h, dim, k, options);CHKERRXX(ierr);
  return PetscExpScalar(PETSC_i*k[0]*options->coords[0]);
}

#undef __FUNCT__
#define __FUNCT__ "step3D"
/*
  This is the step function \theta(|x - (0.5,0.5,0.5)| - 0.25)
*/
PetscScalar step3D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal diff[3] = {PetscRealPart(x[0]) - 0.5, PetscRealPart(x[1]) - 0.5, PetscRealPart(x[2]) - 0.5};
  PetscReal r       = sqrt(diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]);

  if (r <= 0.125) {
    return 1.0;
  }
  return 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "step3DHat"
/*
  This is the Fourier transform of the step function \theta(|x - (0.5,0.5,0.5)| - 0.25):

    \omega^3 = 4\pi/k^3 (sin kR - kR cos kR)

  The limit as k goes to 0 is 4\pi/3 R^3.
*/
PetscScalar step3DHat(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3], K, KR;
  PetscErrorCode ierr;

  ierr = CalculateK(x, h, dim, k, options);CHKERRXX(ierr);
  K  = sqrt(k[0]*k[0] + k[1]*k[1] + k[2]*k[2]);
  KR = K*options->R;
  if (fabs(K) < epsilon) {
    return M_PI/(3*128);
  }
  return 4.0*M_PI/(K*K*K)*(sin(KR) - KR*cos(KR));
}

#undef __FUNCT__
#define __FUNCT__ "cos3D"
/*
  This is the signal function \cos(2\pi x/L)\cos(2\pi y/L)\cos(2\pi z/L)
*/
PetscScalar cos3D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  return cos(2.0*M_PI*x[0]/(dim[0]*h[0]))*cos(2.0*M_PI*x[1]/(dim[1]*h[1]))*cos(2.0*M_PI*x[2]/(dim[2]*h[2]));
}

#undef __FUNCT__
#define __FUNCT__ "cos3DHat"
/*
  This is the Fourier transform of the signal function \cos(2\pi x/L)\cos(2\pi y/L)\cos(2\pi z/L):

    (\frac{1}{2} \delta(k_x - 2\pi/L) + \frac{1}{2} \delta(k_x + 2\pi/L))
    (\frac{1}{2} \delta(k_y - 2\pi/L) + \frac{1}{2} \delta(k_y + 2\pi/L))
    (\frac{1}{2} \delta(k_z - 2\pi/L) + \frac{1}{2} \delta(k_z + 2\pi/L))
*/
PetscScalar cos3DHat(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3];
  PetscReal      v = 1.0;
  PetscErrorCode ierr;

  ierr = CalculateK(x, h, dim, k, options);CHKERRXX(ierr);
  for(int d = 0; d < 3; ++d) {
    if (fabs(k[d] - 2.0*M_PI) < epsilon) {
      v *= 0.5;
    } else if (fabs(k[d] + 2.0*M_PI) < epsilon) {
      v *= 0.5;
    } else {
      v *= 0.0;
    }
  }
  return v;
}

#undef __FUNCT__
#define __FUNCT__ "phase3DHat"
/*
  This is the phase for the FT method:

    e^{ikx}
*/
PetscScalar phase3D(const PetscScalar x[], const PetscInt dim[], const PetscReal h[], Options *options) {
  PetscReal      k[3];
  PetscErrorCode ierr;

  ierr = CalculateK(x, h, dim, k, options);CHKERRXX(ierr);
  return PetscExpScalar(PETSC_i*(k[0]*options->coords[0] + k[1]*options->coords[1] + k[2]*options->coords[2]));
}

#undef __FUNCT__
#define __FUNCT__ "Function_1d"
PetscErrorCode Function_1d(DALocalInfo *info, PetscScalar x[], PetscScalar f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, Options *) = options->func;
  DA             coordDA;
  Vec            coordinates;
  PetscScalar   *coords;
  PetscReal      L[3];
  PetscInt       dim[3];
  PetscReal      h[3];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = CalculateGeometry(info->da, dim, L, h, options);CHKERRQ(ierr);
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
    ///std::cout << "coords["<<i<<"]: " << coords[i] << std::endl;
    f[i] = func(&coords[i], dim, h, options);
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = VecDestroy(coordinates);CHKERRQ(ierr);
  ierr = DADestroy(coordDA);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_2d"
PetscErrorCode Function_2d(DALocalInfo *info, PetscScalar *x[], PetscScalar *f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, Options *) = options->func;
  DA             coordDA;
  Vec            coordinates;
  DACoor2d     **coords;
  PetscReal      L[3];
  PetscInt       dim[3];
  PetscReal      h[3];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = CalculateGeometry(info->da, dim, L, h, options);CHKERRQ(ierr);
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
    for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
      f[j][i] = func((PetscScalar *) &coords[j][i], dim, h, options);
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = VecDestroy(coordinates);CHKERRQ(ierr);
  ierr = DADestroy(coordDA);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_3d"
PetscErrorCode Function_3d(DALocalInfo *info, PetscScalar **x[], PetscScalar **f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, Options *) = options->func;
  DA             coordDA;
  Vec            coordinates;
  DACoor3d    ***coords;
  PetscReal      L[3];
  PetscInt       dim[3];
  PetscReal      h[3];
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = CalculateGeometry(info->da, dim, L, h, options);CHKERRQ(ierr);
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        f[k][j][i] = func((PetscScalar *) &coords[k][j][i], dim, h, options);
      }
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = VecDestroy(coordinates);CHKERRQ(ierr);
  ierr = DADestroy(coordDA);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateFunction"
PetscErrorCode CreateFunction(DA da, Vec Y, Options *options)
{
  PetscInt       dim;
  Vec            X;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetInfo(da, &dim, 0,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(da, &X);CHKERRQ(ierr);
  if (dim == 1) {
    ierr = DAFormFunctionLocal(da, (DALocalFunction1) Function_1d, X, Y, (void *) options);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DAFormFunctionLocal(da, (DALocalFunction1) Function_2d, X, Y, (void *) options);CHKERRQ(ierr);
  } else if (dim == 3) {
	ierr = DAFormFunctionLocal(da, (DALocalFunction1) Function_3d, X, Y, (void *) options);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  ierr = DARestoreGlobalVector(da, &X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

class FunctionTestFFT : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(FunctionTestFFT);

  CPPUNIT_TEST(testStep);
  CPPUNIT_TEST(testCos);
  CPPUNIT_TEST(testConvolution);
  CPPUNIT_TEST(testPseudoConvolution);

  CPPUNIT_TEST_SUITE_END();
protected:
  DA        _da[3];
  Mat       _F[3];
  PetscReal _scale[3];
  Options   _options;
public :
  /// Setup data.
  void setUp(void) {
    ProcessOptions(PETSC_COMM_WORLD, &_options);
    // Create mesh
    for(int dim = 0; dim < 3; ++dim) {
      PetscInt       dof = 1;
      PetscInt       dims[3];
      PetscInt       fftwDims[3];
      PetscReal      L[3];
      PetscReal      h[3];
      PetscErrorCode ierr;

      switch(dim+1) {
      case 1:
        ierr = DACreate1d(PETSC_COMM_WORLD, DA_NONPERIODIC, -3, dof, 1, PETSC_NULL, &_da[dim]);CHKERRXX(ierr);break;
      case 2:
        ierr = DACreate2d(PETSC_COMM_WORLD, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof, 1,
                          PETSC_NULL, PETSC_NULL, &_da[dim]);CHKERRXX(ierr);break;
      case 3:
        ierr = DACreate3d(PETSC_COMM_WORLD, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof, 1,
                          PETSC_NULL, PETSC_NULL, PETSC_NULL, &_da[dim]);CHKERRXX(ierr);break;
      }
      ierr = CalculateGeometry(_da[dim], dims, L, h, &_options);CHKERRXX(ierr);
      ierr = DASetUniformCoordinates(_da[dim], 0.0, L[0], 0.0, L[1], 0.0, L[2]);CHKERRXX(ierr);
      // FFTW wants dimensions in reverse order (since we have Fortran storage)
      _scale[dim] = 1.0;
      for(int d = 0; d < dim+1; ++d) {
        _scale[dim] *= dims[d];
        fftwDims[dim-d] = dims[d];
      }
      _scale[dim] = 1.0/_scale[dim];
      ierr = MatCreateSeqFFTW(PETSC_COMM_WORLD, dim+1, fftwDims, &_F[dim]);CHKERRXX(ierr);
    }
  };

  /// Tear down data.
  void tearDown(void) {
    PetscErrorCode ierr;

    for(int dim = 0; dim < 3; ++dim) {
      ierr = MatDestroy(_F[dim]);CHKERRXX(ierr);
      ierr = DADestroy(_da[dim]);CHKERRXX(ierr);
    }
  };

  /// Test a function.
  void testFunction(const int dim, DA da, Mat F, PetscReal scale, bool removeGibbs, PetscReal volume,
                    PetscScalar (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *),
                    PetscScalar (*funcHat)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *)) {
      Vec            step, stepHat, stepFFT, stepInvFFT, errorVec;
      PetscInt       dims[3];
      PetscReal      L[3];
      PetscReal      h[3];
      PetscReal      error, fftNorm;
      PetscErrorCode ierr;

      ierr = CalculateGeometry(da, dims, L, h, &_options);CHKERRXX(ierr);
      // Create a step function
      _options.func = func;
      ierr = DAGetGlobalVector(da, &step);CHKERRXX(ierr);
      ierr = CreateFunction(da, step, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"Function\n");CHKERRXX(ierr);ierr = VecView(step, PETSC_NULL);CHKERRXX(ierr);}
      // Check the volume
      if (volume > 0.0) {
        PetscScalar vol;
        ierr = VecSum(step, &vol);CHKERRXX(ierr);
        PetscReal   error  = volume - PetscRealPart(vol)*h[0]*h[1]*h[2];

        if (_options.debug) {std::cout << "vol: " << PetscRealPart(vol)*h[0]*h[1]*h[2] << " volume error " << error << " rel error " << error/volume << std::endl;}
        // This is good for M,N,P >= 16
        CPPUNIT_ASSERT(fabs(error)/volume < 5.0e-2);
      }
      // Create the analytic Fourier transform
      _options.func = funcHat;
      ierr = DAGetGlobalVector(da, &stepHat);CHKERRXX(ierr);
      ierr = CreateFunction(da, stepHat, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"FT Function\n");CHKERRXX(ierr);ierr = VecView(stepHat, PETSC_NULL);CHKERRXX(ierr);}
      // Check the FFT against the analytic result
      ierr = DAGetGlobalVector(da, &stepFFT);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &errorVec);CHKERRXX(ierr);
      ierr = MatMult(F, step, stepFFT);CHKERRXX(ierr);
      ierr = VecScale(stepFFT, scale);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"FFT Function\n");CHKERRXX(ierr);ierr = VecView(stepFFT, PETSC_NULL);CHKERRXX(ierr);}
      ierr = VecWAXPY(errorVec, -1.0, stepFFT, stepHat);CHKERRXX(ierr);
      ierr = VecNorm(errorVec, NORM_2, &error);CHKERRXX(ierr);
      ierr = VecNorm(stepHat, NORM_2, &fftNorm);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"FFT Error\n");CHKERRXX(ierr);ierr = VecView(errorVec, PETSC_NULL);CHKERRXX(ierr);}
      if (_options.debug) {std::cout << "FFT error " << error << " rel error " << error/fftNorm << std::endl;}
      switch(dim+1) {
      case 1:
        //   My fit to the error is e = 3/(4 sqrt(M)), which is good for M >= 32
        if (removeGibbs) {
          CPPUNIT_ASSERT_DOUBLES_EQUAL((0.75/sqrt(dims[0])), error, error*1.5e-2);
        } else {
          CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, error, epsilon);
        }
        break;
      case 3:
        //   This seems to remain constant, and the relative error ~ 1 which means this is crap
        CPPUNIT_ASSERT(error < 0.27);
        break;
      }
      // Check the inverse FFT of the analytic result against the function
      ierr = DAGetGlobalVector(da, &stepInvFFT);CHKERRXX(ierr);
      ierr = MatMultTranspose(F, stepHat, stepInvFFT);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"InvFFT Function\n");CHKERRXX(ierr);ierr = VecView(stepInvFFT, PETSC_NULL);CHKERRXX(ierr);}
      ierr = VecWAXPY(errorVec, -1.0, stepInvFFT, step);CHKERRXX(ierr);
      ierr = VecNorm(errorVec, NORM_2, &error);CHKERRXX(ierr);
      if (_options.debug) {ierr = PetscPrintf(PETSC_COMM_WORLD,"InvFFT Error\n");CHKERRXX(ierr);ierr = VecView(errorVec, PETSC_NULL);CHKERRXX(ierr);}
      if (_options.debug) {std::cout << "invFFT error " << error << std::endl;}
      if (!removeGibbs) {
        switch(dim+1) {
        case 1:
          CPPUNIT_ASSERT(error < epsilon);
          break;
        case 3:
          CPPUNIT_ASSERT(error < epsilon);
          break;
        }
      } else {
        //   Do not know any other way to take out Gibbs ringing
        PetscScalar *v;
        PetscInt     len;
        PetscReal    maxError, frac = 0.0;
        ierr = VecNorm(errorVec, NORM_INFINITY, &maxError);CHKERRXX(ierr);
        ierr = VecGetArray(errorVec, &v);CHKERRXX(ierr);
        ierr = VecGetLocalSize(errorVec, &len);CHKERRXX(ierr);
        for(int i = 0; i < len; ++i) {
          frac += (PetscAbsScalar(v[i]) > 0.2*maxError);
        }
        frac /= len;
        ierr = VecRestoreArray(errorVec, &v);CHKERRXX(ierr);
        if (_options.debug) {std::cout << "invFFT frac " << frac << std::endl;}
        switch(dim+1) {
        case 1:
          //   This 8% is good for M >= 32, 1% is good for M >= 256, should also check the median value here
          CPPUNIT_ASSERT(frac < 8.0e-2);
          break;
        case 3:
          //   This seems to remain constant, and the error grows each time
          CPPUNIT_ASSERT(frac < 8.0e-2);
          break;
        }
      }
      // Cleanup
      ierr = DARestoreGlobalVector(da, &step);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &stepHat);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &stepFFT);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &stepInvFFT);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &errorVec);CHKERRXX(ierr);
  };

  /// Test step function.
  void testStep(void) {
    for(int dim = 0; dim < 3; ++dim) {
      PetscScalar (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *);
      PetscScalar (*funcHat)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *);
      PetscReal     volume;

      switch(dim+1) {
      case 1:
        func    = step1D;
        funcHat = step1DHat;
        volume  = 0.0;
        break;
      case 2:
        continue;
      case 3:
        func    = step3D;
        funcHat = step3DHat;
        volume  = 4.0*M_PI/(3.0*8.0*8.0*8.0);
        break;
      }
      testFunction(dim, _da[dim], _F[dim], _scale[dim], true, volume, func, funcHat);
    }
  };

  /// Test cosine function.
  void testCos(void) {
    for(int dim = 0; dim < 3; ++dim) {
      PetscScalar (*func)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *);
      PetscScalar (*funcHat)(const PetscScalar *, const PetscInt *, const PetscReal *, struct _Options *);
      PetscReal     volume = 0.0;

      switch(dim+1) {
      case 1:
        func    = cos1D;
        funcHat = cos1DHat;
        break;
      case 2:
        continue;
      case 3:
        func    = cos3D;
        funcHat = cos3DHat;
        break;
      }
      testFunction(dim, _da[dim], _F[dim], _scale[dim], false, volume, func, funcHat);
    }
  };

  /// Test convolution with a step function.
  void testConvolution(void) {
    for(int dim = 0; dim < 3; ++dim) {
      DA             da    = _da[dim];
      Mat            F     = _F[dim];
      PetscReal      scale = _scale[dim];
      Vec            step,   stepHat,   stepFFT;
      Vec            signal, signalHat, signalFFT;
      Vec            convHat, convFFT, convAnal, convComp, convExact, convError, ftError;
      PetscInt       dims[3];
      PetscReal      L[3];
      PetscReal      h[3];
      PetscReal      error, convNorm, ftNorm;
      PetscViewer    funcViewer = PETSC_VIEWER_DRAW_WORLD;
      PetscErrorCode ierr;

      ierr = CalculateGeometry(da, dims, L, h, &_options);CHKERRXX(ierr);
      // Create a step function
      switch(dim+1) {
      case 1:
        _options.func = step1D; break;
      case 2:
        continue;
      case 3:
        _options.func = step3D; break;
      }
      ierr = DAGetGlobalVector(da, &step);CHKERRXX(ierr);
      ierr = CreateFunction(da, step, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, step, funcViewer, "Step");CHKERRXX(ierr);}
      // Create the analytic Fourier transform
      switch(dim+1) {
      case 1:
        _options.func = step1DHat; break;
      case 2:
        continue;
      case 3:
        _options.func = step3DHat; break;
      }
      ierr = DAGetGlobalVector(da, &stepHat);CHKERRXX(ierr);
      ierr = CreateFunction(da, stepHat, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, stepHat, funcViewer, "FT Step");CHKERRXX(ierr);}
      // Create the FFT
      ierr = DAGetGlobalVector(da, &stepFFT);CHKERRXX(ierr);
      ierr = MatMult(F, step, stepFFT);CHKERRXX(ierr);
      ierr = VecScale(stepFFT, scale);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, stepFFT, funcViewer, "FFT Step");CHKERRXX(ierr);}
      // Check analytic and computational FT
      ierr = DAGetGlobalVector(da, &ftError);CHKERRXX(ierr);
      ierr = VecWAXPY(ftError, -1.0, stepHat, stepFFT);CHKERRXX(ierr);
      ierr = VecNorm(ftError, NORM_2, &error);CHKERRXX(ierr);
      ierr = VecNorm(stepHat, NORM_2, &ftNorm);CHKERRXX(ierr);
      if (_options.debug) {std::cout << "A vs C Step FT error " << error << " rel error " << error/ftNorm << std::endl;}
      // Create the signal function
      switch(dim+1) {
      case 1:
        _options.func = cos1D; break;
      case 2:
        continue;
      case 3:
        _options.func = cos3D; break;
      }
      ierr = DAGetGlobalVector(da, &signal);CHKERRXX(ierr);
      ierr = CreateFunction(da, signal, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, signal, funcViewer, "Signal");CHKERRXX(ierr);}
      // Create the analytic Fourier transform
      switch(dim+1) {
      case 1:
        _options.func = cos1DHat; break;
      case 2:
        continue;
      case 3:
        _options.func = cos3DHat; break;
      }
      ierr = DAGetGlobalVector(da, &signalHat);CHKERRXX(ierr);
      ierr = CreateFunction(da, signalHat, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, signalHat, funcViewer, "FT Signal");CHKERRXX(ierr);}
      // Create the FFT
      ierr = DAGetGlobalVector(da, &signalFFT);CHKERRXX(ierr);
      ierr = MatMult(F, signal, signalFFT);CHKERRXX(ierr);
      ierr = VecScale(signalFFT, scale);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, signalFFT, funcViewer, "FFT Signal");CHKERRXX(ierr);}
      // Check analytic and computational FT
      ierr = VecWAXPY(ftError, -1.0, signalHat, signalFFT);CHKERRXX(ierr);
      ierr = VecNorm(ftError, NORM_2, &error);CHKERRXX(ierr);
      ierr = VecNorm(signalHat, NORM_2, &ftNorm);CHKERRXX(ierr);
      if (_options.debug) {std::cout << "A vs C Signal FT error " << error << " rel error " << error/ftNorm << std::endl;}
      // Create the analytic convolution
      ierr = DAGetGlobalVector(da, &convHat);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &convAnal);CHKERRXX(ierr);
      ierr = VecPointwiseMult(convHat, signalHat, stepHat);CHKERRXX(ierr);
      ierr = MatMultTranspose(F, convHat, convAnal);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, convAnal, funcViewer, "Analytic Conv");CHKERRXX(ierr);}
      // Create the computational convolution
      ierr = DAGetGlobalVector(da, &convFFT);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &convComp);CHKERRXX(ierr);
      ierr = VecPointwiseMult(convFFT, signalFFT, stepFFT);CHKERRXX(ierr);
      ierr = MatMultTranspose(F, convFFT, convComp);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, convComp, PETSC_VIEWER_DRAW_WORLD, "Computed Conv");CHKERRXX(ierr);}
      // Compare analytic and computational convolutions
      ierr = DAGetGlobalVector(da, &convError);CHKERRXX(ierr);
      ierr = VecWAXPY(convError, -1.0, convAnal, convComp);CHKERRXX(ierr);
      ierr = VecNorm(convError, NORM_2, &error);CHKERRXX(ierr);
      ierr = VecNorm(convAnal, NORM_2, &convNorm);CHKERRXX(ierr);
      if (_options.debug) {std::cout << "A vs C Convolution error " << error << " rel error " << error/convNorm << std::endl;}
      switch(dim+1) {
      case 1:
        //   My fit to the error is e = 1/(2 sqrt(M)), which is good for M >= 32
        CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5/sqrt(dims[0]), error, 5.0e-3);
        break;
      case 3:
        //   3D does not seem to converge at all, I keep getting relative errors around 1.23
        break;
      }
      // Create the exact convolution function
      switch(dim+1) {
      case 1:
        _options.func = conv1D;

        ierr = DAGetGlobalVector(da, &convExact);CHKERRXX(ierr);
        ierr = CreateFunction(da, convExact, &_options);CHKERRXX(ierr);
        if (_options.debug) {ierr = VecViewCenter(da, convExact, funcViewer, "Exact Conv");CHKERRXX(ierr);}
        // Compare analytic and exact convolutions
        ierr = VecWAXPY(convError, -1.0, convAnal, convExact);CHKERRXX(ierr);
        ierr = VecNorm(convError, NORM_2, &error);CHKERRXX(ierr);
        ierr = VecNorm(convExact, NORM_2, &convNorm);CHKERRXX(ierr);
        if (_options.debug) {std::cout << "E vs A Convolution error " << error << " rel error " << error/convNorm << std::endl;}
        CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, error, epsilon);
        ierr = DARestoreGlobalVector(da, &convExact);CHKERRXX(ierr);
        break;
      }
      // Cleanup
      ierr = DARestoreGlobalVector(da, &step);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &stepHat);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &stepFFT);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &ftError);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &signal);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &signalHat);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &signalFFT);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &convHat);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &convAnal);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &convFFT);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &convComp);CHKERRXX(ierr);
      ierr = DARestoreGlobalVector(da, &convError);CHKERRXX(ierr);
    }
  };

  /// Test pseudo-convolution with a step function.
  void testPseudoConvolution(void) {
    for(int dim = 0; dim < 3; ++dim) {
      DA             da    = _da[dim];
      Mat            F     = _F[dim];
      //PetscReal      scale = _scale[dim];
      Vec            stepHat;
      Vec            signalHat;
      Vec            kernelHat, phase;
      Vec            convHat, convAnal, convExact, convFT, convError;
      PetscInt       dims[3];
      PetscReal      L[3];
      PetscReal      h[3];
      PetscReal      error, convNorm;
      PetscViewer    funcViewer = PETSC_VIEWER_DRAW_WORLD;
      PetscErrorCode ierr;

      ierr = CalculateGeometry(da, dims, L, h, &_options);CHKERRXX(ierr);
      // Create step function analytic Fourier transform
      switch(dim+1) {
      case 1:
        _options.func = step1DHat; break;
      case 2:
        continue;
      case 3:
        _options.func = step3DHat; break;
      }
      ierr = DAGetGlobalVector(da, &stepHat);CHKERRXX(ierr);
      ierr = CreateFunction(da, stepHat, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, stepHat, funcViewer, "FT Step");CHKERRXX(ierr);}
      // Create signal analytic Fourier transform
      switch(dim+1) {
      case 1:
        _options.func = cos1DHat; break;
      case 2:
        continue;
      case 3:
        _options.func = cos3DHat; break;
      }
      ierr = DAGetGlobalVector(da, &signalHat);CHKERRXX(ierr);
      ierr = CreateFunction(da, signalHat, &_options);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, signalHat, funcViewer, "FT Signal");CHKERRXX(ierr);}
      // Create the analytic convolution
      ierr = DAGetGlobalVector(da, &convHat);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &convAnal);CHKERRXX(ierr);
      ierr = VecPointwiseMult(convHat, signalHat, stepHat);CHKERRXX(ierr);
      ierr = MatMultTranspose(F, convHat, convAnal);CHKERRXX(ierr);
      if (_options.debug) {ierr = VecViewCenter(da, convAnal, funcViewer, "Analytic Conv");CHKERRXX(ierr);}
      // Create the exact convolution function
      ierr = DAGetGlobalVector(da, &convError);CHKERRXX(ierr);
      switch(dim+1) {
      case 1:
        _options.func = conv1D;

        ierr = DAGetGlobalVector(da, &convExact);CHKERRXX(ierr);
        ierr = CreateFunction(da, convExact, &_options);CHKERRXX(ierr);
        if (_options.debug) {ierr = VecViewCenter(da, convExact, funcViewer, "Exact Conv");CHKERRXX(ierr);}
        // Compare analytic and exact convolutions
        ierr = VecWAXPY(convError, -1.0, convAnal, convExact);CHKERRXX(ierr);
        ierr = VecNorm(convError, NORM_2, &error);CHKERRXX(ierr);
        ierr = VecNorm(convExact, NORM_2, &convNorm);CHKERRXX(ierr);
        if (_options.debug) {std::cout << "E vs A Convolution error " << error << " rel error " << error/convNorm << std::endl;}
        CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, error, epsilon);
        ierr = DARestoreGlobalVector(da, &convExact);CHKERRXX(ierr);
        break;
      }
      // Create Fourier space integral
      ierr = DAGetGlobalVector(da, &convFT);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &phase);CHKERRXX(ierr);
      ierr = DAGetGlobalVector(da, &kernelHat);CHKERRXX(ierr);
      switch(dim+1) {
      case 1:
        {
        DALocalInfo  info;
        DA           coordDA;
        Vec          coordinates;
        PetscScalar *coords;
        PetscScalar *array;

        ierr = DAGetLocalInfo(da, &info);CHKERRXX(ierr);
        ierr = DAGetCoordinateDA(da, &coordDA);CHKERRXX(ierr);
        ierr = DAGetCoordinates(da, &coordinates);CHKERRXX(ierr);
        ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRXX(ierr);
        ierr = DAVecGetArray(da, convFT, &array);CHKERRXX(ierr);
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
          _options.func   = phase1D;
          _options.coords = &coords[i];

          ierr = CreateFunction(da, phase, &_options);CHKERRXX(ierr);
          ierr = VecPointwiseMult(kernelHat, phase, stepHat);CHKERRXX(ierr);
          ierr = VecDot(signalHat, kernelHat, &array[i]);CHKERRXX(ierr);
        }
        ierr = DAVecRestoreArray(da, convFT, &array);CHKERRXX(ierr);
        ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRXX(ierr);
        ierr = VecDestroy(coordinates);CHKERRXX(ierr);
        ierr = DADestroy(coordDA);CHKERRXX(ierr);
        }
        break;
      case 3:
        {
        DALocalInfo    info;
        DA             coordDA;
        Vec            coordinates;
        DACoor3d    ***coords;
        PetscScalar ***array;

        ierr = DAGetLocalInfo(da, &info);CHKERRXX(ierr);
        ierr = DAGetCoordinateDA(da, &coordDA);CHKERRXX(ierr);
        ierr = DAGetCoordinates(da, &coordinates);CHKERRXX(ierr);
        ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRXX(ierr);
        ierr = DAVecGetArray(da, convFT, &array);CHKERRXX(ierr);
	if (_options->cuda) {
	  PetscScalar   *signal1D;
	  ierr = VecGetArray(signalHat, &signal1D);CHKERRQ(ierr);
	  ierr = PetscPrintf(PETSC_COMM_WORLD, "Starting reference density calculation...\n");CHKERRQ(ierr);
	  {
	    PetscInt N;
	    double  *gamma1DReal, *signal1DReal, *rhoref1DReal;
	    
	    ierr = VecGetSize(gammaVec, &N);CHKERRQ(ierr);
	    ierr = VecGetSize(rhoRefVec, &NRho);CHKERRQ(ierr);
	    ierr = PetscMalloc3(N,double,&gamma1DReal,N,double,&rhoBar1DReal,N,double,&rhoRef1DReal);CHKERRQ(ierr);
	    for(PetscInt i = 0; i < N; ++i) {
	      gamma1DReal[i]  = PetscRealPart(gamma1D[i]);
	    }
	    for(PetscInt i = 0; i < N; ++i) {
	      rhoBar1DReal[i] = PetscRealPart(signal1DReal[i]);
	    }
	    screening(geomOpts->dims, geomOpts->numSpecies, geomOpts->h, geomOpts->R, gamma1DReal, rhoBar1DReal, rhoRef1DReal);
	    for(PetscInt i = 0; i < NRho; ++i) {
	      rhoRef1D[i] = rhoRef1DReal[i];
	    }
	    ierr = PetscFree3(gamma1DReal,rhoBar1DReal,rhoRef1DReal);CHKERRQ(ierr);
	  }
	} else {
	  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
	    if (_options.debug) {std::cout << "Computing k-plane " << k << std::endl;}
	    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
	      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
		_options.func   = phase3D;
		_options.coords = (PetscScalar *) &coords[k][j][i];
		
		ierr = CreateFunction(da, phase, &_options);CHKERRXX(ierr);
		ierr = VecPointwiseMult(kernelHat, phase, stepHat);CHKERRXX(ierr);
		ierr = VecDot(signalHat, kernelHat, &array[k][j][i]);CHKERRXX(ierr);
	      }
	    }
	  }
	}
        ierr = DAVecRestoreArray(da, convFT, &array);CHKERRXX(ierr);
        ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRXX(ierr);
        ierr = VecDestroy(coordinates);CHKERRXX(ierr);
        ierr = DADestroy(coordDA);CHKERRXX(ierr);
        }
        break;
      }
      if (_options.debug) {ierr = VecViewCenter(da, convFT, funcViewer, "FT Conv");CHKERRXX(ierr);}
      // Compare analytic and FT convolutions
      ierr = VecWAXPY(convError, -1.0, convAnal, convFT);CHKERRXX(ierr);
      ierr = VecNorm(convError, NORM_2, &error);CHKERRXX(ierr);
      ierr = VecNorm(convAnal, NORM_2, &convNorm);CHKERRXX(ierr);
      if (_options.debug) {std::cout << "A vs FT Convolution error " << error << " rel error " << error/convNorm << std::endl;}
      CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, error, epsilon);
    }
  };
};


#undef __FUNCT__
#define __FUNCT__ "RegisterFFTFunctionSuite"
PetscErrorCode RegisterFFTFunctionSuite() {
  CPPUNIT_TEST_SUITE_REGISTRATION(FunctionTestFFT);
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}
