import numpy

from math import pi, log
from dftRunner import NUMBER_TO_MOLAR, kT

# Constants
e       = 1.602e-19
epsilon = 78.4 * 8.8541878176e-39

class EquilibriumHardSphere:
  def __init__(self, rho, radii, isConfined = None):
    '''Input
rho:   Densities in Molar
radii: Radii in nm
'''
    self.rho   = numpy.array(rho)/NUMBER_TO_MOLAR # Species density in nm^{-3}
    self.sigma = 2*numpy.array(radii)             # Species radii in nm
    if isConfined is None:
      self.isConfined = [False]*len(self.rho)
    else:
      self.isConfined = isConfined
    return

  def muHSBath(self):
    xi = numpy.array([0.0, 0.0, 0.0, 0.0])
    for i, r in enumerate(self.rho):
      sigma = self.sigma[i]
      xi += (r, r*sigma, r*sigma*sigma, r*sigma*sigma*sigma)
    xi   *= pi/6.0
    Delta = 1.0 - xi[3];
    mu    = []
    for i in range(len(self.rho)):
      sigma = self.sigma[i]
      if self.isConfined[i]:
        mu.append(0.0)
      else:
        mu.append(kT*(-log(Delta) +
                       (3.0*xi[2]*sigma + 3.0*xi[1]*sigma*sigma)/Delta +
                       (9.0*xi[2]*xi[2]*sigma*sigma)/(2.0*Delta*Delta) +
                       (xi[0]/Delta + 3.0*xi[1]*xi[2]/(Delta*Delta) + 3.0*xi[2]*xi[2]*xi[2]/(Delta*Delta*Delta))*sigma*sigma*sigma))
    return mu

class EquilbilrumElectrostatics:
  '''These methods are defined in <citation of Dirk,Deszo>'''
  def __init__(self, rho, radii, z, isConfined = None):
    '''Input
rho:   Densities in Molar
radii: Radii in nm
z:     Valences
'''
    self.rho   = numpy.array(rho)/NUMBER_TO_MOLAR # Species density in nm^{-3}
    self.sigma = 2*numpy.array(radii)             # Species radii in nm
    self.z     = numpy.array(z)                   # Species valences
    if isConfined is None:
      self.isConfined = [False]*len(self.rho)
    else:
      self.isConfined = isConfined
    return

  def Delta(self):
    s = 0.0
    for i in range(len(self.rho)):
      s+= self.rho[i]*self.sigma[i]**3
    return 1.0 - (pi*s)/6.0

  def omega(self, Gamma):
    s = 0.0
    for i in range(len(self.rho)):
      s+= (self.rho[i]*self.sigma[i]**3)/(1.0 + Gamma*self.sigma[i])
    return 1.0 + (pi*s)/(2.0*self.Delta())

  def eta(self, Gamma):
    s = 0.0
    for i in range(len(self.rho)):
      s+= (self.rho[i]*self.sigma[i]*self.z[i])/(1.0 + Gamma*self.sigma[i])
    return (pi*s)/(2.0*self.Delta()*self.omega(Gamma))

  def residual(self, Gamma):
    '''The residual for a Gamma guess'''
    factor = (e*e)/(kT*epsilon)
    s      = 0.0
    for i in range(len(self.rho)):
      s += self.rho[i]*((self.z[i] - self.eta(Gamma)*self.sigma[i]*self.sigma[i])/(1.0 + Gamma*self.sigma[i]))**2
    return 4.0*Gamma*Gamma - factor*s

  def Gamma(self, Gamma0):
    '''The inverse screening parameter'''
    eps = 1.0e-10
    g   = Gamma0
    res = self.residual(g)
    it  = 0 
    while abs(res) > 1.0e-10:
      if it > 1000: raise RuntimeError('No convergence to Gamma solution')
      it += 1
      J   = (self.residual(g+eps) - res)/eps
      dG  = -res/J
      g  += dG
      res = self.residual(g)
    return g

  def muESBath(self, Gamma):
    eta = self.eta(Gamma)
    mu  = []
    for i in range(len(self.rho)):
      sigma = self.sigma[i]
      z     = self.z[i]
      if self.isConfined[i]:
        mu.append(0.0)
      else:
        mu.append(-e*e/(4.0*pi*epsilon)*
                   ((Gamma*z*z)/(1.0 + Gamma*sigma) + eta*sigma*((2.0*z - eta*sigma*sigma)/(1.0 + Gamma*sigma) + eta*sigma*sigma/3.0)));
    return mu

class PetDFTData:
  def __init__(self):
    return

  def readFile(self, filename):
    values = []
    f = file(filename)
    for line in f.readlines():
      if line.startswith('Vector') or line.startswith('  type'): continue
      values.append(float(line.split()[0]))
      #values.append(map(float, line.split()))
    f.close()
    return numpy.array(values)

  def readData(self, numSpecies, Lz):
    '''Only accepts rho_i.out and phi.out for input right now'''
    rho = []
    for i in range(numSpecies):
      d = self.readFile('rho_%d.out' % i)
      d *= NUMBER_TO_MOLAR
      rho.append(d)
    N   = len(rho[0])
    phi = self.readFile('phi.out')
    phi /= kT
    rhoRef = []
    for i in range(numSpecies):
      d = self.readFile('rhoRef_%d.out' % i)
      d *= NUMBER_TO_MOLAR
      rhoRef.append(d)
    h   = Lz/(N-1)
    z_0 = Lz/20.0 # Default wall position
    z   = [h*i - z_0 for i in range(N)]
    return numpy.array(z), numpy.array(rho), phi, numpy.array(rhoRef)

class DirkData:
  def __init__(self):
    return

  def dirkFloat(self, num):
    if num == '*':
      return 0.0
    return float(num)

  def readFile(self, filename, numSpecies):
    '''Return a set of vectors v:
    v[0]:              z positions in nm
    v[s+1]:            concentration of species s at each z position
    v[s+2]:            the electrostatic potential phi at each z position
    v[s+numSpecies+2]: reference concentration of species s at each z position
    '''
    values = []
    f = file(filename)
    for line in f.readlines():
      values.append(map(self.dirkFloat, line.split()))
    f.close()
    v = numpy.transpose(numpy.array(values))
    v[0] *= 0.1 # Convert from angstroms to nm
    if len(v) == numSpecies+2:
      return v[0], v[1:numSpecies+1], v[numSpecies+1]
    elif len(v) == 2*numSpecies+2:
      return v[0], v[1:numSpecies+1], v[numSpecies+1], v[numSpecies+2:]
    raise RuntimeError('Data length did not match the number of species')

  def uploadFile(self, path):
    from ftplib import FTP
    import os
    ftp = FTP('ftp.rush.edu')       # connect to host, default port
    ftp.login('molebio', '9426454') # user anonymous, passwd anonymous@
    ftp.cwd('Dirk/DFT figs')
    ftp.storbinary('STOR '+os.path.basename(path), open(path, 'rb'))
    ftp.quit()
    return

if __name__ == '__main__':
  import sys
  DirkData().uploadFile(sys.argv[1])
