#!/usr/bin/env python
'''This file plots a solution to problems in Table 1 of Dirk's paper with Deszo

We compare 1D and 3D DFT

PetDFT must output rho_i.out, rhoRef_i.out, and phi.out. These are output by Solve()
if debug == 2.
'''
import os,sys
sys.path.insert(0, os.path.join(os.environ['PETSC_DIR'], 'config', 'BuildSystem'))
import script
import math
import numpy
from pylab import *

import dftData

class Plotter(script.Script):
  titleSize = 40
  labelSize = 30
  numSize   = 30

  def __init__(self):
    import RDict
    script.Script.__init__(self, argDB = RDict.RDict())
    from matplotlib import rc
    rc('text', usetex = True)
    self.setup()
    self.dataDir    = self.argDB['dataDir']
    self.dirkData   = dftData.DirkData()
    self.petDFTData = dftData.PetDFTData()
    if self.argDB['raw']:
      self.numSize = 10
    return

  def setupHelp(self, help):
    import nargs

    script.Script.setupHelp(self, help)
    help.addArgument('Plotter', '-show=<bool>', nargs.ArgBool(None, True, 'Display the plot rather than saving to a file', isTemporary = True))
    help.addArgument('Plotter', '-refDensity=<bool>', nargs.ArgBool(None, True, 'Display the reference density', isTemporary = True))
    help.addArgument('Plotter', '-dataDir=<directory>', nargs.ArgDir(None, os.getcwd(), 'Directory for data modules', isTemporary = True))
    help.addArgument('Plotter', '-raw=<bool>', nargs.ArgBool(None, False, 'Display all data and small legend', isTemporary = True))
    return

  def run(self):
    '''Display 1D and 3D DFT results alongside MC simulations
  - Graphs cations, then anions, then phi
    '''
    numSpecies = 2
    speciesName = ['Cation', 'Anion']
    z,   rho,   phi,   rhoRef   = self.petDFTData.readData(numSpecies, 6.0)
    zBF, rhoBF, phiBF           = self.dirkData.readFile(os.path.join(self.dataDir, 'problem2', 'Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0_Rosen.txt'), numSpecies)
    zRF, rhoRF, phiRF, rhoRefRF = self.dirkData.readFile(os.path.join(self.dataDir, 'problem2', 'Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0.txt'), numSpecies)
    # Rho plot
    fontProp = matplotlib.font_manager.FontProperties(size = self.numSize)
    for s in range(numSpecies):
      if self.argDB['refDensity']:
        plot(z,   rho[s],   'bo', z,   rhoRef[s],   'ro')
        plot(zRF, rhoRF[s], 'bs', zRF, rhoRefRF[s], 'rs')
        plot(zBF, rhoBF[s], 'g^')
        legend(('3D RFD DFT density','3D RFD DFT reference density','1D RFD DFT density','1D RFD DFT reference density','1D BF density'), shadow=True, loc=2, prop = fontProp)
      else:
        plot(z,   rho[s],   'bo')
        plot(zRF, rhoRF[s], 'bs')
        plot(zBF, rhoBF[s], 'g^')
        legend(('3D RFD DFT density','1D RFD DFT density','1D BF density'), shadow=True, loc=9, prop = fontProp)
      if not self.argDB['raw']:
        if s == 0:
          xlim(0.0, 1.0)
          ylim(0.8, 1.05)
        else:
          xlim(0.0,  1.0)
          ylim(0.95, 1.25)
      title(speciesName[s]+' Density', fontsize = self.titleSize)
      xlabel('z (nm)', fontsize = self.labelSize)
      ylabel('concentration (nm${}^{-3}$)', fontsize = self.labelSize)
      xticks(fontsize = self.numSize)
      yticks(fontsize = self.numSize)
      show()
      clf()
    # Phi plot
    plot(z, phi, 'bo', zRF, phiRF, 'bs', zBF, phiBF, 'g^')
    if not self.argDB['raw']:
      xlim(0.0, 1.5)
    title('Electrostatic Potential', fontsize=self.titleSize)
    xlabel('z (nm)', fontsize = self.labelSize)
    ylabel('potential (V)', fontsize = self.labelSize)
    xticks(fontsize = self.numSize)
    yticks(fontsize = self.numSize)
    legend(('3D RFD DFT','1D RFD DFT','1D BF DFT'), shadow=True, loc=9, prop = fontProp)
    show()
    return

  def plotMC(self):
    mcData = readFile(os.path.join(self.dataDir, 'MC', 'eps80-R4.5', '300-38', 'Na.av'))
    mcData = mcData.transpose()
    print mcData
    plot(mcData[0], mcData[1], 'bs')
    show()
    return

if __name__ == '__main__':
  Plotter().run()
