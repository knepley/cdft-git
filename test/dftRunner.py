#!/usr/bin/env python
import math,os,sys
import numpy as np
sys.path.insert(0, os.path.join(os.environ['PETSC_DIR'], 'config', 'BuildSystem'))

import script
import daemon

NUMBER_TO_MOLAR = 1.6606
kT      = 0.00421               # Boltzmann's constant and room temperature [kg nm^2/s^2]
epsilon = 78.4*8.8541878176e-39 # Water's permitivity * vacuum permitivity epsilon_0 [C^2 s^2 kg^-1 nm^-3]
e       = 1.602e-19             # Electron charge [C]

class DFT(script.Script):
  titleSize = 20
  labelSize = 12

  # In this class, we refer to species univerally by the periodic table abbreviation
  ionNames = {'Na':  '"Na$""{}^+$"',
              'K':   '"K$""{}^+$"',
              'Cs':  '"Cs$""{}^+$"',
              'Ca':  '"Ca$""{}^{2+}$"',
              'Cl':  '"Cl$""{}^-$"',
              'O':   '"O$""{}^{-1/2}$"',
              'H2O': '"H$""{}_2$""O"',
              'Test0': '"Test0$""{}^+$"',
              'Test1': '"Test1$""{}^-$"',
              'Test2': '"Test2$""{}^-$"'}
  ionRadii = {'Na':  0.1,
              'K':   0.138,
              'Cs':  0.17,
              'Ca':  0.099,
              'Cl':  0.181,
              'O':   0.14,
              'H2O': 0.14,
              'Test0': 0.1,
              'Test1': 0.1,
              'Test2': 0.2125}
  ionCharges = {'Na':   1.0,
                'K':    1.0,
                'Cs':   1.0,
                'Ca':   2.0,
                'Cl':  -1.0,
                'O':   -0.5,
                'H2O':  0.0,
                'Test0': 1.0,
                'Test1': -1.0,
                'Test2': -1.0}

  def __init__(self, argDB = None):
    if argDB is None:
      import RDict
      argDB = RDict.RDict()
    script.Script.__init__(self, argDB = argDB)
    self.logName    = 'dft.log'
    if 'DFT_DIR' in os.environ:
      self.binDir   = os.path.join(os.environ['DFT_DIR'], 'bin')
    else:
      self.binDir   = os.path.join('..', 'bin')
    self.setup()
    self.dim        = self.argDB['dim']
    self.executable = self.findExecutable()
    self.numSpecies = len(self.argDB['ion_species'])
    if self.argDB['build']: self.buildExecutable()
    return

  def findExecutable(self):
    prog  = 'dft-fft-%dd' % self.dim
    paths = []
    if 'DFT_ARCH' in os.environ:
      paths.append(os.path.join(self.binDir, os.environ['DFT_ARCH'], prog))
    paths.append(os.path.join(self.binDir, os.environ['PETSC_ARCH'], prog))
    for d in paths:
      exe = os.path.abspath(d)
      self.logPrint('Checking executable '+exe)
      if os.path.isfile(exe) and os.access(exe, os.X_OK):
        return exe
    raise RuntimeError('Could not locate executable %s. Perhaps your PETSC_ARCH is wrong.' % prog)

  def buildExecutable(self):
    import shutil

    if not self.argDB['dry_run']:
      if not os.path.isdir(self.binDir):
        os.makedirs(self.binDir)
      if os.path.isdir(self.executable+'.dSYM'):
        shutil.rmtree(self.executable+'.dSYM')
      cmd = 'make NUM_SPECIES=%d %s' % (self.numSpecies, os.path.basename(self.executable))
      if self.argDB['dft_debug']: print cmd
      (output, err, status) = self.executeShellCommand(cmd, timeout = self.argDB['timeout'])
    return

  def setupHelp(self, help):
    import nargs

    script.Script.setupHelp(self, help)
    help.addArgument('DFT', '-build', nargs.ArgBool(None, False, 'Flag for building the executable', isTemporary = True))
    help.addArgument('DFT', '-dim', nargs.ArgInt(None, 3, 'Problem dimension', isTemporary = True))
    help.addArgument('DFT', '-run_type', nargs.Arg(None, 'calciumChannel', 'Type of DFT problem', isTemporary = True))
    help.addArgument('DFT', '-dft_debug=<int>', nargs.ArgInt(None, 0, 'Debugging flag', min = 0, isTemporary = True))
    help.addArgument('DFT', '-dft_grid=<int or list>', nargs.Arg(None, 0, 'Set of grids for DFT', isTemporary = True))
    help.addArgument('DFT', '-dft_channel=<int or list>', nargs.Arg(None, 0, 'Set of channel geometries for DFT', isTemporary = True))
    help.addArgument('DFT', '-daemonize=<bool>', nargs.ArgBool(None, False, 'Run the job as a daemon', isTemporary = True))
    help.addArgument('DFT', '-ion_species=<list>', nargs.Arg(None, ['Na','Ca','Cl','O'], 'Ion species in problem (table abbrev)', isTemporary = True))
    help.addArgument('DFT', '-ion_density=<list>', nargs.Arg(None, [0.1,0.0,0.1,0.0], 'Ion species in problem (table abbrev)', isTemporary = True))
    help.addArgument('DFT', '-ca_concentration=<real or list>', nargs.Arg(None, [0.0000001, 0.000001, 0.00001, 0.0001, 0.001, 0.01], 'Set of [Ca] for runs', isTemporary = True))
    help.addArgument('DFT', '-update_reference=<int>', nargs.ArgInt(None, 1, 'Lag for updating reference density (0 for no update)', min = 0, isTemporary = True))
    help.addArgument('DFT', '-dry_run=<bool>', nargs.ArgBool(None, False, 'Only log what would have run', isTemporary = True))
    help.addArgument('DFT', '-timeout=<int>', nargs.ArgInt(None, 1000000, 'Number of seconds until job is declared dead', min = 0, max = 4000000, isTemporary = True))
    help.addArgument('DFT', '-num_oxygen=<real or list>', nargs.Arg(None, [8.0], 'Number of oxygen atoms in the filter', isTemporary = True))
    help.addArgument('DFT', '-max_it=<int>', nargs.ArgInt(None, 200, 'Maximum number of nonlinear iterates', min = 0, isTemporary = True))
    help.addArgument('DFT', '-dataDir=<directory>', nargs.ArgDir(None, os.getcwd(), 'Directory for output', mustExist = False, isTemporary = True))
    help.addArgument('DFT', '-domainLength=<real>', nargs.ArgReal(None, 4.0, 'Length of the periodic domain', min = 0.0, isTemporary = True))
    help.addArgument('DFT', '-useES=<bool>', nargs.ArgBool(None, True, 'Flag for electrostatics', isTemporary = True))
    help.addArgument('DFT', '-usePB=<bool>', nargs.ArgBool(None, True, 'Flag for Poisson-Boltzmann mean field', isTemporary = True))
    help.addArgument('DFT', '-screening=<type>', nargs.Arg(None, 'spectralQuadratureDimReduct', 'Screening calculation type', isTemporary = True))
    help.addArgument('DFT', '-production=<bool>', nargs.ArgBool(None, False, 'Add flags for production runs', isTemporary = True))

    help.addArgument('HardSphere', '-hs_grid=<num>',         nargs.ArgInt(None, 4800, 'The number of grid points in each dimension for hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_min_rho_bath=<num>', nargs.ArgReal(None, 0.0006, 'Minimum rho_bath for hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_max_rho_bath=<num>', nargs.ArgReal(None, 73.000, 'Maximum rho_bath for hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_min_radius=<num>',   nargs.ArgReal(None, 0.1, 'Minimum radius in nm for hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_max_radius=<num>',   nargs.ArgReal(None, 0.1, 'Maximum radius in nm hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_num_runs=<num>',     nargs.ArgInt(None, 10, 'Number of exponentially spaced runs for hard sphere verification', isTemporary = True))
    help.addArgument('HardSphere', '-hs_show',               nargs.ArgBool(None, True, 'Show plot for hard sphere verification', isTemporary = True))
    return

  def setup(self):
    script.Script.setup(self)
    self.newtonOptions  = ['-ksp_type lgmres', '-ksp_gmres_restart 10', '-ksp_rtol 1.0e-4']
    self.defaultOptions = ['-dmmg_jacobian_mf_fd', '-snes_type picard', '-snes_picard quadratic', '-snes_rtol 1.0e-6', '-snes_max_it '+str(self.argDB['max_it']), '-snes_monitor', '-snes_view', '-ksp_monitor', '-log_summary summary.log']
    # Removed '-pb_snes_mf'
    self.defaultOptions.extend(['-pb_snes_monitor', '-pb_ksp_monitor', '-pb_snes_rtol 1.0e-12', '-pb_snes_atol 1.0e-10'])
    self.defaultOptions.extend(['-msa_snes_atol 1.0e-8'])
    self.defaultOptions.extend(['-nCheckTol 1.0e-5', '-volFracTol 1.0e-2'])
    if not type(self.argDB['dft_channel']) is list:
      self.argDB['dft_channel'] = [self.argDB['dft_channel']]
    self.argDB['dft_channel'] = map(int, self.argDB['dft_channel'])
    if not type(self.argDB['dft_grid']) is list:
      self.argDB['dft_grid'] = [self.argDB['dft_grid']]
    self.argDB['dft_grid'] = map(int, self.argDB['dft_grid'])
    if not type(self.argDB['num_oxygen']) is list:
      self.argDB['num_oxygen'] = [self.argDB['num_oxygen']]
    self.argDB['num_oxygen'] = map(float, self.argDB['num_oxygen'])
    if not type(self.argDB['ca_concentration']) is list:
      self.argDB['ca_concentration'] = [self.argDB['ca_concentration']]
    self.argDB['ca_concentration'] = map(float, self.argDB['ca_concentration'])
    self.logPrint('Starting DFT run')
    return

  def getIonOptions(self):
    namesOpt = '-ionNames '
    radiiOpt = '-R '
    for i,ion in enumerate(self.argDB['ion_species']):
      if i > 0:
        namesOpt += ','
        radiiOpt += ','
      namesOpt += self.ionNames[ion]
      radiiOpt += str(self.ionRadii[ion])
    options  = [namesOpt, radiiOpt]
    return options

  def getBaseGeometryOptions(self):
    return ['-L 2.0,2.0,'+str(self.argDB['domainLength'])]

  def getPhysicsOptions(self, bufferSpecies = 'Cl'):
    chargeOpt = '-z '
    for i,ion in enumerate(self.argDB['ion_species']):
      if i > 0:
        chargeOpt += ','
      chargeOpt += str(self.ionCharges[ion])
    options = ['-numSpecies '+str(self.numSpecies), chargeOpt]
    if not bufferSpecies is None:
      options.append('-forceNeutrality '+str(self.argDB['ion_species'].index('Cl')))
    if self.argDB['useES']:
      options += ['-useES 1', '-useMuES 1', '-usePoisson 1', '-screeningComputationType '+self.argDB['screening']]
    else:
      options += ['-useES 0', '-useMuES 0', '-usePoisson 0']
    if self.argDB['usePB']:
      options.extend(['-usePoissonBoltzmann 1', '-calcPoissonBoltzmannFourier 0'])
    options.extend(['-updateReference '+str(self.argDB['update_reference'])])
    return options

  def getGridOptions(self, gridNum):
    if gridNum == 0:
      options  = ['-da_grid_x 41', '-da_grid_y 41', '-da_grid_z 41', '-minResolution 0']
    elif gridNum == 1:
      options  = ['-da_grid_x 81', '-da_grid_y 81', '-da_grid_z 81', '-minResolution 0']
    elif gridNum == 2:
      options  = ['-da_grid_x 81', '-da_grid_y 81', '-da_grid_z 161', '-minResolution 0']
    elif gridNum == 3:
      options  = ['-da_grid_x 21', '-da_grid_y 21', '-da_grid_z 161', '-minResolution 0']
    elif gridNum == 4:
      options  = ['-da_grid_x 41', '-da_grid_y 41', '-da_grid_z 161', '-minResolution 0']
    elif gridNum == 5:
      options  = ['-da_grid_x 41', '-da_grid_y 41', '-da_grid_z 321', '-minResolution 0']
    elif gridNum == 6:
      options  = ['-da_grid_x 41', '-da_grid_y 41', '-da_grid_z 81', '-minResolution 0']
    elif gridNum == 7:
      options  = ['-da_grid_x 21', '-da_grid_y 21', '-da_grid_z 21', '-minResolution 0']
    elif gridNum == 8:
      options  = ['-da_grid_x 161', '-da_grid_y 161', '-da_grid_z 81', '-minResolution 0']
    elif gridNum == 9:
      options  = ['-da_grid_x 161', '-da_grid_y 161', '-da_grid_z 161', '-minResolution 0']
    elif gridNum == 10:
      options  = ['-da_grid_x 115', '-da_grid_y 115', '-da_grid_z 81', '-minResolution 0']
    elif gridNum == 11:
      options  = ['-da_grid_x 21', '-da_grid_y 21', '-da_grid_z 81', '-minResolution 0']
    else:
      raise RuntimeError('Invalid grid number: '+str(gridNum))
    return options

  def getOutputOptions(self, outputVTK, outputData):
    options = []
    if outputVTK:
      options.extend(['-doOutput', '-outputPeriod 2'])
    if outputData:
      options.append('-debug 2')
    return options

  def checkCommand(self, command, status, output, error):
    '''Raise an error if the exit status is nonzero'''
    if status:
      self.log.write('Could not execute \''+command+'\':\n'+output+error)

  def runSimulation(self, opts, defaultOpts = None):
    if defaultOpts is None: defaultOpts = self.defaultOptions
    cmd = ' '.join([self.executable]+defaultOpts+opts)
    if self.argDB['dft_debug']: print cmd
    if self.argDB['dry_run']:
      self.logPrint(cmd)
      output = ''
    else:
      self.logPrint(cmd)
      (output, err, status) = self.executeShellCommand(cmd, checkCommand = self.checkCommand, timeout = self.argDB['timeout'])
      if status:
        raise RuntimeError('Execution failed!\n%s' % err)
    return output

  def printOutput(self, output):
    with file('dft.out', 'a') as f:
      f.write(output)
    return

  def storeOutput(self, grid, numOxygen = None, caConcentration = None):
    # Move all output files to storage
    import datetime
    outputLabel = str(grid)
    if not numOxygen is None:
      outputLabel += "_"+str(numOxygen)
    if not caConcentration is None:
      outputLabel += "_"+str(caConcentration)
    outputLabel += "_"+str(datetime.datetime.now()).replace(" ", "_")
    if self.argDB['dataDir'] == os.getcwd():
      return
    import re
    namePat = re.compile(r'dft\.log|build\.log|summary\.log|[\w_0-9]+\.out|dft_\d+\.py|\w+_\d+\.vtk')
    if not os.path.isdir(self.argDB['dataDir']):
      os.makedirs(self.argDB['dataDir'])
    outputdir = os.path.join(self.argDB['dataDir'], outputLabel)
    if not os.path.isdir(outputdir):
      os.makedirs(outputdir)
    for name in os.listdir(os.getcwd()):
      if namePat.match(name):
        self.logPrint('Moving '+os.path.join(os.getcwd(), name)+' to '+os.path.join(outputdir, name))
        os.rename(os.path.join(os.getcwd(), name), os.path.join(outputdir, name))
    return

  def calciumChannel(self):
    baseGeomOpts = self.getBaseGeometryOptions()+self.getIonOptions()
    physOpts     = self.getPhysicsOptions()
    outOpts      = self.getOutputOptions(self.argDB['production'], False)
    for channel in self.argDB['dft_channel']:
      geomOpts = []
      geomOpts.extend(baseGeomOpts)
      if channel == 0:
        geomOpts.extend(['-domainType channel', '-channelLength 1.0', '-channelRadius 0.45', '-vestibuleRadius 0.5'])
      elif channel == 1:
        geomOpts.extend(['-domainType channel', '-channelLength 1.0', '-channelRadius 0.35', '-vestibuleRadius 0.5'])
      elif channel == 2:
        geomOpts.extend(['-domainType channel', '-channelLength 1.0', '-channelRadius 0.40', '-vestibuleRadius 0.5'])
      elif channel == 3:
        geomOpts.extend(['-domainType channel', '-channelLength 1.0', '-channelRadius 0.50', '-vestibuleRadius 0.5'])
      for grid in self.argDB['dft_grid']:
        discOpts = self.getGridOptions(grid)
        self.logPrint('Running with channel %d and grid %d' % (channel, grid))
        for numOxygen in self.argDB['num_oxygen']:
          confinedOpt     = '-is_confined '
          numParticlesOpt = '-num_particles '
          for i,ion in enumerate(self.argDB['ion_species']):
            if i > 0:
              confinedOpt     += ','
              numParticlesOpt += ','
            if ion == 'O':
              confinedOpt     += '1'
              numParticlesOpt += str(numOxygen)
            else:
              confinedOpt     += '0'
              numParticlesOpt += '0'
          for caConcentration in self.argDB['ca_concentration']:
            self.logPrint('Running calcium channel with %g oxygen and [Ca] %g' % (numOxygen, caConcentration))
            rhoBathOpt = '-rhoBathM '
            for i,ion in enumerate(self.argDB['ion_species']):
              if i > 0:
                rhoBathOpt += ','
              if ion == 'Ca':
                rhoBathOpt += str(caConcentration)
              else:
                rhoBathOpt += str(self.argDB['ion_density'][i])
            output = self.runSimulation(geomOpts+discOpts+physOpts+outOpts+[rhoBathOpt, confinedOpt, numParticlesOpt])
            f = file('dft.out', 'a')
            f.write(output)
            f.close()
            self.storeOutput(grid, numOxygen, caConcentration)
    self.logPrint('Ending DFT run')
    return

  def table1(self):
    '''Reproduce runs from Table 1 in the Dirk and Dezso paper'''
    # Here I have hardcoded run 2
    geomOpts = ['-L 2.0,2.0,6.0', '-R 0.1,0.2125']
    physOpts = ['-numSpecies 2', '-useES 1', '-useMuES 1', '-usePoisson 1']
    outputOpts = ['-debug 6']
    physOpts += ['-z 1.0,-1.0']
    rhoBathOpts = ['-rhoBathM 1.0,1.0']
    for grid in self.argDB['dft_grid']:
      self.logPrint('Running table1 test 2 with grid %d' % grid)
      discOpts = self.getGridOptions(grid)
      self.printOutput(self.runSimulation(geomOpts+physOpts+discOpts+rhoBathOpts+outputOpts))
      self.storeOutput(grid)
    self.logPrint('Ending DFT run')
    return

  def extractHardSphereData(self, output):
    try:
      for line in output.split('\n'):
        if line.find('R:') >= 0:
          R = float([b for b in line.split(' ') if b][-1])
        if line.find('eta') >= 0:
          eta = float([b for b in line.split(' ') if b][-1])
        if line.find('P_{HS}') >= 0:
          P = float([b for b in line.split(' ') if b][-1])
        if line.find('kT rho') >= 0:
          kTrho = float([b for b in line.split(' ') if b][-1])
      data = R, eta, P, kTrho
    except UnboundLocalError:
      print output
      raise RuntimeError('Could not parse output for hard sphere simulation')
    return data

  def plotHardSphereVolumeFractionData(self, data):
    from pylab import title, xlabel, ylabel, plot, savefig, show

    d = data.transpose()
    title('Accuracy of CDFT for Hard Sphere Gas', fontsize=self.titleSize)
    xlabel('Volume Fraction (eta)', fontsize = self.labelSize)
    ylabel('Sum Rule Relative Error (1 - kT rho/P)', fontsize = self.labelSize)
    plot(d[1], np.abs(d[2]-d[3])/d[2], 'bs')
    savefig('hardsphereVolFrac.png')
    if self.argDB['hs_show']: show()
    return

  def plotHardSphereRadiusData(self, data):
    from pylab import title, xlabel, ylabel, plot, savefig, show

    d = data.transpose()
    title('Accuracy of CDFT for Hard Sphere Gas', fontsize=self.titleSize)
    xlabel('Ion Radius (nm)', fontsize = self.labelSize)
    ylabel('Sum Rule Relative Error (1 - kT rho/P)', fontsize = self.labelSize)
    plot(d[0], np.abs(d[2]-d[3])/d[2], 'bs')
    savefig('hardsphereRadius.png')
    if self.argDB['hs_show']: show()
    return

  def hardSphere(self):
    physOpts   = ['-numSpecies %d' % self.numSpecies]+self.getIonOptions()
    solverOpts = ['-snes_type nrichardson', '-snes_monitor', '-snes_view', '-ksp_monitor', '-ksp_type lgmres', '-ksp_gmres_restart 10']
    gridOpts   = ['-da_grid_x %d' % self.argDB['hs_grid']]
    checkOpts  = ['-nCheckTol 1.0e-5 -volFracTol 1.0e-2']
    data       = []
    minRho = self.argDB['hs_min_rho_bath']
    maxRho = self.argDB['hs_max_rho_bath']
    minR   = self.argDB['hs_min_radius']
    maxR   = self.argDB['hs_max_radius']
    num    = self.argDB['hs_num_runs']
    if maxRho > minRho:
      for logRhoBath in np.arange(math.log(minRho), math.log(maxRho)+1e-6, math.log(maxRho/minRho)/num):
        format  = ','.join(['%g'] * self.numSpecies)
        rhoBath = tuple([math.exp(logRhoBath)] * self.numSpecies)
        self.logPrint('rho_bath: '+format % rhoBath, debugSection = 'screen')
        output = self.runSimulation(['-rhoBath '+format % rhoBath], physOpts+solverOpts+gridOpts+checkOpts)
        data.append(self.extractHardSphereData(output))
        self.printOutput(output)
      self.plotHardSphereVolumeFractionData(np.array(data))
    checkOpts  = ['-nCheckTol 1.0e-4 -volFracTol 1.0e-2']
    if maxR > minR:
      if self.numSpecies != 1: raise RuntimeError('This test only makes sense for a single species')
      for R in np.arange(minR, maxR+1e-6, (maxR - minR)/num):
        self.logPrint('R: %g' % R, debugSection = 'screen')
        output = self.runSimulation(['-R %g' % R], physOpts+solverOpts+gridOpts+checkOpts)
        data.append(self.extractHardSphereData(output))
        self.printOutput(output)
      self.plotHardSphereRadiusData(np.array(data))
    self.logPrint('Ending DFT run')
    return

  def run(self):
    try:
      getattr(self, self.argDB['run_type'])()
    except AttributeError:
      sys.exit('Invalid run type: '+self.argDB['run_type'])
    return

if __name__ == '__main__':
  import nargs
  if nargs.Arg.findArgument('daemonize', sys.argv[1:]):
    print 'Starting daemon'
    daemon.createDaemon('.')
  DFT().run()
