BIN = ./bin/${PETSC_ARCH}/

all: veryclean dft-fft-3d

dft-fft-3d:
	@cd src; make PETSC_ARCH=${PETSC_ARCH} EXTRA_FLAGS=${EXTRA_FLAGS} $@
	${MV} src/$@      ${BIN}
	if [ -d "src/$@.dSYM" ]; then \
	  ${RM} -r ${BIN}/$@.dSYM; \
	  ${MV} src/$@.dSYM ${BIN}; \
	fi;

veryclean:
	cd src; make PETSC_ARCH=${PETSC_ARCH} clean

.PHONY: veryclean

include ${PETSC_DIR}/lib/petsc-conf/variables
include ${PETSC_DIR}/lib/petsc-conf/rules
