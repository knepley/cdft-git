from pylab import *
import sys

data = {
    600:  [4.647580041715e+00, 1.270763573167e-05, 2.253204669789e-15],
    1200: [6.572670692384e+00, 1.810157263277e-05, 4.953951074098e-15],
    2400: [9.295160031103e+00, 2.564781945805e-05, 8.176198359249e-15],
    4800: [1.314534138014e+01, 3.628984901259e-05, 1.937102469732e-14]
}
d = []
labels = []
for size,res in data.items():
    d.append(range(len(res)))
    d.append(log10(res))
    labels.append(str(size)+' Nodes')
plot(*d)
title('Nonlinear Convergence for DFT')
xlabel('iterations')
ylabel('residual')
legend(labels,'upper right', shadow=True)
savefig(sys.stdout)
#show()
