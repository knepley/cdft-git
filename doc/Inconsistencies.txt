1) Why is $O_2$ nonzero outside the channel?

   Because the rho BC associated with PB was inconsistent for confined species

2) Why doesn't Occupancy match between module and report?

   For reasons I do not understand, using higher order (1st) interpolation in the occupancy calculation with the radius
   (RhoBoltzmann) and without the radius (CalculateOccupancy) give different results. Constant interpolation agrees.

3) Why doesn't Occupancy match rhoSum integral?

   Same reason as above.

4) Why doesn't Occupancy match MC?

5) Why is VTK printing messed up?

6) Turn off electrostatics and run channel with hard spheres where we push in the ions with a potential

7) Use hard sphere Oxygen as an initial guess for charged run

