#!/usr/bin/env python
'''This is a generic utility to plot data from dft_*.py files output from PetDFT runs'''
import os,sys
sys.path.insert(0, os.path.join(os.environ['PETSC_DIR'], 'config', 'BuildSystem'))
import script
import math
import numpy

class Plotter(script.Script):
  NUMBER_TO_MOLAR = 1.6606
  ionNames = {'Na':  'Na${}^+$',
              'K':   'K${}^+$',
              'Cs':  'Cs${}^+$',
              'Ca':  'Ca${}^{2+}$',
              'Cl':  'Cl${}^-$',
              'O':   'O${}^{-1/2}$',
              'H2O': 'H${}_2$O',
              'Test0': '"Test0$""{}^+$"',
              'Test1': '"Test1$""{}^-$"',
              'Test2': '"Test2$""{}^-$"'}
  ionNamesMC = {'Na${}^+$':    'Na',
                'Ca${}^{2+}$': 'Ca',
                'Cl${}^-$':    'Cl'}

  def __init__(self):
    import RDict
    argDB = RDict.RDict(None, None, 0, 0)
    script.Script.__init__(self, argDB = argDB)
    from matplotlib import rc
    rc('text', usetex = True)
    self.setup()
    return

  def setupHelp(self, help):
    import nargs

    script.Script.setupHelp(self, help)
    help.addArgument('Plotter', '-reScale=<bool>', nargs.ArgBool(None, False, 'Fix if the code is scaled incorrectly.', isTemporary = True))
    help.addArgument('Plotter', '-show=<bool>', nargs.ArgBool(None, True, 'Display the plot rather than saving to a file', isTemporary = True))
    help.addArgument('Plotter', '-zDensity=<bool>', nargs.ArgBool(None, False, 'Plot the densities along the z-axis', isTemporary = True))
    help.addArgument('Plotter', '-zDensitySingle=<ion name>', nargs.Arg(None, None, 'Plot a single ion density along the z-axis', isTemporary = True))
    help.addArgument('Plotter', '-occupancy=<bool>', nargs.ArgBool(None, False, 'Plot the occupancy against Ca concentration', isTemporary = True))
    help.addArgument('Plotter', '-min=<bool>', nargs.ArgInt(None, 0, 'Minimum DFT output number', min = 0, isTemporary = True))
    help.addArgument('Plotter', '-max=<bool>', nargs.ArgInt(None, 5, 'Maximum DFT output number', min = 0, isTemporary = True))
    help.addArgument('Plotter', '-dataDir=<directory>', nargs.ArgDir(None, os.getcwd(), 'Directory for data modules', isTemporary = True))
    help.addArgument('Plotter', '-dataDirMC=<directory>', nargs.ArgDir(None, os.getcwd(), 'Directory for Monte Carlo data files', isTemporary = True))
    help.addArgument('Plotter', '-MCprefix=<str>', nargs.Arg(None, None, 'Prefix for Monte Carlo data files', isTemporary = True))
    return

  def readMCFile(self, filename):
    values = []
    f = open(filename)
    for line in f.readlines():
      values.append(map(float, line.split()))
      f.close()
    return numpy.array(values)

  def plotZDensity(self, dft, caName, ionNames = None):
    from pylab import figure, savefig, setp, show

    if ionNames is None:
      ionNames = dft.rhoSum.keys()
    # Data
    radii = dft.Geometry.R
    data  = []
    ionNames_noGamma = []
    for name in ionNames:
      if name != "Gamma":
        ionNames_noGamma.append(name)
    for name in ionNames_noGamma:
      print "Ion", name
      if name=="Gamma": continue
      #Ri  = radii[species]
      Ri = radii[dft.Geometry.ionNames.index(name)]
      num = dft.rhoSum[name]
      zs  = [dft.Geometry.h[2]*i - dft.Geometry.L[2]/2.0 for i in range(len(num))]
      occ = 0.0
      for i, z in enumerate(zs):
        if z >= -dft.Geometry.channelLength/2.0 and z < dft.Geometry.channelLength/2.0:
          occ += num[i]*dft.Geometry.h[2]
      print 'Ion',name,'occupancy:',occ
      for i, z in enumerate(zs):
        if (self.argDB["reScale"]):
          num[i] *= dft.Geometry.h[2]
        print '  z',z
        if z >= -dft.Geometry.channelLength/2.0 and z < dft.Geometry.channelLength/2.0:
          print '    in channel',num[i]
          num[i] = num[i]*self.NUMBER_TO_MOLAR/(math.pi * (dft.Geometry.channelRadius - Ri)**2 * dft.Geometry.h[2])
          #num[i] = num[i]*self.NUMBER_TO_MOLAR/(math.pi * (dft.Geometry.channelRadius - Ri)**2)
          print '      -->',num[i]
        elif z >= -dft.Geometry.channelLength/2.0 - dft.Geometry.vestibuleRadius and z < dft.Geometry.channelLength/2.0 + dft.Geometry.vestibuleRadius:
          Rplus = dft.Geometry.vestibuleRadius + Ri
          h = Rplus*math.sqrt(1 - (abs(z) - dft.Geometry.channelLength/2.0)**2/Rplus**2)
          R = dft.Geometry.channelRadius + dft.Geometry.vestibuleRadius - h
          print '    in vestibule',num[i]
          num[i] = num[i]*self.NUMBER_TO_MOLAR/(math.pi * R**2 * dft.Geometry.h[2])
          #num[i] = num[i]*self.NUMBER_TO_MOLAR/(math.pi * R**2)
          print '      -->',num[i]
        else:
          print '    in bath',num[i]
          num[i] = num[i]*self.NUMBER_TO_MOLAR/((dft.Geometry.L[0] + dft.Geometry.h[0])*(dft.Geometry.L[1] + dft.Geometry.h[1]) * dft.Geometry.h[2])
          #num[i] = num[i]*self.NUMBER_TO_MOLAR/(dft.Geometry.L[0]*dft.Geometry.L[1] * dft.Geometry.h[2])
          ##num[i] = num[i]*self.NUMBER_TO_MOLAR/(dft.Geometry.L[0]*dft.Geometry.L[1])
          print '      -->',num[i]
      data.extend([zs, num])
    caRhoBath = dft.HardSphere.rhoBath[caName]*self.NUMBER_TO_MOLAR
    # Plot
    fig        = figure()
    yprops     = dict(rotation = 0, horizontalalignment = 'right', verticalalignment = 'center', x = -0.01)
    axprops    = dict()
    start      = 0.175
    width      = 0.825
    height     = 0.8/len(ionNames_noGamma)
    titleSpace = 0.05
    ticksSpace = 0.075
    if len(ionNames_noGamma) == 1:
      space    = 0
    else:
      space    = (0.2 - titleSpace - ticksSpace)/(len(ionNames_noGamma)-1)
    for n, name in enumerate(ionNames_noGamma):
      if n == 0:
        y  = 1.0 - height - titleSpace
        ax = fig.add_axes([start, y, width, height], title = 'Ion density along Calcium Channel for %.1g M Ca${}^{2+}$' % caRhoBath, **axprops)
        if n > 1: setp(ax.get_xticklabels(), visible = False)
        axprops['sharex'] = ax
      elif n == len(ionNames_noGamma)-1:
        ax = fig.add_axes([start, y, width, height], xlabel = 'z (nm)', **axprops)
      else:
        # force x axes to remain in register, even with toolbar navigation
        ax = fig.add_axes([start, y, width, height], **axprops)
        # turn off x ticklabels for all but the lower axes
        setp(ax.get_xticklabels(), visible = False)
      ax.plot(data[n*2], data[n*2+1])
      ax.set_ylabel(name+' (M)', **yprops)
      y -= height + space
    # Add Dezso data
    if name in self.ionNamesMC:
      if 'MCprefix' in self.argDB:
        MCfilename = os.path.join(self.argDB['dataDirMC'], self.argDB['MCprefix']+self.ionNamesMC[name]+'.av')
      else:
        MCfilename = os.path.join(self.argDB['dataDirMC'], self.ionNamesMC[name]+'.av')
      if os.path.isfile(MCfilename):
        #mcData = self.readMCFile(os.path.join('..', 'test', 'data', 'MC', 'eps80-R4.5', '300-38', 'Ca.av'))
        mcData = self.readMCFile(MCfilename)
        mcData = mcData.transpose()
        ax.plot(mcData[0]/10.0, mcData[1], 'g^')
      # Get occupancy
      occ = 0.0
      dz  = (mcData[0][1] - mcData[0][0])/10.0
      for z, d in zip(mcData[0]/10.0, mcData[1]):
        if z >= -dft.Geometry.channelLength/2.0 and z < dft.Geometry.channelLength/2.0:
          occ += d
      print 'Occupancy',occ*dz
    # Render
    if self.argDB['show']:
      show()
    else:
      f = open('density.ca%.1e.png' % caRhoBath, 'w')
      savefig(f)
      f.close()
    return

  def plotOccupancy(self, dfts, names):
    from pylab import gca, legend, plot, savefig, show, title, xlabel, ylabel

    CaConcentration = [dft.HardSphere.rhoBath[names[0]]*self.NUMBER_TO_MOLAR for dft in dfts]
    d               = []
    for name in names:
      d.append(CaConcentration)
      d.append([dft.occupancy[name] for dft in dfts])
    gca().set_xscale('log')
    plot(*d)
    title('Filter Occupancy in Calcium Channel')
    xlabel('[Ca${}^{2+}$] M')
    ylabel('Number of ions in Filter')
    legend(names, 'upper center', shadow = True)
    if self.argDB['show']:
      show()
    else:
      f = open('occupancy.png', 'w')
      savefig(f)
      f.close()
    return

  def run(self):
    CaName  = 'Ca${}^{2+}$'
    NaName  = 'Na${}^+$'
    OxyName = 'O${}^{-1/2}$'
    sys.path.append(self.argDB['dataDir'])
    if self.argDB['zDensity']:
      for i in range(self.argDB['min'], self.argDB['max']):
        self.plotZDensity(getattr(__import__('dft_%05d' % i), 'DFT')(), CaName)
    if self.argDB['occupancy']:
      data = []
      for i in range(self.argDB['min'], self.argDB['max']):
        data.append(getattr(__import__('dft_%05d' % i), 'DFT')())
      self.plotOccupancy(data, [CaName, NaName])
    if 'zDensitySingle' in self.argDB:
      for i in range(self.argDB['min'], self.argDB['max']):
        self.plotZDensity(getattr(__import__('dft_%05d' % i), 'DFT')(), CaName, [self.ionNames[self.argDB['zDensitySingle']]])
    # Plot of all calcium z-densities on the same graph, labeled by concentration
    return

if __name__ == '__main__':
  Plotter().run()
