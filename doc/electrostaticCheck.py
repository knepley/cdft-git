#!/usr/bin/env python
from math import pi

# Constants
kT      = 4.21e-3
e       = 1.602e-19
epsilon = 78.4 * 8.8541878176e-39

# Inputs (should do by options)
case = 3
if case == 0:
  # Na, Ca, Cl, O
  rho     = [0.0602192, 6.02192e-7, 0.0602204, 0.0]
  sigma   = [2*0.1, 2*0.099, 2*0.181, 2*0.14]
  z       = [1, 2, -1, -0.5]
  Gamma   = 0.456295
elif case == 1:
  # This is likely wrong. Checking a mistake
  # Na, Ca, Cl, O
  rho     = [0.0602192, 6.02192e-7, 0.0602204, 9718.65]
  sigma   = [2*0.1, 2*0.099, 2*0.181, 2*0.14]
  z       = [1, 2, -1, -0.5]
  Gamma   = 2.45482
elif case == 2:
  # Na, Cl
  rho     = [0.0602192, 0.0602192]
  sigma   = [2*0.1, 2*0.181]
  z       = [1, -1]
  Gamma   = 0.456289
elif case == 3:
  # Line 2 from Table 1 in the RFD paper
  rho     = [0.602192, 0.602192]
  sigma   = [2*0.1, 2*0.2125]
  z       = [1, -1]
  Gamma   = 1.19475

def Delta():
  s = 0.0
  for i in range(len(rho)):
    s+= rho[i]*sigma[i]**3
  return 1.0 - (pi*s)/6.0

def omega(gamma):
  s = 0.0
  for i in range(len(rho)):
    s+= (rho[i]*sigma[i]**3)/(1.0 + gamma*sigma[i])
  return 1.0 + (pi*s)/(2.0*Delta())

def eta(gamma):
  s = 0.0
  for i in range(len(rho)):
    s+= (rho[i]*sigma[i]*z[i])/(1.0 + gamma*sigma[i])
  return (pi*s)/(2.0*Delta()*omega(gamma))

def residual(gamma):
  factor = (e*e)/(kT*epsilon)
  s      = 0.0
  for i in range(len(rho)):
    s += rho[i]*((z[i] - eta(gamma)*sigma[i]*sigma[i])/(1.0 + gamma*sigma[i]))**2
  return 4.0*gamma*gamma - factor*s

if __name__ == '__main__':
  print 'Starting'
  print 'Delta',Delta()
  print 'omega',omega(Gamma)
  print 'eta',eta(Gamma)
  res = residual(Gamma)
  print 'For Gamma',Gamma,'residual is',res
  print 'Done'
