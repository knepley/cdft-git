\documentclass{elsart}

\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amscd}
%\usepackage{esint}
%\usepackage{babel}

\def\code#1{{\tt #1}}
\newcommand{\comment}[1]{\begin{tabular}{||p{\textwidth}}\small \textbf{Comment:} \it #1 \end{tabular}}

% hyperref package should always be loaded as the very last one
% to be sure that it has the last word ...
\usepackage[
  pdftex,
  pdftitle={Implementation of a Poisson-Nernst-Planck equilibrium ion flux model using classical Density Functional Theory},
  pdfauthor={Matthew~G. Knepley, Dmitry~A. Karpeev, Seth Davidovits, Robert~S. Eisenberg, Dirk Gillespie},
  pdfpagemode={UseOutlines},
  bookmarks, bookmarksopen,bookmarksnumbered={True},
  colorlinks, linkcolor={blue},citecolor={blue},urlcolor={blue}
]{hyperref}

\begin{document}
\begin{frontmatter}
% Title, authors and addresses

% use the thanksref command within \title, \author or \address for footnotes;
% use the corauthref command within \author for corresponding author footnotes;
% use the ead command for the email address,
% and the form \ead[url] for the home page:
% \title{Title\thanksref{label1}}
% \thanks[label1]{}
% \author{Name\corauthref{cor1}\thanksref{label2}}
% \ead{email address}
% \ead[url]{home page}
% \thanks[label2]{}
% \corauth[cor1]{}
% \address{Address\thanksref{label3}}
% \thanks[label3]{}

\title{Implementation of a Poisson-Nernst-Planck equilibrium ion flux model using classical Density Functional Theory}

\author{Matthew~G. Knepley\thanksref{CI}}
\author{Dmitry~A. Karpeev\thanksref{CI}}
\author{Seth Davidovits\thanksref{CU}}
\author{Robert~S. Eisenberg\thanksref{Rush}}
\author{Dirk Gillespie\thanksref{Rush}}

\address[CI]{Computation Institute, University of Chicago}
\address[CU]{Department of Applied Physics and Applied Mathematics, Columbia University}
\address[Rush]{Department of Molecular Biophysics and Physiology, Rush University Medical Center}

\begin{abstract}
  DFT works! fast! in 3D!
\end{abstract}
\end{frontmatter}

\section{Introduction}

We present an implementation of the full three dimensional density functional theory (DFT) approach to the
Poisson-Nernst-Planck (PNP) problem as described in \cite{GillespieNonnerEisenberg02}. We use this model to represent
ion channels between baths of fixed concentration, in particular the ryanodine receptor calcium channel. The principal
channel mechanism investigated is ion selectivity. One dimensional computations using this model have been successful in
predicting experimental results, and have also agreed closely with established Monte Carlo simulations. With the PNP/DFT
model, we may reach timescales unavailable to previous simulations based upon Molecular Dynamics (MD) or quantum
mechanical models. 

Following the treatment in \cite{GillespieNonnerEisenberg02}, our code is currently applicable to steady state, zero
flux calculations. However, we take a simplified version of the electrostatics that limits us to one bath simulations,
as opposed to the two bath calculations presented there. Extensions to the two bath case, and to flux calculations are
planned.

\section{PNP}

Following~\cite{GillespieNonnerEisenberg02}, but in three dimensions, the steady state particle flux is represented
by
\begin{equation}
  -\vec{J}_i = \frac{1}{kT} D_i\left(\vec{x}\right) \rho_i\left(\vec{x}\right) \nabla\mu_i
  \label{eq:diffusion}
\end{equation}
along with the continuity equation
\begin{equation}
  \nabla\cdot\vec{J_i} = 0
  \label{eq:continuity}
\end{equation}
where the equations are indexed by the ion species, i. Here, $\rho$ represents the particle density, $\mu$ is the
chemical potential, and $J$ the particle flux. The parameter $D$ is the diffusion coefficient, $k$ Boltzmann's constant,
and $T$ the Kelvin temperature. In the zero flux case, we have
\begin{equation}
  \nabla\mu_i = 0
  \label{eq:chemicalPotentialGradient}
\end{equation}
which means the chemical potential is constant. We are led to the equilibrium condition for $\rho_i$,
see~\cite{GillespieNonnerEisenberg02},
\begin{equation}
  \mu^{bath}_i = \mu^{ext}_i\left(\vec{x}\right) + \mu^{ideal}_i\left(\vec{x}\right) + \mu^{int}_i\left(\vec{x}\right)
  \label{eq:chemicalPotentials}
\end{equation}
where the ideal-gas portion of the chemical potential is given by
\begin{equation}
  \mu^{ideal}_i\left(\vec{x}\right) = z_i e \phi\left(\vec{x}\right)
    + kT \ln\left(\frac{\rho_i\left(\vec{x}\right)}{\rho^{bath}_i}\right).
  \label{eq:idealChemicalPotential}
\end{equation}
Here $\mu^{ext}_i$ is the concentration independent part of the chemical potential, perhaps from a hard wall,
$\mu^{int}_i$ comes from particle interactions, and $\mu^{bath}_i$ is the bath potential described in
Section~\ref{sub:Calc-of-muBath}. Also, $\rho_i^{bath}$ in (\ref{eq:idealChemicalPotential}) is the bath concentration
of species $i$, $z_i$ is the valence of species $i$, e is the elementary charge, and $\phi$ the electrostatic
potential. Solving (\ref{eq:chemicalPotentials}) and (\ref{eq:idealChemicalPotential}) for $\rho$, we have
\begin{equation}
  \rho_i\left(\vec{x}\right) = \rho_i^{bath}\exp\left(\frac{\mu^{bath}_i - \mu_i^{ext}\left(\vec{x}\right)
    - \mu^{int}_i\left(\vec{x}\right) - z_i e \phi\left(\vec{x}\right)}{kT}\right)
  \label{eq:rhoEquation}
\end{equation}

\subsection{Interaction Chemical Potential}

The interaction chemical potential is broken down into two components,
\begin{equation}
  \mu^{int}_i = \mu^{HS}_i + \mu^{ES}_i
  \label{eq:interactionChemicalPotential}
\end{equation}
the hard sphere (HS) and electrostatic (ES) interactions~\cite{GillespieNonnerEisenberg02}.

\subsection{Calculation of the Bath Chemical Potential\label{sub:Calc-of-muBath}}

The bath chemical potential $\mu^{bath}_i$ has two components~\footnote{This is problem dependent, in that problems with
more potentials entering~(\ref{eq:chemicalPotentials}) may need more bath terms. For example, problems with an applied
electrostatic potential will have an additional bath chemical potential.},
\begin{equation}
  \mu^{bath}_i = \mu^{HS,bath}_i + \mu^{ES,bath}_i
  \label{eq:muBathBreakdown}
\end{equation}
the HS and ES, which are calculated thermodynamically, as in~\cite{NonnerGillespieHendersonEisenberg01},
\begin{eqnarray}
  \mu^{HS,bath}_i &=& kT \left( -\ln\Delta + \frac{3\xi_{2}\sigma_i + 3\xi_{1}\sigma_i^{2}}{\Delta}
    + \frac{9\xi_{2}^{2}\sigma_i^{2}}{2\Delta^{2}} + \frac{\pi P^{HS}_{bath}\sigma_i^{3}}{6kT} \right)
    \label{eq:muHSBath}\\
  P_{bath}^{HS}   &=& \frac{6kT}{\pi} \left( \frac{\xi_{0}}{\Delta} + \frac{3\xi_{1}\xi_{2}}{\Delta^{2}}
    + \frac{3\xi_{2}^{3}}{\Delta^{3}} \right)
    \label{eq:HSPressure}\\
  \xi_{n}         &=& \frac{\pi}{6} \sum_{j} \rho_{bath,j} \sigma_{j}^{n} \\
  \Delta          &=& 1-\xi_{3}
\end{eqnarray}
The calculation of $\mu_i^{HS,bath}$ as given above is straightforward, but $\mu_i^{ES,bath}$, on the other hand, is
dependent on an implicitly defined parameter $\Gamma$, the mean spherical approximation (MSA) screening parameter. This
implicit relationship is a quartic equation, which we solve using a Newton based solver. For brevity these equations
are left out, they can be found as equations 11--14,19--20 in~\cite{NonnerGillespieHendersonEisenberg01}. 

\subsection{Poisson Equation}

The last piece of the PNP system, in addition to (\ref{eq:diffusion}) and (\ref{eq:continuity}) is the Poisson equation,
which we treat with constant dielectric coefficient $\epsilon$,
\begin{equation}
  \epsilon \Delta\phi = e \sum_i z_i \rho_i\left(\vec{x}\right)
  \label{eq:Poisson}
\end{equation}

\section{Hard Sphere Interaction}

\subsection{Definition}

The HS chemical potential can be cast in terms of a convolution~\cite{GillespieNonnerEisenberg02,Roth,Rosenfeld93,Kilani89}
\begin{equation}
  \mu^{HS}_i\left(x\right) = kT \sum_\alpha \int \frac{\partial\Phi_{HS}}{\partial n_\alpha}\left(x'\right)
    \omega^{\alpha}_i\left(x - x'\right) dx'
  \label{eq:muHS}
\end{equation}
where $\alpha \in {0, 1, 2, 3, V1, V2}$, $V1$ and $V2$ each have three components, and
\begin{equation}
  \Phi_{HS}\left(n_{\alpha}\right) = -n_0 \ln\left(1 - n_3\right) + \frac{n_1 n_2 - \vec{n}_{V1}\cdot\vec{n}_{V2}}{1 - n_3}
    + \frac{n^3_2}{24\pi \left(1 - n_3\right)^2} \left(1 - \frac{\vec{n}_{V2}\cdot\vec{n}_{V2}}{n^2_2}\right)^3.
  \label{eq:Phi}
\end{equation}
We take the derivatives of $\Phi_{HS}$ with respect to the $n_\alpha$ analytically. The vectors $n_\alpha$, or weighted
densities, constitute a reduced basis for the problem.

\subsection{Calculation of $n_\alpha$\label{sub:Calculation-of-the-n}}

From~\cite{GillespieNonnerEisenberg02} we have
\begin{equation}
  n_\alpha(x) = \sum_i \int \rho_i(x') \omega^\alpha_i(x' - x) dx'
  \label{eq:nDef}
\end{equation}
where the integral is taken over all space. The $\omega_i^{(\alpha)}$ functions, or fundamental measures, given
in~\cite{GillespieNonnerEisenberg02} are
\begin{eqnarray}
  \omega^2_i(\vec{r})    &=& \delta(|\vec{r}| - R_i)                          \label{eq:omega2}\\
  \omega^3_i(\vec{r})    &=& \theta(|\vec{r}| - R_i)                          \label{eq:omega3}\\
  \omega^{V2}_i(\vec{r}) &=& \frac{\vec{r}}{|\vec{r}|}\delta(|\vec{r}| - R_i) \label{eq:omegaV2}\\
  \omega^0_i(\vec{r})    &=& \frac{1}{4\pi R^{2}_i} \omega^2_i(\vec{r})       \label{eq:omega0}\\
  \omega^1_i             &=& \frac{1}{4\pi R_i} \omega^2_i(\vec{r})           \label{eq:omega1}\\
  \omega^{V1}_i(\vec{r}) &=& \frac{1}{4\pi R_i} \omega^{V2}_i(\vec{r})        \label{eq:omegaV1}
\end{eqnarray}
Here, $r$ is the spherical radial vector, so $\omega^{V1}$ and $\omega^{V2}$ will each have three components. The
$n_\alpha$ integrals (\ref{eq:nDef}) are, up to the sign of the omega argument, convolutions. Since the weight
functions $\omega^\alpha$ are either even or odd, we can always convert the integral to a proper convolution. We
can therefore evaluate them using the Fourier transform and the convolution theorem:
\begin{eqnarray}
  n_{\alpha}(x) &=& \sum_i \int \rho_i(x') \omega^\alpha_i(x' - x) dx' \\
                &=& \mathcal{F}^{-1}\left(\mathcal{F}\left(\rho_i\right) \cdot \mathcal{F}\left(\omega^\alpha_i\right)\right)\\
                &=& \mathcal{F}^{-1}\left(\hat{\rho}_i \cdot \hat{\omega^\alpha_i}\right)
  \label{eq:conv}
\end{eqnarray}
Note that $n_{V1},n_{V2}$ are vectors, exactly as the associated omegas.

The strategy for calculation of $\mu^{HS}_i$ mirrors that described for $n_\alpha$ except that $\rho$ is replaced by
$\frac{\partial\Phi_{HS}}{\partial n_{\alpha}}$. Also note that since this is a true convolution, no multiplication
by a negative sign is needed.

\subsection{Calculation of the Fundamental Measures}

To evaluate equation~(\ref{eq:conv}), we use the FFT for both the transformation of $\rho_i$ and the inverse transform
of the product $\hat{\rho}_i \cdot \hat{\omega^\alpha_i}$. However the $\omega^\alpha_i$ are distributions, and are not
easily represented on a rectangular grid. A straightforward discretization would introduce unacceptably large errors.
Instead, we compute the Fourier transform of each function analytically, and then evaluate them on the same mesh in
Fourier space as used by FFTW (see~\ref{sub:Matching-K-Values}).

We begin with the calculation of $\hat{\omega}^2_i$.
\begin{eqnarray}
  \hat{\omega}^2_i &=& \int^{2\pi}_0 d\phi \int^\pi_{0} d\theta\sin\theta \int^\infty_0 dr\ r^2
    \delta\left(|r| - R_i\right) e^{-\imath\vec{k}\cdot\vec{x}} \\
                   &=& \int^{2\pi}_0 d\phi \int^\pi_{0} d\theta\sin\theta R^2_i e^{-\imath R_i\vec{k}\cdot\hat{x}}
\end{eqnarray}
To evaluate the integral, we choose a rotated coordinate system (the prime system) in which $\vec{k}$ points purely in
the $z'$ direction, taking advantage of the rotational symmetry of the problem. In the new coordinate system:
\begin{eqnarray}
  \hat{\omega}^2_i &=& \int^{2\pi}_0 d\phi' \int^\pi_0 d\theta' \sin\theta' R^2_i e^{-\imath R_i k'_z\cos\theta'} \\
                   &=& 2\pi R^2_i \int^\pi_0 d\theta' \sin\theta' \left(\cos\left(R_i k'_z\cos\theta'\right) -
                       \imath \sin\left(R_i k'_z\cos\theta'\right)\right) \\
                   &=& \frac{4\pi R_i \sin\left(R_i k'_z\right)}{k'_z}
\end{eqnarray}
which, in the original coordinate system, is
\begin{equation}
  \hat{\omega}^2_i = \frac{4\pi R_i\sin\left(R_i |\vec{k}|\right)}{|\vec{k}|}
  \label{eq:OmegaHat2}
\end{equation}
From equations (\ref{eq:omega0}) and (\ref{eq:omega0}) we easily find
\begin{eqnarray}
  \hat{\omega}^0_i &=& \frac{\sin\left(R_i |\vec{k}|\right)}{R_i |\vec{k}|} \label{eq:omegaHat0} \\
  \hat{\omega}^1_i &=& \frac{\sin\left(R_i |\vec{k}|\right)}{|\vec{k}|}     \label{eq:omegaHat1}
\end{eqnarray}

Recognizing that the theta function can be obtained as the integral of a delta function, we have
\begin{eqnarray}
  \hat{\omega}^3_i &=& \int^{R_i}_0 dr \hat{\omega}^2_i \mid|_{R_i = r} \\
                   &=& \frac{4\pi}{|\vec{k}|} \int^{R_i}_0 dr\ r \sin\left(r |\vec{k}|\right) \\
                   &=& \frac{4\pi}{|\vec{k}|^3} \left(\sin\left(R_i |\vec{k}|\right) - R_i |\vec{k}|\cos\left(R_i |\vec{k}|\right)\right) \label{eq:omegaHat3}
\end{eqnarray}

Following a similar procedure as in the $\hat{\omega}^2_i$ calculation, but keeping track of the vector nature of
$\omega^{V1}$ and $\omega^{V2}$,
\begin{eqnarray}
  \hat{\omega}^{V2}_i &=& \int^{2\pi}_0 d\phi' \int^\pi_0 d\theta' \sin\theta' R_i^{2} e^{-\imath R_i k'_z\cos\theta} \hat{x} \\
                      &=& -2\pi\imath R_i \int^\pi_0 d\theta' \sin\theta' \cos\theta' \sin\left(R_i k'_z\cos\theta'\right) \hat{k'_z} \\
                      &=& \frac{-4\pi\imath}{|\vec{k}|^{2}}\left(\sin\left(R_i |\vec{k}|\right)
                          - R_i |\vec{k}|\cos\left(R_i |\vec{k}|\right)\right) \hat{k}
  \label{eq:omegaHatV2}
\end{eqnarray}

\subsubsection{Limits}

The preceding expressions for $\hat{\omega}^\alpha_i$ may be evaluated at $|\vec{k}| = 0$ (see
\ref{sub:Matching-K-Values}), but care must be taken when calculating the limit.
\begin{eqnarray}
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^0_i &=& \underset{|\vec{k}|\rightarrow0}{\lim} \frac{\sin\left(R_i|\vec{k}|\right)}{R_i|\vec{k}|}
    = 1 \label{eq:limOmegaHat0} \\
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^1_i &=& R_i \label{eq:limOmegaHat1} \\
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^2_i &=& 4\pi R_i^{2} \label{eq:limOmegaHat2} \\
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^3_i &=& \underset{|\vec{k}|\rightarrow0}{\lim}
    \frac{4\pi}{|\vec{k}|^{3}} \left( \left(R_i |\vec{k}| - \frac{\left(R_i |\vec{k}|\right)^{3}}{6}\right) -
    R_i| \vec{k}| \left(1 - \frac{\left(R_i| \vec{k}|\right)^{2}}{2}\right)\right)
    = \frac{4}{3}\pi R_i^{3} \label{eq:limOmegaHat3} \\
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^{V1}_i &=& 0 \label{eq:limOmegaHatV1} \\
  \underset{|\vec{k}|\rightarrow0}{\lim} \hat{\omega}^{V2}_i &=& 0 \label{eq:limOmegaHatV2}
\end{eqnarray}

It should be noted these are the limits one would expect, since in
the $|\vec{k}|=0$ case we are simply integrating either a spherical
delta or step function over all space, thereby recovering surface
area and volume expressions for a sphere.

\subsection{Matching K Values\label{sub:Matching-K-Values}}

As previously mentioned, analytic Fourier transforms must be evaluated at the same k values, in the same order, as those
computed using the FFT. Given a $D$ dimensional grid, the vector $\{k_d\}$ which corresponds to the vertex $\{j_d\}$ is
given by
\begin{equation}
  k_d = \begin{cases}
    \frac{ 2\pi j_d}{N_d h_d}         & j_d \leq \frac{N_d}{2} \\
    \frac{-2\pi (N_d - j_d)}{N_d h_d} & j_d >    \frac{N_d}{2}
  \end{cases}
  \label{eq:Kvalues}
\end{equation}
where $N_d$ is the number of grid points in dimension $d$, and $h_d$ is the grid spacing $\frac{L_d}{N_d - 1}$.

\section{Electrostatics}

\subsection{Mean Field}

In order to obtain the mean electrostatic field, we solve the Poisson equation~(\ref{eq:Poisson}), for which the source
is the charge density $z_i \rho_i$. Since we have access to $\hat{\rho}_i$ from the calculation of $n_{\alpha}$, we may
solve~(\ref{eq:Poisson}) in the Fourier domain, in which the Laplacian is diagonal, and thus we may find $\phi$ by
dividing by the eigenvalues of the discrete Fourier transform. At grid vertex $\vec{j}$, we have
\begin{equation}
  \phi(\vec{j}) = \frac{e \sum_i z_i \rho_i(\vec{j})}
    {2\epsilon \left(\frac{1 - \cos k_x}{h^2_x} + \frac{1 - \cos k_y}{h^2_y} + \frac{1 - \cos k_z}{h^2_z}\right)}
  \label{eq:potentialSolution}
\end{equation}
where $k_x$, $k_y$, $k_z$ are calculated as described in Section~\ref{sub:Matching-K-Values}. To fully specify the
potential, we choose $\phi(0) = 0$ which is equivalent to having the additional constraint
\begin{equation}
  \int_{\Omega} \phi = 0
  \label{eq:potentialConstraint}
\end{equation}

\subsection{Local Correction}

The $\mu^{ES}_i$ component of~(\ref{eq:interactionChemicalPotential}) attempts to account for close range electrostatic interactions, where
screening is a factor. It is calculated as an expansion around the bath concentration
(see~\ref{sub:Calc-of-muBath}). From~\cite{GillespieNonnerEisenberg02} we have
\begin{equation}
  \mu^{ES}_i = \mu^{ES,bath}_i - \sum_j \int_{|x-x'| \leq R_{ij}} dx'
    \left(c^{(2)}_{ij}\left(x,x'\right) + \psi_{ij}\left(x,x'\right)\right) \Delta\rho_j(x')
  \label{eq:muESexpansion}
\end{equation}
where $R_{ij} = R_i + R_j$, $\Delta\rho_j = \rho_j - \rho_{bath}$ and
\begin{eqnarray}
  c\psi_{ij} &=& c^{(2)}_{ij}\left(x,x'\right) + \psi_{ij}\left(x,x'\right) \\
             &=& \frac{z_i z_j e^2}{8\pi\epsilon}
    \left(\frac{|x-x'|}{2\lambda_i\lambda_j} - \frac{\lambda_i + \lambda_j}{\lambda_i\lambda_j} +
    \frac{1}{|x-x'|} \left(\frac{\left(\lambda_i - \lambda_j\right)^2}{2\lambda_i\lambda_j} + 2\right)\right)
  \label{eq:CplusPsiFunc}
\end{eqnarray}
where $\lambda_k = R_k + s$ with $s = \frac{1}{2\Gamma}$. Gamma is the MSA screening parameter mentioned in
Secion~\ref{sub:Calc-of-muBath}. Unlike in \cite{GillespieNonnerEisenberg02}, we use a single bath. However, the
possible solutions to which we are able to perturb is limited. The integral in the expansion for $\mu^{ES}_i$ is a
convolution, which we also evaluate in the Fourier domain. This requires $\hat{\Delta\rho_j}$, calculated using the FFT,
and the transform of equation~(\ref{eq:CplusPsiFunc}) which is calculated anlaytically.
\begin{equation}
  \mu^{ES}_i = \mu_i^{ES,bath} - \sum_{j} \mathcal{F}^{-1} \left(\mathcal{F}\left(\Delta\rho_j\right) \cdot \mathcal{F}\left(c\psi_{ij}\right)\right)
  \label{eq:muESEval}
\end{equation}
It should be noted that in this simplified model of electrostatics, transformations of the $c\psi_{ij}$ need only be
calculated once, since they are fixed by the problem parameters. Additionally, since
\begin{equation}
  \mathcal{F}\left(\Delta\rho_j\right) = \mathcal{F}\left(\rho_j - \rho_{bath}\right)
    = \mathcal{F}\left(\rho_j\right) - \mathcal{F}\left(\rho_{bath}\right)
\end{equation}
where we have already calculated $\mathcal{F}\left(\rho_j\right)$ (in the $n_\alpha$
calculation~\ref{sub:Calculation-of-the-n}), and $\mathcal{F}\left(\rho_{bath}\right)$ need only be calculated once, the
only necessary Fourier transform each iteration is the inverse transformation.

In order to calculate each piece of $\hat{c\psi}_{ij}$, we must take the Fourier transform of powers of $r$. The generic
term has the form
\begin{eqnarray}
  \int_{{\cal B}(R)} r^n e^{i {\bf k}\cdot{\bf v}}
  &=& 2\pi \int^R_0 dr\ r^2 \int^\pi_0 d\theta \sin\theta r^n e^{i k r \cos\theta} \\
  &=& 2\pi \int^R_0 dr\ r^{n+2} \int^1_{-1} du e^{i k r u} \\
  &=& 2\pi \int^R_0 dr\ r^{n+2} \left[ {e^{i k r u} \over i k r} \right]^1_{-1} \\
  &=& 2\pi \int^R_0 dr\ r^{n+2} {e^{i k r} - e^{-i k r} \over i k r} \\
  &=& {4\pi\over k} \int^R_0 dr\ r^{n+1} \sin(k r) \\
  &=& {4\pi\over k} I_n
\end{eqnarray}
We may derive a recursive definition for the integral $I_n$ using integration by parts,
\begin{eqnarray}
  I_n = \int^R_0 dr\ r^{n+1} \sin(k r)
  &=& \begin{cases}\left[-{r^{n+1}\over k} \cos(k r)\right]^R_0 + {n+1\over k} J_n & n \ge -1\\
             0 & n < -1\end{cases} \\
  J_n = \int^R_0 dr\ r^n \cos(k r)
  &=& \begin{cases}\left[{r^{n+1}\over k} \sin(k r)\right]^R_0 - {n\over k} J_{n-2} & n \ge 0\\
             0 & n < 0\end{cases} \\
\end{eqnarray}
For the DFT electrostatics, we need the terms
\begin{eqnarray}
  I_{-1} &=& {1\over k} \left(1 - \cos(k R)\right) \\
  I_0    &=& -{R\over k} \cos(k R) + {1\over k^2} \sin(k R) \\
  I_1    &=& -{R^2\over k} \cos(k R) + 2{R\over k^2} \sin(k R) - {2\over k^3} \left(1 - \cos(k R)\right) \\
\end{eqnarray}
and their limits as $k$ tends to 0,
\begin{eqnarray}
  \lim_{k\to0} \frac{4\pi}{k} I_{-1} &=&  2\pi R^2 \\
  \lim_{k\to0} \frac{4\pi}{k} I_0    &=& \frac{4\pi R^3}{3} \\
  \lim_{k\to0} \frac{4\pi}{k} I_1    &=& \pi R^4 \\
\end{eqnarray}
so that
\begin{equation}
  \hat{c\psi}_{ij} = \frac{z_i z_j e^2}{\epsilon|\vec{k}|}
    \left(\frac{1}{2\lambda_i\lambda_j} I_1 - \frac{\lambda_i + \lambda_j}{\lambda_i\lambda_j} I_0 +
    \left(\frac{\left(\lambda_i - \lambda_j\right)^2}{2\lambda_i\lambda_j} + 2\right) I_{-1} \right)
  \label{eq:CplusPsiHat}
\end{equation}

\input{Writeup-Confined}

\section{Discretization and Solution of the Equilibrium Condition}

The problem~(\ref{eq:rhoEquation}) is solved on a rectangular prism domain, supporting a different system length in each
Cartesian direction. This geometry is well supported by the PETSc \code{DA} abstraction, and will allow for easy
parallelization. The grid is uniform in all directions, which allows to compute the convolutions using Fourier transform
techniques, specifically the FFTW package~\cite{FFTW}. Periodic boundary conditions are naturally enforced by the FFT.

The bath potential, $\mu^{bath}_i$, and external potential, $\mu^{ext}_i$, are calculated just once during the problem
setup. Hard walls defined by the external potential can be used to represent the channel protein, or to confine ion
species in the channel so that they can represent structural charges. The interaction chemical potential is dependent on
the concentration, as is the electrostatic potential, so these are recalculated each Newton step. The evaluation of the
ten $\frac{\partial\Phi_{HS}}{\partial n_{\alpha}}$ and the $n_{\left(\alpha\right)}$ at each grid point must be done
each residual evaluation since they are dependent on $\rho_i$.

\subsection{Nonlinear solver}

The equation~(\ref{eq:rhoEquation}) is solved using a Picard iteration. With higher bath densities, it is necessary to
use a line search during the Picard update. We currently have a quadratic line search which fits the squared $L_2$ norms
of the residuals as this seemed to better match curves in the search parameter we sampled for testing. Moreover, the
$n_3$ function is actually the local packing fraction, and thus should never exceed unity and we bound it by 0.9. Since
it is a linear function, we can simply bound the maximum allowable search parameter
\begin{eqnarray*}
  ||n_3((1 - \alpha) \rho_0) + n_3(\alpha \rho_1)||_\infty &<& ||(1 - \alpha) n_3(\rho_0)||_\infty + ||\alpha n_3(\rho_1)||_\infty \\
                                                           &=& (1 - \alpha) ||n_3(\rho_0)||_\infty + \alpha ||n_3(\rho_1)||_\infty \\
                                                           &<& 0.9
\end{eqnarray*}
so that finally
\begin{equation}
  \alpha < \frac{0.9 - ||n_3(\rho_0)||_\infty}{||n_3(\rho_1)||_\infty - ||n_3(\rho_0)||_\infty}
\end{equation}

We have also experimented with Newton's method, forming the action of the Jacobian operator using finite
differences. The linear systems are solved with GMRES. We tried linear system tolerances which were both fixed and
chosen according to the Eisenstat-Walker scheme. However, the Newton method was not competitive with Picard in complex
geometries due to linear convergence through most of the run and the large cost of computing Jacobian action using
differences.

\input{Writeup-Stability}

\section{Verification}

    At several points in the calculation, we are able to verify the consistency of the results. Moreover, we can compare
to thermodynamic solutions, in the limit of very fine meshes. We first check that we recover the fundamental measures,
which are easily computed analytically, when we compute $n_\alpha(\rho)$ with a constant unit density. We may also check
that in the bath, $n_3$ must be equal to the combined volume fraction of the ion bath concentrations. Moreover, we can
verify that in symmetric situations, such as a hard wall, the solution msut be homogeneous over each plane parallel to
the wall.

    We can solve an effectively one-dimensional problem with a wall at $z = 0$ and periodic in each dimension in order
to compare with thermodynamic results~\cite{Roth}. With purely hard sphere interactions, we have a relation between the
pressure on the wall and the density of each species at the wall
\begin{equation}
  \beta P = \sum_i \rho_i(R_i),
\end{equation}
where we evaluate the density at the point of closest approach to the wall. This relation continues to hold
approximately when electrostatics are included using a suitable modification of the pressure calculation. In this
calculation, we are careful to make the wall thick enough to prevent electrostatic interaction through the wall, and
also the domain long enough so that bath concentrations are reached far from the wall.

\section{Runs}

\begin{itemize}
  \item Multispecies

  \item Hard sphere and electrostatics

  \item Channel geometry
\end{itemize}

\comment{Get picture of channel geometry}

\comment{Check ryanidine receptor calculations against MC}

\section{Conclusion/Further Work}

We have presented a full numerical strategy for solving the three
dimensional PNP/DFT system in the zero flux steady state. We are currently
in the process of assessing the correctness of the results produced
by simulations from the code.

\begin{itemize}
  \item Improved electrostatics
  \begin{itemize}
    \item Incorporate self-consistency iteration of reference fluid into Newton for $\rho$
  \end{itemize}

  \item Generally usable code
  \begin{itemize}
    \item Easy installation
    \item Channel problem
    \item Geometry library interface
  \end{itemize}

  \item Alternative convolution calculation
  \begin{itemize}
    \item USFFT
    \item Replace FFT
  \end{itemize}

  \item Solver
  \begin{itemize}
    \item Use AD for the Jacobian?
    \item Bootstrap Newton with Picard
  \end{itemize}

  \item Parallelism
  \begin{itemize}
    \item Target multicore as well
    \item Investigate Spiral (student)
  \end{itemize}
\end{itemize}

\section{Acknowledgments}

We need to acknowledge Rush, ANL, and CI funding sources.

\bibliographystyle{plain}
\bibliography{petscapp,Writeup}

\end{document}
