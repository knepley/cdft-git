#!/usr/bin/env python
from pylab import *
import numpy
import os

def readFile(filename):
  print 'Reading',filename
  values = []
  f = open(filename)
  for line in f.readlines():
    values.append(float(line.split()[0]))
#    values.append(map(float, line.split()))
  f.close()
  return numpy.array(values)

def dirkFloat(num):
  if num == '*':
    return 0.0
  return float(num)

def readDirkFile(filename):
  values = []
  f = open(filename)
  for line in f.readlines():
    values.append(map(dirkFloat, line.split()))
  f.close()
  return numpy.transpose(numpy.array(values))

def problem2():
  numSpecies = 2
  kT = 0.00421
  data = []
  for i in range(numSpecies):
    d = readFile('rho_%d.out' % i)
    d *= 1.6606
    data.append(d)
  data.append(readFile('phi.out'))
  data[2] /= kT
  dataRef = []
  for i in range(numSpecies):
    d = readFile('rhoRef_%d.out' % i)
    d *= 1.6606
    dataRef.append(d)
  dirkBFData = readDirkFile('data/problem2/Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0_Rosen.txt')
  dirkBFData[0] *= 0.1 # Convert from angstroms to nm
  dirkRFDData = readDirkFile('data/problem2/Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0.txt')
  dirkRFDData[0] *= 0.1 # Convert from angstroms to nm
  Lz = 6.0
  P = len(data[0])
  h = Lz/(P-1)
  z_0 = 0.3
  z = [h*i - z_0 for i in range(len(data[0]))]
  # Rho plot
  for s in range(numSpecies):
    plot(z,              data[s],          'bo', z,              dataRef[s],                  'ro')
    plot(dirkRFDData[0], dirkRFDData[s+1], 'bs', dirkRFDData[0], dirkRFDData[s+numSpecies+2], 'rs')
    plot(dirkBFData[0],  dirkBFData[s+1],  'gs')
    show()
    clf()
  # Phi plot
  plot(z, data[2], 'bo', dirkRFDData[0], dirkRFDData[3], 'bs', dirkBFData[0], dirkBFData[3], 'gs')
  show()
  return

def MC():
  mcData = readFile(os.path.join('..', 'test', 'data', 'MC', 'eps80-R4.5', '300-38', 'Na.av'))
  mcData = mcData.transpose()
  print mcData
  plot(mcData[0], mcData[1], 'bs')
  show()
  return

if __name__ == '__main__':
  problem2()
