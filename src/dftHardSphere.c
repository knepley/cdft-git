#include <dftHardSphere.h>
#include <dftOutput.h>

HardSphereOptions *globalHardSphereOptions;

#undef __FUNCT__
#define __FUNCT__ "ProcessHardSphereOptions"
PetscErrorCode ProcessHardSphereOptions(MPI_Comm comm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscInt       n;
  PetscBool      flag, flag2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscOptionsBegin(comm, "", "DFT HardSphere Options", "DMMG");CHKERRQ(ierr);
    for(PetscInt i = 0; i < geomOptions->numSpecies; ++i) {
      if(i == 0){
        options->rhoBath[i] = 6.0e-1;   // 6 10^{26} ions/m^3
      } else{
        options->rhoBath[i] = 1.2e-0;   // 12 10^{26} ions/m^3
      }
    }

    n = geomOptions->numSpecies;
    ierr = PetscOptionsRealArray("-rhoBath", "The density of species i in the bath", __FILE__, options->rhoBath, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != geomOptions->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");}
    n = geomOptions->numSpecies;
    ierr = PetscOptionsRealArray("-rhoBathM", "The molar density of species i in the bath", __FILE__, options->rhoBath, &n, &flag2);CHKERRQ(ierr);
    if (flag2 && (n != geomOptions->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of molar bath potentials given");}
    if (flag && flag2) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Cannot specify both potentials and molar potentials");}
    if (flag2) {for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) options->rhoBath[sp] /= NUMBER_TO_MOLAR;}
    for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
      if ((geomOptions->domainType == DOMAIN_CHANNEL || geomOptions->domainType == DOMAIN_CHANNEL_SMOOTH)
          && geomOptions->isConfined[sp] && options->rhoBath[sp] == 0.0) {
        const PetscReal L  = geomOptions->channelLength;
        const PetscReal R  = geomOptions->channelRadius;
        const PetscReal Ri = geomOptions->R[sp];

        options->rhoBoltzmann[sp] = geomOptions->numParticles[sp]/(M_PI*PetscSqr(R - Ri)*(L - Ri));
      } else {
        options->rhoBoltzmann[sp] = options->rhoBath[sp];
      }
    }
    n = geomOptions->numSpecies;
    ierr = PetscOptionsRealArray("-muHSBath", "The chemical potential of species i in the bath", __FILE__, options->muHSBath, &n, &flag);CHKERRQ(ierr);
    if (!flag) {
      ierr = CalculateMuHSBath(geomOptions->numSpecies, geomOptions->R, geomOptions->isConfined, options->rhoBath, options->muHSBath);CHKERRQ(ierr);
      if (debug >= 1) {ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculated the hard sphere part of the chemical potential in the bath:\n"); CHKERRQ(ierr);}
    } else if (n != geomOptions->numSpecies) {
      SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");
    } else {
      if (debug >= 1) {ierr = PetscPrintf(PETSC_COMM_WORLD, "Read the hard sphere part of the chemical potential in the bath from options:\n"); CHKERRQ(ierr);}
    }
    if (debug >= 1) {
      for(PetscInt i = 0; i < geomOptions->numSpecies; ++i) {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "muHSBath_%d: %g\n", i, options->muHSBath[i]); CHKERRQ(ierr);
      }
    }
  ierr = PetscOptionsEnd();

  ierr = PetscLogEventRegister("Calc n_alpha", PETSC_SMALLEST_CLASSID, &options->nEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc PhiDer",  PETSC_SMALLEST_CLASSID, &options->phiDerEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc mu_HS",   PETSC_SMALLEST_CLASSID, &options->muHSEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintHardSphereModule"
PetscErrorCode PrintHardSphereModule(PetscViewer viewer, const char indent[], GeometryOptions *geomOpts, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer, "%sclass HardSphere:\n", indent);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  rhoBath = {", indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "'%s': %g", geomOpts->ionNames[sp], options->rhoBath[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "}\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  muHSBath = {", indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "'%s': %g", geomOpts->ionNames[sp], options->muHSBath[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "}\n");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateWindowFunctions"
PetscErrorCode CreateWindowFunctions(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  const PetscInt    dim      = 3;
  const PetscInt    numAlpha = geomOptions->numAlpha;
  PetscScalar     (*WHatFunc[10])(const PetscScalar [], const int);
  Vec               X;
  EvaluationOptions ctx;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  if (numAlpha != 10) SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Invalid bound for alpha, %d should be %d", numAlpha, 10);
  WHatFunc[0] = OmegaHat_0;
  WHatFunc[1] = OmegaHat_1;
  WHatFunc[2] = OmegaHat_2;
  WHatFunc[3] = OmegaHat_3;
  WHatFunc[4] = OmegaHat_V1_Kx;
  WHatFunc[5] = OmegaHat_V1_Ky;
  WHatFunc[6] = OmegaHat_V1_Kz;
  WHatFunc[7] = OmegaHat_V2_Kx;
  WHatFunc[8] = OmegaHat_V2_Ky;
  WHatFunc[9] = OmegaHat_V2_Kz;

  ierr = PetscMalloc(numAlpha * sizeof(Vec), &options->W);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < 10; ++alpha) {
    ierr = DMCreateGlobalVector(dm, &options->W[alpha]);CHKERRQ(ierr);
    ctx.numSpecies = geomOptions->numSpecies;
    ctx.func1      = WHatFunc[alpha];
    if (dim == 1) {
      ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_1d, X, options->W[alpha], (void *) &ctx);CHKERRQ(ierr);
    } else if (dim == 2) {
      ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_2d, X, options->W[alpha], (void *) &ctx);CHKERRQ(ierr);
    } else if (dim == 3) {
	  ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_3d, X, options->W[alpha], (void *) &ctx);CHKERRQ(ierr);
	}  else {
      SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
    }
  }
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyWindowFunctions"
PetscErrorCode DestroyWindowFunctions(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  for(PetscInt alpha = 0; alpha < geomOptions->numAlpha; ++alpha) {
    ierr = VecDestroy(&options->W[alpha]);CHKERRQ(ierr);
  }
  ierr = PetscFree(options->W);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreatePhiDerFunctions"
PetscErrorCode CreatePhiDerFunctions(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (geomOptions->numAlpha != 10) SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Invalid bound for alpha, %d should be %d", geomOptions->numAlpha, 10);
  ierr = PetscMalloc(geomOptions->numAlpha*sizeof(PetscScalar (*)(const PetscScalar [])), &options->PhiDerFunc);CHKERRQ(ierr);
  options->PhiDerFunc[0] = PhiDer_0;
  options->PhiDerFunc[1] = PhiDer_1;
  options->PhiDerFunc[2] = PhiDer_2;
  options->PhiDerFunc[3] = PhiDer_3;
  options->PhiDerFunc[4] = PhiDer_V1_Kx;
  options->PhiDerFunc[5] = PhiDer_V1_Ky;
  options->PhiDerFunc[6] = PhiDer_V1_Kz;
  options->PhiDerFunc[7] = PhiDer_V2_Kx;
  options->PhiDerFunc[8] = PhiDer_V2_Ky;
  options->PhiDerFunc[9] = PhiDer_V2_Kz;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyPhiDerFunctions"
PetscErrorCode DestroyPhiDerFunctions(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscFree(options->PhiDerFunc);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscScalar uniformBath(const PetscScalar x[], const int species) {
  if (globalGeometryOptions->isConfined[species]) {
	return 0.0;
  }
  return globalHardSphereOptions->rhoBath[species];
}

#undef __FUNCT__
#define __FUNCT__ "CalculateRhoBath"
PetscErrorCode CalculateRhoBath(DM dm, const PetscInt numSpecies, Vec rhoBathVec)
{
  const PetscInt    dim = 3;
  Vec               X;
  EvaluationOptions ctx;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  ctx.numSpecies = numSpecies;
  ctx.func1      = uniformBath;
  if (dim == 1) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_1d, X, rhoBathVec, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_2d, X, rhoBathVec, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 3) {
	ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_3d, X, rhoBathVec, (void *) &ctx);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateRhoBath"
PetscErrorCode CreateRhoBath(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMCreateGlobalVector(dm, &options->rhoBathVec);CHKERRQ(ierr);
  ierr = CalculateRhoBath(dm, geomOptions->numSpecies, options->rhoBathVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyRhoBath"
PetscErrorCode DestroyRhoBath(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&options->rhoBathVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateFFT"
PetscErrorCode CreateFFT(DM dm, HardSphereOptions *options)
{
  const PetscInt dim = 3;
  PetscInt       dims[3];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  // Note FFTW wants dimensions in reverse order (since we have Fortran storage)
  ierr = DMDAGetInfo(dm, 0, &dims[2], &dims[1], &dims[0], 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = MatCreateFFT(PETSC_COMM_WORLD, dim, dims, MATFFTW, &options->F);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyFFT"
PetscErrorCode DestroyFFT(DM dm, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MatDestroy(&options->F);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateHardSphere"
PetscErrorCode CreateHardSphere(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  // Check packing fraction
  for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
    geomOptions->eta[sp] = options->rhoBath[sp]*(4.0*M_PI/3.0)*(PetscSqr(geomOptions->R[sp])*geomOptions->R[sp]);
    if (!geomOptions->isConfined[sp] && geomOptions->eta[sp] > 0.49) {
      SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Excessive volume fraction: %g for %s", geomOptions->eta[sp], geomOptions->ionNames[sp]);
    }
  }
  ierr = CreateWindowFunctions(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = CreatePhiDerFunctions(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm, &options->rhoHat);CHKERRQ(ierr);
  ierr = CreateRhoBath(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = CreateFFT(dm, options);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyHardSphere"
PetscErrorCode DestroyHardSphere(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DestroyWindowFunctions(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = DestroyPhiDerFunctions(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = VecDestroy(&options->rhoHat);CHKERRQ(ierr);
  ierr = DestroyRhoBath(dm, geomOptions, options);CHKERRQ(ierr);
  ierr = DestroyFFT(dm, options);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePressureHS"
PetscErrorCode CalculatePressureHS(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], PetscReal *P)
{
  PetscReal xi_0 = 0.0, xi_1 = 0.0, xi_2 = 0.0, xi_3 = 0.0, Delta;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    const PetscReal sigma = 2.0*R[sp];

    xi_0 += rhoBath[sp];
    xi_1 += rhoBath[sp]*sigma;
    xi_2 += rhoBath[sp]*PetscSqr(sigma);
    xi_3 += rhoBath[sp]*PetscSqr(sigma)*sigma;
  }
  xi_0 *= (M_PI/6.0);
  xi_1 *= (M_PI/6.0);
  xi_2 *= (M_PI/6.0);
  xi_3 *= (M_PI/6.0);
  Delta = 1.0 - xi_3;

  *P = (6.0/M_PI)*kT*(xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHSBath"
PetscErrorCode CalculateMuHSBath(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], PetscReal muHSBath[])
{
  PetscReal      xi_0 = 0.0, xi_1 = 0.0, xi_2 = 0.0, xi_3 = 0.0, Delta;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    const PetscReal sigma = 2.0*R[sp];

    xi_0 += rhoBath[sp];
    xi_1 += rhoBath[sp]*sigma;
    xi_2 += rhoBath[sp]*PetscSqr(sigma);
    xi_3 += rhoBath[sp]*PetscSqr(sigma)*sigma;

  }
  xi_0 *= (M_PI/6.0);
  xi_1 *= (M_PI/6.0);
  xi_2 *= (M_PI/6.0);
  xi_3 *= (M_PI/6.0);
  Delta = 1.0 - xi_3;

  if (debug == 4) {ierr = PetscPrintf(PETSC_COMM_WORLD, "Delta %g\n", Delta);CHKERRQ(ierr);}
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    const PetscReal sigma = 2.0*R[sp];

    if (isConfined[sp]) {
      muHSBath[sp] = 0.0;
    } else {
      muHSBath[sp] = kT*(-log(Delta) +
                        (3.0*xi_2*sigma + 3.0*xi_1*PetscSqr(sigma))/Delta +
                        (9.0*PetscSqr(xi_2)*PetscSqr(sigma))/(2.0*PetscSqr(Delta)) +
                        (xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta))*PetscSqr(sigma)*sigma);
    }
    if (debug == 4) {ierr = PetscPrintf(PETSC_COMM_WORLD, "rho^%d_{bath} %g mu^{HS,%d}_{bath} %g\n", sp, rhoBath[sp], sp, muHSBath[sp]);CHKERRQ(ierr);}
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN_3d"
PetscErrorCode CalculateN_3d(DM dm, Vec rho, const int numSpecies, const int numAlpha, DM singleDA, HardSphereOptions *options, Vec n)
{
  Vec           *rhoHatSplit;
  Vec           *WAlphaSplit;
  Vec           *nSplit;
  Vec            y1, y2;
  PetscInt       M, N, P;
  PetscReal      scale;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->nEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dm, 0, &M, &N, &P, 0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  scale = 1.0/((PetscReal) M*N*P);
  ierr = PetscMalloc3(numSpecies+1, Vec, &rhoHatSplit, numSpecies+1, Vec, &WAlphaSplit, numAlpha, Vec, &nSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp) {
	ierr = DMGetGlobalVector(singleDA, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
	ierr = DMGetGlobalVector(singleDA, &nSplit[alpha]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(singleDA, &y1);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(rho, rhoHatSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = PrintNorm(rhoHatSplit[sp], "rho", sp, -1);CHKERRQ(ierr);
    ierr = MatMult(options->F, rhoHatSplit[sp], y1);CHKERRQ(ierr);
    ierr = VecCopy(y1, rhoHatSplit[sp]);CHKERRQ(ierr);
    ierr = PrintNorm(rhoHatSplit[sp], "\\hat rho", sp, -1);CHKERRQ(ierr);
  }
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = VecStrideGatherAll(options->W[alpha], WAlphaSplit, INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecSet(nSplit[alpha], 0.0);
    for(PetscInt sp = 0; sp < numSpecies; ++sp) {
      ierr = VecPointwiseMult(y1, rhoHatSplit[sp], WAlphaSplit[sp]);CHKERRQ(ierr);
      ierr = MatMultTranspose(options->F, y1, y2);CHKERRQ(ierr);
      ierr = VecAXPY(nSplit[alpha], 1.0, y2);CHKERRQ(ierr);
    }
    // Correct for sign difference in convolution of odd functions
    if ((alpha == 4) || (alpha == 5) || (alpha==6) || (alpha==7) || (alpha==8) || (alpha==9)) {
      ierr = VecScale(nSplit[alpha], -scale);CHKERRQ(ierr);
    } else {
      ierr = VecScale(nSplit[alpha], scale);CHKERRQ(ierr);
    }
    ierr = PrintNorm(nSplit[alpha], "n", alpha, -1);CHKERRQ(ierr);
    // Check for valid packing fraction
    if (alpha == 3) {
      DMDALocalInfo  info;
      PetscScalar ***n3;

      ierr = DMDAGetLocalInfo(singleDA, &info);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(singleDA, nSplit[alpha], &n3);CHKERRQ(ierr);
      for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
        for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
          for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
            if (PetscRealPart(n3[k][j][i]) >= 1.0) {
              SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Invalid packing fraction: n3[%d][%d][%d]: %g", k, j, i, PetscRealPart(n3[k][j][i]));
            }
          }
        }
      }
      ierr = DMDAVecRestoreArray(singleDA, nSplit[alpha], &n3);CHKERRQ(ierr);
    }
  }
  ierr = VecStrideScatterAll(rhoHatSplit, options->rhoHat, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecStrideScatterAll(nSplit, n, INSERT_VALUES);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
	ierr = DMRestoreGlobalVector(singleDA, &nSplit[alpha]);CHKERRQ(ierr);
  }
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
	ierr = DMRestoreGlobalVector(singleDA, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree3(rhoHatSplit, WAlphaSplit, nSplit);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(options->nEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN3_3d"
PetscErrorCode CalculateN3_3d(DM dm, Vec rho, const int numSpecies, DM singleDA, HardSphereOptions *options, Vec n3)
{
  Vec           *rhoHatSplit;
  Vec           *WAlphaSplit;
  Vec            y1, y2;
  PetscInt       M, N, P;
  PetscReal      scale;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = DMDAGetInfo(dm, 0, &M, &N, &P, 0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  scale = 1.0/((PetscReal) M*N*P);
  ierr = PetscMalloc2(numSpecies+1, Vec, &rhoHatSplit, numSpecies+1, Vec, &WAlphaSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp) {
	ierr = DMGetGlobalVector(singleDA, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(singleDA, &y1);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(rho, rhoHatSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = PrintNorm(rhoHatSplit[sp], "rho", sp, -1);CHKERRQ(ierr);
    ierr = MatMult(options->F, rhoHatSplit[sp], y1);CHKERRQ(ierr);
    ierr = VecCopy(y1, rhoHatSplit[sp]);CHKERRQ(ierr);
    ierr = PrintNorm(rhoHatSplit[sp], "\\hat rho", sp, -1);CHKERRQ(ierr);
  }
  ierr = VecStrideGatherAll(options->W[3], WAlphaSplit, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecSet(n3, 0.0);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = VecPointwiseMult(y1, rhoHatSplit[sp], WAlphaSplit[sp]);CHKERRQ(ierr);
    ierr = MatMultTranspose(options->F, y1, y2);CHKERRQ(ierr);
    ierr = VecAXPY(n3, 1.0, y2);CHKERRQ(ierr);
  }
  ierr = VecScale(n3, scale);CHKERRQ(ierr);
  ierr = PrintNorm(n3, "n", 3, -1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
	ierr = DMRestoreGlobalVector(singleDA, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree2(rhoHatSplit, WAlphaSplit);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePhiDer_3d"
PetscErrorCode CalculatePhiDer_3d(DMDALocalInfo *info, N **n[], const int numAlpha, PetscScalar (**PhiDerFunc)(const PetscScalar []), HardSphereOptions *options, Vec PhiDer[]) {
  PetscScalar ***phiDer;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->phiDerEvent,0,0,0,0);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = DMDAVecGetArray(info->da, PhiDer[alpha], &phiDer);CHKERRQ(ierr);
	for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
      for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
        for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
          phiDer[k][j][i] = (*PhiDerFunc[alpha])(n[k][j][i].v);
          if (PetscIsInfOrNanScalar(phiDer[k][j][i])) {
            for(PetscInt beta = 0; beta < numAlpha; ++beta) {
              ierr = PetscPrintf(PETSC_COMM_SELF, "alpha: %d n[%d][%d][%d].v[%d]: %g\n", alpha, k, j, i, beta, PetscRealPart(n[k][j][i].v[beta]));
            }
            SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");
          }
        }
      }
	}
    ierr = DMDAVecRestoreArray(info->da, PhiDer[alpha], &phiDer);CHKERRQ(ierr);
    ierr = VecViewCenter(info->da, PhiDer[alpha], PETSC_VIEWER_DRAW_WORLD, "PhiDer", alpha, -1);CHKERRQ(ierr);
    ierr = PrintNorm(PhiDer[alpha], "PhiDer", alpha, -1);CHKERRQ(ierr);
  }
  ierr = PetscLogEventEnd(options->phiDerEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHS"
// Could we use a block FFT here?
PetscErrorCode CalculateMuHS(DM dm, const int numSpecies, const int numAlpha, DM singleDA, Vec PhiDer[], HardSphereOptions *options, Vec muHS)
{
  Vec           *muSplit;
  Vec           *WAlphaSplit;
  Vec            phiDerHat_alpha, y2, y3;
  PetscInt       M, N, P;
  PetscScalar    scale;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->muHSEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dm, 0, &M, &N, &P, 0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  scale = 1.0/((PetscReal) M*N*P);
  ierr = PetscMalloc2(numSpecies+1, Vec, &muSplit, numSpecies+1, Vec, &WAlphaSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp) {
	ierr = DMGetGlobalVector(singleDA, &muSplit[sp]);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
    ierr = VecSet(muSplit[sp], 0.0);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(singleDA, &phiDerHat_alpha);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &y3);CHKERRQ(ierr);
  ierr = VecSet(muHS, 0.0);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
	ierr = VecStrideGatherAll(options->W[alpha], WAlphaSplit, INSERT_VALUES);CHKERRQ(ierr);
	ierr = MatMult(options->F, PhiDer[alpha], phiDerHat_alpha);CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < numSpecies; ++sp) {
      ierr = VecPointwiseMult(y2, phiDerHat_alpha, WAlphaSplit[sp]);CHKERRQ(ierr);
      ierr = MatMultTranspose(options->F, y2, y3);CHKERRQ(ierr);
      ierr = VecScale(y3, scale);CHKERRQ(ierr);//Do this later to the whole vector?
      if (debug > 4) {
	    PetscScalar ***a;

	    ierr = DMDAVecGetArray(singleDA, y3, &a);CHKERRQ(ierr);
		for(PetscInt k = 0; k < P; ++k) {
          for(PetscInt j = 0; j < N; ++j) {
            for(PetscInt i = 0; i < M; ++i) {
              if (PetscIsInfOrNanScalar(a[k][j][i])) {
                ierr = PetscPrintf(PETSC_COMM_SELF, "alpha: %d species: %d\n", alpha, sp);
                ierr = VecView(PhiDer[alpha], PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
                ierr = VecView(options->W[alpha], PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
                ierr = VecView(y3, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
                SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");
              };
            }
          }
		}
	    ierr = DMDAVecRestoreArray(singleDA, y3, &a);CHKERRQ(ierr);
      }
      ierr = VecAXPY(muSplit[sp], 1.0, y3);CHKERRQ(ierr);
      ierr = PrintNorm(muSplit[sp], "muHSsplit", sp, -1);CHKERRQ(ierr);
    }
  }
  ierr = VecStrideScatterAll(muSplit, muHS, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecScale(muHS, kT);CHKERRQ(ierr);
  ierr = PrintNorm(muHS, "muHS", -1, -1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &phiDerHat_alpha);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y2);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &y3);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp) {
	ierr = DMRestoreGlobalVector(singleDA, &muSplit[sp]);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(singleDA, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree2(muSplit, WAlphaSplit);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(options->muHSEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}
