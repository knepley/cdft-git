#include <dftUtils.h>

GeometryOptions *globalGeometryOptions;

PetscInt        debug   = 0;
const PetscReal kT      = 4.21e-3;               // 4.21e-21 [kg m^2/s^2]
const PetscReal epsilon = 78.4*8.8541878176e-39; // Water's permitivity, \epsilon_0 = 8.8541878176e-39 C^2 s^2 kg^-1 nm^-3, vacuum permitivity
const PetscReal e       = 1.602e-19;             // Electron charge [Coulomb]
const char     *domainTypes[9] = {"pillar", "mortice", "channel", "channelSmooth", "sphere", "wall", "DomainType", "what?", PETSC_NULL};

const char *muExternalComputationTypes[8] = {"quadrature", "spectralQuadrature", "MuExternalComputationType", "what?", PETSC_NULL};

#undef __FUNCT__
#define __FUNCT__ "ProcessGeometryOptions"
PetscErrorCode ProcessGeometryOptions(MPI_Comm comm, GeometryOptions *options)
{
  PetscInt       n;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  options->domainType       = DOMAIN_WALL;
  options->numSpecies       = 2;
  options->numAlpha         = 10;
  options->L[0]             = 10;
  options->L[1]             = 10;
  options->L[2]             = 10;
  options->minResolution    = 5;
  options->barrierHeight    = 100.0;
  options->muExternalComputationType = MU_EXTERNAL_QUADRATURE;
  options->confinementDecay = 12.0;

  ierr = PetscOptionsBegin(comm, "", "DFT Geometry Options", "DMMG");CHKERRQ(ierr);
    ierr = PetscOptionsEnum("-domainType", "The domain geometry", __FILE__, domainTypes, (PetscEnum) options->domainType, (PetscEnum*) &options->domainType, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-numSpecies", "The number of ion species", __FILE__, options->numSpecies, &options->numSpecies, PETSC_NULL);CHKERRQ(ierr);
    if (options->numSpecies != NUM_SPECIES) {
      SETERRQ1(comm, PETSC_ERR_SUP, "I hate this too, but you have to compile in the number of species, which is currently %d.", NUM_SPECIES);
    }
    ierr = PetscOptionsInt("-minResolution", "The minimum resolution", __FILE__, options->minResolution, &options->minResolution, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-barrierHeight", "Height of a berrier, in kT", __FILE__, options->barrierHeight, &options->barrierHeight, &flag);CHKERRQ(ierr);
    n    = 3;
    ierr = PetscOptionsRealArray("-L", "The domain dimensions (nm)", __FILE__, options->L, &n, &flag);CHKERRQ(ierr);
    if (flag && (n < 3)) {for(PetscInt i = n; i < 3; ++i) options->L[i] = options->L[n-1];}
    options->wallPos = options->L[2]/20.0;
    ierr = PetscOptionsReal("-wallPos", "Position of the wall", __FILE__, options->wallPos, &options->wallPos, &flag);CHKERRQ(ierr);
    options->wallCut = 0.0;
    ierr = PetscOptionsReal("-wallCut", "X height of the wall", __FILE__, options->wallCut, &options->wallCut, &flag);CHKERRQ(ierr);
    options->channelLength = options->L[2]/3.0;
    ierr = PetscOptionsReal("-channelLength", "Length of the selectivity filer", __FILE__, options->channelLength, &options->channelLength, &flag);CHKERRQ(ierr);
    options->channelRadius = options->L[0]/4.0;
    ierr = PetscOptionsReal("-channelRadius", "Radius of the selectivity filer", __FILE__, options->channelRadius, &options->channelRadius, &flag);CHKERRQ(ierr);
    options->vestibuleRadius = options->channelRadius/2.0;
    ierr = PetscOptionsReal("-vestibuleRadius", "Radius of the vestibule", __FILE__, options->vestibuleRadius, &options->vestibuleRadius, &flag);CHKERRQ(ierr);
    options->cellStart = (options->L[2] - options->channelLength)/2.0;
    ierr = PetscOptionsReal("-cellStart", "Start of the confinement cell", __FILE__, options->cellStart, &options->cellStart, &flag);CHKERRQ(ierr);
    options->cellEnd   = (options->L[2] + options->channelLength)/2.0;
    ierr = PetscOptionsReal("-cellEnd", "End of the confinement cell", __FILE__, options->cellEnd, &options->cellEnd, &flag);CHKERRQ(ierr);
    ierr = PetscOptionsEnum("-muExternalComputationType", "The type of computation done to produce the external potential", __FILE__, muExternalComputationTypes, (PetscEnum) options->muExternalComputationType, (PetscEnum*) &options->muExternalComputationType, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-confinementDecay", "Number of cells over which confinement decays", __FILE__, options->confinementDecay, &options->confinementDecay, &flag);CHKERRQ(ierr);

    for(PetscInt i = 0; i < options->numSpecies; ++i) {
      if(i == 0){
        options->R[i]       = 0.1;      // 1 Angstrom ion radius
      } else{
        options->R[i]       = 0.1;      // 1 Angstrom ion radius
      }
    }

    n = options->numSpecies;
    ierr = PetscOptionsStringArray("-ionNames", "Ion names (can be TeX)", __FILE__, options->ionNames, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of ion names given");}
    if (!flag) {
      for(PetscInt i = 0; i < options->numSpecies; ++i) {
        const PetscInt length = ((int) log10(i+1))+1+1+9;

        ierr = PetscMalloc(length, &options->ionNames[i]);CHKERRQ(ierr);
        ierr = PetscSNPrintf(options->ionNames[i], length-1, "Species %d", i);CHKERRQ(ierr);
      }
    }
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-R", "The radius of species i", __FILE__, options->R, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of radii given");}
    for(PetscInt i = 0; i < options->numSpecies; ++i) {
      options->isConfined[i]   = PETSC_FALSE;
      options->numParticles[i] = 0.0;
    }

    n = options->numSpecies;
    ierr = PetscOptionsBoolArray("-is_confined", "The flag for confined ions", __FILE__, options->isConfined, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of confinement flags given");}
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-num_particles", "The flag for confined ions", __FILE__, options->numParticles, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of confined particles given");}
  ierr = PetscOptionsEnd();
  PetscFunctionReturn(0);
}

PetscScalar wall(const PetscScalar x[], const int species) {
  if (globalGeometryOptions->isConfined[species]) {
    if ((PetscRealPart(x[2]) <= globalGeometryOptions->cellStart + globalGeometryOptions->R[species]) ||
        (PetscRealPart(x[2]) >= globalGeometryOptions->cellEnd   - globalGeometryOptions->R[species]))
    {
      return globalGeometryOptions->barrierHeight*kT;
    }
  } else {
    if ((PetscRealPart(x[2]) <= globalGeometryOptions->wallPos+globalGeometryOptions->R[species]) ||
        (PetscRealPart(x[2]) >= (globalGeometryOptions->L[2]-globalGeometryOptions->wallPos)-globalGeometryOptions->R[species]))
    {
      if (globalGeometryOptions->wallCut > 0.0) {
        if ((PetscRealPart(x[0]) <= globalGeometryOptions->wallCut+globalGeometryOptions->R[species]) ||
            (PetscRealPart(x[0]) >= globalGeometryOptions->L[0]-globalGeometryOptions->R[species])) {
          {
            return globalGeometryOptions->barrierHeight*kT;
          }
        }
      } else {
        return globalGeometryOptions->barrierHeight*kT;
      }
    }
  }
  return 0.0;
}

PetscScalar wallFourier(const PetscScalar x[], const int species) {
  // Need to work this out (should be a sinc)
  return 0.0;
}

#undef __FUNCT__
#define __FUNCT__ "PrintGeometryModule"
PetscErrorCode PrintGeometryModule(PetscViewer viewer, const char indent[], GeometryOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer, "%sclass Geometry:\n", indent);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  domainType       = '%s'\n", indent, domainTypes[options->domainType]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  numSpecies       = %d\n", indent, options->numSpecies);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  numAlpha         = %d\n", indent, options->numAlpha);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  L                = [",    indent);CHKERRQ(ierr);
  for(PetscInt d = 0; d < 3; ++d) {
    if (d > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%g", options->L[d]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  h                = [",    indent);CHKERRQ(ierr);
  for(PetscInt d = 0; d < 3; ++d) {
    if (d > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%g", options->h[d]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  wallPos          = %g\n", indent, options->wallPos);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  channelLength    = %g\n", indent, options->channelLength);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  channelRadius    = %g\n", indent, options->channelRadius);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  vestibuleRadius  = %g\n", indent, options->vestibuleRadius);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  dims             = [",    indent);CHKERRQ(ierr);
  for(PetscInt d = 0; d < 3; ++d) {
    if (d > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%d", options->dims[d]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  ionNames         = [",    indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "'%s'", options->ionNames[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  R                = [",    indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%g", options->R[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  isConfined       = [",    indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    if (options->isConfined[sp]) {
      ierr = PetscViewerASCIIPrintf(viewer, "True");CHKERRQ(ierr);
    } else {
      ierr = PetscViewerASCIIPrintf(viewer, "False");CHKERRQ(ierr);
    }
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  numParticles     = [",    indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%g", options->numParticles[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  confinementDecay = %g\n", indent, options->confinementDecay);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  resolution       = %d\n", indent, options->resolution);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  barrierHeight    = %g\n", indent, options->barrierHeight);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscScalar sphere(const PetscScalar x[], const int species) {
  if (PetscSqr(PetscRealPart(x[0]) - globalGeometryOptions->L[0]/2.0) +
      PetscSqr(PetscRealPart(x[1]) - globalGeometryOptions->L[1]/2.0) +
      PetscSqr(PetscRealPart(x[2]) - globalGeometryOptions->L[2]/2.0) <= PetscSqr(globalGeometryOptions->wallPos + globalGeometryOptions->R[species]))
  {
    return globalGeometryOptions->barrierHeight*kT;
  }
  return 0.0;
}

PetscScalar sphereFourier(const PetscScalar x[], const int species) {
  const PetscScalar pos[3] = {PetscRealPart(x[0]) - globalGeometryOptions->L[0]/2.0,
                              PetscRealPart(x[1]) - globalGeometryOptions->L[1]/2.0,
                              PetscRealPart(x[2]) - globalGeometryOptions->L[2]/2.0};
  const PetscReal   R      = globalGeometryOptions->wallPos + globalGeometryOptions->R[species];
  const PetscScalar height = globalGeometryOptions->barrierHeight*kT;
  PetscReal      k[3], alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(pos, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));

  // OmegaHat_2 goes to 4*pi*R^2 as alpha goes to 0
  if (alpha < 1.0e-10) {
    return height*4.0*M_PI*R*R;
  }
  return height*4.0*M_PI*R*sin(R*alpha)/alpha;
}

// tanh(3.0) = 0.995055 tanh(x) = -tanh(-x)
// 0.5*tanh((x - x_0)/a) + 1 where the length of the ramp is 6a, centered at x_0
PetscScalar channelSmooth(const PetscScalar x[], const int species) {
  const PetscReal L  = globalGeometryOptions->channelLength;
  const PetscReal hz = globalGeometryOptions->h[2];
  const PetscReal R  = globalGeometryOptions->channelRadius;
  const PetscReal C  = globalGeometryOptions->vestibuleRadius;
  const PetscReal Ri = globalGeometryOptions->R[species];
  const PetscReal xi = PetscRealPart(x[0]) - globalGeometryOptions->L[0]/2.0;
  const PetscReal yi = PetscRealPart(x[1]) - globalGeometryOptions->L[1]/2.0;
  const PetscReal zi = PetscRealPart(x[2]) - globalGeometryOptions->L[2]/2.0;
  const PetscReal zc = PetscAbsScalar(zi) - L/2.0;
  const PetscReal a  = globalGeometryOptions->confinementDecay*hz/6.0;

  if (globalGeometryOptions->isConfined[species]) {
    // Is it radially outside the pore?
    if (xi*xi + yi*yi > PetscSqr(PetscMax(R - Ri, 0.0))) {
      return globalGeometryOptions->barrierHeight*kT;
    }
    return globalGeometryOptions->barrierHeight*kT*(0.5*(tanh(zc/a - 3.0) + 1.0));
  }
  // Is it in the membrane slab?
  if (zc < C + Ri) {
    // Is it entirely in the slab?
    if (xi*xi + yi*yi > PetscSqr(R + C)) {
      return globalGeometryOptions->barrierHeight*kT;
    } else {
      // Is it in the selectivity filter?
      if (zc < 0.0) {
        // Is it radially in the pore?
        if (xi*xi + yi*yi < PetscSqr(PetscMax(R - Ri, 0.0))) {
          return 0.0;
        } else {
          return globalGeometryOptions->barrierHeight*kT;
        }
      } else {
        // Is it inside the vestibule?
        if (PetscSqr(R+C - sqrt(xi*xi + yi*yi)) + PetscSqr(zc) > PetscSqr(C + Ri)) {
          return 0.0;
        } else {
          return globalGeometryOptions->barrierHeight*kT;
        }
      }
    }
  } else if (globalGeometryOptions->isConfined[species]) {
    return globalGeometryOptions->barrierHeight*kT;
  }
  return 0.0;
}

PetscScalar channel(const PetscScalar x[], const int species) {
  const PetscReal L  = globalGeometryOptions->channelLength;
  const PetscReal R  = globalGeometryOptions->channelRadius;
  const PetscReal C  = globalGeometryOptions->vestibuleRadius;
  const PetscReal Ri = globalGeometryOptions->R[species];
  const PetscReal xi = PetscRealPart(x[0]) - globalGeometryOptions->L[0]/2.0;
  const PetscReal yi = PetscRealPart(x[1]) - globalGeometryOptions->L[1]/2.0;
  const PetscReal zi = PetscRealPart(x[2]) - globalGeometryOptions->L[2]/2.0;
  const PetscReal zc = PetscAbsScalar(zi) - L/2.0;

  // Is it in the membrane slab?
  if (globalGeometryOptions->isConfined[species] && zc + Ri >= 0.0) {
    return globalGeometryOptions->barrierHeight*kT;
  }
  if (zc < C + Ri) {
    // Is it entirely in the slab?
    if (xi*xi + yi*yi > PetscSqr(R + C)) {
      return globalGeometryOptions->barrierHeight*kT;
    } else {
      // Is it in the selectivity filter?
      if (zc < 0.0) {
        // Is it radially in the pore?
        if (xi*xi + yi*yi < PetscSqr(PetscMax(R - Ri, 0.0))) {
          return 0.0;
        } else {
          return globalGeometryOptions->barrierHeight*kT;
        }
      } else {
        // Is it inside the vestibule?
        if (PetscSqr(R+C - sqrt(xi*xi + yi*yi)) + PetscSqr(zc) > PetscSqr(C + Ri)) {
          return 0.0;
        } else {
          return globalGeometryOptions->barrierHeight*kT;
        }
      }
    }
  } else if (globalGeometryOptions->isConfined[species]) {
    return globalGeometryOptions->barrierHeight*kT;
  }
  return 0.0;
}

PetscScalar mortice(const PetscScalar x[], const int species) {
  const PetscReal L  = globalGeometryOptions->channelLength;
  const PetscReal R  = globalGeometryOptions->channelRadius;
  const PetscReal Ri = globalGeometryOptions->R[species];
  const PetscReal xi = PetscRealPart(x[0]) - globalGeometryOptions->L[0]/2.0;
  const PetscReal yi = PetscRealPart(x[1]) - globalGeometryOptions->L[1]/2.0;
  const PetscReal zi = PetscRealPart(x[2]) - globalGeometryOptions->L[2]/2.0;
  const PetscReal zc = PetscAbsScalar(zi) - L/2.0;

  // Is it in the membrane slab?
  if (zc < Ri) {
    // Is it entirely in the slab?
    if (PetscAbsScalar(xi) > PetscMax(R - Ri, 0.0)) {
      return globalGeometryOptions->barrierHeight*kT;
    } else if (PetscAbsScalar(yi) > PetscMax(R - Ri, 0.0)) {
      return globalGeometryOptions->barrierHeight*kT;
    }
  }
  return 0.0;
}

// Takes in a position x and species i
// Returns the external chemical potential at x for species i
//   Inside a barrier, the potential is typically globalGeometryOptions->barrierHeight*kT
//   Outside a barrier, the potential is typically zero
PetscScalar pillar(const PetscScalar x[], const int species) {
 const PetscReal NPx = 3;
 const PetscReal NPy = 3;
 const PetscReal xi = PetscRealPart(x[0]);
 const PetscReal yi = PetscRealPart(x[1]);
 //const PetscReal zi = PetscRealPart(x[2]);
 const PetscReal hx = 1.01*globalGeometryOptions->L[0]/(2*NPx + 1);
 const PetscReal hy = 1.01*globalGeometryOptions->L[1]/(2*NPy + 1);

 if((int(floor (xi/hx)) % 2 != 0) && (int(floor (yi/hy)) % 2 != 0))
 {
   return globalGeometryOptions->barrierHeight*kT;
 }
 return 0.0;
}


extern PetscErrorCode VecViewCenter_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);

#undef __FUNCT__
#define __FUNCT__ "CalculateMuExternal_Quadrature"
PetscErrorCode CalculateMuExternal_Quadrature(DM dm, const PetscInt numSpecies, const DomainType domainType, Vec muExt)
{
  const PetscInt    dim = 3;
  Vec               X;
  EvaluationOptions ctx;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  ctx.numSpecies = numSpecies;
  if (domainType == DOMAIN_WALL) {
    ctx.func1    = wall;
  } else if (domainType == DOMAIN_SPHERE) {
    ctx.func1    = sphere;
  } else if (domainType == DOMAIN_CHANNEL) {
    ctx.func1    = channel;
  } else if (domainType == DOMAIN_CHANNEL_SMOOTH) {
    ctx.func1    = channelSmooth;
  } else if (domainType == DOMAIN_MORTICE) {
    ctx.func1    = mortice;
  } else if (domainType == DOMAIN_PILLAR) {
    ctx.func1    = pillar;
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Invalid domain type: %d", domainType);
  }
  if (dim == 1) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_1d, X, muExt, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_2d, X, muExt, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 3) {
	ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_3d, X, muExt, (void *) &ctx);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  if (debug == 6) {
    PetscViewer viewer;

    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "muExt.vtk", &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(dm, viewer);CHKERRQ(ierr);
    ierr = VecView(muExt, viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = VecViewCenter_Private(dm, muExt, PETSC_VIEWER_DRAW_WORLD, "muExt", -1, -1);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuExternal_SpectralQuadrature"
PetscErrorCode CalculateMuExternal_SpectralQuadrature(DM dm, const PetscInt numSpecies, const DomainType domainType, Vec muExt)
{
  const PetscInt    dim = 3;
  PetscInt          dims[3];
  Mat               F;
  Vec               X, muExtHat;
  EvaluationOptions ctx;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  // Note FFTW wants dimensions in reverse order (since we have Fortran storage)
  ierr = DMDAGetInfo(dm, 0, &dims[2], &dims[1], &dims[0], 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = MatCreateFFT(PETSC_COMM_WORLD, dim, dims, MATFFTW, &F);CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &muExtHat);CHKERRQ(ierr);
  ctx.numSpecies = numSpecies;
  if (domainType == DOMAIN_WALL) {
    ctx.func1 = wallFourier;
  } else if (domainType == DOMAIN_SPHERE) {
    ctx.func1 = sphereFourier;
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Invalid domain type: %d", domainType);
  }
  if (dim == 1) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_1d, X, muExtHat, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_2d, X, muExtHat, (void *) &ctx);CHKERRQ(ierr);
  } else if (dim == 3) {
	ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_3d, X, muExtHat, (void *) &ctx);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  ierr = MatMultTranspose(F, muExtHat, muExt);CHKERRQ(ierr);
  ierr = MatDestroy(&F);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &muExtHat);CHKERRQ(ierr);
  if (debug == 6) {
    PetscViewer viewer;

    ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, "muExt.vtk", &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(dm, viewer);CHKERRQ(ierr);
    ierr = VecView(muExt, viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = VecViewCenter_Private(dm, muExt, PETSC_VIEWER_DRAW_WORLD, "muExt", -1, -1);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuExternal"
PetscErrorCode CalculateMuExternal(DM dm, const PetscInt numSpecies, const DomainType domainType, const MuExternalComputationType muExternalComputationType, Vec muExt)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (muExternalComputationType == MU_EXTERNAL_QUADRATURE) {
    ierr = CalculateMuExternal_Quadrature(dm, numSpecies, domainType, muExt);CHKERRQ(ierr);
  } else if (muExternalComputationType == MU_EXTERNAL_SPECTRAL_QUADRATURE) {
    ierr = CalculateMuExternal_SpectralQuadrature(dm, numSpecies, domainType, muExt);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Unknown external potential computation type: %d", muExternalComputationType);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateMuExternal"
PetscErrorCode CreateMuExternal(DM dm, GeometryOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMCreateGlobalVector(dm, &options->muExt);CHKERRQ(ierr);
  ierr = CalculateMuExternal(dm, options->numSpecies, options->domainType, options->muExternalComputationType, options->muExt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyMuExternal"
PetscErrorCode DestroyMuExternal(DM dm, GeometryOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&options->muExt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateGeometry"
PetscErrorCode CreateGeometry(DM dm, GeometryOptions *options)
{
  const PetscInt dim = 3;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetInfo(dm, 0, &options->dims[0], &options->dims[1], &options->dims[2], 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);

  for(PetscInt d = 0; d < 3; ++d) {
    options->h[d] = options->L[d]/(options->dims[d] - 1);
  }
  options->maxR = options->R[0];
  for(PetscInt sp = 1; sp < options->numSpecies; ++sp) {
    options->maxR = std::max(options->maxR, options->R[sp]);
  }
  if (options->domainType == DOMAIN_WALL) {
    options->bathIndex[0] = 0;
    options->bathIndex[1] = 0;
    options->bathIndex[2] = options->dims[2]/2;
  } else if ((options->domainType == DOMAIN_SPHERE) || (options->domainType == DOMAIN_CHANNEL) || (options->domainType == DOMAIN_CHANNEL_SMOOTH) || (options->domainType == DOMAIN_MORTICE) || (options->domainType == DOMAIN_PILLAR)) {
    options->bathIndex[0] = options->dims[0]/2;
    options->bathIndex[1] = options->dims[1]/2;
    options->bathIndex[2] = options->dims[2]-2;
  } else {
    SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Invalid domain type: %d", options->domainType);
  }
  // Check accuracy
  options->resolution = options->minResolution;
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    const PetscReal R = options->R[sp];

    for(PetscInt d = dim-1; d >= 0; --d) {
      const PetscInt s = (PetscInt) (R/options->h[d]);

      if (s < options->minResolution) SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_ARG_OUTOFRANGE, "Insufficient resolution %d for species %d", s, sp);
      options->resolution = PetscMax(options->resolution, s);
    }
  }
  ierr = CreateMuExternal(dm, options);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyGeometry"
PetscErrorCode DestroyGeometry(DM dm, GeometryOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DestroyMuExternal(dm, options);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->numSpecies; ++sp) {
    ierr = PetscFree(options->ionNames[sp]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateK"
// Calculate the corresponding k wavevector from an x position
PetscErrorCode CalculateK(const PetscScalar x[], const PetscReal h[], const PetscInt dims[], PetscReal k[]) {
  PetscReal indices[3] = {PetscRealPart(x[0])/h[0], PetscRealPart(x[1])/h[1], PetscRealPart(x[2])/h[2]};

  PetscFunctionBegin;
  for(PetscInt d = 0; d < 3; ++d){
    if (indices[d] <= dims[d]/2.0) {
      k[d] =  2.0*M_PI*indices[d]/(dims[d]*h[d]);
    } else {
      k[d] = -2.0*M_PI*(dims[d] - indices[d])/(dims[d]*h[d]);
    }
  }
  PetscFunctionReturn(0);
};

#undef __FUNCT__
#define __FUNCT__ "Function_1d"
PetscErrorCode Function_1d(DMDALocalInfo *info, Rho x[], Rho f[], void *ctx)
{
  EvaluationOptions *options    = (EvaluationOptions *) ctx;
  const int          numSpecies = options->numSpecies;
  PetscScalar      (*func)(const PetscScalar *, const int) = options->func1;
  DM                 coordDA;
  Vec                coordinates;
  PetscScalar       *coords;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
    for(PetscInt sp = 0; sp < numSpecies; ++sp) {
      f[i].v[sp] = func(&coords[i], sp);
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_2d"
PetscErrorCode Function_2d(DMDALocalInfo *info, Rho *x[], Rho *f[], void *ctx)
{
  EvaluationOptions *options    = (EvaluationOptions *) ctx;
  const int          numSpecies = options->numSpecies;
  PetscScalar      (*func)(const PetscScalar *, const int) = options->func1;
  DM                 coordDA;
  Vec                coordinates;
  DMDACoor2d       **coords;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
    for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
      for(PetscInt sp = 0; sp < numSpecies; ++sp) {
        f[j][i].v[sp] = func((PetscScalar *) &coords[j][i], sp);
      }
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_3d"
PetscErrorCode Function_3d(DMDALocalInfo *info, Rho **x[], Rho **f[], void *ctx)
{
  EvaluationOptions *options    = (EvaluationOptions *) ctx;
  const int          numSpecies = options->numSpecies;
  PetscScalar      (*func)(const PetscScalar *, const int) = options->func1;
  DM                 coordDA;
  Vec                coordinates;
  DMDACoor3d      ***coords;
  PetscErrorCode     ierr;
	
  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          f[k][j][i].v[sp] = func((PetscScalar *) &coords[k][j][i], sp);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Function_3d_TwoSpecies"
PetscErrorCode	Function_3d_TwoSpecies(DMDALocalInfo *info, PetscScalar **x[], PetscScalar **f[], void *ctx)
{
  EvaluationOptions *options = (EvaluationOptions *) ctx;
  Rho             ***field   = options->field;
  PetscScalar      (*func)(const PetscScalar *, const PetscScalar *, const int, const int) = options->func2;
  DM                 coordDA;
  Vec                coordinates;
  DMDACoor3d      ***coords;
  PetscErrorCode     ierr;
	
  PetscFunctionBegin;
  ierr = DMDAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        f[k][j][i] = func((PetscScalar *) &coords[k][j][i], (PetscScalar *) &field[k][j][i], options->spI, options->spJ);
      }
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
