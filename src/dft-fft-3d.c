static char help[] = "This program solves for a steady-state density distribution of a hard sphere electrolyte in 3D.\n"
                      "The steady-state condition is defined by the Density Functional Theory (DFT) developed by Gillespie et al.\n\n";
/*
  R_i:             Radius of species i
  \rho^i(x):       Density of species i
  \mu^i_{HS}(x):   Chemical potential of species i due to hard sphere interaction
  \mu^i_{ext}(x):  Chemical potential of species i due to external forces
  \rho^i_{bath}:   Density of species i in the bath
  \mu^i_{HS,bath}: Chemical potential of species i due to hard sphere interaction in the bath

  \rho^i(x) = \rho^i_{bath} \exp{(\mu^i_{HS,bath} - \mu^i_{HS}[{\rho^k(x')}; x] - \mu^i_{ext}(x))/kT}

  \mu^i_{ext}(x) = \infty     inside barrier
                 = 0         outside barrier

  \Phi_{HS}(x) = -n_0 \ln(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over 1 - n_3} + {n^3_2 \over 24\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3

  \mu^i_{HS}(x) = kT \sum_\alpha \int^{x+R_i}_{x-R_i} {\partial\Phi_{HS} \over \partial n_\alpha}(x') W^i_\alpha(x - x') dx'

  n_\alpha(x) = \sum_i \int^{x+R_i}_{x-R_i} \rho_i(x') W^i_\alpha(x' - x) dx'

  {\partial\Phi_{HS}}{\partial n_0} = -\ln(1 - n_3)

  {\partial\Phi_{HS}}{\partial n_1} = {n_2 \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_2} = {n_1 \over 1 - n_3} + {n^2_2 \over 8\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3 + {n^2_{V2} \over 4\pi n_2 (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

  {\partial\Phi_{HS}}{\partial n_3} = n_0/(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over (1 - n_3)^2} + {n^3_2 \over 12\pi (1 - n_3)^3} (1 - {n^2_{V2}\over n^2_2})^3

  {\partial\Phi_{HS}}{\partial n_{V1}} = {-n_{V2} \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_{V2}} = {-n_{V1} \over 1 - n_3} - {n_2 n_{V2} \over 4\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

Sum Rule for Hard Spheres:

  P_{HS} = kT \sum_i \rho^i(wallPos + R_i)

where the pressure is

  P_{HS} = kT {6\over\pi} ({\xi_0\over\Delta} + {3\xi_1\xi_2\over\Delta^2} + {3\xi^3_2\over\Delta^3})

  \Delta = 1 - \xi_3

  \xi_n = {\pi\over6} \sum_i \rho^i (2 R_i)^n

Conversion to Molar:

  1 Molar = 1 mol/L
  N_A     = 6.022 10^23 mol^-1



  rho nm^-3 = rho 10^27 m^-3
            = rho 10^24 L^-1
            = rho 6.022^-1 10^1 mol/L
            = 1.6606 rho M
*/
#include <dftHardSphere.h>
#include <dftElectrostatics.h>
#include <dftVerification.h>

#include <iostream>
#include <iomanip>

#include <set>

struct vertex : public std::pair<std::pair<int,int>,int>
{
  vertex(int i, int j, int k) {
    this->first.first  = i;
    this->first.second = j;
    this->second       = k;
  };
  int i() const {return this->first.first;};
  int j() const {return this->first.second;};
  int k() const {return this->second;};
};

typedef enum {CONT_NONE, CONT_RHO_BATH, CONT_MU_EXT} ContinuationType;

typedef struct {
  MPI_Comm              comm;
  DMMG                 *dmmg;
  GeometryOptions       geomOpts;
  PetscBool             useHS;    // Flag for hard sphere interaction
  HardSphereOptions     hsOpts;
  PetscBool             useES;    // Flag for electrostatic interaction
  PetscBool             doGammaSolve;
  ElectrostaticsOptions esOpts;
  VerificationOptions   verifyOpts;

  ContinuationType      contType; // Gives the variable to continue in
  int                   pauseIterate;
  int                   outputPeriod;
  PetscBool             doOutput;
  PetscReal             infTolerance;

  DM            dm;         // DA for the densities
  DM            nDA;        // DA for the n functionals
  DM            singleDA;   // DA for single fields
  DM            kDA;        // DA for wavenumbers k
  std::set<vertex> subdomain; // Identifies a subdomain on which the residual is large

  PetscLogStage gammaResidualStage;
  PetscLogEvent confinedRhoEvent, rhoResidualEvent, gammaResidualEvent;
} Options;

Options    *globalOptions;
const char *contTypes[7] = {"none", "rhoBath", "muExt", "ContinuationType", "what?", PETSC_NULL};

PetscErrorCode PrintModule(DMMG *dmmg, Options *options);

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
PetscErrorCode ProcessOptions(MPI_Comm comm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  debug                 = 0;
  options->comm         = comm;
  options->useHS        = PETSC_TRUE;
  options->useES        = PETSC_FALSE;
  options->doGammaSolve = PETSC_TRUE;
  options->contType     = CONT_NONE;
  options->pauseIterate = -1;
  options->outputPeriod = -1;
  options->doOutput     = PETSC_FALSE;
  options->infTolerance = 1.0e-3;

  ierr = PetscOptionsBegin(comm, "", "DFT Problem Options", "DMMG");CHKERRQ(ierr);
    ierr = PetscOptionsInt("-debug", "The debugging level", __FILE__, debug, &debug, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-useHS", "Flag for hard sphere interaction", __FILE__, options->useHS, &options->useHS, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-useES", "Flag for electrostatic interaction", __FILE__, options->useES, &options->useES, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsEnum("-contType", "The continuation type", __FILE__, contTypes, (PetscEnum) options->contType, (PetscEnum*) &options->contType, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-pauseIterate", "The iterate to start pausing the views", __FILE__, options->pauseIterate, &options->pauseIterate, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-outputPeriod", "The number of iterates to wait before solution output", __FILE__, options->outputPeriod, &options->outputPeriod, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-doOutput", "Flag for field output", __FILE__, options->doOutput, &options->doOutput, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-infTol", "The absolute tolerance for the infinity residual", __FILE__, options->infTolerance, &options->infTolerance, PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  ierr = ProcessGeometryOptions(comm, &options->geomOpts);CHKERRQ(ierr);
  ierr = ProcessHardSphereOptions(comm, &options->geomOpts, &options->hsOpts);CHKERRQ(ierr);
  ierr = ProcessElectrostaticsOptions(comm, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
  ierr = ProcessVerificationOptions(comm, &options->verifyOpts);CHKERRQ(ierr);

  ierr = PetscLogStageRegister("Gamma Residual", &options->gammaResidualStage);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Confined rho_boltz",  PETSC_SMALLEST_CLASSID, &options->confinedRhoEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc rho residual",   PETSC_SMALLEST_CLASSID, &options->rhoResidualEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc gamma residual", PETSC_SMALLEST_CLASSID, &options->gammaResidualEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateMesh"
PetscErrorCode CreateMesh(MPI_Comm comm, DM *dm, Options *options)
{
  DM             dms[4];
  const PetscInt dim    = 3;
  PetscInt       dof[4] = {options->geomOpts.numSpecies+1, options->geomOpts.numAlpha, 1, dim+1};
  PetscErrorCode ierr;

  PetscFunctionBegin;
  for(PetscInt i = 0; i < 4; ++i) {
    if (dim == 1) {
      ierr = DMDACreate1d(comm, DMDA_BOUNDARY_NONE, -3, dof[i], 1, PETSC_NULL, &dms[i]);CHKERRQ(ierr);
    } else if (dim == 2) {
      ierr = DMDACreate2d(comm, DMDA_BOUNDARY_NONE, DMDA_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof[i], 1, PETSC_NULL, PETSC_NULL, &dms[i]);CHKERRQ(ierr);
    } else if (dim == 3) {
      if (i == 2) {
        ierr = DMDACreate3d(comm, DMDA_BOUNDARY_PERIODIC, DMDA_BOUNDARY_PERIODIC, DMDA_BOUNDARY_PERIODIC, DMDA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof[i], 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &dms[i]);CHKERRQ(ierr);
      } else {
        ierr = DMDACreate3d(comm, DMDA_BOUNDARY_NONE, DMDA_BOUNDARY_NONE, DMDA_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof[i], 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &dms[i]);CHKERRQ(ierr);
      }
    } else {
      SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
    }
    ierr = DMDASetUniformCoordinates(dms[i], 0.0, options->geomOpts.L[0], 0.0, options->geomOpts.L[1], 0.0, options->geomOpts.L[2]);CHKERRQ(ierr);
  }
  *dm               = dms[0];
  options->dm       = dms[0];
  options->nDA      = dms[1];
  options->singleDA = dms[2];
  options->kDA      = dms[3];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyMesh"
PetscErrorCode DestroyMesh(DM dm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DMDestroy(&options->nDA);CHKERRQ(ierr);
  ierr = DMDestroy(&options->singleDA);CHKERRQ(ierr);
  ierr = DMDestroy(&options->kDA);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateConfinedRhoBoltzmann"
// Calculate the bath potential for confined species
PetscErrorCode CalculateConfinedRhoBoltzmann(DM dm, Vec muHSVec, Vec muESVec, Vec phiVec, Vec rhoVec, PetscReal rhoBoltzmann[], Options *options)
{
  DM                singleDA     = options->singleDA;
  PetscInt          numSpecies   = options->geomOpts.numSpecies;
  const PetscReal  *R            = options->geomOpts.R;
  const PetscBool   useHS        = options->useHS;
  const PetscBool   useES        = options->useES;
  const PetscReal  *muHSBath     = options->hsOpts.muHSBath;
  const PetscReal  *muESBath     = options->esOpts.muESBath;
  Vec               muExtVec     = options->geomOpts.muExt;
  const PetscReal  *z            = options->esOpts.z;
  const PetscReal  *h            = options->geomOpts.h;
  const PetscBool  *isConfined   = options->geomOpts.isConfined;
  const PetscReal  *numParticles = options->geomOpts.numParticles;
  const PetscReal  *L            = options->geomOpts.L;
  const PetscReal   channelL     = options->geomOpts.channelLength;
  const PetscReal   channelR     = options->geomOpts.channelRadius;
  DMDALocalInfo     info;
  DM                coordDA;
  Vec               coordinates;
  DMDACoor3d     ***coords;
  Rho            ***muHS;
  Rho            ***muES;
  Rho            ***muExt;
  Rho            ***rho;
  Rho            ***rhoRef;
  PetscScalar    ***phiArray;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->confinedRhoEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, muExtVec, &muExt);CHKERRQ(ierr);
  if (useHS) {
    ierr = DMDAVecGetArray(dm, muHSVec, &muHS);CHKERRQ(ierr);
  }
  if (useES) {
    ierr = DMDAVecGetArray(dm, muESVec, &muES);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(singleDA, phiVec, &phiArray);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
  }
  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (!isConfined[sp]) continue;
    if (debug >= 4) {
      if (useES) {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculating confined rhoBoltzmann using muHSBath_%d: %g muESBath_%d: %g\n", sp, muHSBath[sp], sp, muESBath[sp]); CHKERRQ(ierr);
      } else {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculating confined rhoBoltzmann using muHSBath_%d: %g\n", sp, muHSBath[sp]); CHKERRQ(ierr);
      }
    }
    PetscScalar integral = 0.0;
    const PetscReal Ri = options->geomOpts.R[sp];
    const PetscReal R2 = PetscSqr(PetscMax(channelR - Ri, 0.0));

    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      const PetscReal zi = PetscRealPart(coords[k][0][0].z) - L[2]/2.0;

      if (PetscAbsScalar(zi) - channelL/2.0 + Ri > 1.0e-10) {
        continue;
      }
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {

#if 0
          for(PetscInt dk = 0; dk < 1; ++dk) {
            const PetscInt kk = k+dk < 0 ? info.zs+info.zm-1 : (k+dk > info.zs+info.zm-1 ? 0 : k+dk);
            for(PetscInt dj = 0; dj < 1; ++dj) {
              const PetscInt jj = j+dj < 0 ? info.ys+info.ym-1 : (j+dj > info.ys+info.ym-1 ? 0 : j+dj);
              for(PetscInt di = 0; di < 1; ++di) {
                const PetscInt ii = i+di < 0 ? info.xs+info.xm-1 : (i+di > info.xs+info.xm-1 ? 0 : i+di);
#else
                PetscInt kk = k, jj = j, ii = i;
#endif
                if (useES) {
                  PetscReal rhoReference[NUM_SPECIES];
                  PetscReal muESReference[NUM_SPECIES];

                  for(PetscInt sp2 = 0; sp2 < numSpecies; ++sp2) {rhoReference[sp2] = PetscRealPart(rhoRef[k][j][i].v[sp2]);}
                  ierr = CalculateMuESBath(numSpecies, R, isConfined, rhoReference, z, PetscRealPart(rho[k][j][i].Gamma), muESReference);CHKERRQ(ierr);

                  if (PetscSqr(PetscRealPart(coords[kk][jj][ii].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[kk][jj][ii].y) - L[1]/2.0) < R2) {
                    integral += PetscExpScalar((muHSBath[sp]
                                                + muESBath[sp]
                                                - muHS[kk][jj][ii].v[sp]
                                                - muExt[kk][jj][ii].v[sp]
                                                - z[sp]*phiArray[kk][jj][ii]
                                                - muESReference[sp]
                                                - muES[kk][jj][ii].v[sp])/kT);
                  }
                } else {
                  if (PetscSqr(PetscRealPart(coords[kk][jj][ii].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[kk][jj][ii].y) - L[1]/2.0) < R2) {
                    integral += PetscExpScalar((muHSBath[sp]
                                                - muHS[kk][jj][ii].v[sp]
                                                - muExt[kk][jj][ii].v[sp])/kT);
                  }
                }
#if 0
              }
            }
          }
#endif
        }
      }
    }
    if (PetscIsInfOrNanScalar(integral)) {SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid integral: %g", PetscRealPart(integral));}
    PetscReal rhoBoltzmannVal = numParticles[sp]/(h[0]*h[1]*h[2]*PetscRealPart(integral));
    if (debug >= 1) {
      ierr = PetscPrintf(PETSC_COMM_WORLD, "confined rhoBoltzmann_%d: %g\n", sp, rhoBoltzmannVal); CHKERRQ(ierr);
    }
    rhoBoltzmann[sp] = rhoBoltzmannVal;
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  if (useES) {
    ierr = DMDAVecRestoreArray(dm, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, muESVec, &muES);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(singleDA, phiVec, &phiArray);CHKERRQ(ierr);
  }
  if (useHS) {
    ierr = DMDAVecRestoreArray(dm, muHSVec, &muHS);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(dm, muExtVec, &muExt);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(options->confinedRhoEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "enforceConfinement"
PetscErrorCode enforceConfinement(DM dm, Vec rhoVec, Options *options)
{
  PetscInt          numSpecies   = options->geomOpts.numSpecies;
  const PetscReal  *R            = options->geomOpts.R;
  const PetscBool  *isConfined   = options->geomOpts.isConfined;
  const PetscReal  *L            = options->geomOpts.L;
  const PetscReal   channelL     = options->geomOpts.channelLength;
  const PetscReal   channelR     = options->geomOpts.channelRadius;
  Vec               muExtVec     = options->geomOpts.muExt;
  DMDALocalInfo     info;
  DM                coordDA;
  Vec               coordinates;
  DMDACoor3d     ***coords;
  Rho            ***rho;
  Rho            ***muExt;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, muExtVec, &muExt);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (!isConfined[sp]) continue;
    const PetscReal Ri = R[sp];
    const PetscReal R2 = PetscSqr(PetscMax(channelR - Ri, 0.0));

    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      const PetscReal zi = PetscRealPart(coords[k][0][0].z) - L[2]/2.0;
      if (PetscAbsScalar(zi) - channelL/2.0 + Ri > 1.0e-10) {
        /* Outside */
        for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
          for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
            if (PetscRealPart(muExt[k][j][i].v[sp]) < 1.0e-10) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be an external potential outside the channel");
            if (PetscAbsScalar(PetscRealPart(rho[k][j][i].v[sp])) > 1.0e-10) SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be no confined species concentration outside the channel, (%d, %d, %d) %g", i, j , k, PetscRealPart(rho[k][j][i].v[sp]));
          }
        }
      } else {
        for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
          for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
            if (PetscSqr(PetscRealPart(coords[k][j][i].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k][j][i].y) - L[1]/2.0) < R2) {
              /* Inside */
              if (PetscRealPart(muExt[k][j][i].v[sp]) > 1.0e-10) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be no external potential inside the channel");
              if (PetscAbsScalar(PetscRealPart(rho[k][j][i].v[sp])) < 1.0e-10) SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be confined species concentration inside the channel, (%d, %d, %d) %g", i, j , k, PetscRealPart(rho[k][j][i].v[sp]));
            } else {
              /* Outside */
              if (PetscRealPart(muExt[k][j][i].v[sp]) < 1.0e-10) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be an external potential outside the channel");
              if (PetscAbsScalar(PetscRealPart(rho[k][j][i].v[sp])) > 1.0e-10) SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_LIB, "There should be no confined species concentration outside the channel, (%d, %d, %d) %g", i, j , k, PetscRealPart(rho[k][j][i].v[sp]));
            }
          }
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, muExtVec, &muExt);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "enforceNonnegativity"
PetscErrorCode enforceNonnegativity(DMDALocalInfo *info, int numSpecies, Rho **in[], Rho **out[])
{
  PetscFunctionBegin;
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
	for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          if (PetscRealPart(in[k][j][i].v[sp]) < 0.0) {
            //if (PetscRealPart(in[k][j][i].v[sp]) < -1.0e-8) {SETERRQ(PETSC_COMM_SELF, PETSC_ERR_ARG_WRONG, "Negative values of rho are illegal");}
            in[k][j][i].v[sp] = 0.0;
          }
        }
        out[k][j][i] = in[k][j][i];
      }
	}
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Rhs_3d"
PetscErrorCode Rhs_3d(DMDALocalInfo *info, Rho **x[], Rho **f[], void *ctx)
{
  Options          *options      = (Options *) ctx;
  DMDALocalInfo     singleInfo;
  const PetscBool   useES        = options->useES;
  const PetscReal  *R            = options->geomOpts.R;
  const PetscBool  *isConfined   = options->geomOpts.isConfined;
  const PetscInt   *bathIndex    = options->geomOpts.bathIndex;
  PetscReal        *rhoBoltzmann = options->hsOpts.rhoBoltzmann;
  const PetscReal  *muHSBath     = options->hsOpts.muHSBath;
  const PetscReal  *muESBath     = options->esOpts.muESBath;
  const PetscReal  *z            = options->esOpts.z;
  Vec              *PhiDer; // {\partial\Phi_{HS}}{\partial n_\alpha} functions 
  Vec               nVec, muHSVec,muESVec, xVec, phiVec = PETSC_NULL;
  Rho            ***xArray;
  N              ***nArray;
  Rho            ***muHS, ***muExt,***muES, ***rhoRef, ***frozenPB;
  PetscScalar    ***phiArray;
  PetscInt          numSpecies   = options->geomOpts.numSpecies;
  PetscInt          numAlpha     = options->geomOpts.numAlpha;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  // Get work vectors and arrays
  ierr = DMDAGetLocalInfo(options->singleDA, &singleInfo);CHKERRQ(ierr);
  ierr = PetscMalloc(numAlpha * sizeof(Vec), &PhiDer);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = DMGetGlobalVector(options->singleDA, &PhiDer[alpha]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(info->da, &xVec);CHKERRQ(ierr);
  if (useES) {
    ierr = DMGetGlobalVector(info->da, &muESVec);CHKERRQ(ierr);
    phiVec = options->esOpts.phi;
  }
  ierr = DMDAVecGetArray(info->da, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
  // Make xVec (is this necessary?)
  //   Also removes slightly negative concentrations
  ierr = DMDAVecGetArray(info->da, xVec, &xArray);CHKERRQ(ierr);
  ierr = enforceNonnegativity(info, numSpecies, x, xArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(info->da, xVec, &xArray);CHKERRQ(ierr);
  ierr = enforceConfinement(info->da, xVec, options);CHKERRQ(ierr);
  {
    PetscReal occupancy[NUM_SPECIES];

    ierr = CalculateFilterOccupancy(info->da, &options->geomOpts, xVec, occupancy);CHKERRQ(ierr);
    for(int sp = 0; sp < numSpecies; ++sp) {
      ierr = PetscPrintf(PETSC_COMM_WORLD, "==> occupancy[%d]: %g\n", sp, occupancy[sp]);CHKERRQ(ierr);
    }
  }
  // 1) Calculate n_\alpha
  ierr = CalculateN_3d(info->da, xVec, numSpecies, options->geomOpts.numAlpha, options->singleDA, &options->hsOpts, nVec);CHKERRQ(ierr);
  // 2) Calculate \frac{\partial\Phi}{\partial n}
  ierr = DMDAVecGetArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  ierr = CalculatePhiDer_3d(&singleInfo, nArray, options->geomOpts.numAlpha, options->hsOpts.PhiDerFunc, &options->hsOpts, PhiDer);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  // 3) Calculate \mu_{HS}
  ierr = CalculateMuHS(info->da, numSpecies, options->geomOpts.numAlpha, options->singleDA, PhiDer, &options->hsOpts, muHSVec);CHKERRQ(ierr);
  //   Should enable this automatically at each computation step
  ierr = VecViewCenter(info->da, muHSVec, PETSC_VIEWER_DRAW_WORLD, "muHS", -1, -1);CHKERRQ(ierr);
  ierr = VecViewVTK(DMMGGetSNES(options->dmmg), info->da, muHSVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  if (useES) {
    // 4) Calculate the Fourier c \psi functions
    ierr = CalculateCPsiHatFunctions(options->singleDA, numSpecies, cPsiHat, options->esOpts.cPsiHatVecs, x, &options->esOpts);CHKERRQ(ierr);
    if (options->esOpts.useMuES) {
      // 5) Calculate \rho^{ref}
      ierr = CalculateRhoReference(options->dmmg, info->da, options->singleDA, options->kDA, xVec, options->hsOpts.rhoBathVec, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
      // 6) Calculate \mu_{ES}
      ierr = CalculateMuES(info->da, numSpecies, xVec, options->singleDA, &options->hsOpts, &options->esOpts, options->esOpts.cPsiHatVecs, muESVec);CHKERRQ(ierr);
    }
    ierr = DMDAVecGetArray(info->da, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(info->da, muESVec, &muES);CHKERRQ(ierr);
  }

  if (useES) {
    if (options->esOpts.usePoisson) {
      ierr = PetscLogEventBegin(options->esOpts.pbFrozenEvent,0,0,0,0);CHKERRQ(ierr);
      // 10) Calculate frozen factor for PB
      ierr = DMDAVecGetArray(info->da, options->esOpts.frozenPBVariables, &frozenPB);CHKERRQ(ierr);
      for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
        for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
          for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
            PetscReal rhoReference[NUM_SPECIES];
            PetscReal muESReference[NUM_SPECIES];

            // 9) Calculate \mu^{ref}_{ES}
            for(PetscInt sp = 0; sp < numSpecies; ++sp) {rhoReference[sp] = PetscRealPart(rhoRef[k][j][i].v[sp]);}
            ierr = CalculateMuESBath(numSpecies, R, isConfined, rhoReference, z, PetscRealPart(x[k][j][i].Gamma), muESReference);CHKERRQ(ierr);
            for(PetscInt sp = 0; sp < numSpecies; ++sp) {
              frozenPB[k][j][i].v[sp] = PetscExpScalar((muHSBath[sp]
                                                        + muESBath[sp]
                                                        - PetscRealPart(muHS[k][j][i].v[sp])
                                                        - PetscRealPart(muExt[k][j][i].v[sp])
                                                        - muESReference[sp]
                                                        - PetscRealPart(muES[k][j][i].v[sp]))/kT);
            }
          }
        }
      }
      ierr = DMDAVecRestoreArray(info->da, options->esOpts.frozenPBVariables, &frozenPB);CHKERRQ(ierr);
      ierr = PetscLogEventEnd(options->esOpts.pbFrozenEvent,0,0,0,0);CHKERRQ(ierr);
      // 7) Calculate \phi
      ierr = CalculatePhi(info, options->singleDA, &options->geomOpts, &options->hsOpts, &options->esOpts, xVec, phiVec);CHKERRQ(ierr);
      if (!options->esOpts.usePoissonBoltzmann) {
        // This check should go in the calculation function
        ierr = CheckPotential(info->da, xVec, options->singleDA, phiVec, options->esOpts.z, &options->geomOpts);CHKERRQ(ierr);
      }
      ierr = VecViewCenterSingle(options->singleDA, phiVec, PETSC_VIEWER_DRAW_WORLD, "phi", -1, -1);CHKERRQ(ierr);
    }
    ierr = DMDAVecGetArray(options->singleDA, phiVec, &phiArray);CHKERRQ(ierr);
  }
  // 8) Calculate \rho^{Boltzmann} for confined species (need this to adjust to latest \phi)
  ierr = CalculateConfinedRhoBoltzmann(info->da, muHSVec, muESVec, phiVec, xVec, rhoBoltzmann, options);CHKERRQ(ierr);
  // 11) Calculate full residual
  ierr = PetscLogEventBegin(options->rhoResidualEvent,0,0,0,0);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
	for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        PetscReal rhoReference[NUM_SPECIES];
        PetscReal muESReference[NUM_SPECIES];

        // Boundary condition for \rho: \rho(bath) = \rho^{bath}
        //   We should determine how many points lie in a diameter, but at our small sizes we will start with 1
        //#define RHO_BC
#ifdef RHO_BC
        if (abs(bathIndex[2] - k) < 1) {
          for(PetscInt sp = 0; sp < numSpecies; ++sp) {
            if (isConfined[sp]) continue;
            f[k][j][i].v[sp] = x[k][j][i].v[sp] - rhoBoltzmann[sp];
          }
        } else {
#endif
        if (useES) {
          // 9) Calculate \mu^{ref}_{ES}
          for(PetscInt sp = 0; sp < numSpecies; ++sp) {rhoReference[sp] = PetscRealPart(rhoRef[k][j][i].v[sp]);}
          ierr = CalculateMuESBath(numSpecies, R, isConfined, rhoReference, z, PetscRealPart(x[k][j][i].Gamma), muESReference);CHKERRQ(ierr);
        }
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          if ((debug == 5) || ((debug == 4) && (PetscRealPart((muHSBath[sp] - muHS[k][j][i].v[sp] - muExt[k][j][i].v[sp])/kT) > 50.0))) {
            if (useES) {
              ierr = PetscPrintf(options->comm, "[%d][%d][%d]Species %d \\mu^{bath}_{HS}: %g  \\mu^{bath}_{ES}: %g  \\mu^{ref}_{ES}: %g\\mu_{HS}: %g \\mu_{ES}: %g \\mu_{ext}: %g \\ESComp: %g arg: %g Rho: %g\n",
                                 k, j, i, sp, muHSBath[sp], muESBath[sp], muESReference[sp], PetscRealPart(muHS[k][j][i].v[sp]), PetscRealPart(muES[k][j][i].v[sp]), PetscRealPart(muExt[k][j][i].v[sp]),
                                 PetscRealPart(z[sp]*phiArray[k][j][i]),
                                 PetscRealPart((muHSBath[sp] + muESBath[sp] - muHS[k][j][i].v[sp] - muESReference[sp] - muES[k][j][i].v[sp] - muExt[k][j][i].v[sp] - z[sp]*phiArray[k][j][i])/kT),
                                 PetscRealPart(x[k][j][i].v[sp]));CHKERRQ(ierr);
            } else {
              ierr = PetscPrintf(options->comm, "[%d][%d][%d]Species %d \\mu^{bath}_{HS}: %g \\mu_{HS}: %g \\mu_{ext}: %g arg: %g Rho: %g\n",
                                 k, j, i, sp, muHSBath[sp], PetscRealPart(muHS[k][j][i].v[sp]), PetscRealPart(muExt[k][j][i].v[sp]),
                                 PetscRealPart((muHSBath[sp] + muESBath[sp] - muHS[k][j][i].v[sp] - muExt[k][j][i].v[sp])/kT),
                                 PetscRealPart(x[k][j][i].v[sp]));CHKERRQ(ierr);
            }
          }
          if (useES) {
            f[k][j][i].v[sp] = x[k][j][i].v[sp]
              - rhoBoltzmann[sp]*PetscExpScalar((muHSBath[sp]
                                            + muESBath[sp]
                                            - PetscRealPart(muHS[k][j][i].v[sp])
                                            - PetscRealPart(muExt[k][j][i].v[sp])
                                            - z[sp]*PetscRealPart(phiArray[k][j][i])
                                            - muESReference[sp]
                                            - PetscRealPart(muES[k][j][i].v[sp]))/kT);
          } else {
            f[k][j][i].v[sp] = x[k][j][i].v[sp]
              - rhoBoltzmann[sp]*PetscExpScalar((muHSBath[sp]
                                            - PetscRealPart(muHS[k][j][i].v[sp])
                                            - PetscRealPart(muExt[k][j][i].v[sp]))/kT);
          }
          ///if (PetscImaginaryPart(f[k][j][i].v[sp]) > 1.0e-5) SETERRQ3(PETSC_COMM_SELF, PETSC_ERR_LIB, "This is fucked up (%d, %d, %d)", i, j, k);
          if (PetscIsInfOrNanScalar(f[k][j][i].v[sp])) {
            if (useES) {
              ierr = PetscPrintf(options->comm, "[%d][%d][%d]Species %d \\mu^{bath}_{HS}: %g  \\mu^{bath}_{ES}: %g  \\mu^{ref}_{ES}: %g\\mu_{HS}: %g \\mu_{ES}: %g \\mu_{ext}: %g \\ESComp: %g arg: %g Rho: %g\n",
                                 k, j, i, sp, muHSBath[sp], muESBath[sp], muESReference[sp], PetscRealPart(muHS[k][j][i].v[sp]), PetscRealPart(muES[k][j][i].v[sp]), PetscRealPart(muExt[k][j][i].v[sp]),
                                 PetscRealPart(z[sp]*phiArray[k][j][i]),
                                 PetscRealPart((muHSBath[sp] + muESBath[sp] - muHS[k][j][i].v[sp] - muESReference[sp] - muES[k][j][i].v[sp] - muExt[k][j][i].v[sp] - z[sp]*phiArray[k][j][i])/kT),
                                 PetscRealPart(x[k][j][i].v[sp]));CHKERRQ(ierr);
            } else {
              ierr = PetscPrintf(options->comm, "[%d][%d][%d]Species %d \\mu^{bath}_{HS}: %g \\mu_{HS}: %g \\mu_{ext}: %g arg: %g Rho: %g\n",
                                 k, j, i, sp, muHSBath[sp], PetscRealPart(muHS[k][j][i].v[sp]), PetscRealPart(muExt[k][j][i].v[sp]),
                                 PetscRealPart((muHSBath[sp] + muESBath[sp] - muHS[k][j][i].v[sp] - muExt[k][j][i].v[sp])/kT),
                                 PetscRealPart(x[k][j][i].v[sp]));CHKERRQ(ierr);
            }
            SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");
          };
        }
#ifdef RHO_BC
        }
#endif
      }
    }
  }
  ierr = PetscLogEventEnd(options->rhoResidualEvent,0,0,0,0);CHKERRQ(ierr);
  // 12) Calculate \Gamma
  if (options->doGammaSolve) {
    MSAContext     msa;
    PetscReal      rhoRefG[NUM_SPECIES];

    ierr = PetscLogStagePush(options->gammaResidualStage);CHKERRQ(ierr);
    ierr = PetscLogEventBegin(options->gammaResidualEvent,0,0,0,0);CHKERRQ(ierr);
    ierr = MSAScreeningSetup(rhoRefG, &options->geomOpts, &options->esOpts, &msa);CHKERRQ(ierr);
    for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
      for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
        for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
          if ((i == info->xm/2) && (j == info->ym/2) && (k == info->zm/2)) {
            KSP ksp;

            ierr = SNESMonitorSet(msa.snes, SNESMonitorDefault, PETSC_NULL, PETSC_NULL);CHKERRQ(ierr);
            ierr = SNESGetKSP(msa.snes, &ksp);CHKERRQ(ierr);
            ierr = KSPMonitorSet(ksp, KSPMonitorDefault, PETSC_NULL, PETSC_NULL);CHKERRQ(ierr);
          }
          // Gamma equation: \Gamma - MSA(\Rho_{ref}(\rho, \Gamma), \Gamma)
          if (useES) {
            PetscReal Gamma;

            // Apply MSA operator (really a solve) to the reference density
            for(int sp = 0; sp < NUM_SPECIES; ++sp) rhoRefG[sp] = PetscRealPart(rhoRef[k][j][i].v[sp]);
            ierr = CalculateGamma(&msa, x[k][j][i].Gamma, &Gamma);CHKERRQ(ierr);
            if (Gamma <= 0.0) {
              //SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_LIB, "Gamma cannot be negative, Gamma[%d][%d][%d]: %g", i, j, k, Gamma);
              f[k][j][i].Gamma = 0.0;
            } else {
              f[k][j][i].Gamma = PetscRealPart(x[k][j][i].Gamma) - Gamma;
            }
          } else {
            f[k][j][i].Gamma = 0.0;
          }
          if ((i == info->xm/2) && (j == info->ym/2) && (k == info->zm/2)) {
            KSP ksp;

            ierr = SNESMonitorCancel(msa.snes);CHKERRQ(ierr);
            ierr = SNESGetKSP(msa.snes, &ksp);CHKERRQ(ierr);
            ierr = KSPMonitorCancel(ksp);CHKERRQ(ierr);
          }
        }
      }
    }
    ierr = MSAScreeningCleanup(&msa);CHKERRQ(ierr);
    ierr = PetscLogEventEnd(options->gammaResidualEvent,0,0,0,0);CHKERRQ(ierr);
    ierr = PetscLogStagePop();CHKERRQ(ierr);
  } else {
    PetscReal rhoRefG[NUM_SPECIES];

    for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
      for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
        for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
          const PetscReal Gamma = PetscRealPart(x[k][j][i].Gamma);
          PetscReal       sum   = 0.0;
          PetscReal       eta;

          for(int sp = 0; sp < numSpecies; ++sp) rhoRefG[sp] = PetscRealPart(rhoRef[k][j][i].v[sp]);
          ierr = CalculateEta(numSpecies, R, isConfined, rhoRefG, z, Gamma, &eta);CHKERRQ(ierr);
          for(PetscInt sp = 0; sp < numSpecies; ++sp){
            const PetscReal rho_i = PetscRealPart(rhoRef[k][j][i].v[sp]);
            const PetscReal sigma = 2.0*R[sp];

            sum += rho_i*PetscSqr((z[sp] - eta*PetscSqr(sigma))/(1.0 + Gamma*sigma));
          }
          f[k][j][i].Gamma = 4.0*PetscSqr(Gamma) - (e*e/(kT*epsilon))*sum;
        }
      }
    }
  }
  // Cleanup
  if (useES) {
    ierr = DMDAVecRestoreArray(info->da, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(options->singleDA, phiVec, &phiArray);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(info->da, muESVec, &muES);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(info->da, &muESVec);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(info->da, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &xVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = DMRestoreGlobalVector(options->singleDA, &PhiDer[alpha]);CHKERRQ(ierr);
  }
  ierr = PetscFree(PhiDer);CHKERRQ(ierr);
#if 0
  // Diagnostic viewing (should be turned off if debug flag is off)
  //   Also, should just have a version that takes the array
  {
    Vec    fVec;
    Rho ***fArray;

    ierr = DMGetGlobalVector(info->da, &fVec);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(info->da, fVec, &fArray);CHKERRQ(ierr);
    for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
      for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
        for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
          fArray[k][j][i] = f[k][j][i];
        }
      }
    }
    ierr = DMDAVecRestoreArray(info->da, fVec, &fArray);CHKERRQ(ierr);
    ierr = enforceConfinement(options->dm, fVec, options);CHKERRQ(ierr);
    //ierr = VecViewCenter(info->da, fVec, PETSC_VIEWER_DRAW_WORLD, "F", -1, -1);CHKERRQ(ierr);
    ///ierr = CheckHomogeneity(options->dmmg, fVec, &options->geomOpts, &options->verifyOpts);
    ierr = DMRestoreGlobalVector(info->da, &fVec);CHKERRQ(ierr);
  }
#endif
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CreateSolver"
PetscErrorCode CreateSolver(DM dm, DMMG **dmmg, Options *options)
{
  MPI_Comm       comm;
  const PetscInt dim = 3;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMMGCreate(comm, 1, options, dmmg);CHKERRQ(ierr);
  ierr = DMMGSetDM(*dmmg, dm);CHKERRQ(ierr);
  if (dim == 1) {
    ierr = DMMGSetSNESLocal(*dmmg, Rhs_3d, 0, 0, 0);CHKERRQ(ierr);
  } else if(dim == 3) {
    ierr = DMMGSetSNESLocal(*dmmg, Rhs_3d, 0, 0, 0);CHKERRQ(ierr);
  } else {
    SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  for(int l = 1; l < DMMGGetLevels(*dmmg); ++l) {
    ierr = DMDASetUniformCoordinates((*dmmg)[l]->dm, 0.0, options->geomOpts.L[0], 0.0, options->geomOpts.L[1], 0.0, options->geomOpts.L[2]);CHKERRQ(ierr);
  }
  /* if (options->bcType == NEUMANN) {
     // With Neumann conditions, we tell DMMG that constants are in the null space of the operator
       ierr = DMMGSetNullSpace(*dmmg, PETSC_TRUE, 0, PETSC_NULL);CHKERRQ(ierr);
  } */
  ierr = DMMGSetFromOptions(*dmmg);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) DMMGGetx(*dmmg), "rho");CHKERRQ(ierr);
  options->dmmg = *dmmg;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetInitialGuess"
PetscErrorCode SetInitialGuess(DMMG *dmmg, PetscBool setToBath, Options *options)
{
  DM             dm = DMMGGetFine(dmmg)->dm;
  DMDALocalInfo  info;
  MSAContext     msa;
  Rho         ***rho;
  Rho         ***rhoRef;
  Rho         ***muExt;
  PetscReal      rhoBath[NUM_SPECIES];
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < options->geomOpts.numSpecies; ++sp) {
          if (PetscRealPart(muExt[k][j][i].v[sp]) > 1.0e-10) {
            rho[k][j][i].v[sp] = options->hsOpts.rhoBoltzmann[sp]*PetscExpScalar(-muExt[k][j][i].v[sp]/kT);
          } else if (setToBath) {
            rho[k][j][i].v[sp] = options->hsOpts.rhoBoltzmann[sp];
          }
        }
        rho[k][j][i].Gamma = options->esOpts.GammaBath;
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  if (options->useES) {
    ierr = CalculateRhoReference(options->dmmg, dm, options->singleDA, options->kDA, DMMGGetx(dmmg), options->hsOpts.rhoBathVec, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
    ierr = MSAScreeningSetup(rhoBath, &options->geomOpts, &options->esOpts, &msa);CHKERRQ(ierr);
    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
          PetscReal Gamma;

          // Solve MSA at each point
          for(int sp = 0; sp < NUM_SPECIES; ++sp) rhoBath[sp] = PetscRealPart(rhoRef[k][j][i].v[sp]);
          ierr = CalculateGamma(&msa, 1.0, &Gamma);CHKERRQ(ierr);
          rho[k][j][i].Gamma = Gamma;
        }
      }
    }
    ierr = MSAScreeningCleanup(&msa);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, options->hsOpts.rhoBathVec, &rhoRef);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  }
  ierr = VecViewCenter(dm, DMMGGetx(dmmg), PETSC_VIEWER_DRAW_WORLD, "InitialGuess", -1, -1);CHKERRQ(ierr);
  ierr = PrintNorm(DMMGGetx(dmmg), "InitialGuess", -1, -1);CHKERRQ(ierr);
  ierr = PrintNormSplit(DMMGGetx(dmmg), options->singleDA, options->geomOpts.numSpecies, "InitialGuess", -1, -1);CHKERRQ(ierr);
  if (debug == 6) {
    PetscViewer viewer;

    ierr = PetscViewerASCIIOpen(options->comm, "initialGuess.vtk", &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(dmmg[0]->dm, viewer);CHKERRQ(ierr);
    ierr = VecView(DMMGGetx(dmmg), viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  //ierr = CheckInitialGuess(dmmg, options->singleDA, options->hsOpts.F, DMMGGetx(dmmg), &options->geomOpts);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateSubdomain"
PetscErrorCode CalculateSubdomain(SNES snes, DM dm, std::set<vertex>& subdomain, Options *options)
{
  DMDALocalInfo  info;
  Vec            residual;
  Rho         ***r;
  PetscReal      rmax;
  const PetscInt numSpecies = options->geomOpts.numSpecies;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  subdomain.clear();
  ierr = SNESGetFunction(snes, &residual, PETSC_NULL, PETSC_NULL);CHKERRQ(ierr);
  ierr = VecNorm(residual, NORM_INFINITY, &rmax);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, residual, &r);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
	for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          if (PetscRealPart(r[k][j][i].v[sp]) > .20*rmax) {
            for(PetscInt c = PetscMax(k-1, info.zs); c <= PetscMin(k+1, info.zs+info.zm); ++c) {
              for(PetscInt b = PetscMax(j-1, info.ys); b <= PetscMin(j+1, info.ys+info.ym); ++b) {
                for(PetscInt a = PetscMax(i-1, info.xs); a <= PetscMin(i+1, info.xs+info.xm); ++a) {
                  subdomain.insert(vertex(a, b, c));
                }
              }
            }
          }
        }
      }
	}
  }
  ierr = DMDAVecRestoreArray(dm, residual, &r);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyConvergenceTest"
PetscErrorCode MyConvergenceTest(SNES snes, PetscInt it, PetscReal xnorm, PetscReal pnorm, PetscReal fnorm, SNESConvergedReason *reason, void *ctx)
{
  Options       *options = (Options *) ctx;
  Vec            F;
  PetscReal      infNorm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = SNESGetFunction(snes, &F, PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  ierr = VecNorm(F, NORM_INFINITY, &infNorm);CHKERRQ(ierr);  
  if (infNorm < options->infTolerance) {
    *reason = SNES_CONVERGED_FNORM_ABS;
  } else {
    ierr = SNESDefaultConverged(snes, it, xnorm, pnorm, fnorm, reason, ctx);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MyMonitor"
PetscErrorCode MyMonitor(SNES snes, PetscInt it, PetscReal fnorm, void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscInt       iter;
  PetscErrorCode ierr;

  PetscFunctionBegin;
#if 0
  // Lagging does not seem to work
  if (it > 3 && (it % 2)) {
    options->doGammaSolve = PETSC_FALSE;
  } else {
    options->doGammaSolve = PETSC_TRUE;
  }
#endif
  if (it == options->pauseIterate) {
    PetscDraw draw;

    ierr = PetscViewerDrawGetDraw(PETSC_VIEWER_DRAW_WORLD, 0, &draw);CHKERRQ(ierr);
    ierr = PetscDrawSetPause(draw, -1);CHKERRQ(ierr);
  }
  ierr = VecViewCenter(options->dmmg[0]->dm, DMMGGetx(options->dmmg), PETSC_VIEWER_DRAW_WORLD, "Rho", -1, -1);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(snes, &iter);
  if (options->outputPeriod >= 0) {
    PetscViewer viewer;

    ierr = PetscViewerCreate(options->comm, &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetType(viewer, PETSCVIEWERASCII);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);CHKERRQ(ierr);
    ierr = PetscViewerFileSetName(viewer, "convergence.txt");CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "%3D SNES Function norm %14.12e\n", iter, fnorm);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  if ((debug == 6) || ((options->outputPeriod >= 0) && (iter > 0) && !(iter % options->outputPeriod))) {
    PetscViewer        viewer;
    std::ostringstream hsFilename;

    hsFilename << "rho_" << iter << ".vtk";
    ierr = PetscViewerASCIIOpen(options->comm, hsFilename.str().c_str(), &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(options->dmmg[0]->dm, viewer);CHKERRQ(ierr);
    ierr = VecView(DMMGGetx(options->dmmg), viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    ierr = PrintModule(options->dmmg, options);CHKERRQ(ierr);
  }
  if (debug == 6) {
    PetscViewer        viewer;
    Vec                r;
    std::ostringstream resFilename;

    resFilename << "res_" << iter << ".vtk";
    ierr = PetscViewerASCIIOpen(options->comm, resFilename.str().c_str(), &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(options->dmmg[0]->dm, viewer);CHKERRQ(ierr);
    ierr = SNESGetFunction(snes, &r, PETSC_NULL, PETSC_NULL);CHKERRQ(ierr);
    ierr = VecView(r, viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  {
    PetscInt numSpecies = options->geomOpts.numSpecies;
    Vec     *rSplit;
    Vec      r;

    ierr = SNESGetFunction(snes, &r, PETSC_NULL, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscMalloc((numSpecies+1) * sizeof(Vec), &rSplit);CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
      ierr = DMGetGlobalVector(options->singleDA, &rSplit[sp]);CHKERRQ(ierr);
    }
    ierr = VecStrideGatherAll(r, rSplit, INSERT_VALUES);CHKERRQ(ierr);
    ierr = PetscPrintf(options->comm, "DFT norms:");CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < numSpecies; ++sp) {
      PetscReal norm2, normInf;

      ierr = VecNorm(rSplit[sp], NORM_2, &norm2);CHKERRQ(ierr);
      ierr = VecNorm(rSplit[sp], NORM_INFINITY, &normInf);CHKERRQ(ierr);
      ierr = PetscPrintf(options->comm, " %s %g %g", options->geomOpts.ionNames[sp], norm2, normInf);CHKERRQ(ierr);
    }
    {
      PetscReal norm2, normInf;

      ierr = VecNorm(rSplit[numSpecies], NORM_2, &norm2);CHKERRQ(ierr);
      ierr = VecNorm(rSplit[numSpecies], NORM_INFINITY, &normInf);CHKERRQ(ierr);
      ierr = PetscPrintf(options->comm, " %s %g %g", "Gamma", norm2, normInf);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(options->comm, "\n");CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
      ierr = DMRestoreGlobalVector(options->singleDA, &rSplit[sp]);CHKERRQ(ierr);
    }
    ierr = PetscFree(rSplit);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DFTLineSearchQuadratic"
PetscErrorCode DFTLineSearchQuadratic(SNES snes, void *lsctx, Vec X, Vec F, Vec dX, PetscReal fnorm, PetscReal dummyXnorm, Vec dummyG, Vec W, PetscReal *dummyYnorm, PetscReal *gnorm, PetscBool *flag)
{
  Options       *options = (Options *) lsctx;
  Vec            n3, Y = W;
  PetscReal      minAlpha = 0.05;
  PetscReal      alphas[3];
  PetscReal      norms[3], infNorm;
  PetscReal      n3_x, n3_y, alpha;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  alphas[0] = 0.0;
  norms[0]  = fnorm;
  ierr = DMGetGlobalVector(options->singleDA, &n3);CHKERRQ(ierr);
  ierr = VecViewCenter(options->dm, X, PETSC_VIEWER_DRAW_WORLD, "X", -1, -1);CHKERRQ(ierr);
  // OPT: We could store the value from last time
  ierr = CalculateN3_3d(options->dm, X, options->geomOpts.numSpecies, options->singleDA, &options->hsOpts, n3);CHKERRQ(ierr);
  ierr = VecNorm(n3, NORM_INFINITY, &n3_x);CHKERRQ(ierr);
  ierr = VecWAXPY(Y, 1.0, dX, X);CHKERRQ(ierr);
  ierr = VecViewCenter(options->dm, Y, PETSC_VIEWER_DRAW_WORLD, "Y", -1, -1);CHKERRQ(ierr);
  ierr = CalculateN3_3d(options->dm, Y, options->geomOpts.numSpecies, options->singleDA, &options->hsOpts, n3);CHKERRQ(ierr);
  ierr = VecNorm(n3, NORM_INFINITY, &n3_y);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(options->singleDA, &n3);CHKERRQ(ierr);
  if (n3_y > 0.9) {
    alphas[2]  = (0.9 - n3_x)/(n3_y - n3_x);
    ierr = PetscPrintf(options->comm, "DFT limited alpha < %g\n", alphas[2]);CHKERRQ(ierr);
  } else {
    alphas[2] = 1.0;
  }
  alphas[1] = 0.5*alphas[2];
  /* Calculate trial solutions */
  for(PetscInt  i = 1; i < 3; ++i) {
    /* Calculate X^{n+1} = (1 - \alpha) X^n + \alpha Y = (1 - \alpha) X^n + \alpha (X^n + dX) = X^n + \alpha dX */
    ierr = VecWAXPY(W, alphas[i], dX, X);CHKERRQ(ierr);
    ierr = VecViewCenter(options->dm, W, PETSC_VIEWER_DRAW_WORLD, "W", i, -1);CHKERRQ(ierr);
    //ierr = PetscExceptionTry1(SNESComputeFunction(snes, W, F), PETSC_ERR_FP);
    ierr = SNESComputeFunction(snes, W, F);
    if (ierr == PETSC_ERR_FP) {
      norms[i] = 1.0e4*norms[0];
      ierr = PetscPrintf(options->comm, "DFT calculated at alpha %g, norm was invalid, using %g\n", alphas[i], norms[i]);CHKERRQ(ierr);
    } else {
      CHKERRQ(ierr);
      ierr = VecNorm(F, NORM_2, &norms[i]);CHKERRQ(ierr);
      ierr = PetscPrintf(options->comm, "DFT calculated at alpha %g, norm %g\n", alphas[i], norms[i]);CHKERRQ(ierr);
    }
    // TODO: Should also reset muESBath here
  }
  for(PetscInt  i = 0; i < 3; ++i) {
    norms[i] = PetscSqr(norms[i]);
  }
  /* Fit a quadratic:
       If we have x_{0,1,2} = 0, x_1, x_2 which generate norms y_{0,1,2}
       a = (x_1 y_2 - x_2 y_1 + (x_2 - x_1) y_0)/(x^2_2 x_1 - x_2 x^2_1)
       b = (x^2_1 y_2 - x^2_2 y_1 + (x^2_2 - x^2_1) y_0)/(x_2 x^2_1 - x^2_2 x_1)
       c = y_0
       x_min = -b/2a

       If we let x_{0,1,2} = 0, 0.5, 1.0
       a = 2 y_2 - 4 y_1 + 2 y_0
       b =  -y_2 + 4 y_1 - 3 y_0
       c =   y_0
  */
  const PetscReal a = (alphas[1]*norms[2] - alphas[2]*norms[1] + (alphas[2] - alphas[1])*norms[0])/
    (PetscSqr(alphas[2])*alphas[1] - alphas[2]*PetscSqr(alphas[1]));
  const PetscReal b = (PetscSqr(alphas[1])*norms[2] - PetscSqr(alphas[2])*norms[1] + (PetscSqr(alphas[2]) - PetscSqr(alphas[1]))*norms[0])/
    (alphas[2]*PetscSqr(alphas[1]) - PetscSqr(alphas[2])*alphas[1]);
  /* Check for positive a (concave up) */
  if (a >= 0.0) {
    alpha = -b/(2.0*a);
    alpha = PetscMin(alpha, alphas[2]);
    alpha = PetscMax(alpha, minAlpha);
  } else {
    if (norms[2] < 2.0*norms[0]) {
      alpha = alphas[2];
    } else {
      alpha = minAlpha;
    }
  }
  ierr = PetscPrintf(options->comm, "DFT norms[0] = %g, norms[1] = %g, norms[2] = %g\n", sqrt(norms[0]), sqrt(norms[1]), sqrt(norms[2]));CHKERRQ(ierr);
  ierr = PetscPrintf(options->comm, "DFT choose alpha = %g\n", alpha);CHKERRQ(ierr);
  ierr = VecAXPY(X, alpha, dX);CHKERRQ(ierr);
  ierr = SNESComputeFunction(snes, X, F);CHKERRQ(ierr);
  ierr = VecNorm(F, NORM_2, gnorm);CHKERRQ(ierr);
  ierr = VecNorm(F, NORM_INFINITY, &infNorm);CHKERRQ(ierr);
  ierr = PetscPrintf(options->comm, "DFT gnorm = %g inf norm %g\n", *gnorm, infNorm);CHKERRQ(ierr);
  *flag = PETSC_TRUE;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Solve"
PetscErrorCode Solve(DMMG *dmmg, Options *options)
{
  SNES                snes;
  MPI_Comm            comm;
  PetscInt            its;
  SNESConvergedReason reason;
  PetscErrorCode      ierr;

  PetscFunctionBegin;
  snes = DMMGGetSNES(dmmg);
  ierr = SNESMonitorSet(snes, MyMonitor, options, PETSC_NULL);CHKERRQ(ierr);
  ierr = SNESSetConvergenceTest(snes, MyConvergenceTest, options, PETSC_NULL);CHKERRQ(ierr);
  ierr = SNESLineSearchSet(snes, DFTLineSearchQuadratic, options);CHKERRQ(ierr);
  ierr = DMMGSolve(dmmg);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(snes, &its);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes, &reason);CHKERRQ(ierr);
  ierr = PetscObjectGetComm((PetscObject) snes, &comm);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Number of nonlinear iterations = %D\n", its);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Reason for solver termination: %s\n", SNESConvergedReasons[reason]);CHKERRQ(ierr);
  ierr = VecViewCenterSolnFile(dmmg[0]->dm, DMMGGetx(dmmg), "rho", "Rho^*", -1, -1);CHKERRQ(ierr);
  ierr = VecViewXYSumSolnFile(dmmg[0]->dm, DMMGGetx(dmmg), "rhoSum", "RhoSum^*", -1, -1, options->geomOpts.h);CHKERRQ(ierr);
  if (options->useES && (debug == 2)) {
    Vec           phi;
    DMDALocalInfo info;
    PetscViewer   viewer;

    ierr = VecViewCenterSolnFile(dmmg[0]->dm, options->hsOpts.rhoBathVec, "rhoRef", "RhoRef^*", -1, -1);CHKERRQ(ierr);
    ierr = PetscViewerASCIIOpen(options->comm, "phi.out", &viewer);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(options->singleDA, &phi);CHKERRQ(ierr);
    ierr = DMDAGetLocalInfo(dmmg[0]->dm, &info);CHKERRQ(ierr);
    ierr = CalculatePhi(&info, options->singleDA, &options->geomOpts, &options->hsOpts, &options->esOpts, DMMGGetx(dmmg), phi);CHKERRQ(ierr);
    ierr = VecViewCenterSingle_Private(options->singleDA, phi, viewer, "phi", -1, -1);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(options->singleDA, &phi);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  if (debug == 6) {
    PetscViewer viewer;

    ierr = PetscViewerASCIIOpen(options->comm, "rho.vtk", &viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
    ierr = DMView(dmmg[0]->dm, viewer);CHKERRQ(ierr);
    ierr = VecView(DMMGGetx(dmmg), viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "UpdateContinuation"
PetscErrorCode UpdateContinuation(DMMG *dmmg, PetscInt& continuationStep, Options *options)
{
  static PetscReal rhoBathInc[NUM_SPECIES];
  static PetscReal muExtInc;
  static PetscInt  numSteps = 0;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  if (debug > 0) {
    switch(options->contType) {
    case CONT_NONE:
      break;
    case CONT_RHO_BATH:
      ierr = PetscPrintf(options->comm, "RhoBath Continuation Step %d\n", continuationStep);CHKERRQ(ierr);
      break;
    case CONT_MU_EXT:
      ierr = PetscPrintf(options->comm, "MuExt Continuation Step %d\n", continuationStep);CHKERRQ(ierr);
      break;
    default:
      SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Unknown continuation type: %d", options->contType);
    }
  }
  if (continuationStep == 0) {
    // Initialize
    switch(options->contType) {
    case CONT_NONE:
      break;
    case CONT_RHO_BATH:
      {
        PetscReal  rhoBathInit[NUM_SPECIES];
        PetscInt   n  = options->geomOpts.numSpecies;
        PetscInt   n2 = options->geomOpts.numSpecies;
        PetscBool flag, flag2;

        ierr = PetscOptionsGetRealArray(PETSC_NULL, "-rhoBathInit", rhoBathInit, &n, &flag);CHKERRQ(ierr);
        ierr = PetscOptionsGetRealArray(PETSC_NULL, "-rhoBathInitM", rhoBathInit, &n2, &flag2);CHKERRQ(ierr);
        ierr = PetscOptionsGetInt(PETSC_NULL, "-rhoBathNum", &numSteps, PETSC_NULL);CHKERRQ(ierr);
        if (!flag && !flag2)                             {SETERRQ(options->comm, PETSC_ERR_ARG_WRONG, "Initial bath potentials not given for continuation loop");}
        if (flag && flag2)                               {SETERRQ(options->comm, PETSC_ERR_ARG_WRONG, "Cannot specify both initial potentials and molar potentials");}
        if (flag  && n  != options->geomOpts.numSpecies) {SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Invalid number of initial bath potentials given: %d", n);}
        if (flag2 && n2 != options->geomOpts.numSpecies) {SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Invalid number of initial bath potentials given: %d", n);}
        if (numSteps < 2)                                {SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Invalid number of continuation steps given: %d", numSteps);}
        for(PetscInt sp = 0; sp < options->geomOpts.numSpecies; ++sp) {
          if (flag2) {rhoBathInit[sp] /= NUMBER_TO_MOLAR;}
          rhoBathInc[sp]              = (options->hsOpts.rhoBath[sp] - rhoBathInit[sp])/(numSteps - 1);
          options->hsOpts.rhoBath[sp] = rhoBathInit[sp];
          ierr = PetscPrintf(options->comm, "  Initial rhoBath[%d] = %g\n", sp, options->hsOpts.rhoBath[sp]);CHKERRQ(ierr);
        }
      }
      break;
    case CONT_MU_EXT:
      {
        PetscReal muExtInit;
        PetscBool flag, flag2;

        ierr = PetscOptionsGetReal(PETSC_NULL, "-muExtInit", &muExtInit, &flag);CHKERRQ(ierr);
        ierr = PetscOptionsGetInt(PETSC_NULL, "-muExtNum", &numSteps, &flag2);CHKERRQ(ierr);
        if (!flag)        {SETERRQ(options->comm, PETSC_ERR_ARG_WRONG, "Initial external potential not given for continuation loop");}
        if (numSteps < 2) {SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Invalid number of continuation steps given: %d", numSteps);}
        muExtInc                        = (options->geomOpts.barrierHeight - muExtInit)/(numSteps - 1);
        options->geomOpts.barrierHeight = muExtInit;
        ierr = PetscPrintf(options->comm, "  Initial muExt = %g\n", options->geomOpts.barrierHeight);CHKERRQ(ierr);
      }
      break;
    default:
      SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Unknown continuation type: %d", options->contType);
    }
  } else if (continuationStep >= numSteps) {
    PetscFunctionReturn(1);
  } else {
    // Update parameters
    switch(options->contType) {
    case CONT_NONE:
      break;
    case CONT_RHO_BATH:
      for(PetscInt sp = 0; sp < options->geomOpts.numSpecies; ++sp) {
        options->hsOpts.rhoBath[sp] += rhoBathInc[sp];
        ierr = PetscPrintf(options->comm, "  New rhoBath[%d] = %g\n", sp, options->hsOpts.rhoBath[sp]);CHKERRQ(ierr);
      }
      break;
    case CONT_MU_EXT:
      options->geomOpts.barrierHeight += muExtInc;
      ierr = PetscPrintf(options->comm, "  New muExt = %g\n", options->geomOpts.barrierHeight);CHKERRQ(ierr);
      break;
    default:
      SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Unknown continuation type: %d", options->contType);
    }
  }
  // Update dependent quantities
  switch(options->contType) {
  case CONT_NONE:
    break;
  case CONT_RHO_BATH:
    // Here we should rescale the initial guess to the correct bath value
    ierr = CalculateRhoBath(dmmg[0]->dm, options->geomOpts.numSpecies, options->hsOpts.rhoBathVec);CHKERRQ(ierr);
    if (options->useHS) {
      ierr = CalculateMuHSBath(options->geomOpts.numSpecies, options->geomOpts.R, options->geomOpts.isConfined, options->hsOpts.rhoBath, options->hsOpts.muHSBath);CHKERRQ(ierr);
    }
    if (options->useES) {
#ifdef TODO
      ierr = CalculateGamma(&options->esOpts, 1.0, &options->esOpts.Gamma);CHKERRQ(ierr);
      ierr = CalculateMuESBath(options->geomOpts.numSpecies, options->geomOpts.R, options->geomOpts.isConfined, options->hsOpts.rhoBath, options->esOpts.z, options->esOpts.Gamma, options->esOpts.muESBath);CHKERRQ(ierr);
#endif
    }
    break;
  case CONT_MU_EXT:
    ierr = CalculateMuExternal(dmmg[0]->dm, options->geomOpts.numSpecies, options->geomOpts.domainType, options->geomOpts.muExternalComputationType, options->geomOpts.muExt);CHKERRQ(ierr);
    break;
  default:
    SETERRQ1(options->comm, PETSC_ERR_ARG_WRONG, "Unknown continuation type: %d", options->contType);
  }
  if (continuationStep == 0) {
    ierr = SetInitialGuess(dmmg, PETSC_TRUE, options);CHKERRQ(ierr);
  } else {
    ierr = SetInitialGuess(dmmg, PETSC_FALSE, options);CHKERRQ(ierr);
  }
  ++continuationStep;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ContinuationSolve"
PetscErrorCode ContinuationSolve(DMMG *dmmg, Options *options)
{
  PetscInt       continuationStep = 0;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  while(!(ierr = UpdateContinuation(dmmg, continuationStep, options))) {
    ierr = Solve(dmmg, options);CHKERRQ(ierr);
  }
  if (ierr != 1) CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintReport"
PetscErrorCode PrintReport(DMMG *dmmg, Options *options)
{
  MPI_Comm               comm     = options->comm;
  GeometryOptions       *geomOpts = &options->geomOpts;
  HardSphereOptions     *hsOpts   = &options->hsOpts;
  ElectrostaticsOptions *esOpts   = &options->esOpts;
  PetscReal              occupancy[NUM_SPECIES];
  PetscReal              P_HS, P_ES;
  PetscInt               i;
  PetscErrorCode         ierr;

  PetscFunctionBegin;
  ierr = CalculatePressureHS(geomOpts->numSpecies, geomOpts->R, geomOpts->isConfined, hsOpts->rhoBath, &P_HS);CHKERRQ(ierr);
  ierr = CalculatePressureES(geomOpts->numSpecies, geomOpts->R, geomOpts->isConfined, hsOpts->rhoBath, esOpts->z, esOpts->GammaBath, &P_ES);CHKERRQ(ierr);
  if (geomOpts->domainType == DOMAIN_CHANNEL) {
    ierr = CalculateFilterOccupancy(options->dm, geomOpts, DMMGGetx(dmmg), occupancy);CHKERRQ(ierr);
  }

  ierr = PetscPrintf(comm, "Simulation Input\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter      Description                        Units          Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "----------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "kT:            Boltzmann's constant * Temperature (kg nm^2/s^2): %g\n", kT);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Lx:            System Length in x                 (nm):          %g\n", geomOpts->L[0]);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Ly:            System Length in y                 (nm):          %g\n", geomOpts->L[1]);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Lz:            System Length in z                 (nm):          %g\n", geomOpts->L[2]);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "hx:            Plaque Length in x                 (nm):          %g\n", geomOpts->h[0]);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "hy:            Plaque Length in y                 (nm):          %g\n", geomOpts->h[1]);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "hz:            Plaque Length in z                 (nm):          %g\n", geomOpts->h[2]);CHKERRQ(ierr);
  if (geomOpts->domainType == DOMAIN_WALL) {
    ierr = PetscPrintf(comm, "x_{wall}:      Wall position                      (nm):          %g\n", geomOpts->wallPos);CHKERRQ(ierr);
  } else if (geomOpts->domainType == DOMAIN_SPHERE) {
    ierr = PetscPrintf(comm, "R_{sphere}:    Sphere radius                      (nm):          %g\n", geomOpts->wallPos);CHKERRQ(ierr);
  } else if (geomOpts->domainType == DOMAIN_CHANNEL || geomOpts->domainType == DOMAIN_CHANNEL_SMOOTH) {
    ierr = PetscPrintf(comm, "L_{channel}:   Length of selectivity filter       (nm):          %g\n", geomOpts->channelLength);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "R_{channel}:   Radius of selectivity filter       (nm):          %g\n", geomOpts->channelRadius);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "C_{vest}:      Radius of vestibule                (nm):          %g\n", geomOpts->vestibuleRadius);CHKERRQ(ierr);
  }
  for(i = 0; i < geomOpts->numSpecies; ++i) {
    if (geomOpts->isConfined[i]) {
      ierr = PetscPrintf(comm, "%s (Confined):\n", geomOpts->ionNames[i]);CHKERRQ(ierr);
    } else {
      ierr = PetscPrintf(comm, "%s:\n", geomOpts->ionNames[i]);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(comm, "  R:           Ion Radius                         (nm):          %g\n", geomOpts->R[i]);CHKERRQ(ierr);
    if (options->useES) {
      ierr = PetscPrintf(comm, "  z:           Ion Valence                        (unitless):    %g\n", esOpts->z[i]);CHKERRQ(ierr);
    }
    ierr = PetscPrintf(comm, "  rho_{bath}:  Density of ions in bath            (nm^{-3}):     %g\n", hsOpts->rhoBath[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "                                                  (M):           %g\n", hsOpts->rhoBath[i]*NUMBER_TO_MOLAR);CHKERRQ(ierr);
  }
  if (esOpts->usePoisson) {
    ierr = PetscPrintf(comm, "Calculating mean electrostatics\n");CHKERRQ(ierr);
  }
  if (esOpts->useMuES) {
    ierr = PetscPrintf(comm, "Calculating RFD local electrostatics\n");CHKERRQ(ierr);
  }
  ierr = PetscPrintf(comm, "\nDerived Quantities\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter       Description                                                   Units Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "-----------------------------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Resolution:     min(R*N/L)                                                    None:          %d\n", geomOpts->resolution);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "P:              Pressure due to hard sphere/ES interaction in the bath        (kg/nm s^2):   %g\n\n", P_HS+P_ES);CHKERRQ(ierr);
  for(i = 0; i < geomOpts->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "%s:\n", geomOpts->ionNames[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  eta:          Volume fraction of ions                                       None:          %g\n", geomOpts->eta[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  mu_{HS,bath}: Chemical potential due to HS interaction in the bath          (kg nm^2/s^2): %g\n", hsOpts->muHSBath[i]);CHKERRQ(ierr);
    if (options->useES) {
      ierr = PetscPrintf(comm, "  mu_{ES,bath}: Chemical potential due to ES interaction in the bath          (kg nm^2/s^2): %g\n", esOpts->muESBath[i]);CHKERRQ(ierr);
    }
    if (geomOpts->domainType == DOMAIN_CHANNEL) {
      ierr = PetscPrintf(comm, "  occupancy:    Occupancy in the channel filter                               None:          %g\n", occupancy[i]);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintModule"
PetscErrorCode PrintModule(DMMG *dmmg, Options *options)
{
  PetscViewer        viewer;
  std::ostringstream filename;
  PetscReal          P_HS, P_ES;
  PetscInt           moduleNum;
  PetscMPIInt        rank;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr = CalculatePressureHS(options->geomOpts.numSpecies, options->geomOpts.R, options->geomOpts.isConfined, options->hsOpts.rhoBath, &P_HS);CHKERRQ(ierr);
  ierr = CalculatePressureES(options->geomOpts.numSpecies, options->geomOpts.R, options->geomOpts.isConfined, options->hsOpts.rhoBath, options->esOpts.z, options->esOpts.GammaBath, &P_ES);CHKERRQ(ierr);
  if (!rank) {
    for(moduleNum = 0; moduleNum < 10000; ++moduleNum) {
      std::ostringstream trial;
      PetscBool          exists;

      trial << "dft_" << std::setw(5) << std::setfill('0') << moduleNum << ".py";
      ierr = PetscTestFile(trial.str().c_str(), 'r', &exists);CHKERRQ(ierr);
      if (!exists) break;
    }
  }
  ierr = MPI_Bcast(&moduleNum, 1, MPIU_INT, 0, PETSC_COMM_WORLD);CHKERRQ(ierr);
  filename << "dft_" << std::setw(5) << std::setfill('0') << moduleNum << ".py";
  ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "class DFT:\n");CHKERRQ(ierr);
  ierr = PrintGeometryModule(viewer, "  ", &options->geomOpts);CHKERRQ(ierr);
  ierr = PrintHardSphereModule(viewer, "  ", &options->geomOpts, &options->hsOpts);CHKERRQ(ierr);
  ierr = PrintElectrostaticsModule(viewer, "  ", &options->geomOpts, &options->esOpts);CHKERRQ(ierr);
  if (options->useHS) {
    ierr = PetscViewerASCIIPrintf(viewer, "  useHS     = True\n");CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer, "  useHS     = False\n");CHKERRQ(ierr);
  }
  if (options->useES) {
    ierr = PetscViewerASCIIPrintf(viewer, "  useES     = True\n");CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer, "  useES     = False\n");CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "  contType  = '%s'\n", contTypes[options->contType]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "  pressure  = %g\n", P_HS+P_ES);CHKERRQ(ierr);
  if (options->geomOpts.domainType == DOMAIN_CHANNEL) {
    PetscReal occupancy[NUM_SPECIES];

    ierr = CalculateFilterOccupancy(options->dm, &options->geomOpts, DMMGGetx(dmmg), occupancy);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "  occupancy = {");CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < options->geomOpts.numSpecies; ++sp) {
      if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
      ierr = PetscViewerASCIIPrintf(viewer, "'%s': %g", options->geomOpts.ionNames[sp], occupancy[sp]);CHKERRQ(ierr);
    }
    ierr = PetscViewerASCIIPrintf(viewer, "}\n");CHKERRQ(ierr);
  }
  DMDALocalInfo info;
  Vec           sum;

  ierr = DMDAGetLocalInfo(options->dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &sum);CHKERRQ(ierr);
  ierr = VecSetSizes(sum, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(sum);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "  rhoSum = {\n");CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < options->geomOpts.numSpecies+1; ++sp) {
    const char  *name = "Gamma";
    PetscScalar *a;

    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ",\n");CHKERRQ(ierr);}
    if (sp < options->geomOpts.numSpecies) name = options->geomOpts.ionNames[sp];
    ierr = PetscViewerASCIIPrintf(viewer, "            '%s': [", name);CHKERRQ(ierr);
    ierr = CalculateXYSum(options->dm, sp, DMMGGetx(dmmg), sum, options->geomOpts.h);CHKERRQ(ierr);
    ierr = VecGetArray(sum, &a);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      if (k > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
      ierr = PetscViewerASCIIPrintf(viewer, "%g", PetscRealPart(a[k]));CHKERRQ(ierr);
    }
    ierr = VecRestoreArray(sum, &a);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "]");CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "\n           }\n");CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintQuantities"
PetscErrorCode PrintQuantities(Options *options)
{
  MPI_Comm               comm     = options->comm;
  GeometryOptions       *geomOpts = &options->geomOpts;
  HardSphereOptions     *hsOpts   = &options->hsOpts;
  ElectrostaticsOptions *esOpts   = &options->esOpts;
  PetscReal              volume[NUM_SPECIES];
  PetscErrorCode         ierr;

  PetscFunctionBegin;
  if ((geomOpts->domainType == DOMAIN_MORTICE) || (geomOpts->domainType == DOMAIN_CHANNEL) || (geomOpts->domainType == DOMAIN_CHANNEL_SMOOTH)) {
    ierr = CalculateFilterVolume(options->dm, geomOpts, volume);CHKERRQ(ierr);
    for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
      ierr = PetscPrintf(comm, "Filter volume for species %s: %g\n", geomOpts->ionNames[sp], volume[sp]);CHKERRQ(ierr);
    }
  }
  for(PetscInt i = 0; i < geomOpts->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "muHS_bath_%d = %e\n", i, hsOpts->muHSBath[i]);CHKERRQ(ierr);
  }
  if (options->useES) {
    ierr = PetscPrintf(comm, "Gamma_bath =  %g\n", esOpts->GammaBath);CHKERRQ(ierr);
    for(PetscInt i = 0; i < geomOpts->numSpecies; ++i) {
      ierr = PetscPrintf(comm, "muES_bath_%d = %e\n", i, esOpts->muESBath[i]);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "OutputFields"
PetscErrorCode OutputFields(Options *options)
{
  DM             dm         = options->dmmg[0]->dm;
  const PetscInt numSpecies = options->geomOpts.numSpecies;
  const PetscInt numAlpha   = options->geomOpts.numAlpha;
  Vec            phiVec     = options->esOpts.phi;
  DMDALocalInfo  info, singleInfo;
  Vec            nVec, muHSVec, muESVec, tmpVec;
  Vec           *PhiDer;
  N           ***nArray;
  Rho         ***tmp;
  PetscScalar ***phi;
  PetscViewer    viewer;
  time_t         t;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (!options->doOutput) PetscFunctionReturn(0);
  t    = time(NULL);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(options->singleDA, &singleInfo);CHKERRQ(ierr);
  // Calculate intermediate quantities
  ierr = DMGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = PetscMalloc(numAlpha * sizeof(Vec), &PhiDer);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = DMGetGlobalVector(options->singleDA, &PhiDer[alpha]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(dm, &muHSVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &muESVec);CHKERRQ(ierr);
  ierr = CalculateN_3d(dm, DMMGGetx(options->dmmg), numSpecies, numAlpha, options->singleDA, &options->hsOpts, nVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  ierr = CalculatePhiDer_3d(&singleInfo, nArray, numAlpha, options->hsOpts.PhiDerFunc, &options->hsOpts, PhiDer);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  ierr = CalculateMuHS(dm, numSpecies, numAlpha, options->singleDA, PhiDer, &options->hsOpts, muHSVec);CHKERRQ(ierr);
  if (options->useES) {
    Rho ***rho;

    ierr = DMDAVecGetArray(dm, DMMGGetx(options->dmmg), &rho);CHKERRQ(ierr);
    ierr = CalculateCPsiHatFunctions(options->singleDA, numSpecies, cPsiHat, options->esOpts.cPsiHatVecs, rho, &options->esOpts);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, DMMGGetx(options->dmmg), &rho);CHKERRQ(ierr);
    if (options->esOpts.useMuES) {
      ierr = CalculateRhoReference(options->dmmg, dm, options->singleDA, options->kDA, DMMGGetx(options->dmmg), options->hsOpts.rhoBathVec, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
      ierr = CalculateMuES(dm, numSpecies, DMMGGetx(options->dmmg), options->singleDA, &options->hsOpts, &options->esOpts, options->esOpts.cPsiHatVecs, muESVec);CHKERRQ(ierr);
    }
    if (options->esOpts.usePoisson) {
      ierr = CalculatePhi(&info, options->singleDA, &options->geomOpts, &options->hsOpts, &options->esOpts, DMMGGetx(options->dmmg), phiVec);CHKERRQ(ierr);
    }
  }
  ierr = DMRestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  for(PetscInt alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = DMRestoreGlobalVector(options->singleDA, &PhiDer[alpha]);CHKERRQ(ierr);
  }
  ierr = PetscFree(PhiDer);CHKERRQ(ierr);
  // Check intermediate quantities
  if (0) {
    Rho         ***rho;
    Rho         ***muHS;
    Rho         ***muES;
    Rho         ***muExt;
    PetscScalar ***phiArray;

    ierr = DMDAVecGetArray(dm, DMMGGetx(options->dmmg), &rho);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, muHSVec, &muHS);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, muESVec, &muES);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(options->singleDA, phiVec, &phiArray);CHKERRQ(ierr);
    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
          for(PetscInt sp = 0; sp < numSpecies; ++sp) {
            PetscReal res;

            if (options->useES) {
              res = PetscAbsScalar(rho[k][j][i].v[sp] - options->hsOpts.rhoBath[sp]*PetscExpScalar((options->hsOpts.muHSBath[sp]
                                                                                                    + options->esOpts.muESBath[sp]
                                                                                                    - muHS[k][j][i].v[sp]
                                                                                                    - muExt[k][j][i].v[sp]
                                                                                                    - options->esOpts.z[sp]*phiArray[k][j][i]
                                                                                                    - options->esOpts.muESBath[sp]
                                                                                                    - muES[k][j][i].v[sp])/kT));
            } else {
              res = PetscAbsScalar(rho[k][j][i].v[sp] - options->hsOpts.rhoBath[sp]*PetscExpScalar((options->hsOpts.muHSBath[sp]
                                                                                                    - muHS[k][j][i].v[sp]
                                                                                                    - muExt[k][j][i].v[sp])/kT));
            }
            if (res > 1.0e-3) {
              //ierr = PetscPrintf(PETSC_COMM_WORLD, "Invalid residual %g at (%d,%d,%d,%d)\n", res, i, j, k, sp);CHKERRQ(ierr);
              SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_LIB, "Invalid residual %g at (%d,%d,%d,%d)", res, i, j, k, sp);
            }
          }
        }
      }
    }
    ierr = DMDAVecRestoreArray(dm, DMMGGetx(options->dmmg), &rho);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, muHSVec, &muHS);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, muESVec, &muES);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, options->geomOpts.muExt, &muExt);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(options->singleDA, phiVec, &phiArray);CHKERRQ(ierr);
  }
  // \rho_i
  std::ostringstream filename1;
  ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) tmpVec, "rho");CHKERRQ(ierr);
  ierr = VecCopy(DMMGGetx(options->dmmg), tmpVec);CHKERRQ(ierr);
  ierr = VecScale(tmpVec, NUMBER_TO_MOLAR);CHKERRQ(ierr);
  filename1 << "rho." << t << ".vtk";
  ierr = PetscViewerASCIIOpen(options->comm, filename1.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  // \ln\rho_i
  std::ostringstream filename2;
  ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) tmpVec, "ln_rho");CHKERRQ(ierr);
  ierr = VecCopy(DMMGGetx(options->dmmg), tmpVec);CHKERRQ(ierr);
  ierr = VecScale(tmpVec, NUMBER_TO_MOLAR);CHKERRQ(ierr);
  ierr = VecLog(tmpVec);CHKERRQ(ierr);
  filename2 << "ln_rho." << t << ".vtk";
  ierr = PetscViewerASCIIOpen(options->comm, filename2.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  // \ln\frac{\rho_i}{\rho^{bath}_i}
  std::ostringstream filename4;
  ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) tmpVec, "ln_rho_rho_bath");CHKERRQ(ierr);
  ierr = VecCopy(DMMGGetx(options->dmmg), tmpVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          tmp[k][j][i].v[sp] = std::log(tmp[k][j][i].v[sp]/options->hsOpts.rhoBath[sp]);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
  filename4 << "ln_rho_norm." << t << ".vtk";
  ierr = PetscViewerASCIIOpen(options->comm, filename4.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  // \mu^{HS}_i
  std::ostringstream filename3;
  ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) tmpVec, "mu_HS");CHKERRQ(ierr);
  ierr = VecCopy(muHSVec, tmpVec);CHKERRQ(ierr);
  ierr = VecScale(tmpVec, 1.0/(kT));CHKERRQ(ierr);
  filename3 << "muHS." << t << ".vtk";
  ierr = PetscViewerASCIIOpen(options->comm, filename3.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  // \Delta \mu^{HS}_i = \mu^{HS}_i - \mu^{HS,bath}_i
  std::ostringstream filename5;
  ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) tmpVec, "Delta_mu_HS");CHKERRQ(ierr);
  ierr = VecCopy(muHSVec, tmpVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          tmp[k][j][i].v[sp] -= options->hsOpts.muHSBath[sp];
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
  ierr = VecScale(tmpVec, 1.0/(kT));CHKERRQ(ierr);
  filename5 << "Delta_muHS." << t << ".vtk";
  ierr = PetscViewerASCIIOpen(options->comm, filename5.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
  if (options->useES) {
    if (options->esOpts.useMuES) {
      // \mu^{SC}_i
      std::ostringstream filename6;
      ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) tmpVec, "mu_SC");CHKERRQ(ierr);
      ierr = VecCopy(muESVec, tmpVec);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
      for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
        for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
          for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
            for(PetscInt sp = 0; sp < numSpecies; ++sp) {
              tmp[k][j][i].v[sp] += options->esOpts.muESBath[sp];
            }
          }
        }
      }
      ierr = DMDAVecRestoreArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
      ierr = VecScale(tmpVec, 1.0/(kT));CHKERRQ(ierr);
      filename6 << "muSC." << t << ".vtk";
      ierr = PetscViewerASCIIOpen(options->comm, filename6.str().c_str(), &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      ierr = DMView(dm, viewer);CHKERRQ(ierr);
      ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
      // \Delta \mu^{SC}_i = \mu^{SC}_i - \mu^{SC,bath}_i
      std::ostringstream filename7;
      ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) tmpVec, "Delta_mu_SC");CHKERRQ(ierr);
      ierr = VecCopy(muESVec, tmpVec);CHKERRQ(ierr);
      ierr = VecScale(tmpVec, 1.0/(kT));CHKERRQ(ierr);
      filename7 << "Delta_muSC." << t << ".vtk";
      ierr = PetscViewerASCIIOpen(options->comm, filename7.str().c_str(), &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      ierr = DMView(dm, viewer);CHKERRQ(ierr);
      ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
    }
    if (options->esOpts.usePoisson) {
      // \z_i e \phi : phi is already scaled by e
      std::ostringstream filename8;
      ierr = DMGetGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) tmpVec, "z_e_phi");CHKERRQ(ierr);
      ierr = DMDAVecGetArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(options->singleDA, phiVec, &phi);CHKERRQ(ierr);
      for(PetscInt k = singleInfo.zs; k < singleInfo.zs+singleInfo.zm; ++k) {
        for(PetscInt j = singleInfo.ys; j < singleInfo.ys+singleInfo.ym; ++j) {
          for(PetscInt i = singleInfo.xs; i < singleInfo.xs+singleInfo.xm; ++i) {
            for(PetscInt sp = 0; sp < numSpecies; ++sp) {
              tmp[k][j][i].v[sp] = options->esOpts.z[sp]*phi[k][j][i];
            }
          }
        }
      }
      ierr = DMDAVecRestoreArray(options->singleDA, phiVec, &phi);CHKERRQ(ierr);
      ierr = DMDAVecRestoreArray(dm, tmpVec, &tmp);CHKERRQ(ierr);
      ierr = VecScale(tmpVec, 1.0/(kT));CHKERRQ(ierr);
      filename8 << "phi." << t << ".vtk";
      ierr = PetscViewerASCIIOpen(options->comm, filename8.str().c_str(), &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      ierr = DMView(dm, viewer);CHKERRQ(ierr);
      ierr = VecView(tmpVec, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(dm, &tmpVec);CHKERRQ(ierr);
    }
  }
  // Cleanup
  ierr = DMRestoreGlobalVector(dm, &muHSVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &muESVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateProblem"
PetscErrorCode CreateProblem(MPI_Comm comm, DM *dm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = CreateMesh(comm, dm, options);CHKERRQ(ierr);
  ierr = CreateGeometry(*dm, &options->geomOpts);CHKERRQ(ierr);
  if (options->useHS) {
    ierr = CreateHardSphere(*dm, &options->geomOpts, &options->hsOpts);CHKERRQ(ierr);
  }
  if (options->useES) {
    ierr = CreateElectrostatics(*dm, options->singleDA, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
  } else {
    options->esOpts.GammaBath = 0.0;
    for(PetscInt sp = 0; sp < options->geomOpts.numSpecies; ++sp) {options->esOpts.muESBath[sp] = 0.0;}
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckProblem"
PetscErrorCode CheckProblem(DMMG *dmmg, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (options->geomOpts.domainType == DOMAIN_WALL) {
    ierr = CheckSumRule(dmmg, options->hsOpts.rhoBath, options->singleDA, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
  }
  if (options->geomOpts.domainType != DOMAIN_PILLAR) {
    ierr = CheckVolumeFraction(dmmg, options->nDA, options->singleDA, &options->geomOpts, &options->hsOpts, &options->verifyOpts);CHKERRQ(ierr);
  }
  if ((options->geomOpts.domainType != DOMAIN_CHANNEL) && (options->geomOpts.domainType != DOMAIN_CHANNEL_SMOOTH) && (options->geomOpts.domainType != DOMAIN_MORTICE) && (options->geomOpts.domainType != DOMAIN_PILLAR)) {
    ierr = CheckHomogeneity(dmmg, DMMGGetx(dmmg), &options->geomOpts, &options->verifyOpts);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyProblem"
PetscErrorCode DestroyProblem(DM dm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (options->useES) {
    ierr = DestroyElectrostatics(dm, &options->geomOpts, &options->hsOpts, &options->esOpts);CHKERRQ(ierr);
  }
  if (options->useHS) {
    ierr = DestroyHardSphere(dm, &options->geomOpts, &options->hsOpts);CHKERRQ(ierr);
  }
  ierr = DestroyGeometry(dm, &options->geomOpts);CHKERRQ(ierr);
  ierr = DestroyMesh(dm, options);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  MPI_Comm       comm;
  Options        options;
  DM             dm;
  DMMG          *dmmg;
  PetscLogStage  setupStage, outputStage;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscInitialize(&argc, &argv, (char *) 0, help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = PetscLogStageRegister("Setup Stage", &setupStage);CHKERRQ(ierr);
  ierr = PetscLogStageRegister("Output Stage", &outputStage);CHKERRQ(ierr);
  globalOptions               = &options;
  globalGeometryOptions       = &options.geomOpts;
  globalHardSphereOptions     = &options.hsOpts;
  globalElectrostaticsOptions = &options.esOpts;
  ierr = ProcessOptions(comm, &options);CHKERRQ(ierr);
  // Setup Stage
  ierr = PetscLogStagePush(setupStage);CHKERRQ(ierr);
  ierr = CreateProblem(comm, &dm, &options);CHKERRQ(ierr);
  ierr = CheckElectroneutrality(options.geomOpts.numSpecies, options.geomOpts.isConfined, &options.hsOpts, &options.esOpts);CHKERRQ(ierr);
  ierr = CheckN(dm, options.nDA, options.singleDA, &options.geomOpts, &options.hsOpts, &options.verifyOpts);CHKERRQ(ierr);
  //ierr = CheckScreeningIntegral(dm, &options.geomOpts, &options.esOpts, &options.verifyOpts);CHKERRQ(ierr);
  ierr = CreateSolver(dm, &dmmg, &options);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  // Main Stage
  ierr = ContinuationSolve(dmmg, &options);CHKERRQ(ierr);
  // Output Stage
  ierr = PetscLogStagePush(outputStage);CHKERRQ(ierr);
  ierr = PrintReport(dmmg, &options);CHKERRQ(ierr);
  ierr = PrintModule(dmmg, &options);CHKERRQ(ierr);
  ierr = PrintQuantities(&options);CHKERRQ(ierr);
  ierr = CheckProblem(dmmg, &options);CHKERRQ(ierr);
  ierr = OutputFields(&options);CHKERRQ(ierr);
  ierr = PetscLogStagePop();CHKERRQ(ierr);
  // Cleanup
  ierr = DMMGDestroy(dmmg);CHKERRQ(ierr);
  ierr = DestroyProblem(dm, &options);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
