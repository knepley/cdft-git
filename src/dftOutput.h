#ifndef __DFT_OUTPUT_H
#define __DFT_OUTPUT_H

#include<dftUtils.h>

extern PetscErrorCode VecViewVTK(SNES snes, DM dm, Vec v);
extern PetscErrorCode VecViewCenter_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewCenter(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewCenterSoln(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewCenterSolnFile(DM dm, Vec v, const char filebase[], const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewCenterSingle_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewCenterSingle(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewMax_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode VecViewMaxImag_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode PrintNorm_Private(Vec v, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode PrintNorm(Vec v, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode PrintNormSplit(Vec v, DM singleDA, int numSpecies, const char name[], PetscInt i, PetscInt j);
extern PetscErrorCode CalculateFilterOccupancy(DM dm, GeometryOptions *geomOpts, Vec rhoVec, PetscReal occupancy[]);
extern PetscErrorCode CalculateFilterVolume(DM dm, GeometryOptions *geomOpts, PetscReal volume[]);
extern PetscErrorCode CalculateXYSum(DM dm, const PetscInt species, Vec v, Vec sumVec, PetscReal *h);
extern PetscErrorCode VecViewXYSumSolnFile(DM dm, Vec v, const char filebase[], const char name[], PetscInt i, PetscInt j, PetscReal *h);

#endif /* __DFT_OUTPUT_H */
