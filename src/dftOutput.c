#include <dftOutput.h>

#undef __FUNCT__
#define __FUNCT__ "VecViewVTK"
PetscErrorCode VecViewVTK(SNES snes, DM dm, Vec v)
{
  MPI_Comm           comm;
  PetscViewer        viewer;
  PetscInt           iter;
  std::ostringstream hsFilename;
  PetscErrorCode     ierr;

  PetscFunctionBegin;
  if (debug != 6) {PetscFunctionReturn(0);}
  ierr = PetscObjectGetComm((PetscObject) snes, &comm);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(snes, &iter);CHKERRQ(ierr);
  hsFilename << "muHS_" << iter << ".vtk";
  ierr = PetscViewerASCIIOpen(comm, hsFilename.str().c_str(), &viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
  ierr = DMView(dm, viewer);CHKERRQ(ierr);
  ierr = VecView(v, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenter_Private"
PetscErrorCode VecViewCenter_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  Rho         ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  for(int sp = 0; sp < NUM_SPECIES+1; ++sp) {
    ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecGetArray(c, &b);CHKERRQ(ierr);
    for(PetscInt k = 0, i = info.mx/2, j = info.my/2; k < info.mz; ++k) {
      b[k] = a[k][j][i].v[sp];
    }
    ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);
    ierr = VecView(c, viewer);
  }
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenter"
PetscErrorCode VecViewCenter(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug != 4) PetscFunctionReturn(0);
  ierr = VecViewCenter_Private(dm, v, viewer, name, i, j);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenterSoln"
PetscErrorCode VecViewCenterSoln(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug != 2) PetscFunctionReturn(0);
  ierr = VecViewCenter_Private(dm, v, viewer, name, i, j);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenterSolnFile"
PetscErrorCode VecViewCenterSolnFile(DM dm, Vec v, const char filebase[], const char name[], PetscInt i, PetscInt j)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  Rho         ***a;
  PetscScalar   *b;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (debug != 2) PetscFunctionReturn(0);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  for(int sp = 0; sp < NUM_SPECIES+1; ++sp) {
    ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecGetArray(c, &b);CHKERRQ(ierr);
    for(PetscInt k = 0, i = info.mx/2, j = info.my/2; k < info.mz; ++k) {
      b[k] = a[k][j][i].v[sp];
    }
    ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);

    char filename[2048];
    ierr = PetscSNPrintf(filename, 2047, "%s_%d.out", filebase, sp);CHKERRQ(ierr);
    ierr = PetscViewerASCIIOpen(comm, filename, &viewer);CHKERRQ(ierr);
    ierr = VecView(c, viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateXYSum"
PetscErrorCode CalculateXYSum(DM dm, const PetscInt species, Vec v, Vec sumVec, PetscReal *h)
{
  DMDALocalInfo  info;
  Rho         ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
  ierr = VecGetArray(sumVec, &b);CHKERRQ(ierr);
  for(PetscInt k = 0; k < info.mz; ++k) {
    PetscReal sum = 0.0;

    for(PetscInt j = 0; j < info.my; ++j) {
      for(PetscInt i = 0; i < info.mx; ++i) {
        sum += PetscRealPart(a[k][j][i].v[species]);
      }
    }
    b[k] = sum*h[0]*h[1];
  }
  ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
  ierr = VecRestoreArray(sumVec, &b);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewXYSumSolnFile"
PetscErrorCode VecViewXYSumSolnFile(DM dm, Vec v, const char filebase[], const char name[], PetscInt i, PetscInt j, PetscReal *h)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (debug != 2) PetscFunctionReturn(0);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  for(int sp = 0; sp < NUM_SPECIES; ++sp) {
    ierr = CalculateXYSum(dm, sp, v, c, h);CHKERRQ(ierr);

    char filename[2048];
    ierr = PetscSNPrintf(filename, 2047, "%s_%d.out", filebase, sp);CHKERRQ(ierr);
    ierr = PetscViewerASCIIOpen(comm, filename, &viewer);CHKERRQ(ierr);
    ierr = VecView(c, viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewMax_Private"
PetscErrorCode VecViewMax_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  Rho         ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  for(int sp = 0; sp < NUM_SPECIES+1; ++sp) {
    ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecGetArray(c, &b);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      b[k] = 0.0;

      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          b[k] = PetscMax(PetscRealPart(a[k][j][i].v[sp]), PetscRealPart(b[k]));
        }
      }
    }
    ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);
    ierr = VecView(c, viewer);
  }
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewMaxImag_Private"
PetscErrorCode VecViewMaxImag_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  Rho         ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  for(int sp = 0; sp < NUM_SPECIES+1; ++sp) {
    ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecGetArray(c, &b);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      b[k] = 0.0;

      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          b[k] = PetscMax(PetscImaginaryPart(a[k][j][i].v[sp]), PetscImaginaryPart(b[k]));
        }
      }
    }
    ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);
    ierr = VecView(c, viewer);
  }
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenterSingle_Private"
PetscErrorCode VecViewCenterSingle_Private(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            c;
  PetscScalar ***a;
  PetscScalar   *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Viewing %s\n", name);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Viewing %s[%d]\n", name, i);
    } else {
      ierr = PetscPrintf(comm, "Viewing %s[%d,%d]\n", name, i, j);
    }
  }
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = VecCreate(comm, &c);CHKERRQ(ierr);
  ierr = VecSetSizes(c, info.zm - info.zs, info.mz);CHKERRQ(ierr);
  ierr = VecSetFromOptions(c);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
  ierr = VecGetArray(c, &b);CHKERRQ(ierr);
  for(PetscInt k = 0, i = info.mx/2, j = info.my/2; k < info.mz; ++k) {
    b[k] = a[k][j][i];
  }
  ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
  ierr = VecRestoreArray(c, &b);CHKERRQ(ierr);
  ierr = VecView(c, viewer);
  ierr = VecDestroy(&c);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "VecViewCenterSingle"
PetscErrorCode VecViewCenterSingle(DM dm, Vec v, PetscViewer viewer, const char name[], PetscInt i, PetscInt j)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug != 4) PetscFunctionReturn(0);
  ierr = VecViewCenterSingle_Private(dm, v, viewer, name, i, j);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintNorm_Private"
PetscErrorCode PrintNorm_Private(Vec v, const char name[], PetscInt i, PetscInt j)
{
  MPI_Comm       comm;
  PetscReal      infNorm, norm2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) v, &comm);CHKERRQ(ierr);
  ierr = VecNorm(v, NORM_INFINITY, &infNorm);CHKERRQ(ierr);
  ierr = VecNorm(v, NORM_2, &norm2);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Norm of %s: infinity %g l2 %g\n", name, infNorm, norm2);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Norm of %s[%d]: infinity %g l2 %g\n", name, i, infNorm, norm2);
    } else {
      ierr = PetscPrintf(comm, "Norm of %s[%d,%d]: infinity %g l2 %g\n", name, i, j, infNorm, norm2);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintNorm"
PetscErrorCode PrintNorm(Vec v, const char name[], PetscInt i, PetscInt j)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug < 3) PetscFunctionReturn(0);
  ierr = PrintNorm_Private(v, name, i, j);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintNormSplit"
PetscErrorCode PrintNormSplit(Vec v, DM singleDA, int numSpecies, const char name[], PetscInt i, PetscInt j)
{
  Vec           *vSplit;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug < 3) PetscFunctionReturn(0);
  ierr = PetscMalloc((numSpecies+1) * sizeof(Vec), &vSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp) {
	ierr = DMGetGlobalVector(singleDA, &vSplit[sp]);CHKERRQ(ierr);
  }
  ierr = VecStrideGatherAll(v, vSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = PrintNorm(vSplit[sp], name, sp, j);CHKERRQ(ierr);
  }
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
	ierr = DMRestoreGlobalVector(singleDA, &vSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree(vSplit);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateOccupancy"
PetscErrorCode CalculateOccupancy(DM dm, GeometryOptions *geomOpts, Vec rhoVec, PetscInt sp, PetscReal *occupancy)
{
  const PetscReal  Lc  = geomOpts->channelLength;
  const PetscReal *L   = geomOpts->L;
  const PetscReal *h   = geomOpts->h;
  PetscReal        occ = 0.0;
  DMDALocalInfo    info;
  DM               coordDA;
  Vec              coordinates;
  DMDACoor3d    ***coords;
  Rho           ***rho;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    const PetscReal zi = PetscRealPart(coords[k][0][0].z) - L[2]/2.0;

    if (PetscAbsScalar(zi) - Lc/2.0 < 1.0e-10) {
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {

#if 0
          for(PetscInt dk = 0; dk < 1; ++dk) {
            const PetscInt kk = k+dk < 0 ? info.zs+info.zm-1 : (k+dk > info.zs+info.zm-1 ? 0 : k+dk);
            for(PetscInt dj = 0; dj < 1; ++dj) {
              const PetscInt jj = j+dj < 0 ? info.ys+info.ym-1 : (j+dj > info.ys+info.ym-1 ? 0 : j+dj);
              for(PetscInt di = 0; di < 1; ++di) {
                const PetscInt ii = i+di < 0 ? info.xs+info.xm-1 : (i+di > info.xs+info.xm-1 ? 0 : i+di);
                occ += PetscRealPart(rho[kk][jj][ii].v[sp]);
              }
            }
          }
#else
          occ += PetscRealPart(rho[k][j][i].v[sp]);
#endif
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  *occupancy = occ*h[0]*h[1]*h[2];
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateFilterOccupancy"
PetscErrorCode CalculateFilterOccupancy(DM dm, GeometryOptions *geomOpts, Vec rhoVec, PetscReal occupancy[])
{
  const PetscInt numSpecies = geomOpts->numSpecies;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = CalculateOccupancy(dm, geomOpts, rhoVec, sp, &occupancy[sp]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateFilterVolume"
PetscErrorCode CalculateFilterVolume(DM dm, GeometryOptions *geomOpts, PetscReal volume[])
{
  DMDALocalInfo  info;
  Vec            constantRho;
  Rho         ***rho;
  Rho         ***muExt;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &constantRho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, constantRho, &rho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, geomOpts->muExt, &muExt);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
          rho[k][j][i].v[sp] = 1.0*PetscExpScalar(-muExt[k][j][i].v[sp]/kT);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, geomOpts->muExt, &muExt);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, constantRho, &rho);CHKERRQ(ierr);
  ierr = CalculateFilterOccupancy(dm, geomOpts, constantRho, volume);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &constantRho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
