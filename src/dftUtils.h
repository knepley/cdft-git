#ifndef __DFT_UTILS_H
#define __DFT_UTILS_H

#include<petscdmmg.h>

#define NUMBER_TO_MOLAR 1.6606

// Debugging levels:
// 0: No output
// 1: Output phase headers
// 2: View solution
// 3: Output norms
// 4: View intermediates
// 5: Output full Rhs and other debugging
extern PetscInt        debug;   // Debugging level
extern const PetscReal kT;      // Boltzmann's constant and room temperature [kg nm^2/s^2]
extern const PetscReal epsilon; // Water's permitivity [C^2 s^2 kg^-1 nm^-3]
extern const PetscReal e;       // Electron charge [C]

typedef struct {
  PetscScalar v[10];
} N;

struct Rho {
  PetscScalar v[NUM_SPECIES]; // Density of each ion species [nm^{-3}]
  PetscScalar Gamma;          // MSA screening parameter     [nm^{-1}]
};

struct Kvec {
  PetscScalar kx; // x-component of k
  PetscScalar ky; // x-component of k
  PetscScalar kz; // x-component of k
  PetscScalar k;  // |k|
};

typedef struct {
  PetscInt      numSpecies;
  PetscScalar (*func1)(const PetscScalar [], const int);
  PetscScalar (*func2)(const PetscScalar [], const PetscScalar [], const int, const int);
  PetscInt      spI, spJ;
  Rho        ***field;
} EvaluationOptions;

typedef enum {DOMAIN_PILLAR, DOMAIN_MORTICE, DOMAIN_CHANNEL, DOMAIN_CHANNEL_SMOOTH, DOMAIN_SPHERE, DOMAIN_WALL} DomainType;

typedef enum {MU_EXTERNAL_QUADRATURE, MU_EXTERNAL_SPECTRAL_QUADRATURE} MuExternalComputationType;

typedef struct {
  DomainType domainType;                // The geometry of the domain
  PetscInt   numSpecies;                // Number of ion species
  PetscInt   numAlpha;                  // Number of omega/W functions (6 for 1D, 10 for 3D)
  PetscReal  L[3];                      // System length [nm] in each direction
  PetscReal  h[3];                      // Plaque length [nm] in each direction
  PetscReal  wallPos;                   // Position of the wall [nm]
  PetscReal  wallCut;                   // X height of the wall [nm] (or 0.0)
  PetscReal  channelLength;             // Length L of the selectivity filer [nm]
  PetscReal  channelRadius;             // Radius R of the selectivity filer [nm]
  PetscReal  vestibuleRadius;           // Radius C of the vestibule [nm]
  PetscReal  cellStart;                 // Start of the confinement cell [nm]
  PetscReal  cellEnd;                   // End of the confinement cell [nm]
  PetscInt   dims[3];                   // Number of vertices  in each direction
  PetscInt   bathIndex[3];              // DA indices for bath values
  char      *ionNames[NUM_SPECIES];     // Species names
  PetscReal  R[NUM_SPECIES];            // Radius of species i [nm]
  PetscReal  maxR;                      // Maximum radius on any species
  PetscReal  eta[NUM_SPECIES];          // Volume fraction of ions
  PetscBool  isConfined[NUM_SPECIES];   // Flag for confined ion species
  PetscReal  numParticles[NUM_SPECIES]; // Number of confined ions in the volume
  PetscReal  confinementDecay;          // Number of cells over which confinement decays
  PetscInt   minResolution;             // The minimum resolution (number of DA vertices per particle radius)
  PetscInt   resolution;                // The resolution (number of DA vertices per particle radius)
  Vec        muExt;                     // Chemical potential due to external forces
  PetscReal  barrierHeight;             // Chemical potential of a barrier in units of kT
  MuExternalComputationType muExternalComputationType; // The type of computation done to produce the external potential
} GeometryOptions;

extern GeometryOptions *globalGeometryOptions;

extern PetscErrorCode ProcessGeometryOptions(MPI_Comm comm, GeometryOptions *options);
extern PetscErrorCode PrintGeometryModule(PetscViewer viewer, const char indent[], GeometryOptions *options);
extern PetscErrorCode CalculateK(const PetscScalar x[], const PetscReal h[], const PetscInt dims[], PetscReal k[]);
extern PetscErrorCode CalculateMuExternal(DM dm, const PetscInt numSpecies, const DomainType domainType, const MuExternalComputationType, Vec muExt);
extern PetscErrorCode Function_1d(DMDALocalInfo *info, Rho x[], Rho f[], void *ctx);
extern PetscErrorCode Function_2d(DMDALocalInfo *info, Rho *x[], Rho *f[], void *ctx);
extern PetscErrorCode Function_3d(DMDALocalInfo *info, Rho **x[], Rho **f[], void *ctx);
extern PetscErrorCode Function_3d_TwoSpecies(DMDALocalInfo *info, PetscScalar **x[], PetscScalar **f[], void *ctx);
extern PetscErrorCode CreateGeometry(DM dm, GeometryOptions *options);
extern PetscErrorCode DestroyGeometry(DM dm, GeometryOptions *options);

#endif /* __DFT_UTILS_H */
