static char help[] = "This example solves the DFT for hard sphere.\n\n";
/*
  R_i:             Radius of species i
  \rho^i(x):       Density of species i
  \mu^i_{HS}(x):   Chemical potential of species i due to hard sphere interaction
  \mu^i_{ext}(x):  Chemical potential of species i due to external forces
  \rho^i_{bath}:   Density of species i in the bath
  \mu^i_{HS,bath}: Chemical potential of species i due to hard sphere interaction in the bath

  \rho^i(x) = \rho^i_{bath} \exp{(\mu^i_{HS,bath} - \mu^i_{HS}[{\rho^k(x')}; x] - \mu^i_{ext}(x))/kT}

  \mu^i_{ext}(x) = \infty     inside barrier
                 = 0         outside barrier

  \Phi_{HS}(x) = -n_0 \ln(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over 1 - n_3} + {n^3_2 \over 24\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3

  \mu^i_{HS}(x) = kT \sum_\alpha \int^{x+R_i}_{x-R_i} {\partial\Phi_{HS} \over \partial n_\alpha}(x') W^i_\alpha(x - x') dx'

  n_\alpha(x) = \sum_i \int^{x+R_i}_{x-R_i} \rho_i(x') W^i_\alpha(x' - x) dx'

  {\partial\Phi_{HS}}{\partial n_0} = -\ln(1 - n_3)

  {\partial\Phi_{HS}}{\partial n_1} = {n_2 \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_2} = {n_1 \over 1 - n_3} + {n^2_2 \over 8\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3 + {n^2_{V2} \over 4\pi n_2 (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

  {\partial\Phi_{HS}}{\partial n_3} = n_0/(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over (1 - n_3)^2} + {n^3_2 \over 12\pi (1 - n_3)^3} (1 - {n^2_{V2}\over n^2_2})^3

  {\partial\Phi_{HS}}{\partial n_{V1}} = {-n_{V2} \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_{V2}} = {-n_{V1} \over 1 - n_3} - {n_2 n_{V2} \over 4\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

Sum Rule for Hard Spheres:

  P_{HS} = kT \sum_i \rho^i(wallPos + R_i)

where the pressure is

  P_{HS} = kT {6\over\pi} ({\xi_0\over\Delta} + {3\xi_1\xi_2\over\Delta^2} + {3\xi^3_2\over\Delta^3})

  \Delta = 1 - \xi_3

  \xi_n = {\pi\over6} \sum_i \rho^i (2 R_i)^n
*/
#include <petscdmmg.h>

typedef struct {
  MPI_Comm      comm;
  PetscInt      debug;      // Debugging level
  PetscInt		sDebug;
  PetscInt      dim;        // Topological mesh dimension
  PetscInt      numSpecies; // Number of ion species
  PetscReal     L;          // System length [nm]
  PetscReal    *R;          // Radius of species i [nm]
  PetscReal    *rhoBath;    // Density of species i in the bath [nm^{-3}]
  PetscReal    *rhoBathInit;//   Initial density for continuation loop
  PetscInt      numRhoBath; //   Number of continuation steps
  PetscReal    *muHSBath;   // Chemical potential of species i due to hard sphere interaction in the bath
  PetscReal     presHS;     // Pressure of species i due to hard sphere interaction in the bath
  PetscReal     kT;         // Boltzmann's constant * Temperature [J]
  PetscReal     wallPos;    // Position of the wall [nm]
  PetscReal    *eta;        // Volume fraction of ions
  DA            nDA;        // DA for the n functionals
  PetscReal     nCheckTol;  // Tolerance for n convolution check (should be calculated from h)
  PetscReal     volFracTol; // Tolerance for volume fraction check (should be calculated from h)
  Vec           muExt;      // Chemical potential due to external forces
  Vec          *n;          // n^\alpha functions
  Vec          *W;          // W^\alpha window functions
  Vec          *PhiDer;     // {\partial\Phi_{HS}}{\partial n_\alpha} functions
  Vec           error;      // Error in density
  PetscInt     *convS;      // Storage for the convolution integration domain half-length s
  PetscReal   **convArg;    // Storage for the convolution argument values (\rho or PhiDer)
  PetscReal   **convWts;    // Storage for the convolution weights (mostly h)
  PetscReal   **convDiffs;  // Storage for the convolution coordinate differences
  PetscReal   **convDiffs2; // Storage for the convolution reverse coordinate differences
  PetscScalar (*muExtFunc)(const double [], const int);   // Function defining the external potential
  PetscScalar (**WFunc)(const double [], const int);      // Functions W
  PetscScalar (**PhiDerFunc)(const double [], const int); // Functions {\partial\Phi_{HS}}{\partial n_\alpha}
  PetscScalar (*func)(const double [], const int);        // The function to project
} Options;

typedef struct {
  PetscScalar v[6];
} N;

struct Rho {
  PetscScalar v[NUM_SPECIES];
};

Options *globalOptions;

PetscScalar zero(const double x[], const int species) {
  return 0.0;
}

PetscScalar wall(const double x[], const int species) {
  if (x[0] <= globalOptions->wallPos+globalOptions->R[species]) //|| x[0] >= (globalOptions->L - globalOptions->wallPos - globalOptions->R[species])
    return 100.0*globalOptions->kT;
  return 0.0;
}

PetscScalar W_0(const double x[], const int species) {
  return 1.0/(2.0*globalOptions->R[species]);
}

PetscScalar W_1(const double x[], const int species) {
  return 0.5;
}

PetscScalar W_2(const double x[], const int species) {
  return 2.0*M_PI*globalOptions->R[species];
}

PetscScalar W_3(const double x[], const int species) {
  return M_PI*(globalOptions->R[species]*globalOptions->R[species] - x[0]*x[0]);
}

PetscScalar W_V1(const double x[], const int species) {
  return 0.5*x[0]/globalOptions->R[species];
}

PetscScalar W_V2(const double x[], const int species) {
  return 2.0*M_PI*x[0];
}

PetscScalar PhiDer_0(const double n[], const int species) {
  return -log(1.0 - n[3]);
}

PetscScalar PhiDer_1(const double n[], const int species) {
  return n[2]/(1.0 - n[3]);
}

PetscScalar PhiDer_2(const double n[], const int species) {
  const PetscScalar ratio = ((n[2] == 0.0) && (n[5] == 0.0)) ? 0.0 : PetscSqr(n[5])/PetscSqr(n[2]);
  const PetscScalar term  = 1.0 - ratio;
  return n[1]/(1.0 - n[3]) + (PetscSqr(n[2])/(8.0*M_PI*PetscSqr(1.0 - n[3])))*term*term*term + (n[5]*n[5]/(4.0*M_PI*PetscSqr(1.0 - n[3])))*PetscSqr(term);
}

PetscScalar PhiDer_3(const double n[], const int species) {
  const PetscScalar ratio = ((n[2] == 0.0) && (n[5] == 0.0)) ? 0.0 : PetscSqr(n[5])/PetscSqr(n[2]);
  const PetscScalar term  = 1.0 - ratio;
  return n[0]/(1.0 - n[3]) + (n[1]*n[2] - n[4]*n[5])/PetscSqr(1.0 - n[3]) + (n[2]*n[2]*n[2]/(12.0*M_PI*(1.0 - n[3])*(1.0 - n[3])*(1.0 - n[3])))*term*term*term;
}

PetscScalar PhiDer_V1(const double n[], const int species) {
  return -n[5]/(1.0 - n[3]);
}

PetscScalar PhiDer_V2(const double n[], const int species) {
  const PetscScalar ratio = ((n[2] == 0.0) && (n[5] == 0.0)) ? 0.0 : PetscSqr(n[5])/PetscSqr(n[2]);
  const PetscScalar term  = 1.0 - ratio;
  return (-n[4]/(1.0 - n[3])) - (n[2]*n[5]/(4.0*M_PI*PetscSqr(1.0 - n[3])))*PetscSqr(term);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHSBath"
PetscErrorCode CalculateMuHSBath(Options *options, PetscReal *muHSBath)
{
  PetscReal xi_0 = 0.0, xi_1 = 0.0, xi_2 = 0.0, xi_3, Delta;
  PetscInt  i;

  PetscFunctionBegin;
  for(i = 0; i < options->numSpecies; ++i) {
    const PetscReal sigma = 2.0*options->R[i];

    xi_0 += options->rhoBath[i];
    xi_1 += options->rhoBath[i]*sigma;
    xi_2 += options->rhoBath[i]*PetscSqr(sigma);
    xi_3 += options->rhoBath[i]*PetscSqr(sigma)*sigma;

  }
  xi_0 *= (M_PI/6.0);
  xi_1 *= (M_PI/6.0);
  xi_2 *= (M_PI/6.0);
  xi_3 *= (M_PI/6.0);
  Delta = 1.0 - xi_3;

  for(i = 0; i < options->numSpecies; ++i) {
    const PetscReal sigma = 2.0*options->R[i];

    muHSBath[i] = options->kT*(-log(Delta) +
                               (3.0*xi_2*sigma + 3.0*xi_1*PetscSqr(sigma))/Delta +
                               (9.0*PetscSqr(xi_2)*PetscSqr(sigma))/(2.0*PetscSqr(Delta)) +
                               (xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta))*PetscSqr(sigma)*sigma);
  }
  options->presHS = (6.0/M_PI)*options->kT*(xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
PetscErrorCode ProcessOptions(MPI_Comm comm, Options *options)
{
  PetscInt       n, i;
  PetscTruth     flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  options->comm       = comm;
  options->debug      = 0;
  options->sDebug     = 1;
  options->dim        = 1;
  options->numSpecies = 2;
  options->L          = 10;       // 100 Angstrom system length
  options->kT         = 4.21e-3;  // 4.21e-21 [kg m^2/s^2]
  options->nCheckTol  = 1.0e-9;
  options->volFracTol = 1.0e-3;

  ierr = PetscOptionsBegin(comm, "", "DFT Problem Options", "DMMG");CHKERRQ(ierr);
    ierr = PetscOptionsInt("-debug", "The debugging level", "dft.cxx", options->debug, &options->debug, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "dft.cxx", options->dim, &options->dim, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-L", "The length of the domain (nm)", "dft.cxx", options->L, &options->L, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-wallPos", "Position of the wall", "dft.cxx", options->wallPos, &options->wallPos, &flag);CHKERRQ(ierr);
    if (!flag) {options->wallPos = options->L/10;}
    ierr = PetscOptionsReal("-kT", "Temperature in natural units", "dft.cxx", options->kT, &options->kT, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-nCheckTol", "Tolerance for n check", "dft.cxx", options->nCheckTol, &options->nCheckTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-volFracTol", "Tolerance for volume fraction check", "dft.cxx", options->volFracTol, &options->volFracTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-numSpecies", "The number of ion species", "dft.cxx", options->numSpecies, &options->numSpecies, PETSC_NULL);CHKERRQ(ierr);

    if (options->numSpecies != NUM_SPECIES) {
      SETERRQ1(PETSC_ERR_SUP, "I hate this too, but you have to compile in the number of species, which is currently %d.", NUM_SPECIES);
    }
    ierr = PetscMalloc5(options->numSpecies,PetscReal,&options->R,
                        options->numSpecies,PetscReal,&options->rhoBath,
                        options->numSpecies,PetscReal,&options->rhoBathInit,
                        options->numSpecies,PetscReal,&options->muHSBath,
                        options->numSpecies,PetscReal,&options->eta);CHKERRQ(ierr);
    for(i = 0; i < options->numSpecies; ++i) {
	    if(i == 0){
			options->R[i]           = 0.1;      // 1 Angstrom ion radius
			options->rhoBath[i]     = 6.0e-1;   // 6 10^{26} ions/m^3
			options->rhoBathInit[i] = 6.0e-1;   // 6 10^{26} ions/m^3
		}
		else{
			options->R[i]           = 0.15;     // 1.5 Angstrom ion radius
			options->rhoBath[i]     = 4.0e-1;   // 4 10^{26} ions/m^3
			options->rhoBathInit[i] = 4.0e-1;   // 4 10^{26} ions/m^3
		}			
    }

    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-R", "The radius of species i", "dft.cxx", options->R, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(PETSC_ERR_ARG_WRONG, "Invalid number of radii given");}
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-rhoBath", "The density of species i in the bath", "dft.cxx", options->rhoBath, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");}
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-rhoBathInit", "The initial density of species i in the bath for the continuation loop", "dft.cxx", options->rhoBathInit, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(PETSC_ERR_ARG_WRONG, "Invalid number of initial bath potentials given");}
    options->numRhoBath = 0;
    ierr = PetscOptionsInt("-numRhoBath", "The number of continuation steps", "dft.cxx", options->numRhoBath, &options->numRhoBath, PETSC_NULL);CHKERRQ(ierr);
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-muHSBath", "The chemical potential of species i in the bath", "dft.cxx", options->muHSBath, &n, &flag);CHKERRQ(ierr);
    if (!flag) {
      ierr = CalculateMuHSBath(options, options->muHSBath);CHKERRQ(ierr);
    } else if (n != options->numSpecies) {
      SETERRQ(PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");
    }
  ierr = PetscOptionsEnd();

  options->muExtFunc = wall;
  ierr = PetscMalloc3(6,Vec,&options->W,6,Vec,&options->PhiDer,6,Vec,&options->n);CHKERRQ(ierr);
  ierr = PetscMalloc(6*sizeof(PetscScalar (*)(const double [])), &options->WFunc);CHKERRQ(ierr);
  ierr = PetscMalloc(6*sizeof(PetscScalar (*)(const double [])), &options->PhiDerFunc);CHKERRQ(ierr);
  options->WFunc[0] = W_0;
  options->WFunc[1] = W_1;
  options->WFunc[2] = W_2;
  options->WFunc[3] = W_3;
  options->WFunc[4] = W_V1;
  options->WFunc[5] = W_V2;
  options->PhiDerFunc[0] = PhiDer_0;
  options->PhiDerFunc[1] = PhiDer_1;
  options->PhiDerFunc[2] = PhiDer_2;
  options->PhiDerFunc[3] = PhiDer_3;
  options->PhiDerFunc[4] = PhiDer_V1;
  options->PhiDerFunc[5] = PhiDer_V2;
  /* Check packing fraction */
  for(i = 0; i < options->numSpecies; ++i) {
    options->eta[i] = options->rhoBath[i]*(4.0*M_PI/3.0)*(PetscSqr(options->R[i])*options->R[i]);
    if (options->eta[i] > 0.38) SETERRQ2(PETSC_ERR_ARG_OUTOFRANGE, "Excessive volume fraction: %g for species %d", options->eta[i], i);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateConvolutionQuadtrature"
PetscErrorCode CreateConvolutionQuadtrature(PetscInt sp, PetscReal h, Options *options)
{
  const PetscReal R     = options->R[sp];
  const PetscInt  s     = (PetscInt) (R/h);
  const PetscReal gamma = R - s*h;
  PetscInt        j, k;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  if (s < 5) SETERRQ1(PETSC_ERR_ARG_OUTOFRANGE, "Insufficient resolution: %d", s);
  ierr = PetscPrintf(options->comm, "Points in convolution integration: %d for species %d\n", 2*s + 3, sp);CHKERRQ(ierr);
  options->convS[sp] = s;
  ierr = PetscMalloc4(2*s+3,PetscReal,&options->convArg[sp],2*s+3,PetscReal,&options->convWts[sp],2*s+3,PetscReal,&options->convDiffs[sp],2*s+3,PetscReal,&options->convDiffs2[sp]);CHKERRQ(ierr);
  // Calculate quadrature weights
  options->convWts[sp][0]        = 0.5*gamma*gamma/h;
  options->convWts[sp][1]        = gamma - 0.5*gamma*gamma/h + 0.5*h;
  for(k = 2; k < 2*s+1; ++k) options->convWts[sp][k] = h;
  options->convWts[sp][2*s+1]    = gamma - 0.5*gamma*gamma/h + 0.5*h;
  options->convWts[sp][2*s+2]    = 0.5*gamma*gamma/h;
  // Calculate coordinate differences (x - x')
  options->convDiffs[sp][0]      =  s*h + gamma;
  for(j = s, k = 1; j >= -s; --j, ++k) options->convDiffs[sp][k] = j*h;
  options->convDiffs[sp][2*s+2]  = -s*h - gamma;
  // Calculate reverse coordinate differences (x' - x)
  options->convDiffs2[sp][0]     = -s*h - gamma;
  for(j = -s, k = 1; j <= s; ++j, ++k) options->convDiffs2[sp][k] = j*h;
  options->convDiffs2[sp][2*s+2] =  s*h + gamma;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateMesh"
PetscErrorCode CreateMesh(MPI_Comm comm, DM *dm, Options *options)
{
  DA             da;
  PetscInt       dof = options->numSpecies, M, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (options->dim == 1) {
    ierr = DACreate1d(comm, DA_NONPERIODIC, -3, dof, 1, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 2) {
    ierr = DACreate2d(comm, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 3) {
    ierr = DACreate3d(comm, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = DASetUniformCoordinates(da, 0.0, options->L, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  /* Check accuracy */
  ierr = DAGetInfo(da, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = PetscMalloc5(options->numSpecies,PetscInt,&options->convS,
                      options->numSpecies,PetscReal*,&options->convArg,
                      options->numSpecies,PetscReal*,&options->convWts,
                      options->numSpecies,PetscReal*,&options->convDiffs,
                      options->numSpecies,PetscReal*,&options->convDiffs2);CHKERRQ(ierr);
  for(sp = 0; sp < options->numSpecies; ++sp) {
    ierr = CreateConvolutionQuadtrature(sp, options->L/(M-1), options);CHKERRQ(ierr);
  }
  *dm = (DM) da;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateParameterMesh"
PetscErrorCode CreateParameterMesh(DM dm, Options *options)
{
  MPI_Comm       comm;
  DA             da;
  PetscInt       dof = 6;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DACreate1d(comm, DA_NONPERIODIC, -3, dof, 1, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 2) {
    ierr = DACreate2d(comm, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 3) {
    ierr = DACreate3d(comm, DA_NONPERIODIC, DA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = DASetUniformCoordinates(da, 0.0, options->L, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  options->nDA = da;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyMesh"
PetscErrorCode DestroyMesh(DM dm, Options *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DADestroy((DA) dm);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyParameters"
PetscErrorCode DestroyParameters(Options *options)
{
  PetscInt       i;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DADestroy(options->nDA);CHKERRQ(ierr);
  ierr = PetscFree3(options->W,options->PhiDer,options->n);CHKERRQ(ierr);
  ierr = PetscFree(options->WFunc);CHKERRQ(ierr);
  ierr = PetscFree(options->PhiDerFunc);CHKERRQ(ierr);
  ierr = VecDestroy(options->muExt);CHKERRQ(ierr);
  ierr = PetscFree5(options->R,options->rhoBath,options->rhoBathInit,options->muHSBath,options->muHSBath);CHKERRQ(ierr);
  for(i = 1; i < options->numSpecies; ++i) {
    ierr = PetscFree4(options->convArg[i], options->convWts[i], options->convDiffs[i], options->convDiffs2[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree5(options->convS, options->convArg, options->convWts, options->convDiffs, options->convDiffs2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Function_1d"
PetscErrorCode Function_1d(DALocalInfo *info, Rho x[], Rho f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const double *, const int) = options->func;
  DA             coordDA;
  Vec            coordinates;
  PetscReal     *coords;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(i = info->xs; i < info->xs+info->xm; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      f[i].v[sp] = func((PetscReal *) &coords[i], sp);
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_2d"
PetscErrorCode Function_2d(DALocalInfo *info, Rho *x[], Rho *f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const double *, const int) = options->func;
  DA             coordDA;
  Vec            coordinates;
  DACoor2d     **coords;
  PetscInt       i, j, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(j = info->ys; j < info->ys+info->ym; ++j) {
    for(i = info->xs; i < info->xs+info->xm; ++i) {
      for(sp = 0; sp < options->numSpecies; ++sp) {
        f[j][i].v[sp] = func((PetscReal *) &coords[j][i], sp);
      }
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN_1d"
// Check: Put in rho = 1 and should get integrals of Ws
PetscErrorCode CalculateN_1d(DALocalInfo *info, Rho rho[], N n[], void *ctx)
{
  Options        *options = (Options *) ctx;
  PetscInt       *s       = options->convS;
  PetscReal     **rhos    = options->convArg;
  PetscReal     **weights = options->convWts;
  PetscReal     **diffs   = options->convDiffs2;
  DA              coordDA;
  Vec             coordinates;
  PetscReal      *coords;
  PetscReal       h;
  PetscInt        i, j, k, alpha, sp;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  ierr  = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr  = DAGetGhostedCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr  = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  h     = coords[info->xs+1] - coords[info->xs];
  for(i = info->xs; i < info->xs+info->xm; ++i) {
    for(alpha = 0; alpha < 6; ++alpha) {
      n[i].v[alpha] = 0.0;
      // TODO: Convolution FFT
      for(sp = 0; sp < options->numSpecies; ++sp) {
        for(j = i-s[sp]-1, k = 0; j < 0; ++j, ++k) {
          const PetscReal x[1] = {coords[i] + (k-s[sp]-1)*h};
          rhos[sp][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
        }
        for(j = PetscMax(i-s[sp]-1, 0); j <= PetscMin(i+s[sp]+1, info->mx-1); ++j, ++k) {
          rhos[sp][k] = rho[j].v[sp];
        }
        for(j = info->mx; j <= i+s[sp]+1; ++j, ++k) {
          const PetscReal x[1] = {coords[i] + (k-s[sp]-1)*h};
          rhos[sp][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
        }
        for(k = 0; k < 2*s[sp]+3; ++k) {
          n[i].v[alpha] += rhos[sp][k]*(*options->WFunc[alpha])(&diffs[sp][k], sp)*weights[sp][k];
          if (PetscIsInfOrNanScalar(n[i].v[alpha])) {SETERRQ(PETSC_ERR_FP, "Invalid floating point value");};
        }
      }
    }
  }/*
  if(options->debug>0){
	  for(alpha = 0; alpha < 6; ++alpha) {
		for(i = info->xs; i < info->xs+info->xm;++i){
			printf("Dist(nm): %g  n[%d].v[%d]: %g\n",(i*options->L/1200),i,alpha,n[i].v[alpha]);
		}
	  }
  }*/
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN_2d"
// Check: Put in rho = 1 and should get integrals of Ws
PetscErrorCode CalculateN_2d(DALocalInfo *info, Rho *rho[], N *n[], void *ctx)
{
  Options        *options = (Options *) ctx;
  PetscInt       *s       = options->convS;
  PetscReal    ***rhos    = (PetscReal ***) options->convArg;
  PetscReal    ***weights = (PetscReal ***) options->convWts;
  PetscReal    ***diffs   = (PetscReal ***) options->convDiffs2;
  DA              coordDA;
  Vec             coordinates;
  DACoor2d      **coords;
  PetscReal       h;
  PetscInt        i, j, k, l, a, b, alpha, sp;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  ierr  = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr  = DAGetGhostedCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr  = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  h     = coords[info->ys][info->xs+1].x - coords[info->ys][info->xs].x;
  for(j = info->ys; j < info->ys+info->ym; ++j) {
    for(i = info->xs; i < info->xs+info->xm; ++i) {
      for(alpha = 0; alpha < 6; ++alpha) {
        n[j][i].v[alpha] = 0.0;
        // TODO: Convolution FFT
        for(sp = 0; sp < options->numSpecies; ++sp) {
          for(b = j-s[sp]-1, l = 0; b < 0; ++b, ++l) {
            for(a = i-s[sp]-1, k = 0; a < 0; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
            for(a = PetscMax(i-s[sp]-1, 0); a <= PetscMin(i+s[sp]+1, info->mx-1); ++a, ++k) {
              rhos[sp][l][k] = rho[b][a].v[sp];
            }
            for(a = info->mx; a <= i+s[sp]+1; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
          }
          for(b = PetscMax(j-s[sp]-1, 0); b <= PetscMin(j+s[sp]+1, info->my-1); ++b, ++l) {
            for(a = i-s[sp]-1, k = 0; a < 0; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
            for(a = PetscMax(i-s[sp]-1, 0); a <= PetscMin(i+s[sp]+1, info->mx-1); ++a, ++k) {
              rhos[sp][l][k] = rho[b][a].v[sp];
            }
            for(a = info->mx; a <= i+s[sp]+1; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
          }
          for(b = info->my; b <= j+s[sp]+1; ++b, ++l) {
            for(a = i-s[sp]-1, k = 0; a < 0; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
            for(a = PetscMax(i-s[sp]-1, 0); a <= PetscMin(i+s[sp]+1, info->mx-1); ++a, ++k) {
              rhos[sp][l][k] = rho[b][a].v[sp];
            }
            for(a = info->mx; a <= i+s[sp]+1; ++a, ++k) {
              const PetscReal x[2] = {coords[j][i].x + (k-s[sp]-1)*h, coords[j][i].y + (l-s[sp]-1)*h};
              rhos[sp][l][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
            }
          }
          for(l = 0; l < 2*s[sp]+3; ++l) {
            for(k = 0; k < 2*s[sp]+3; ++k) {
              n[j][i].v[alpha] += rhos[sp][l][k]*(*options->WFunc[alpha])(&diffs[sp][l][k], sp)*weights[sp][l][k];
              if (PetscIsInfOrNanScalar(n[j][i].v[alpha])) {SETERRQ(PETSC_ERR_FP, "Invalid floating point value");};
            }
          }
        }
      }
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHS"
PetscErrorCode CalculateMuHS(DALocalInfo *info, N n[], Rho muHS[], void *ctx)
{
  Options        *options = (Options *) ctx;
  const PetscReal kT      = options->kT;
  PetscInt       *s       = options->convS;
  PetscReal     **PhiDers = options->convArg;
  PetscReal     **weights = options->convWts;
  PetscReal     **diffs   = options->convDiffs;
  DA              coordDA;
  Vec             coordinates;
  PetscReal      *coords;
  PetscReal       h;
  PetscInt        i, j, k, alpha, sp;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  ierr  = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr  = DAGetGhostedCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr  = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  h     = coords[info->xs+1] - coords[info->xs];
  for(i = info->xs; i < info->xs+info->xm; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      muHS[i].v[sp] = 0.0;
      for(alpha = 0; alpha < 6; ++alpha) {
        // TODO: Convolution FFT
        for(j = i-s[sp]-1, k = 0; j < 0; ++j, ++k) {
          // This might be wrong
          PhiDers[sp][k] = (*options->PhiDerFunc[alpha])(n[0].v, sp);
        }
        for(j = PetscMax(i-s[sp]-1, 0); j <= PetscMin(i+s[sp]+1, info->mx-1); ++j, ++k) {
          PhiDers[sp][k] = (*options->PhiDerFunc[alpha])(n[j].v, sp);
        }
        for(j = info->mx; j <= i+s[sp]+1; ++j, ++k) {
          // This might be wrong
          PhiDers[sp][k] = (*options->PhiDerFunc[alpha])(n[info->mx-1].v, sp);
        }
        for(k = 0; k < 2*s[sp]+3; ++k) {
          muHS[i].v[sp] += PhiDers[sp][k]*(*options->WFunc[alpha])(&diffs[sp][k], sp)*weights[sp][k];
          if (PetscIsInfOrNanScalar(muHS[i].v[sp])) {SETERRQ(PETSC_ERR_FP, "Invalid floating point value");};
        }
      }
      muHS[i].v[sp] *= kT;
    }
  }
  if(options->debug>0){
	  for(sp = 0; sp < options->numSpecies; ++sp) {
		for(i = info->xs; i < info->xs+info->xm;++i){
			printf("Dist(nm): %g  muHS[%d].v[%d]: %g\n",(i*options->L/1200),i,sp,muHS[i].v[sp]);
		}
	  }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Rhs_1d"
PetscErrorCode Rhs_1d(DALocalInfo *info, Rho x[], Rho f[], void *ctx)
{
  Options         *options  = (Options *) ctx;
  const PetscReal *rhoBath  = options->rhoBath;
  const PetscReal *muHSBath = options->muHSBath;
  const PetscReal  kT       = options->kT;
  const PetscInt   debug    = options->debug;
  DA               coordDA;
  Vec              coordinates, nVec, muHSVec;
  PetscReal       *coords;
  N               *n;
  Rho             *muHS, *muExt;
  PetscInt         ie = info->xs+info->xm, i, sp;
  PetscErrorCode   ierr;

  PetscFunctionBegin;
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetGhostedCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DAVecGetArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DAVecGetArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  ierr = DAVecGetArray(info->da, options->muExt, &muExt);CHKERRQ(ierr);
  ierr = CalculateN_1d(info, x, n, ctx);CHKERRQ(ierr);
  //if (debug) {ierr = VecView(nVec, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
  if (debug > 1) {
    ierr = PetscPrintf(options->comm, "n:\n");CHKERRQ(ierr);
    ierr = VecView(nVec, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = CalculateMuHS(info, n, muHS, ctx);CHKERRQ(ierr);
  //if (debug) {ierr = VecView(muHSVec, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
  if (debug > 1) {
    ierr = PetscPrintf(options->comm, "\\mu_{HS}:\n");CHKERRQ(ierr);
    ierr = VecView(muHSVec, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  for(i = info->xs; i < ie; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      if (debug) {ierr = PetscPrintf(options->comm, "[%d]Species %d \\mu^{bath}_{HS}: %g \\mu_{HS}: %g \\mu_{ext}: %g arg: %g\n", i, sp, muHSBath[sp], muHS[i].v[sp], muExt[i].v[sp], (muHSBath[sp] - muHS[i].v[sp] - muExt[i].v[sp])/kT);CHKERRQ(ierr);}
      f[i].v[sp] = x[i].v[sp] - rhoBath[sp]*PetscExpScalar((muHSBath[sp] - muHS[i].v[sp] - muExt[i].v[sp])/kT);
      if (PetscIsInfOrNanScalar(f[i].v[sp])) {SETERRQ(PETSC_ERR_FP, "Invalid floating point value");};
    }
  }
  ierr = DAVecRestoreArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(info->da, options->muExt, &muExt);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

// TODO: Create the Jacobian diagonal at least
#undef __FUNCT__
#define __FUNCT__ "Jac_1d"
PetscErrorCode Jac_1d(DALocalInfo *info, PetscScalar x[], Mat J, void *ctx)
{
  DA             coordDA;
  Vec            coordinates;
  PetscReal     *coords;
  MatStencil     row, col[3];
  PetscScalar    v[3];
  PetscInt       ie = info->xs+info->xm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetCoordinateDA(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DAGetGhostedCoordinates(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(int i = info->xs; i < ie; ++i) {
    row.i = i;
    if (i == 0 || i == info->mx-1) {
      v[0] = 1.0;
      ierr = MatSetValuesStencil(J, 1, &row, 1, &row, v, INSERT_VALUES);CHKERRQ(ierr);
    } else {
      col[0].i = i;
      v[0] = 1.0;
      ierr = MatSetValuesStencil(J, 1, &row, 1, col, v, INSERT_VALUES);CHKERRQ(ierr);
    }
  }
  ierr = DAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CreateExternalPotential"
PetscErrorCode CreateExternalPotential(DM dm, Options *options)
{
  DA             da  = (DA) dm;
  const int      dim = options->dim;
  PetscScalar  (*func)(const double *, const int) = options->func;
  Vec            X, U;
  PetscTruth     flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetGlobalVector(da, &X);CHKERRQ(ierr);
  ierr = DACreateGlobalVector(da, &options->muExt);CHKERRQ(ierr);
  options->func = options->muExtFunc;
  U             = options->muExt;
  if (dim == 1) {
    ierr = DAFormFunctionLocal(da, (DALocalFunction1) Function_1d, X, U, (void *) options);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DAFormFunctionLocal(da, (DALocalFunction1) Function_2d, X, U, (void *) options);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  options->func = func;
  ierr = DARestoreGlobalVector(da, &X);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
  if (flag) {ierr = VecView(U, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view_draw", &flag);CHKERRQ(ierr);
  if (flag) {ierr = VecView(U, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckN"
PetscErrorCode CheckN(DM dm, Options *options)
{
  DA             da  = (DA) dm;
  DALocalInfo    info;
  PetscScalar  (*muExtFunc)(const double [], const int);
  Vec            rho, n;
  PetscReal      tol = options->nCheckTol;
  PetscScalar   *rhoBath;
  Rho           *rhoArray;
  N             *nArray;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetGlobalVector(da, &rho);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(options->nDA, &n);CHKERRQ(ierr);
  ierr = VecSet(rho, 1.0);CHKERRQ(ierr);
  ierr = DAVecGetArray(da, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DAVecGetArray(options->nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DAGetLocalInfo(options->nDA, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(options->numSpecies*sizeof(PetscScalar), &rhoBath);CHKERRQ(ierr);
  muExtFunc              = options->muExtFunc;
  options->muExtFunc     = zero;
  for(sp = 0; sp < options->numSpecies; ++sp) {
    rhoBath[sp]          = options->rhoBath[sp];
    options->rhoBath[sp] = 1.0;
  }
  ierr = CalculateN_1d(&info, rhoArray, nArray, options);CHKERRQ(ierr);
  options->muExtFunc  = muExtFunc;
  for(sp = 0; sp < options->numSpecies; ++sp) {
    options->rhoBath[sp] = rhoBath[sp];
  }
  for(i = info.xs; i < info.xs+info.xm; ++i) {
    PetscScalar value;

    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 1.0;}
    if (fabs(nArray[i].v[0] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_0[%d] integral %g should be %g", i, nArray[i].v[0], value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += options->R[sp];}
    if (fabs(nArray[i].v[1] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_1[%d] integral %g should be %g", i, nArray[i].v[1], value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 4.0*M_PI*options->R[sp]*options->R[sp];}
    if (fabs(nArray[i].v[2] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_2[%d] integral %g should be %g", i, nArray[i].v[2], value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 4.0*M_PI*options->R[sp]*options->R[sp]*options->R[sp]/3.0;}
    if (fabs(nArray[i].v[3] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_3[%d] integral %g should be %g", i, nArray[i].v[3], value);
    value = 0.0;
    if (fabs(nArray[i].v[4] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_4[%d] integral %g should be %g", i, nArray[i].v[4], value);
    value = 0.0;
    if (fabs(nArray[i].v[5] - value) > tol) SETERRQ3(PETSC_ERR_PLIB, "Invalid W_5[%d] integral %g should be %g", i, nArray[i].v[5], value);
  }
  ierr = PetscFree(rhoBath);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(options->nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(da, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(da, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckResidual"
PetscErrorCode CheckResidual(DM dm, Vec sol, Options *options)
{
  DA             da = (DA) dm;
  Vec            residual;
  MPI_Comm       comm;
  const char    *name;
  PetscScalar    norm;
  PetscTruth     flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) da, &comm);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(da, &residual);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) residual, "residual");CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DAFormFunctionLocal(da, (DALocalFunction1) Rhs_1d, sol, residual, (void *) options);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = VecNorm(residual, NORM_2, &norm);CHKERRQ(ierr);
  if (flag) {ierr = VecView(residual, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
  ierr = DARestoreGlobalVector(da, &residual);CHKERRQ(ierr);
  ierr = PetscObjectGetName((PetscObject) sol, &name);CHKERRQ(ierr);
  PetscPrintf(comm, "Residual for trial solution %s: %g\n", name, norm);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateSolver"
PetscErrorCode CreateSolver(DM dm, DMMG **dmmg, Options *options)
{
  MPI_Comm       comm;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMMGCreate(comm, 1, options, dmmg);CHKERRQ(ierr);
  ierr = DMMGSetDM(*dmmg, dm);CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DMMGSetSNESLocal(*dmmg, Rhs_1d, Jac_1d, 0, 0);CHKERRQ(ierr);
  } else {
    SETERRQ1(PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  for(int l = 1; l < DMMGGetLevels(*dmmg); ++l) {
    ierr = DASetUniformCoordinates((DA) (*dmmg)[l]->dm, 0.0, options->L, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  }
  /* if (options->bcType == NEUMANN) {
     // With Neumann conditions, we tell DMMG that constants are in the null space of the operator
       ierr = DMMGSetNullSpace(*dmmg, PETSC_TRUE, 0, PETSC_NULL);CHKERRQ(ierr);
  } */
  ierr = DMMGSetFromOptions(*dmmg);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateInitialGuess"
PetscErrorCode CreateInitialGuess(DMMG *dmmg, Options *options)
{
  DA             da = (DA) DMMGGetFine(dmmg)->dm;
  DALocalInfo    info;
  PetscScalar   *rho;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetLocalInfo(da, &info);CHKERRQ(ierr);
  ierr = DAVecGetArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  for(i = info.xs; i < info.xs+info.xm; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      rho[i] = options->rhoBath[sp];
    }
  }
  ierr = DAVecRestoreArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Solve"
PetscErrorCode Solve(DMMG *dmmg, Options *options)
{
  SNES                snes;
  MPI_Comm            comm;
  PetscInt            its;
  PetscTruth          flag;
  SNESConvergedReason reason;
  PetscErrorCode      ierr;

  PetscFunctionBegin;
  ierr = DMMGSolve(dmmg);CHKERRQ(ierr);
  snes = DMMGGetSNES(dmmg);
  ierr = SNESGetIterationNumber(snes, &its);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes, &reason);CHKERRQ(ierr);
  ierr = PetscObjectGetComm((PetscObject) snes, &comm);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Number of nonlinear iterations = %D\n", its);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Reason for solver termination: %s\n", SNESConvergedReasons[reason]);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
  if (flag) {ierr = VecView(DMMGGetx(dmmg), PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view_draw", &flag);CHKERRQ(ierr);
  if (flag && options->dim < 3) {ierr = VecView(DMMGGetx(dmmg), PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
#if 0
  if (DMMGGetLevels(dmmg) == 1) {ierr = CheckError(DMMGGetDM(dmmg), DMMGGetx(dmmg), options);CHKERRQ(ierr);}
#endif
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintReport"
PetscErrorCode PrintReport(Options *options)
{
  MPI_Comm       comm = options->comm;
  PetscInt       i;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscPrintf(comm, "Simulation Input\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter      Description                        Units          Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "----------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "L:            System Length                      (nm):          %g\n", options->L);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "x_{wall}:     Wall position                      (nm):          %g\n", options->wallPos);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "kT:           Boltzmann's constant * Temperature (kg nm^2/s^2): %g\n", options->kT);CHKERRQ(ierr);
  for(i = 0; i < options->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "Species %d:\n", i);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  R:          Ion Radius                         (nm):          %g\n", options->R[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  rho_{bath}: Density of ions in bath            (nm^{-3}):     %g\n", options->rhoBath[i]);CHKERRQ(ierr);
  }
  ierr = PetscPrintf(comm, "\nDerived Quantities\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter       Description                                                   Units Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "-----------------------------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "P_{HS}:         Pressure due to hard sphere interaction in the bath           (nm): %g\n\n", options->presHS);CHKERRQ(ierr);
  for(i = 0; i < options->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "Species %d:\n", i);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  eta:          Volume fraction of ions                                       None: %g\n", options->eta[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  mu_{HS,bath}: Chemical potential due to hard sphere interaction in the bath (nm): %g\n", options->muHSBath[i]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckSumRule"
PetscErrorCode CheckSumRule(DMMG *dmmg, Options *options)
{
  DA             da = (DA) DMMGGetFine(dmmg)->dm;
  PetscInt       M, i, sp;
  Rho           *rho;
  PetscReal      x, frac, rhoP, error;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr  = DAGetInfo(da, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr  = DAVecGetArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  for(sp = 0, rhoP = 0.0; sp < options->numSpecies; ++sp) {
    x     = (options->wallPos+options->R[sp])*(M-1)/options->L;
    // We will linearly extrapolate
    i     = (PetscInt) x + 1;
    frac  = x - i;
    rhoP += options->kT*((1.0 - frac)*rho[i].v[sp] + frac*rho[i+1].v[sp]);
  }
  error = options->presHS - rhoP;
  ierr  = DAVecRestoreArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  if (fabs(error)/options->presHS > 0.1) {
    SETERRQ3(PETSC_ERR_PLIB, "Sum rule violation, P_{HS} %g kT rho %g relative error %g", options->presHS, rhoP, fabs(error)/options->presHS);
  } else {
    PetscPrintf(options->comm, "Sum rule verification\n");
    PetscPrintf(options->comm, "                       P_{HS}          %g\n", options->presHS);
    PetscPrintf(options->comm, "                       kT rho          %g\n", rhoP);
    PetscPrintf(options->comm, "                       relative error  %g\n", fabs(error)/options->presHS);
    PetscPrintf(options->comm, "                       variable number %d\n", i);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckEta"
PetscErrorCode CheckEta(DMMG *dmmg, Options *options)
{
  DA             da = (DA) DMMGGetFine(dmmg)->dm;
  DALocalInfo    info;
  Vec            nVec;
  N             *n;
  Rho           *rho;
  PetscInt       M, i;
  PetscReal      n_3, eta, error, *phiDerArray;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DAGetInfo(da, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DAVecGetArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  ierr = DAVecGetArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DAGetLocalInfo(options->nDA, &info);CHKERRQ(ierr);
  ierr = CalculateN_1d(&info, rho, n, options);CHKERRQ(ierr);
  n_3  = n[M-1].v[3];
  if(options->debug > 0){
  //ierr = CalculatePhiDer_1d(&info, n, options);CHKERRQ(ierr);
	for(int alpha = 0; alpha < 6; ++alpha) {
		for(i = 0; i < M;++i){
			printf("Dist(nm): %g \t n[%d].v[%d]: %g\n",(i*options->L/1200),i,alpha,n[i].v[alpha]);
		}

		/*ierr = DAVecGetArray(da,options->PhiDer[alpha],&phiDerArray);CHKERRQ(ierr);
		for(i = info->xs; i < info->xs+info->xm;++i){
			printf("phiDer_%d[%d]: %g\n",alpha,i,phiDerArray[i]);
		}
		ierr = DAVecRestoreArray(da,options->PhiDer[alpha],&phiDerArray);CHKERRQ(ierr);*/
	}
	for(int sp = 0; sp < options->numSpecies; ++sp){
		for(i = 0; i < M;++i){
			printf("Sp:%d Rho[%d]: %g\n",sp,i,rho[i].v[sp]);
		}
	}
  }
  ierr = DAVecRestoreArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(da, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  for(i = 0, eta = 0.0; i < options->numSpecies; ++i) {eta += options->eta[i];}
  error = eta - n_3;
  if (fabs(error)/eta > options->volFracTol) {
    SETERRQ3(PETSC_ERR_PLIB, "Volume fraction violation, eta %g n_3 %g relative error %g", eta, n_3, fabs(error)/eta);
  } else {
    PetscPrintf(options->comm, "Volume fraction verification\n");
    PetscPrintf(options->comm, "                       eta             %g\n", eta);
    PetscPrintf(options->comm, "                       n_3             %g\n", n_3);
    PetscPrintf(options->comm, "                       relative error  %g\n", fabs(error)/eta);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  MPI_Comm       comm;
  Options        options;
  DM             dm;
  DMMG          *dmmg;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscInitialize(&argc, &argv, (char *) 0, help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  globalOptions = &options;
  ierr = ProcessOptions(comm, &options);CHKERRQ(ierr);
  ierr = CreateMesh(comm, &dm, &options);CHKERRQ(ierr);
  ierr = CreateParameterMesh(dm, &options);CHKERRQ(ierr);
  ierr = CreateExternalPotential(dm, &options);CHKERRQ(ierr);
  ierr = CheckN(dm, &options);CHKERRQ(ierr);
  ierr = CreateSolver(dm, &dmmg, &options);CHKERRQ(ierr);
  // Continuation loop in bath density
  if (options.numRhoBath > 1) {
    PetscReal *rhoBathInc;

    ierr = PetscMalloc(options.numSpecies * sizeof(PetscReal), &rhoBathInc);CHKERRQ(ierr);
    for(int sp = 0; sp < options.numSpecies; ++sp) {
      rhoBathInc[sp]      = (options.rhoBath[sp] - options.rhoBathInit[sp])/(options.numRhoBath - 1);
      options.rhoBath[sp] = options.rhoBathInit[sp];
    }
    ierr = CreateInitialGuess(dmmg, &options);CHKERRQ(ierr);
    for(int rb = 0; rb < options.numRhoBath; ++rb) {
      ierr = CalculateMuHSBath(&options, options.muHSBath);CHKERRQ(ierr);
      ierr = Solve(dmmg, &options);CHKERRQ(ierr);
      if (rb < options.numRhoBath - 1) {
        for(int sp = 0; sp < options.numSpecies; ++sp) {
          options.rhoBath[sp] += rhoBathInc[sp];
        }
      }
    }
    ierr = PetscFree(rhoBathInc);CHKERRQ(ierr);
  } else {
    ierr = CreateInitialGuess(dmmg, &options);CHKERRQ(ierr);
    ierr = Solve(dmmg, &options);CHKERRQ(ierr);
    ierr = Solve(dmmg, &options);CHKERRQ(ierr);
  }
  ierr = PrintReport(&options);CHKERRQ(ierr);
  ierr = CheckSumRule(dmmg, &options);CHKERRQ(ierr);
  ierr = CheckEta(dmmg, &options);CHKERRQ(ierr);
  ierr = DMMGDestroy(dmmg);CHKERRQ(ierr);
  ierr = DestroyParameters(&options);CHKERRQ(ierr);
  ierr = DestroyMesh(dm, &options);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
