One way to generate a callgraph is using gcc, GraphViz and Egypt.
Egypt is a Perl script that uses gcc and GraphViz: http://www.gson.org/egypt/egypt.html

Egypt relies on gcc's capability to generate intermediate info during compilation and to dump
it into an RTL file when supplied with a '-dr' switch.  That intermediate file (or files)
is then used with one of GraphViz utilities to lay out the graph.
For example:

make PETSC_ARCH=${PETSC_ARCH} EXTRA_FLAGS="-dr" dft-fft-3d
egypt dft-fft-3d.c.00.expand  | dot -Gsize=8.5,11 -Grankdir=LR -Tps -o dft-fft-3d-callgraph.ps

will make the callgraph for dft-fft-3d (plus the pesky C++ complex scalar stuff).
