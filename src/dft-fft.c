static char help[] = "This example solves the DFT for hard sphere.\n\n";
/*
  R_i:             Radius of species i
  \rho^i(x):       Density of species i
  \mu^i_{HS}(x):   Chemical potential of species i due to hard sphere interaction
  \mu^i_{ext}(x):  Chemical potential of species i due to external forces
  \rho^i_{bath}:   Density of species i in the bath
  \mu^i_{HS,bath}: Chemical potential of species i due to hard sphere interaction in the bath

  \rho^i(x) = \rho^i_{bath} \exp{(\mu^i_{HS,bath} - \mu^i_{HS}[{\rho^k(x')}; x] - \mu^i_{ext}(x))/kT}

  \mu^i_{ext}(x) = \infty     inside barrier
                 = 0         outside barrier

  \Phi_{HS}(x) = -n_0 \ln(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over 1 - n_3} + {n^3_2 \over 24\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3

  \mu^i_{HS}(x) = kT \sum_\alpha \int^{x+R_i}_{x-R_i} {\partial\Phi_{HS} \over \partial n_\alpha}(x') W^i_\alpha(x - x') dx'

  n_\alpha(x) = \sum_i \int^{x+R_i}_{x-R_i} \rho_i(x') W^i_\alpha(x' - x) dx'

  {\partial\Phi_{HS}}{\partial n_0} = -\ln(1 - n_3)

  {\partial\Phi_{HS}}{\partial n_1} = {n_2 \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_2} = {n_1 \over 1 - n_3} + {n^2_2 \over 8\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^3 + {n^2_{V2} \over 4\pi n_2 (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

  {\partial\Phi_{HS}}{\partial n_3} = n_0/(1 - n_3) + {n_1 n_2 - n_{V1} n_{V2} \over (1 - n_3)^2} + {n^3_2 \over 12\pi (1 - n_3)^3} (1 - {n^2_{V2}\over n^2_2})^3

  {\partial\Phi_{HS}}{\partial n_{V1}} = {-n_{V2} \over 1 - n_3}

  {\partial\Phi_{HS}}{\partial n_{V2}} = {-n_{V1} \over 1 - n_3} - {n_2 n_{V2} \over 4\pi (1 - n_3)^2} (1 - {n^2_{V2}\over n^2_2})^2

Sum Rule for Hard Spheres:

  P_{HS} = kT \sum_i \rho^i(wallPos + R_i)

where the pressure is

  P_{HS} = kT {6\over\pi} ({\xi_0\over\Delta} + {3\xi_1\xi_2\over\Delta^2} + {3\xi^3_2\over\Delta^3})

  \Delta = 1 - \xi_3

  \xi_n = {\pi\over6} \sum_i \rho^i (2 R_i)^n
*/
#include <petsc.h>

typedef PetscErrorCode (*DMDALocalFunction1)(DMDALocalInfo*,void*,void*,void*);

#undef __FUNCT__
#define __FUNCT__ "DMDAComputeFunctionLocal"
/*@C 
   DMDAComputeFunctionLocal - This is a universal function evaluation routine for a local DM function.

   Collective on DMDA

   Input Parameters:
+  dm - the DM context
.  func - The local function
.  X - input vector
.  F - function vector
-  ctx - A user context

   Level: intermediate

.seealso: DMDASetLocalFunction(), DMDASetLocalJacobian(), DMDASetLocalAdicFunction(), DMDASetLocalAdicMFFunction(),
          SNESSetFunction(), SNESSetJacobian()
@*/
static PetscErrorCode DMDAComputeFunctionLocal(DM dm, DMDALocalFunction1 func, Vec X, Vec F, void *ctx)
{
  Vec            localX;
  DMDALocalInfo  info;
  void           *u;
  void           *fu;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetLocalVector(dm, &localX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm, X, INSERT_VALUES, localX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(dm, X, INSERT_VALUES, localX);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(dm, localX, &u);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayDOF(dm, F, &fu);CHKERRQ(ierr);
  ierr = (*func)(&info, u, fu, ctx);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayDOF(dm, localX, &u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArrayDOF(dm, F, &fu);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm, &localX);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

PetscInt debug = 3;

#undef __FUNCT__
#define __FUNCT__ "PrintNorm_Private"
PetscErrorCode PrintNorm_Private(Vec v, const char name[], PetscInt i, PetscInt j)
{
  MPI_Comm       comm;
  PetscReal      infNorm, norm2;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) v, &comm);CHKERRQ(ierr);
  ierr = VecNorm(v, NORM_INFINITY, &infNorm);CHKERRQ(ierr);
  ierr = VecNorm(v, NORM_2, &norm2);CHKERRQ(ierr);
  if (i < 0) {
    ierr = PetscPrintf(comm, "Norm of %s: infinity %g l2 %g\n", name, infNorm, norm2);
  } else {
    if (j < 0) {
      ierr = PetscPrintf(comm, "Norm of %s[%d]: infinity %g l2 %g\n", name, i, infNorm, norm2);
    } else {
      ierr = PetscPrintf(comm, "Norm of %s[%d,%d]: infinity %g l2 %g\n", name, i, j, infNorm, norm2);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintNorm"
PetscErrorCode PrintNorm(Vec v, const char name[], PetscInt i, PetscInt j)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  if (debug < 3) PetscFunctionReturn(0);
  ierr = PrintNorm_Private(v, name, i, j);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

typedef struct {
  MPI_Comm      comm;
  PetscInt      debug;      // Debugging level
  PetscInt      dim;        // Topological mesh dimension
  PetscInt      numSpecies; // Number of ion species
  PetscInt      numAlpha;   // Number of weight functions
  PetscReal     L;          // System length [nm]
  PetscReal    *R;          // Radius of species i [nm]
  PetscReal    *rhoBath;    // Density of species i in the bath [nm^{-3}]
  PetscReal    *muHSBath;   // Chemical potential of species i due to hard sphere interaction in the bath
  PetscReal     presHS;     // Pressure of species i due to hard sphere interaction in the bath
  PetscReal     kT;         // Boltzmann's constant * Temperature [J]
  PetscReal     wallPos;    // Position of the wall [nm]
  PetscReal    *eta;        // Volume fraction of ions
  DM            nDA;        // DA for the n functionals
  DM            singleDA;   // DA for single fields
  PetscReal     nCheckTol;  // Tolerance for n convolution check (should be calculated from h)
  PetscReal     volFracTol; // Tolerance for volume fraction check (should be calculated from h)
  Vec           muExt;      // Chemical potential due to external forces
  Mat           F;          // DFT for the DA
  Vec          *n;          // n^\alpha functions
  Vec          *W;          // W^\alpha window functions
  Vec          *PhiDer;     // {\partial\Phi_{HS}}{\partial n_\alpha} functions
  Vec           rhoHat;     // Fourier transform of density
  Vec           error;      // Error in density
  PetscInt     *convS;      // Storage for the convolution integration domain half-length s
  PetscReal   **convArg;    // Storage for the convolution argument values (\rho or PhiDer)
  PetscReal   **convWts;    // Storage for the convolution weights (mostly h)
  PetscReal   **convDiffs;  // Storage for the convolution coordinate differences
  PetscReal   **convDiffs2; // Storage for the convolution reverse coordinate differences
  PetscScalar (*muExtFunc)(const PetscScalar [], const int);   // Function defining the external potential
  PetscScalar (**WFunc)(const PetscScalar [], const int);      // Functions W
  PetscScalar (**PhiDerFunc)(const PetscScalar [], const int); // Functions {\partial\Phi_{HS}}{\partial n_\alpha}
  PetscScalar (*func)(const PetscScalar [], const int);        // The function to project
} Options;

typedef struct {
  PetscScalar v[6];
} N;

typedef struct {
  PetscScalar v[NUM_SPECIES];
} Rho;

Options *globalOptions;

PetscScalar zero(const PetscScalar x[], const int species) {
  return 0.0;
}

PetscScalar wall(const PetscScalar x[], const int species) {
  if (PetscRealPart(x[0]) <= globalOptions->wallPos+globalOptions->R[species])
    return 100.0*globalOptions->kT;
  return 0.0;
}

PetscScalar W_0(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return 1.0/(2.0*globalOptions->R[species]);
  return 0.0;
}

PetscScalar W_1(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return 0.5;
  return 0.0;
}

PetscScalar W_2(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return 2.0*M_PI*globalOptions->R[species];
  return 0.0;
}

PetscScalar W_3(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return M_PI*(globalOptions->R[species]*globalOptions->R[species] - PetscRealPart(x[0])*PetscRealPart(x[0]));
  return 0.0;
}

PetscScalar W_V1(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return 0.5*PetscRealPart(x[0])/globalOptions->R[species];
  return 0.0;
}

PetscScalar W_V2(const PetscScalar x[], const int species) {
  if ((PetscRealPart(x[0]) >= -globalOptions->R[species]) && (PetscRealPart(x[0]) <= globalOptions->R[species]))
    return 2.0*M_PI*PetscRealPart(x[0]);
  return 0.0;
}

PetscScalar PhiDer_0(const PetscScalar n[], const int species) {
  return -log(1.0 - PetscRealPart(n[3]));
}

PetscScalar PhiDer_1(const PetscScalar n[], const int species) {
  return PetscRealPart(n[2])/(1.0 - PetscRealPart(n[3]));
}

PetscScalar PhiDer_2(const PetscScalar n[], const int species) {
  PetscScalar ratio = ((PetscAbsReal(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16) && (PetscAbsReal(PetscSqr(PetscRealPart(n[5]))) < 1.0e-16)) ? 0.0 : PetscSqr(PetscRealPart(n[5]))/PetscSqr(PetscRealPart(n[2]));
  const PetscScalar term  = 1.0 - ratio;
  if (PetscRealPart(ratio) > 1.0) {
    PetscPrintf(PETSC_COMM_SELF, "n_2: %g |n_V2|^2: %g |n_V2|^2/n^2_2: %g\n", PetscRealPart(n[2]), PetscSqr(PetscRealPart(n[5])), PetscRealPart(ratio));
    ratio = 1.0;
  }
  return (PetscRealPart(n[1])/(1.0 - PetscRealPart(n[3])))+((PetscSqr(PetscRealPart(n[2]))/(8.0*M_PI*PetscSqr(1.0 - PetscRealPart(n[3]))))*term*term*term)+((PetscRealPart(n[5])*PetscRealPart(n[5])/(4.0*M_PI*PetscSqr(1.0 - PetscRealPart(n[3]))))*PetscSqr(term));
}

PetscScalar PhiDer_3(const PetscScalar n[], const int species) {
  const PetscScalar ratio = ((PetscAbsReal(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16) && (PetscAbsReal(PetscSqr(PetscRealPart(n[5]))) < 1.0e-16)) ? 0.0 : PetscSqr(PetscRealPart(n[5]))/PetscSqr(PetscRealPart(n[2]));
  const PetscScalar term  = 1.0 - ratio;
  return PetscRealPart(n[0])/(1.0 - PetscRealPart(n[3])) +
    (PetscRealPart(n[1])*PetscRealPart(n[2]) - PetscRealPart(n[4])*PetscRealPart(n[5]))/PetscSqr(1.0 - PetscRealPart(n[3])) +
    (PetscRealPart(n[2])*PetscRealPart(n[2])*PetscRealPart(n[2])/(12.0*M_PI*(1.0 - PetscRealPart(n[3]))*(1.0 - PetscRealPart(n[3]))*(1.0 - PetscRealPart(n[3]))))*term*term*term;
}

PetscScalar PhiDer_V1(const PetscScalar n[], const int species) {
  return -PetscRealPart(n[5])/(1.0 - PetscRealPart(n[3]));
}

PetscScalar PhiDer_V2(const PetscScalar n[], const int species) {
  const PetscScalar ratio = ((PetscAbsReal(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16) && (PetscAbsReal(PetscSqr(PetscRealPart(n[5]))) < 1.0e-16)) ? 0.0 : PetscSqr(PetscRealPart(n[5]))/PetscSqr(PetscRealPart(n[2]));
  const PetscScalar term  = 1.0 - ratio;
  return (-PetscRealPart(n[4])/(1.0 - PetscRealPart(n[3]))) - (PetscRealPart(n[2])*PetscRealPart(n[5])/(4.0*M_PI*PetscSqr(1.0 - PetscRealPart(n[3]))))*PetscSqr(term);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHSBath"
static PetscErrorCode CalculateMuHSBath(Options *options, PetscReal *muHSBath)
{
  PetscReal xi_0 = 0.0, xi_1 = 0.0, xi_2 = 0.0, xi_3, Delta;
  PetscInt  i;

  PetscFunctionBeginUser;
  for(i = 0; i < options->numSpecies; ++i) {
    const PetscReal sigma = 2.0*options->R[i];

    xi_0 += options->rhoBath[i];
    xi_1 += options->rhoBath[i]*sigma;
    xi_2 += options->rhoBath[i]*PetscSqr(sigma);
    xi_3 += options->rhoBath[i]*PetscSqr(sigma)*sigma;

  }
  xi_0 *= (M_PI/6.0);
  xi_1 *= (M_PI/6.0);
  xi_2 *= (M_PI/6.0);
  xi_3 *= (M_PI/6.0);
  Delta = 1.0 - xi_3;

  for(i = 0; i < options->numSpecies; ++i) {
    const PetscReal sigma = 2.0*options->R[i];

    muHSBath[i] = options->kT*(-log(Delta) +
                               (3.0*xi_2*sigma + 3.0*xi_1*PetscSqr(sigma))/Delta +
                               (9.0*PetscSqr(xi_2)*PetscSqr(sigma))/(2.0*PetscSqr(Delta)) +
                               (xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta))*PetscSqr(sigma)*sigma);
  }
  options->presHS = (6.0/M_PI)*options->kT*(xi_0/Delta + 3.0*xi_1*xi_2/PetscSqr(Delta) + 3.0*PetscSqr(xi_2)*xi_2/(PetscSqr(Delta)*Delta));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ProcessOptions"
static PetscErrorCode ProcessOptions(MPI_Comm comm, Options *options)
{
  PetscInt       n, i;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  options->comm       = comm;
  options->debug      = 0;
  options->dim        = 1;
  options->numSpecies = 1;
  options->numAlpha   = 6;
  options->L          = 10;       // 100 Angstrom system length
  options->kT         = 4.21e-3;  // 4.21e-21 [kg m^2/s^2]
  options->nCheckTol  = 1.0e-9;
  options->volFracTol = 1.0e-3;

  ierr = PetscOptionsBegin(comm, "", "DFT Problem Options", "DFT");CHKERRQ(ierr);
    ierr = PetscOptionsInt("-debug", "The debugging level", "dft.cxx", options->debug, &options->debug, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-dim", "The topological mesh dimension", "dft.cxx", options->dim, &options->dim, PETSC_NULL);CHKERRQ(ierr);
    switch (options->dim) {
    case 1: options->numAlpha = 6;break;
    case 3: options->numAlpha = 10;break;
    default: SETERRQ1(comm, PETSC_ERR_SUP, "Unsupported spatial dimension %d", options->dim);
    }
    ierr = PetscOptionsReal("-L", "The length of the domain (nm)", "dft.cxx", options->L, &options->L, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-wallPos", "Position of the wall", "dft.cxx", options->wallPos, &options->wallPos, &flag);CHKERRQ(ierr);
    if (!flag) {options->wallPos = options->L/10;}
    ierr = PetscOptionsReal("-kT", "Temperature in natural units", "dft.cxx", options->kT, &options->kT, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-nCheckTol", "Tolerance for n check", "dft.cxx", options->nCheckTol, &options->nCheckTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-volFracTol", "Tolerance for volume fraction check", "dft.cxx", options->volFracTol, &options->volFracTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-numSpecies", "The number of ion species", "dft.cxx", options->numSpecies, &options->numSpecies, PETSC_NULL);CHKERRQ(ierr);

    if (options->numSpecies != NUM_SPECIES) {
      SETERRQ1(comm, PETSC_ERR_SUP, "I hate this too, but you have to compile in the number of species, which is currently %d.", NUM_SPECIES);
    }
    ierr = PetscMalloc4(options->numSpecies,&options->R,
                        options->numSpecies,&options->rhoBath,
                        options->numSpecies,&options->muHSBath,
                        options->numSpecies,&options->eta);CHKERRQ(ierr);
    for(i = 0; i < options->numSpecies; ++i) {
      options->R[i]       = 0.1;      // 1 Angstrom ion radius
      options->rhoBath[i] = 6.0e-1;   // 6 10^{26} ions/m^3
    }

    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-R", "The radius of species i", "dft.cxx", options->R, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of radii given");}
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-rhoBath", "The density of species i in the bath", "dft.cxx", options->rhoBath, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != options->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");}
    n = options->numSpecies;
    ierr = PetscOptionsRealArray("-muHSBath", "The chemical potential of species i in the bath", "dft.cxx", options->muHSBath, &n, &flag);CHKERRQ(ierr);
    if (!flag) {
      ierr = CalculateMuHSBath(options, options->muHSBath);CHKERRQ(ierr);
    } else if (n != options->numSpecies) {
      SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of bath potentials given");
    }
  ierr = PetscOptionsEnd();
  /* Check packing fraction */
  for(i = 0; i < options->numSpecies; ++i) {
    options->eta[i] = options->rhoBath[i]*(4.0*M_PI/3.0)*(PetscSqr(options->R[i])*options->R[i]);
    if (options->eta[i] > 0.38) SETERRQ2(comm, PETSC_ERR_ARG_OUTOFRANGE, "Excessive volume fraction: %g for species %d", options->eta[i], i);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateConvolutionQuadtrature"
static PetscErrorCode CreateConvolutionQuadtrature(PetscInt sp, PetscReal h, Options *options)
{
  const PetscReal R     = options->R[sp];
  const PetscInt  s     = (PetscInt) (R/h);
  const PetscReal gamma = R - s*h;
  PetscInt        j, k;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  if (s < 5) SETERRQ1(options->comm, PETSC_ERR_ARG_OUTOFRANGE, "Insufficient resolution: %d", s);
  ierr = PetscPrintf(options->comm, "Points in convolution integration: %d for species %d\n", 2*s + 3, sp);CHKERRQ(ierr);
  options->convS[sp] = s;
  ierr = PetscMalloc4(2*s+3,&options->convArg[sp],2*s+3,&options->convWts[sp],2*s+3,&options->convDiffs[sp],2*s+3,&options->convDiffs2[sp]);CHKERRQ(ierr);
  // Calculate quadrature weights
  options->convWts[sp][0]        = 0.5*gamma*gamma/h;
  options->convWts[sp][1]        = gamma - 0.5*gamma*gamma/h + 0.5*h;
  for(k = 2; k < 2*s+1; ++k) options->convWts[sp][k] = h;
  options->convWts[sp][2*s+1]    = gamma - 0.5*gamma*gamma/h + 0.5*h;
  options->convWts[sp][2*s+2]    = 0.5*gamma*gamma/h;
  // Calculate coordinate differences (x - x')
  options->convDiffs[sp][0]      =  s*h + gamma;
  for(j = s, k = 1; j >= -s; --j, ++k) options->convDiffs[sp][k] = j*h;
  options->convDiffs[sp][2*s+2]  = -s*h - gamma;
  // Calculate reverse coordinate differences (x' - x)
  options->convDiffs2[sp][0]     = -s*h - gamma;
  for(j = -s, k = 1; j <= s; ++j, ++k) options->convDiffs2[sp][k] = j*h;
  options->convDiffs2[sp][2*s+2] =  s*h + gamma;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateMesh"
static PetscErrorCode CreateMesh(MPI_Comm comm, DM *dm, Options *options)
{
  DM             da;
  PetscInt       dof = options->numSpecies, M, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  if (options->dim == 1) {
    ierr = DMDACreate1d(comm, DM_BOUNDARY_NONE, -3, dof, 1, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 2) {
    ierr = DMDACreate2d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else if (options->dim == 3) {
    ierr = DMDACreate3d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
  } else {
    SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = DMDASetUniformCoordinates(da, 0.0, options->L, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  /* TODO Input names of species */
  for(sp = 0; sp < options->numSpecies; ++sp) {
    ierr = DMDASetFieldName(da, sp, "Species");CHKERRQ(ierr);
  }
  /* Check accuracy */
  ierr = DMSetFromOptions(da);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da, &options->rhoHat);CHKERRQ(ierr);
  *dm = da;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateParameters"
static PetscErrorCode CreateParameters(DM dm, Options *options)
{
  MPI_Comm       comm;
  DM             da, da2;
  PetscInt       dof = options->numAlpha, M, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(dm, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = PetscMalloc5(options->numSpecies,&options->convS,
                      options->numSpecies,&options->convArg,
                      options->numSpecies,&options->convWts,
                      options->numSpecies,&options->convDiffs,
                      options->numSpecies,&options->convDiffs2);CHKERRQ(ierr);
  for(sp = 0; sp < options->numSpecies; ++sp) {
    ierr = CreateConvolutionQuadtrature(sp, options->L/(M-1), options);CHKERRQ(ierr);
  }
  ierr = MatCreateFFT(PETSC_COMM_WORLD, options->dim, &M, MATFFTW, &options->F);CHKERRQ(ierr);

  options->muExtFunc = wall;
  ierr = PetscMalloc3(options->numAlpha, &options->W, options->numAlpha, &options->PhiDer, options->numAlpha, &options->n);CHKERRQ(ierr);
  ierr = PetscMalloc(options->numAlpha * sizeof(PetscScalar (*)(const PetscScalar [])), &options->WFunc);CHKERRQ(ierr);
  ierr = PetscMalloc(options->numAlpha * sizeof(PetscScalar (*)(const PetscScalar [])), &options->PhiDerFunc);CHKERRQ(ierr);
  options->WFunc[0] = W_0;
  options->WFunc[1] = W_1;
  options->WFunc[2] = W_2;
  options->WFunc[3] = W_3;
  options->WFunc[4] = W_V1;
  options->WFunc[5] = W_V2;
  options->PhiDerFunc[0] = PhiDer_0;
  options->PhiDerFunc[1] = PhiDer_1;
  options->PhiDerFunc[2] = PhiDer_2;
  options->PhiDerFunc[3] = PhiDer_3;
  options->PhiDerFunc[4] = PhiDer_V1;
  options->PhiDerFunc[5] = PhiDer_V2;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DMDACreate1d(comm, DM_BOUNDARY_NONE, -3, dof, 1, PETSC_NULL, &da);CHKERRQ(ierr);
    ierr = DMDACreate1d(comm, DM_BOUNDARY_NONE, -3, 1,   1, PETSC_NULL, &da2);CHKERRQ(ierr);
  } else if (options->dim == 2) {
    ierr = DMDACreate2d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
    ierr = DMDACreate2d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, PETSC_DECIDE, PETSC_DECIDE, 1,   1, PETSC_NULL, PETSC_NULL, &da2);CHKERRQ(ierr);
  } else if (options->dim == 3) {
    ierr = DMDACreate3d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, dof, 1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &da);CHKERRQ(ierr);
    ierr = DMDACreate3d(comm, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DMDA_STENCIL_BOX, -3, -3, -3, PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, 1,   1, PETSC_NULL, PETSC_NULL, PETSC_NULL, &da2);CHKERRQ(ierr);
  } else {
    SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = DMDASetUniformCoordinates(da,  0.0, options->L, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(da2, -options->L/2.0, options->L/2.0, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  options->nDA      = da;
  options->singleDA = da2;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyParameters"
static PetscErrorCode DestroyParameters(Options *options)
{
  PetscInt       alpha, i;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDestroy(&options->nDA);CHKERRQ(ierr);
  ierr = DMDestroy(&options->singleDA);CHKERRQ(ierr);
  for(alpha = 0; alpha < options->numAlpha; ++alpha) {
    ierr = VecDestroy(&options->W[alpha]);CHKERRQ(ierr);
    ierr = VecDestroy(&options->n[alpha]);CHKERRQ(ierr);
    ierr = VecDestroy(&options->PhiDer[alpha]);CHKERRQ(ierr);
  }
  ierr = MatDestroy(&options->F);CHKERRQ(ierr);
  ierr = VecDestroy(&options->rhoHat);CHKERRQ(ierr);
  ierr = PetscFree3(options->W,options->PhiDer,options->n);CHKERRQ(ierr);
  ierr = PetscFree(options->WFunc);CHKERRQ(ierr);
  ierr = PetscFree(options->PhiDerFunc);CHKERRQ(ierr);
  ierr = VecDestroy(&options->muExt);CHKERRQ(ierr);
  for(i = 0; i < options->numSpecies; ++i) {
    ierr = PetscFree4(options->convArg[i], options->convWts[i], options->convDiffs[i], options->convDiffs2[i]);CHKERRQ(ierr);
  }
  ierr = PetscFree5(options->convS, options->convArg, options->convWts, options->convDiffs, options->convDiffs2);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Function_1d"
static PetscErrorCode Function_1d(DMDALocalInfo *info, PetscScalar *x[], PetscScalar *f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const PetscScalar *, const int) = options->func;
  DM             coordDA;
  Vec            coordinates;
  PetscScalar   *coords;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetCoordinateDM(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(i = info->xs; i < info->xs+info->xm; ++i) {
    for(sp = 0; sp < info->dof; ++sp) {
      f[i][sp] = func(&coords[i], sp);
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "Function_2d"
static PetscErrorCode Function_2d(DMDALocalInfo *info, PetscScalar **x[], PetscScalar **f[], void *ctx)
{
  Options       *options = (Options *) ctx;
  PetscScalar  (*func)(const PetscScalar *, const int) = options->func;
  DM             coordDA;
  Vec            coordinates;
  DMDACoor2d   **coords;
  PetscInt       i, j, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetCoordinateDM(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(j = info->ys; j < info->ys+info->ym; ++j) {
    for(i = info->xs; i < info->xs+info->xm; ++i) {
      for(sp = 0; sp < info->dof; ++sp) {
        f[j][i][sp] = func((PetscScalar *) &coords[j][i], sp);
      }
    }
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN_1d"
// Check: Put in rho = 1 and should get integrals of Ws
static PetscErrorCode CalculateN_1d(Vec rho, Vec n, void *ctx)
{
  Options       *options = (Options *) ctx;
  DM             dm      = options->singleDA;
  Vec            y1, y2;
  PetscInt       alpha, sp, N;
  PetscReal      scale;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecGetSize(rho, &N);CHKERRQ(ierr);
  scale = options->L/((PetscReal) N*N);
  ierr = DMGetGlobalVector(dm, &y1);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &y2);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(n, options->n, INSERT_VALUES);CHKERRQ(ierr);
  // FIX: Are species handled correctly?
  for(alpha = 0; alpha < options->numAlpha; ++alpha) {
    // Could use this to pad vector
    //const PetscReal x[1] = {coords[i] + (k-s[sp]-1)*h};
    //rhos[sp][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
    ierr = MatMult(options->F, rho, y1);CHKERRQ(ierr);
    ierr = MatMult(options->F, options->W[alpha], y2);CHKERRQ(ierr);
    ierr = VecPointwiseMult(y1, y1, y2);CHKERRQ(ierr);
    //if (view && i == 0) {ierr = VecView(y1, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
    ierr = MatMultTranspose(options->F, y1, options->n[alpha]);CHKERRQ(ierr);
    if ((alpha == 4) || (alpha == 5)) {
      ierr = VecScale(options->n[alpha], -scale);CHKERRQ(ierr);
    } else {
      ierr = VecScale(options->n[alpha], scale);CHKERRQ(ierr);
    }
  }
  ierr = VecStrideScatterAll(options->n, n, INSERT_VALUES);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &y1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &y2);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateN_3d"
PetscErrorCode CalculateN_3d(Vec rho, Vec n, void *ctx)
{
  Options       *options    = (Options *) ctx;
  DM             dm         = options->singleDA;
  PetscInt       numSpecies = options->numSpecies;
  PetscInt       numAlpha   = options->numAlpha;
  Vec           *rhoHatSplit;
  Vec           *WAlphaSplit;
  Vec           *nSplit;
  Vec            y1, y2;
  PetscInt       M, N, P, sp, alpha;
  PetscReal      scale;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  //ierr = PetscLogEventBegin(options->nEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dm, 0, &M, &N, &P, 0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  /* I think the 3D code is non-dimensionalized, so this L goes away
     The extra factor of M*N*P comes from the explicit FT of the W window functions. In the 3d code, we form the FT window functions explicitly
   */
  scale = options->L/((PetscReal) M*N*P * M*N*P);
  ierr = PetscMalloc3(numSpecies, &rhoHatSplit, numSpecies, &WAlphaSplit, numAlpha, &nSplit);CHKERRQ(ierr);
  for (sp = 0; sp < numSpecies; ++sp) {
	ierr = DMGetGlobalVector(dm, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMGetGlobalVector(dm, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  for (alpha = 0; alpha < numAlpha; ++alpha) {
	ierr = DMGetGlobalVector(dm, &nSplit[alpha]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(dm, &y1);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &y2);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(rho, rhoHatSplit, INSERT_VALUES);CHKERRQ(ierr);
  for (sp = 0; sp < numSpecies; ++sp) {
    ierr = PrintNorm(rhoHatSplit[sp], "rho", sp, -1);CHKERRQ(ierr);
    ierr = MatMult(options->F, rhoHatSplit[sp], y1);CHKERRQ(ierr);
    ierr = VecCopy(y1, rhoHatSplit[sp]);CHKERRQ(ierr);
    ierr = PrintNorm(rhoHatSplit[sp], "\\hat rho", sp, -1);CHKERRQ(ierr);
  }
  for (alpha = 0; alpha < numAlpha; ++alpha) {
    ierr = VecStrideGatherAll(options->W[alpha], WAlphaSplit, INSERT_VALUES);CHKERRQ(ierr);
    ierr = VecSet(nSplit[alpha], 0.0);
    for (sp = 0; sp < numSpecies; ++sp) {
      ierr = PrintNorm(WAlphaSplit[sp], "w^sp_alpha", alpha, sp);CHKERRQ(ierr);
      ierr = MatMult(options->F, WAlphaSplit[sp], y2);CHKERRQ(ierr);
      ierr = VecPointwiseMult(y1, rhoHatSplit[sp], y2);CHKERRQ(ierr);
      ierr = PrintNorm(y1, "y1", alpha, sp);CHKERRQ(ierr);
      ierr = MatMultTranspose(options->F, y1, y2);CHKERRQ(ierr);
      ierr = VecAXPY(nSplit[alpha], 1.0, y2);CHKERRQ(ierr);
    }
    /* Correct for sign difference in convolution of odd functions */
    if ((alpha == 4) || (alpha == 5) || (alpha==6) || (alpha==7) || (alpha==8) || (alpha==9)) {
      ierr = VecScale(nSplit[alpha], -scale);CHKERRQ(ierr);
    } else {
      ierr = VecScale(nSplit[alpha], scale);CHKERRQ(ierr);
    }
    ierr = PrintNorm(nSplit[alpha], "n", alpha, -1);CHKERRQ(ierr);
#if 0
    /* Check for valid packing fraction */
    if (alpha == 3) {
      DMDALocalInfo  info;
      PetscScalar ***n3;
      PetscInt       i, j, k;

      ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(dm, nSplit[alpha], &n3);CHKERRQ(ierr);
      for (k = info.zs; k < info.zs+info.zm; ++k) {
        for (j = info.ys; j < info.ys+info.ym; ++j) {
          for (i = info.xs; i < info.xs+info.xm; ++i) {
            if (PetscRealPart(n3[k][j][i]) >= 1.0) {
              SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Invalid packing fraction: n3[%d][%d][%d]: %g", k, j, i, PetscRealPart(n3[k][j][i]));
            }
          }
        }
      }
      ierr = DMDAVecRestoreArray(dm, nSplit[alpha], &n3);CHKERRQ(ierr);
    }
#endif
  }
  ierr = VecStrideScatterAll(rhoHatSplit, options->rhoHat, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecStrideScatterAll(nSplit, n, INSERT_VALUES);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &y1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &y2);CHKERRQ(ierr);
  for (alpha = 0; alpha < numAlpha; ++alpha) {
	ierr = DMRestoreGlobalVector(dm, &nSplit[alpha]);CHKERRQ(ierr);
  }
  for (sp = 0; sp < numSpecies; ++sp) {
	ierr = DMRestoreGlobalVector(dm, &rhoHatSplit[sp]);CHKERRQ(ierr);
	ierr = DMRestoreGlobalVector(dm, &WAlphaSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree3(rhoHatSplit, WAlphaSplit, nSplit);CHKERRQ(ierr);
  //ierr = PetscLogEventEnd(options->nEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuHS"
// We should use FieldSplit to pull out each field for the FFT
static PetscErrorCode CalculateMuHS(DM da, Vec n, Vec muHS, void *ctx)
{
  Options        *options = (Options *) ctx;
  const PetscReal kT      = options->kT;
  Vec             y1, y2, y3;
  PetscInt        alpha, sp, N;
  PetscScalar     scale;
  PetscErrorCode  ierr;

  PetscFunctionBeginUser;
  ierr = VecGetSize(muHS, &N);CHKERRQ(ierr);
  scale = options->L/((PetscReal) N*N);
  ierr = DMGetGlobalVector(da, &y1);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(da, &y2);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(da, &y3);CHKERRQ(ierr);
  // TODO: Should be able to combine all the vectors before transform
  ierr = VecSet(muHS, 0.0);CHKERRQ(ierr);
  // FIX: Are species handled correctly?
  for(alpha = 0; alpha < options->numAlpha; ++alpha) {
    // Could use this to pad vector
    //const PetscReal x[1] = {coords[i] + (k-s[sp]-1)*h};
    //rhos[sp][k] = options->rhoBath[sp]*PetscExpScalar(-options->muExtFunc(x, sp));
    ierr = MatMult(options->F, options->PhiDer[alpha], y1);CHKERRQ(ierr);
    ierr = MatMult(options->F, options->W[alpha], y2);CHKERRQ(ierr);
    ierr = VecPointwiseMult(y1, y1, y2);CHKERRQ(ierr);
    ierr = MatMultTranspose(options->F, y1, y3);CHKERRQ(ierr);
    ierr = VecScale(y3, scale);CHKERRQ(ierr);
    if (options->debug > 0) {ierr = VecView(y3, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
    PetscScalar *a;
    ierr = VecGetArray(y3, &a);CHKERRQ(ierr);
    for(int i = 0; i < N; ++i) {
      if (PetscIsInfOrNanScalar(a[i])) {
        ierr = PetscPrintf(PETSC_COMM_SELF, "alpha: %d i: %d\n", alpha, i);
        ierr = VecView(options->PhiDer[alpha], PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
        ierr = VecView(options->W[alpha], PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
        ierr = VecView(y3, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
        SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");
      };
    }
    ierr = VecRestoreArray(y3, &a);CHKERRQ(ierr);
    ierr = VecAXPY(muHS, 1.0, y3);CHKERRQ(ierr);
  }
  ierr = VecScale(muHS, kT);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(da, &y1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(da, &y2);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(da, &y3);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePhiDer_1d"
static PetscErrorCode CalculatePhiDer_1d(DMDALocalInfo *info, N n[], void *ctx) {
  Options       *options  = (Options *) ctx;
  PetscScalar   *phiDer;
  PetscInt       ie = info->xs+info->xm, alpha, i, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  for(alpha = 0; alpha < options->numAlpha; ++alpha) {
    ierr = DMDAVecGetArray(options->singleDA, options->PhiDer[alpha], &phiDer);CHKERRQ(ierr);
    for(i = info->xs; i < ie; ++i) {
      for(sp = 0; sp < options->numSpecies; ++sp) {
        // FIX: Need separate PhiDer for each species
        phiDer[i] = (*options->PhiDerFunc[alpha])(n[i].v, sp);
        if (PetscIsInfOrNanScalar(phiDer[i])) {
          ierr = PetscPrintf(PETSC_COMM_SELF, "alpha: %d sp: %d n[%d].v[0]: %g n[%d].v[1]: %g n[%d].v[2]: %g n[%d].v[3]: %g n[%d].v[4]: %g n[%d].v[5]: %g\n", alpha, sp,
                             i, PetscRealPart(n[i].v[0]), i, PetscRealPart(n[i].v[1]), i, PetscRealPart(n[i].v[2]),
                             i, PetscRealPart(n[i].v[3]), i, PetscRealPart(n[i].v[4]), i, PetscRealPart(n[i].v[5]));
          SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");
        };
      }
    }
    ierr = DMDAVecRestoreArray(options->singleDA, options->PhiDer[alpha], &phiDer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Rhs_1d"
static PetscErrorCode Rhs_1d(DMDALocalInfo *info, Rho x[], Rho f[], void *ctx)
{
  Options         *options  = (Options *) ctx;
  const PetscReal *rhoBath  = options->rhoBath;
  const PetscReal *muHSBath = options->muHSBath;
  const PetscReal  kT       = options->kT;
  const PetscInt   debug    = options->debug;
  DM               coordDA;
  Vec              coordinates, nVec, muHSVec, xVec;
  Rho             *xArray;
  N               *nArray;
  PetscReal       *coords;
  Rho             *muHS, *muExt;
  PetscInt         ie = info->xs+info->xm, i, sp;
  PetscErrorCode   ierr;

  PetscFunctionBeginUser;
  ierr = DMGetCoordinateDM(info->da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(info->da, &coordinates);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(info->da, &xVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, options->muExt, &muExt);CHKERRQ(ierr);
  // OPT: Can avoid this copy by placing the array directly
  ierr = DMDAVecGetArray(info->da, xVec, &xArray);CHKERRQ(ierr);
  for(i = info->xs; i < ie; ++i) {
    xArray[i] = x[i];
  }
  ierr = DMDAVecRestoreArray(info->da, xVec, &xArray);CHKERRQ(ierr);
  ierr = CalculateN_3d(xVec, nVec, ctx);CHKERRQ(ierr);
  if (debug > 1) {
    ierr = PetscPrintf(options->comm, "n:\n");CHKERRQ(ierr);
    ierr = VecView(nVec, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = DMDAVecGetArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  ierr = CalculatePhiDer_1d(info, nArray, ctx);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(options->nDA, nVec, &nArray);CHKERRQ(ierr);
  ierr = CalculateMuHS(info->da, PETSC_NULL, muHSVec, ctx);CHKERRQ(ierr);
  if (debug > 1) {
    ierr = PetscPrintf(options->comm, "\\mu_{HS}:\n");CHKERRQ(ierr);
    ierr = VecView(muHSVec, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  for(i = info->xs; i < ie; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      if (debug) {ierr = PetscPrintf(options->comm, "[%d]Species %d \\mu^{bath}_{HS}: %g \\mu_{HS}: %g \\mu_{ext}: %g arg: %g\n", i, sp, PetscAbsScalar(muHSBath[sp]), PetscAbsScalar(muHS[i].v[sp]), PetscAbsScalar(muExt[i].v[sp]), PetscAbsScalar((muHSBath[sp] - muHS[i].v[sp] - muExt[i].v[sp])/kT));CHKERRQ(ierr);}
      f[i].v[sp] = x[i].v[sp] - rhoBath[sp]*PetscExpScalar((muHSBath[sp] - muHS[i].v[sp] - muExt[i].v[sp])/kT);
      if (PetscIsInfOrNanScalar(f[i].v[sp])) {SETERRQ(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid floating point value");};
    }
  }
  ierr = DMDAVecRestoreArray(info->da, muHSVec, &muHS);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(info->da, options->muExt, &muExt);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &muHSVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &xVec);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "CreateExternalPotential"
static PetscErrorCode CreateExternalPotential(DM dm, Options *options)
{
  const int      dim = options->dim;
  PetscScalar  (*func)(const PetscScalar *, const int) = options->func;
  Vec            X, U;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm, &options->muExt);CHKERRQ(ierr);
  options->func = options->muExtFunc;
  U             = options->muExt;
  if (dim == 1) {
    ierr = DMDAComputeFunctionLocal(dm, (DMDALocalFunction1) Function_1d, X, U, (void *) options);CHKERRQ(ierr);
  } else if (dim == 2) {
    ierr = DMDAComputeFunctionLocal(dm, (DMDALocalFunction1) Function_2d, X, U, (void *) options);CHKERRQ(ierr);
  } else {
    SETERRQ1(PetscObjectComm((PetscObject) dm), PETSC_ERR_SUP, "Dimension not supported: %d", dim);
  }
  options->func = func;
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
  if (flag) {ierr = VecView(U, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view_draw", &flag);CHKERRQ(ierr);
  if (flag) {ierr = VecView(U, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateWindowFunctions"
static PetscErrorCode CreateWindowFunctions(DM dm, Options *options)
{
  DM             da  = options->singleDA;
  const int      dim = options->dim;
  PetscScalar  (*func)(const PetscScalar *, const int) = options->func;
  Vec            X, U;
  PetscBool      flag;
  PetscInt       alpha;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetGlobalVector(da, &X);CHKERRQ(ierr);
  for(alpha = 0; alpha < options->numAlpha; ++alpha) {
    ierr = DMCreateGlobalVector(da, &options->W[alpha]);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(da, &options->n[alpha]);CHKERRQ(ierr);
    ierr = DMCreateGlobalVector(da, &options->PhiDer[alpha]);CHKERRQ(ierr);
    options->func = options->WFunc[alpha];
    U             = options->W[alpha];
    if (dim == 1) {
      ierr = DMDAComputeFunctionLocal(da, (DMDALocalFunction1) Function_1d, X, U, (void *) options);CHKERRQ(ierr);
    } else if (dim == 2) {
      ierr = DMDAComputeFunctionLocal(da, (DMDALocalFunction1) Function_2d, X, U, (void *) options);CHKERRQ(ierr);
    } else {
      SETERRQ1(PetscObjectComm((PetscObject) da), PETSC_ERR_SUP, "Dimension not supported: %d", dim);
    }
    ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
    if (flag) {ierr = VecView(U, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
    ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view_draw", &flag);CHKERRQ(ierr);
    if (flag) {ierr = VecView(U, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
  }
  options->func = func;
  ierr = DMRestoreGlobalVector(da, &X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckN"
static PetscErrorCode CheckN(DM dm, Options *options)
{
  DMDALocalInfo  info;
  PetscScalar  (*muExtFunc)(const PetscScalar [], const int);
  Vec            rho, n;
  PetscReal      tol = options->nCheckTol;
  PetscReal     *rhoBath;
  Rho           *rhoArray;
  N             *nArray;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetGlobalVector(dm, &rho);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(options->nDA, &n);CHKERRQ(ierr);
  ierr = VecSet(rho, 1.0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(options->nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(options->nDA, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(options->numSpecies*sizeof(PetscReal), &rhoBath);CHKERRQ(ierr);
  muExtFunc              = options->muExtFunc;
  options->muExtFunc     = zero;
  for(sp = 0; sp < options->numSpecies; ++sp) {
    rhoBath[sp]          = options->rhoBath[sp];
    options->rhoBath[sp] = 1.0;
  }
  ierr = CalculateN_3d(rho, n, options);CHKERRQ(ierr);
  options->muExtFunc  = muExtFunc;
  for(sp = 0; sp < options->numSpecies; ++sp) {
    options->rhoBath[sp] = rhoBath[sp];
  }
  for(i = info.xs; i < info.xs+info.xm; ++i) {
    PetscReal value;

    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 1.0;}
    if (PetscAbsScalar(nArray[i].v[0] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_0[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[0]), value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += options->R[sp];}
    if (PetscAbsScalar(nArray[i].v[1] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_1[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[1]), value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 4.0*M_PI*options->R[sp]*options->R[sp];}
    if (PetscAbsScalar(nArray[i].v[2] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_2[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[2]), value);
    for(sp = 0, value = 0.0; sp < options->numSpecies; ++sp) {value += 4.0*M_PI*options->R[sp]*options->R[sp]*options->R[sp]/3.0;}
    if (PetscAbsScalar(nArray[i].v[3] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_3[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[3]), value);
    value = 0.0;
    if (PetscAbsScalar(nArray[i].v[4] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_4[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[4]), value);
    value = 0.0;
    if (PetscAbsScalar(nArray[i].v[5] - value) > tol) SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Invalid W_5[%d] integral %g should be %g", i, PetscAbsScalar(nArray[i].v[5]), value);
  }
  ierr = PetscFree(rhoBath);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(options->nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rho);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(options->nDA, &n);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckResidual"
static PetscErrorCode CheckResidual(DM dm, Vec sol, Options *options)
{
  Vec            residual;
  MPI_Comm       comm;
  const char    *name;
  PetscReal      norm;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view", &flag);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &residual);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) residual, "residual");CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DMDAComputeFunctionLocal(dm, (DMDALocalFunction1) Rhs_1d, sol, residual, (void *) options);CHKERRQ(ierr);
  } else {
    SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = VecNorm(residual, NORM_2, &norm);CHKERRQ(ierr);
  if (flag) {ierr = VecView(residual, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);}
  ierr = DMRestoreGlobalVector(dm, &residual);CHKERRQ(ierr);
  ierr = PetscObjectGetName((PetscObject) sol, &name);CHKERRQ(ierr);
  PetscPrintf(comm, "Residual for trial solution %s: %g\n", name, norm);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateSolver"
static PetscErrorCode CreateSolver(DM dm, SNES *snes, Options *options)
{
  MPI_Comm       comm;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = SNESCreate(comm, snes);CHKERRQ(ierr);
  ierr = SNESSetDM(*snes, dm);CHKERRQ(ierr);
  if (options->dim == 1) {
    ierr = DMDASNESSetFunctionLocal(dm, INSERT_VALUES, (PetscErrorCode (*)(DMDALocalInfo*,void*,void*,void*)) Rhs_1d, options);CHKERRQ(ierr);
  } else {
    SETERRQ1(comm, PETSC_ERR_SUP, "Dimension not supported: %d", options->dim);
  }
  ierr = SNESSetFromOptions(*snes);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateInitialGuess"
static PetscErrorCode CreateInitialGuess(DM dm, Vec rhoVec, Options *options)
{
  DMDALocalInfo  info;
  PetscScalar   *rho;
  PetscInt       i, sp;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  for(i = info.xs; i < info.xs+info.xm; ++i) {
    for(sp = 0; sp < options->numSpecies; ++sp) {
      rho[i] = options->rhoBath[sp];
    }
  }
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Solve"
static PetscErrorCode Solve(SNES snes, Vec rho, Options *options)
{
  MPI_Comm            comm;
  PetscInt            its;
  SNESConvergedReason reason;
  PetscErrorCode      ierr;

  PetscFunctionBeginUser;
  ierr = SNESSolve(snes, NULL, rho);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(snes, &its);CHKERRQ(ierr);
  ierr = SNESGetConvergedReason(snes, &reason);CHKERRQ(ierr);
  ierr = PetscObjectGetComm((PetscObject) snes, &comm);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Number of nonlinear iterations = %D\n", its);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Reason for solver termination: %s\n", SNESConvergedReasons[reason]);CHKERRQ(ierr);
  ierr = VecViewFromOptions(rho, NULL, "-vec_view");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintReport"
static PetscErrorCode PrintReport(Options *options)
{
  MPI_Comm       comm = options->comm;
  PetscInt       i;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscPrintf(comm, "Simulation Input\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter      Description                        Units          Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "----------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "L:            System Length                      (nm):          %g\n", options->L);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "x_{wall}:     Wall position                      (nm):          %g\n", options->wallPos);CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "kT:           Boltzmann's constant * Temperature (kg nm^2/s^2): %g\n", options->kT);CHKERRQ(ierr);
  for(i = 0; i < options->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "Species %d:\n", i);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  R:          Ion Radius                         (nm):          %g\n", options->R[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  rho_{bath}: Density of ions in bath            (nm^{-3}):     %g\n", options->rhoBath[i]);CHKERRQ(ierr);
  }
  ierr = PetscPrintf(comm, "\nDerived Quantities\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "Parameter       Description                                                   Units Value\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "-----------------------------------------------------------------------------------------\n");CHKERRQ(ierr);
  ierr = PetscPrintf(comm, "P_{HS}:         Pressure due to hard sphere interaction in the bath           (nm): %g\n\n", options->presHS);CHKERRQ(ierr);
  for(i = 0; i < options->numSpecies; ++i) {
    ierr = PetscPrintf(comm, "Species %d:\n", i);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  eta:          Volume fraction of ions                                       None: %g\n", options->eta[i]);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "  mu_{HS,bath}: Chemical potential due to hard sphere interaction in the bath (nm): %g\n", options->muHSBath[i]);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckSumRule"
static PetscErrorCode CheckSumRule(DM dm, Vec rhoVec, Options *options)
{
  PetscInt       M, i, sp;
  Rho           *rho;
  PetscReal      x, frac, rhoP, error;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(dm, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  for(sp = 0, rhoP = 0.0; sp < options->numSpecies; ++sp) {
    x     = (options->wallPos+options->R[sp])*(M-1)/options->L;
    // We will linearly extrapolate
    i     = (PetscInt) x + 1;
    frac  = x - i;
    rhoP += options->kT*((1.0 - frac)*PetscAbsScalar(rho[i].v[sp]) + frac*PetscAbsScalar(rho[i+1].v[sp]));
  }
  error = PetscAbsReal(options->presHS - rhoP);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  if (error/options->presHS > 0.1) {
    SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Sum rule violation, P_{HS} %g kT rho %g relative error %g", options->presHS, rhoP, error/options->presHS);
  } else {
    PetscPrintf(options->comm, "Sum rule verification\n");
    PetscPrintf(options->comm, "                       P_{HS}          %g\n", options->presHS);
    PetscPrintf(options->comm, "                       kT rho          %g\n", rhoP);
    PetscPrintf(options->comm, "                       relative error  %g\n", error/options->presHS);
    PetscPrintf(options->comm, "                       variable number %d\n", i);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckEta"
static PetscErrorCode CheckEta(DM dm, Vec rho, Options *options)
{
  DMDALocalInfo  info;
  Vec            nVec;
  N             *n;
  PetscInt       M, i;
  PetscReal      n_3, eta, error;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetInfo(dm, 0, &M, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(options->nDA, &info);CHKERRQ(ierr);
  ierr = CalculateN_3d(rho, nVec, options);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  n_3  = PetscAbsScalar(n[M-1].v[3]);
  ierr = DMDAVecRestoreArray(options->nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(options->nDA, &nVec);CHKERRQ(ierr);
  for(i = 0, eta = 0.0; i < options->numSpecies; ++i) {eta += options->eta[i];}
  error = PetscAbsReal(eta - n_3);
  if (error/eta > options->volFracTol) {
    SETERRQ3(PetscObjectComm((PetscObject) dm), PETSC_ERR_PLIB, "Volume fraction violation, eta %g n_3 %g relative error %g", eta, n_3, error/eta);
  } else {
    PetscPrintf(options->comm, "Volume fraction verification\n");
    PetscPrintf(options->comm, "                       eta             %g\n", eta);
    PetscPrintf(options->comm, "                       n_3             %g\n", n_3);
    PetscPrintf(options->comm, "                       relative error  %g\n", error/eta);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[])
{
  MPI_Comm       comm;
  DM             dm;
  SNES           snes;
  Vec            rho;
  Options        options;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscInitialize(&argc, &argv, (char *) 0, help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  globalOptions = &options;
  ierr = ProcessOptions(comm, &options);CHKERRQ(ierr);
  ierr = CreateMesh(comm, &dm, &options);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(dm, &rho);CHKERRQ(ierr);
  /* TODO Create function that gives back DMDA with different dof */
  ierr = CreateParameters(dm, &options);CHKERRQ(ierr);
  ierr = CreateWindowFunctions(dm, &options);CHKERRQ(ierr);
  ierr = CreateExternalPotential(dm, &options);CHKERRQ(ierr);
  ierr = CheckN(dm, &options);CHKERRQ(ierr);
  /* TODO When do coordinates get set for a multilevel solve */
  ierr = CreateSolver(dm, &snes, &options);CHKERRQ(ierr);
  /* TODO Change all species loops to sp */
  ierr = CreateInitialGuess(dm, rho, &options);CHKERRQ(ierr);
  ierr = Solve(snes, rho, &options);CHKERRQ(ierr);
  ierr = PrintReport(&options);CHKERRQ(ierr);
  ierr = CheckSumRule(dm, rho, &options);CHKERRQ(ierr);
  ierr = CheckEta(dm, rho, &options);CHKERRQ(ierr);
  /* Cleanup */
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = VecDestroy(&rho);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = DestroyParameters(&options);CHKERRQ(ierr);
  ierr = PetscFree4(options.R,options.rhoBath,options.muHSBath,options.eta);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
