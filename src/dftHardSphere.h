#ifndef __DFT_HARD_SPHERE_H
#define __DFT_HARD_SPHERE_H

#include<windowFunctions.h>
#include<phiDer.h>

typedef struct {
  PetscReal      rhoBath[NUM_SPECIES];              // Number density of species i in the bath [nm^{-3}]
  Vec            rhoBathVec;                        // Vector containing the reference bath solution
  PetscReal      rhoBoltzmann[NUM_SPECIES];         // Number density of species i as a prefactor for the Boltzmann weight [nm^{-3}]
  PetscReal      muHSBath[NUM_SPECIES];             // Chemical potential of species i due to hard sphere interaction in the bath [kg nm^2 / s^2]
  Vec           *W;                                 // W^\alpha window functions
  PetscScalar (**PhiDerFunc)(const PetscScalar []); // Functions {\partial\Phi_{HS}}{\partial n_\alpha}
  Mat            F;                                 // Discrete Fourier Transform operator for the DA
  Vec            rhoHat;                            // Fourier transformed density, has multiple species
  PetscLogEvent  nEvent, phiDerEvent, muHSEvent;
} HardSphereOptions;

extern HardSphereOptions *globalHardSphereOptions;

extern PetscErrorCode ProcessHardSphereOptions(MPI_Comm comm, GeometryOptions *geomOptions, HardSphereOptions *options);
extern PetscErrorCode PrintHardSphereModule(PetscViewer viewer, const char indent[], GeometryOptions *geomOpts, HardSphereOptions *options);
extern PetscErrorCode CreateHardSphere(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options);
extern PetscErrorCode DestroyHardSphere(DM dm, GeometryOptions *geomOptions, HardSphereOptions *options);
extern PetscErrorCode CalculatePressureHS(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], PetscReal *P);
extern PetscErrorCode CalculateRhoBath(DM dm, const PetscInt numSpecies, Vec rhoBathVec);
extern PetscErrorCode CalculateMuHSBath(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], PetscReal muHSBath[]);
extern PetscErrorCode CalculateN_3d(DM dm, Vec rho, const int numSpecies, const int numAlpha, DM singleDA, HardSphereOptions *options, Vec n);
extern PetscErrorCode CalculateN3_3d(DM dm, Vec rho, const int numSpecies, DM singleDA, HardSphereOptions *options, Vec n3);
extern PetscErrorCode CalculatePhiDer_3d(DMDALocalInfo *info, N **n[], const int numAlpha, PetscScalar (**PhiDerFunc)(const PetscScalar []), HardSphereOptions *options, Vec PhiDer[]);
extern PetscErrorCode CalculateMuHS(DM dm, const int numSpecies, const int numAlpha, DM singleDA, Vec PhiDer[], HardSphereOptions *options, Vec muHS);

#endif /* __DFT_HARD_SPHERE_H */
