#include <dftVerification.h>

#undef __FUNCT__
#define __FUNCT__ "ProcessVerificationOptions"
PetscErrorCode ProcessVerificationOptions(MPI_Comm comm, VerificationOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  options->nCheckTol  = 1.0e-9;
  options->volFracTol = 1.0e-3;
  options->homoTol    = 1.0e-6;

  ierr = PetscOptionsBegin(comm, "", "DFT Verification Options", "DMMG");CHKERRQ(ierr);
    ierr = PetscOptionsReal("-nCheckTol", "Tolerance for n check", __FILE__, options->nCheckTol, &options->nCheckTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-volFracTol", "Tolerance for volume fraction check", __FILE__, options->volFracTol, &options->volFracTol, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-homoTol", "Tolerance for homogeneity check", __FILE__, options->homoTol, &options->homoTol, PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateXYAverage"
PetscErrorCode CalculateXYAverage(DM dm, const int numSpecies, Vec v, Vec **avg) {
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Rho         ***rho;
  PetscScalar   *r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(numSpecies * sizeof(Vec), avg);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &rho);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = VecCreate(comm, &((*avg)[sp]));CHKERRQ(ierr);
    ierr = VecSetSizes((*avg)[sp], info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions((*avg)[sp]);CHKERRQ(ierr);
    ierr = VecGetArray((*avg)[sp], &r);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      PetscScalar sum = 0.0;

      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          sum += rho[k][j][i].v[sp];
        }
      }
      r[k] = PetscRealPart(sum)/(info.mx*info.my);
	}
    ierr = VecRestoreArray((*avg)[sp], &r);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(dm, v, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateRadialAverage"
PetscErrorCode CalculateRadialAverage(DM dm, const int numSpecies, Vec v, Vec **avg) {
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Rho         ***rho;
  PetscScalar   *r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(numSpecies * sizeof(Vec), avg);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &rho);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = VecCreate(comm, &((*avg)[sp]));CHKERRQ(ierr);
    ierr = VecSetSizes((*avg)[sp], info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions((*avg)[sp]);CHKERRQ(ierr);
    ierr = VecGetArray((*avg)[sp], &r);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      PetscReal   rp  = k - info.mz/2;
      PetscScalar sum = 0.0;
      PetscInt    n   = 0;

      for(PetscReal phi = 0.0; phi < 2.0*M_PI; phi += M_PI/4.0) {
        for(PetscReal theta = 0.0; theta < M_PI; theta += M_PI/4.0) {
          PetscInt ip  = (PetscInt) (rp*sin(theta)*cos(phi) + info.mx/2);
          PetscInt jp  = (PetscInt) (rp*sin(theta)*sin(phi) + info.my/2);
          PetscInt kp  = (PetscInt) (rp*cos(theta)          + info.mz/2);
          PetscInt ipp = PetscMin(ip+1, info.mx-1);
          PetscInt jpp = PetscMin(jp+1, info.my-1);
          PetscInt kpp = PetscMin(kp+1, info.mz-1);

          sum += (rho[kp][jp][ip].v[sp]  + rho[kp][jp][ipp].v[sp]  + rho[kp][jpp][ip].v[sp]  + rho[kp][jpp][ipp].v[sp] +
                  rho[kpp][jp][ip].v[sp] + rho[kpp][jp][ipp].v[sp] + rho[kpp][jpp][ip].v[sp] + rho[kpp][jpp][ipp].v[sp]);
          n++;
        }
      }
      r[k] = PetscRealPart(sum)/(n*8.0);
    }
    ierr = VecRestoreArray((*avg)[sp], &r);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(dm, v, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateXYStdDev"
PetscErrorCode CalculateXYStdDev(DM dm, Vec v, Vec *std) {
  DMDALocalInfo  info;
  MPI_Comm       comm;
  PetscScalar ***a;
  PetscScalar   *r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &a);CHKERRQ(ierr);
    ierr = VecCreate(comm, std);CHKERRQ(ierr);
    ierr = VecSetSizes(*std, info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions(*std);CHKERRQ(ierr);
    ierr = VecGetArray(*std, &r);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      PetscScalar avg = 0.0, var = 0.0;

      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          avg += a[k][j][i];
        }
      }
      avg /= (info.mx*info.my);
      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          var += PetscSqr(a[k][j][i] - avg);
        }
      }
      r[k] = sqrt(var);
	}
    ierr = VecRestoreArray(*std, &r);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, v, &a);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateXYStdDev"
PetscErrorCode CalculateXYStdDev(DM dm, const int numSpecies, Vec v, Vec **std) {
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Rho         ***rho;
  PetscScalar   *r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(numSpecies * sizeof(Vec), std);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &rho);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = VecCreate(comm, &((*std)[sp]));CHKERRQ(ierr);
    ierr = VecSetSizes((*std)[sp], info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions((*std)[sp]);CHKERRQ(ierr);
    ierr = VecGetArray((*std)[sp], &r);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      PetscScalar avg = 0.0, var = 0.0;

      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          avg += rho[k][j][i].v[sp];
        }
      }
      avg /= (info.mx*info.my);
      for(PetscInt j = 0; j < info.my; ++j) {
        for(PetscInt i = 0; i < info.mx; ++i) {
          var += PetscSqr(rho[k][j][i].v[sp] - avg);
        }
      }
      r[k] = sqrt(var);
	}
    ierr = VecRestoreArray((*std)[sp], &r);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(dm, v, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateRadialStdDev"
PetscErrorCode CalculateRadialStdDev(DM dm, const int numSpecies, Vec v, Vec **std) {
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Rho         ***rho;
  PetscScalar   *r;
  PetscErrorCode ierr;
  
  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = PetscMalloc(numSpecies * sizeof(Vec), std);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, v, &rho);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = VecCreate(comm, &((*std)[sp]));CHKERRQ(ierr);
    ierr = VecSetSizes((*std)[sp], info.zm - info.zs, info.mz);CHKERRQ(ierr);
    ierr = VecSetFromOptions((*std)[sp]);CHKERRQ(ierr);
    ierr = VecGetArray((*std)[sp], &r);CHKERRQ(ierr);
    for(PetscInt k = 0; k < info.mz; ++k) {
      PetscReal   rp  = k - info.mz/2;
      PetscScalar avg = 0.0, var = 0.0;
      PetscInt    n   = 0;

      for(PetscReal phi = 0.0; phi < 2.0*M_PI; phi += M_PI/4.0) {
        for(PetscReal theta = 0.0; theta < M_PI; theta += M_PI/4.0) {
          PetscInt ip  = (PetscInt) (rp*sin(theta)*cos(phi) + info.mx/2);
          PetscInt jp  = (PetscInt) (rp*sin(theta)*sin(phi) + info.my/2);
          PetscInt kp  = (PetscInt) (rp*cos(theta)          + info.mz/2);
          PetscInt ipp = PetscMin(ip+1, info.mx-1);
          PetscInt jpp = PetscMin(jp+1, info.my-1);
          PetscInt kpp = PetscMin(kp+1, info.mz-1);

          avg += (rho[kp][jp][ip].v[sp]  + rho[kp][jp][ipp].v[sp]  + rho[kp][jpp][ip].v[sp]  + rho[kp][jpp][ipp].v[sp] +
                  rho[kpp][jp][ip].v[sp] + rho[kpp][jp][ipp].v[sp] + rho[kpp][jpp][ip].v[sp] + rho[kpp][jpp][ipp].v[sp]);
          n++;
        }
      }
      avg /= (n*8.0);
      for(PetscReal phi = 0.0; phi < 2.0*M_PI; phi += M_PI/4.0) {
        for(PetscReal theta = 0.0; theta < M_PI; theta += M_PI/4.0) {
          PetscInt ip  = (PetscInt) (rp*sin(theta)*cos(phi) + info.mx/2);
          PetscInt jp  = (PetscInt) (rp*sin(theta)*sin(phi) + info.my/2);
          PetscInt kp  = (PetscInt) (rp*cos(theta)          + info.mz/2);
          PetscInt ipp = PetscMin(ip+1, info.mx-1);
          PetscInt jpp = PetscMin(jp+1, info.my-1);
          PetscInt kpp = PetscMin(kp+1, info.mz-1);

          var += PetscSqr((rho[kp][jp][ip].v[sp]  + rho[kp][jp][ipp].v[sp]  + rho[kp][jpp][ip].v[sp]  + rho[kp][jpp][ipp].v[sp] +
                           rho[kpp][jp][ip].v[sp] + rho[kpp][jp][ipp].v[sp] + rho[kpp][jpp][ip].v[sp] + rho[kpp][jpp][ipp].v[sp])*0.125 - avg);
        }
      }
      r[k] = sqrt(var);
	}
    ierr = VecRestoreArray((*std)[sp], &r);CHKERRQ(ierr);
  }
  ierr = DMDAVecRestoreArray(dm, v, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckElectroneutrality"
PetscErrorCode CheckElectroneutrality(const PetscInt numSpecies, const PetscBool isConfined[], HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions)
{
  PetscReal chargeDensity = 0.0;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (isConfined[sp]) continue;
    chargeDensity += hsOptions->rhoBath[sp]*esOptions->z[sp];
  }
  if (chargeDensity > 1.0e-10) {SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Bulk charge imbalance: %g", chargeDensity);}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckN"
PetscErrorCode CheckN(DM dm, DM nDA, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, VerificationOptions *options)
{
  DMDALocalInfo  info;
  Vec            rho, n;
  PetscReal      tol = options->nCheckTol;
  PetscReal     *R   = geomOptions->R;
  N           ***nArray;
  PetscInt       i,j,k,db, sp;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMGetGlobalVector(dm, &rho);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(nDA, &n);CHKERRQ(ierr);
  ierr = VecSet(rho, 1.0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(nDA, &info);CHKERRQ(ierr);
  ierr = CalculateN_3d(dm, rho, geomOptions->numSpecies, geomOptions->numAlpha, singleDA, hsOptions, n);CHKERRQ(ierr);
  for(k = info.zs; k < info.zs+info.zm; ++k){
    for(j = info.ys; j < info.ys+info.ym; ++j){
      for(i = info.xs; i < info.xs+info.xm; ++i) {
        PetscReal value;

        for(sp = 0, value = 0.0; sp < geomOptions->numSpecies; ++sp) {value += 1.0;}
        if (PetscAbsScalar(nArray[k][j][i].v[0] - value) > tol) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid W_0[%d][%d][%d] integral %g should be %g",k,j, i, PetscAbsScalar(nArray[k][j][i].v[0]), value);
        for(sp = 0, value = 0.0; sp < geomOptions->numSpecies; ++sp) {value += R[sp];}
        if (PetscAbsScalar(nArray[k][j][i].v[1] - value) > tol) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid W_1[%d][%d][%d] integral %g should be %g",k,j, i, PetscAbsScalar(nArray[k][j][i].v[1]), value);
        for(sp = 0, value = 0.0; sp < geomOptions->numSpecies; ++sp) {value += 4.0*M_PI*R[sp]*R[sp];}
        if (PetscAbsScalar(nArray[k][j][i].v[2] - value) > tol) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid W_2[%d][%d][%d] integral %g should be %g",k,j, i, PetscAbsScalar(nArray[k][j][i].v[2]), value);
        for(sp = 0, value = 0.0; sp < geomOptions->numSpecies; ++sp) {value += 4.0*M_PI*R[sp]*R[sp]*R[sp]/3.0;}
        if (PetscAbsScalar(nArray[k][j][i].v[3] - value) > tol) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid W_3[%d][%d][%d] integral %g should be %g",k,j, i, PetscAbsScalar(nArray[k][j][i].v[3]), value);
        value = 0.0;
        for(db = 4; db < 10; ++db){
          if (PetscAbsScalar(nArray[k][j][i].v[db] - value) > tol) SETERRQ6(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid W_%d[%d][%d][%d] integral %g should be %g",db,k,j, i, PetscAbsScalar(nArray[k][j][i].v[db]), value);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(nDA, n, &nArray);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(nDA, &n);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rho);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckScreeningIntegral"
PetscErrorCode CheckScreeningIntegral(DM dm, GeometryOptions *geomOptions, ElectrostaticsOptions *esOptions, VerificationOptions *options)
{
  DMDALocalInfo  info;
  Vec            rhoVec, rhoRefVec;
  PetscReal      tol = options->nCheckTol;
  PetscReal     *R   = geomOptions->R;
  Rho         ***rho;
  Rho         ***rhoRef;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMGetGlobalVector(dm, &rhoVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &rhoRefVec);CHKERRQ(ierr);
  ierr = VecSet(rhoVec, 1.0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = CalculateScreeningIntegral(&info, geomOptions->numSpecies, R, geomOptions->h, rho, rhoRef, esOptions);CHKERRQ(ierr);
  PetscReal value   = 1.0;
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k){
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j){
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
          if (PetscAbsScalar(rhoRef[k][j][i].v[sp] - value) > tol) SETERRQ6(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid rhoRef[%d][%d][%d].%d integral %g should be %g", k, j, i, sp, PetscRealPart(rhoRef[k][j][i].v[sp]), value);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoRefVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoVec);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckPotential"
PetscErrorCode CheckPotential(DM dm, Vec rhoVec, DM singleDA, Vec phi, const PetscReal z[], GeometryOptions *geomOptions)
{
  const PetscInt   numSpecies = geomOptions->numSpecies;
  const PetscInt  *bathIndex  = geomOptions->bathIndex;
  const PetscReal *h          = geomOptions->h;
  PetscScalar ***phiArray;
  Rho         ***rhoArray;
  PetscInt       M, N, P;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetInfo(singleDA,0,&M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  ierr = VecViewCenterSingle(singleDA, phi, PETSC_VIEWER_DRAW_WORLD, "check phi", -1, -1);CHKERRQ(ierr);
  ierr = VecViewCenter(dm, rhoVec, PETSC_VIEWER_DRAW_WORLD, "check rho", -1, -1);CHKERRQ(ierr);

  Vec X;
  PetscScalar ***xArray;
  ierr = DMGetGlobalVector(singleDA, &X);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, X, &xArray);CHKERRQ(ierr);
  for(PetscInt k = 0; k < P; ++k) {
    for(PetscInt j = 0; j < N; ++j) {
      for(PetscInt i = 0; i < M; ++i) {
        xArray[k][j][i] = 0.0;
        for(int sp = 0; sp < numSpecies; ++sp) {
          xArray[k][j][i] += rhoArray[k][j][i].v[sp]*z[sp]*e;
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(singleDA, X, &xArray);CHKERRQ(ierr);
  ierr = VecViewCenterSingle(singleDA, X, PETSC_VIEWER_DRAW_WORLD, "check space charge", -1, -1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &X);CHKERRQ(ierr);

  for(PetscInt k = 0; k < P; ++k) {
    for(PetscInt j = 0; j < N; ++j) {
      for(PetscInt i = 0; i < M; ++i) {
        PetscInt right = i==M-1 ? 0 : i+1, left   = i==0 ? M-1 : i-1;
        PetscInt front = j==N-1 ? 0 : j+1, back   = j==0 ? N-1 : j-1;
        PetscInt top   = k==P-1 ? 0 : k+1, bottom = k==0 ? P-1 : k-1;
        PetscScalar phi_xx = (phiArray[k][j][right] - 2.0*phiArray[k][j][i] + phiArray[k][j][left])/PetscSqr(h[0]);
        PetscScalar phi_yy = (phiArray[k][front][i] - 2.0*phiArray[k][j][i] + phiArray[k][back][i])/PetscSqr(h[1]);
        PetscScalar phi_zz = (phiArray[top][j][i]   - 2.0*phiArray[k][j][i] + phiArray[bottom][j][i])/PetscSqr(h[2]);
        PetscScalar source = 0.0;

        for(int sp = 0; sp < numSpecies; ++sp) {
          source += rhoArray[k][j][i].v[sp]*z[sp]*e;
        }
        PetscScalar error  = epsilon*(phi_xx + phi_yy + phi_zz) + source;
        if (PetscAbsScalar(error) > 1.0e-9) {
          SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_LIB, "Invalid Poisson solution at (%d, %d, %d), error %g", i, j, k, PetscRealPart(error));
        }
      }
    }
  }
  // Check for bath potential
  PetscReal bathPotential = PetscRealPart(phiArray[bathIndex[2]][bathIndex[1]][bathIndex[0]]);
  if (bathPotential > 1.0e-9) {
    SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_LIB, "Invalid nonzero bath potential in Poisson solution, %g", bathPotential);
  }
  ierr = DMDAVecRestoreArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rhoArray);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckVolumeFraction"
PetscErrorCode CheckVolumeFraction(DMMG *dmmg, DM nDA, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, VerificationOptions *options)
{
  DM             dm = DMMGGetFine(dmmg)->dm;
  DMDALocalInfo  info;
  MPI_Comm       comm;
  Vec            nVec;
  N           ***n;
  PetscReal      n_3, eta = 0.0, error;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(nDA, &nVec);CHKERRQ(ierr);
  ierr = CalculateN_3d(dm, DMMGGetx(dmmg), geomOptions->numSpecies, geomOptions->numAlpha, singleDA, hsOptions, nVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(nDA, nVec, &n);CHKERRQ(ierr);
  n_3  = PetscAbsScalar(n[geomOptions->bathIndex[2]][geomOptions->bathIndex[1]][geomOptions->bathIndex[0]].v[3]);
  ierr = DMDAVecRestoreArray(nDA, nVec, &n);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(nDA, &nVec);CHKERRQ(ierr);
  for(int i = 0; i < geomOptions->numSpecies; ++i) {if (!geomOptions->isConfined[i]) eta += geomOptions->eta[i];}
  error = eta - n_3;
  if (fabs(error)/eta > options->volFracTol) {
    SETERRQ3(comm, PETSC_ERR_PLIB, "Volume fraction violation, eta %g n_3 %g relative error %g", eta, n_3, fabs(error)/eta);
  } else {
    PetscPrintf(comm, "Volume fraction verification\n");
    PetscPrintf(comm, "                       eta             %g\n", eta);
    PetscPrintf(comm, "                       n_3             %g\n", n_3);
    PetscPrintf(comm, "                       relative error  %g\n", fabs(error)/eta);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckHomogeneity"
PetscErrorCode CheckHomogeneity(DMMG *dmmg, Vec x, GeometryOptions *geomOptions, VerificationOptions *options)
{
  Vec           *stddev;
  MPI_Comm       comm;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) DMMGGetFine(dmmg)->dm, &comm);CHKERRQ(ierr);
  ierr = PetscOptionsHasName(PETSC_NULL, "-vec_view_avg", &flag);CHKERRQ(ierr);
  if (flag) {
    Vec *avg;

    if (geomOptions->domainType== DOMAIN_SPHERE) {
      ierr = CalculateRadialAverage(DMMGGetFine(dmmg)->dm, geomOptions->numSpecies, x, &avg);CHKERRQ(ierr);
    } else {
      ierr = CalculateXYAverage(DMMGGetFine(dmmg)->dm, geomOptions->numSpecies, x, &avg);CHKERRQ(ierr);
    }
    for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
      ierr = VecView(avg[sp], PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);
      ierr = VecDestroy(&avg[sp]);CHKERRQ(ierr);
    }
    ierr = PetscFree(avg);CHKERRQ(ierr);
  }
  if (geomOptions->domainType== DOMAIN_SPHERE) {
    ierr = CalculateRadialStdDev(DMMGGetFine(dmmg)->dm, geomOptions->numSpecies, x, &stddev);CHKERRQ(ierr);
  } else {
    ierr = CalculateXYStdDev(DMMGGetFine(dmmg)->dm, geomOptions->numSpecies+1, x, &stddev);CHKERRQ(ierr);
  }
  for(PetscInt sp = 0; sp < geomOptions->numSpecies+1; ++sp) {
    PetscReal   s;
    PetscInt    p;
    const char *name = "Gamma";

    ierr = VecMax(stddev[sp], &p, &s);CHKERRQ(ierr);
    if (s > options->homoTol) {
      SETERRQ2(comm, PETSC_ERR_PLIB, "Homogeneity violation, std deviation %g z %d", s, p);
    } else {
      if (sp < geomOptions->numSpecies) {
        name = geomOptions->ionNames[sp];
      }
      PetscPrintf(comm, "%s Homogeneity verification\n", name);
      PetscPrintf(comm, "                       std deviation   %g\n", s);
    }
    if (debug == 4) {ierr = VecView(stddev[sp], PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);}
    ierr = VecDestroy(&stddev[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree(stddev);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckSumRule"
PetscErrorCode CheckSumRule(DMMG *dmmg, const PetscReal rhoBath[], DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions)
{
  DM             dm = DMMGGetFine(dmmg)->dm;
  MPI_Comm       comm;
  Rho         ***rho;
  PetscReal      rhoP = 0.0, P_HS, P_ES, P, error;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
    PetscReal z, frac;
    PetscInt  i;

    z     = (geomOptions->wallPos+geomOptions->R[sp])/geomOptions->h[2];
    // We will linearly extrapolate
    i     = (PetscInt) z + 1;
    frac  = z - i;
    rhoP += kT*((1.0 - frac)*PetscAbsScalar(rho[i][0][0].v[sp]) + frac*PetscAbsScalar(rho[i+1][0][0].v[sp]));
  }
  ierr = DMDAVecRestoreArray(dm, DMMGGetx(dmmg), &rho);CHKERRQ(ierr);
  ierr = CalculatePressureHS(geomOptions->numSpecies, geomOptions->R, geomOptions->isConfined, rhoBath, &P_HS);CHKERRQ(ierr);
  ierr = CalculatePressureES(geomOptions->numSpecies, geomOptions->R, geomOptions->isConfined, rhoBath, esOptions->z, esOptions->GammaBath, &P_ES);CHKERRQ(ierr);
  P     = P_HS + P_ES;
  error = P - rhoP;
  if (fabs(error)/P > 0.20) {
    SETERRQ3(comm, PETSC_ERR_PLIB, "Sum rule violation, P %g kT rho %g relative error %g", P, rhoP, fabs(error)/P);
  } else {
    PetscPrintf(comm, "Sum rule verification\n");
    PetscPrintf(comm, "                       P               %g\n", P);
    PetscPrintf(comm, "                       kT rho          %g\n", rhoP);
    PetscPrintf(comm, "                       relative error  %g\n", fabs(error)/P);
    if (esOptions->usePoisson) {
      DMDALocalInfo  info;
      Vec            phiVec;
      PetscScalar ***phi;
      PetscReal      z, frac;
      PetscReal      phiP = 0.0;
      PetscInt       i;

      ierr = DMGetGlobalVector(singleDA, &phiVec);CHKERRQ(ierr);
      ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
      ierr = CalculatePhi(&info, singleDA, geomOptions, hsOptions, esOptions, DMMGGetx(dmmg), phiVec);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(singleDA, phiVec, &phi);CHKERRQ(ierr);
      z     = (geomOptions->wallPos)/geomOptions->h[2];
      // We will linearly interpolate
      i     = (PetscInt) z + 1;
      frac  = z - i;
      phiP += ((1.0 - frac)*PetscAbsScalar(phi[i][0][0]) + frac*PetscAbsScalar(phi[i+1][0][0]));
      PetscPrintf(comm, "                       phi             %g\n", phiP);
      ierr = DMDAVecRestoreArray(singleDA, phiVec, &phi);CHKERRQ(ierr);
      ierr = DMRestoreGlobalVector(singleDA, &phiVec);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckInitialGuess"
PetscErrorCode CheckInitialGuess(DMMG *dmmg, DM singleDA, Mat F, Vec x, GeometryOptions *geomOptions)
{
  DM             dm         = DMMGGetFine(dmmg)->dm;
  const PetscInt numSpecies = geomOptions->numSpecies;
  Vec            xhat, xnew, xsplit[NUM_SPECIES];
  PetscReal      scale;
  PetscInt       M, N, P;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMDAGetInfo(singleDA, 0, &M, &N, &P, 0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  scale = 1.0/((PetscReal) M*N*P);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = DMGetGlobalVector(singleDA, &xsplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(singleDA, &xhat);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &xnew);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(x, xsplit, INSERT_VALUES);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = MatMult(F, xsplit[sp], xhat);CHKERRQ(ierr);
    ierr = MatMultTranspose(F, xhat, xnew);CHKERRQ(ierr);
    ierr = VecScale(xnew, scale);CHKERRQ(ierr);
    ierr = VecAXPY(xnew, -1.0, xsplit[sp]);CHKERRQ(ierr);
    {
      PetscViewer    viewer;
      std::ostringstream filename;

      filename << "rhoDiff_" << sp << ".vtk";
      ierr = PetscViewerASCIIOpen(PETSC_COMM_WORLD, filename.str().c_str(), &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_VTK);CHKERRQ(ierr);
      ierr = DMView(dm, viewer);CHKERRQ(ierr);
      ierr = VecView(xnew, viewer);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    }
  }
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    ierr = DMRestoreGlobalVector(dm, &xsplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMRestoreGlobalVector(dm, &xhat);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &xnew);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
