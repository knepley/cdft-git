#include <windowFunctions.h>

PetscScalar OmegaHat_2(const PetscScalar x[], const int species) {
  PetscReal      R = globalGeometryOptions->R[species];
  PetscReal      k[3], alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));

  // OmegaHat_2 goes to 4*pi*R^2 as alpha goes to 0
  if (alpha < 1.0e-10) {
    return 4.0*M_PI*R*R;
  }
  return 4.0*M_PI*R*sin(R*alpha)/alpha;
}

PetscScalar OmegaHat_0(const PetscScalar x[], const int species) {
  return OmegaHat_2(x, species)/(4.0*M_PI*PetscSqr(globalGeometryOptions->R[species]));
}

PetscScalar OmegaHat_1(const PetscScalar x[], const int species) {
  return OmegaHat_2(x, species)/(4.0*M_PI*globalGeometryOptions->R[species]);
}

PetscScalar OmegaHat_3(const PetscScalar x[], const int species) {
  PetscReal      R = globalGeometryOptions->R[species];
  PetscReal      k[3], alpha, R_alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));
  R_alpha = R*alpha;

  // OmegaHat_3 goes to 4/3*pi*R^3 in the limit as alpha goes to zero
  if (alpha < 1.0e-10) {
    return 4.0*M_PI*R*R*R/3.0;
  }
  return 4.0*M_PI*(sin(R_alpha) - R_alpha*cos(R_alpha))/(alpha*alpha*alpha);
}

// This is the common part of the V1 & V2 omega vectors
PetscScalar OmegaHatVec(const PetscReal k[], const int species, const PetscReal alpha) {
  // OmegaHat_V2 & V1 (all components) go to 0 in the limit as alpha goes to 0
  if (alpha < 1.0e-10) {
    return 0.0;
  }
  // Extra alpha in denom is the magnitude from breaking up the vector into components
  PetscReal R_alpha = globalGeometryOptions->R[species]*alpha;

  return (-4.0*PETSC_i*M_PI*(sin(R_alpha) - (R_alpha*cos(R_alpha)))/(alpha*PetscSqr(alpha)));
}

PetscScalar OmegaHat_V2_Kx(const PetscScalar x[], const int species) {
  PetscReal      k[3], alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));

  return k[0]*OmegaHatVec(k, species, alpha);
}

PetscScalar OmegaHat_V2_Ky(const PetscScalar x[], const int species) {
  PetscReal      k[3], alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));

  return k[1]*OmegaHatVec(k, species, alpha);
}

PetscScalar OmegaHat_V2_Kz(const PetscScalar x[], const int species) {
  PetscReal      k[3], alpha;
  PetscErrorCode ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);
  alpha = sqrt(PetscSqr(k[0]) + PetscSqr(k[1]) + PetscSqr(k[2]));

  return k[2]*OmegaHatVec(k, species, alpha);
}

PetscScalar OmegaHat_V1_Kx(const PetscScalar x[], const int species) {
  return OmegaHat_V2_Kx(x, species)/(4.0*M_PI*globalGeometryOptions->R[species]);
}
PetscScalar OmegaHat_V1_Ky(const PetscScalar x[], const int species) {
  return OmegaHat_V2_Ky(x, species)/(4.0*M_PI*globalGeometryOptions->R[species]);
}
PetscScalar OmegaHat_V1_Kz(const PetscScalar x[], const int species) {
  return OmegaHat_V2_Kz(x, species)/(4.0*M_PI*globalGeometryOptions->R[species]);
}
