#ifndef __DFT_PHIDER_H
#define __DFT_PHIDER_H

#include <dftUtils.h>

PetscScalar PhiDer_0(const PetscScalar n[]);
PetscScalar PhiDer_1(const PetscScalar n[]);
PetscScalar PhiDer_2(const PetscScalar n[]);
PetscScalar PhiDer_3(const PetscScalar n[]);
PetscScalar PhiDer_V1_Kx(const PetscScalar n[]);
PetscScalar PhiDer_V1_Ky(const PetscScalar n[]);
PetscScalar PhiDer_V1_Kz(const PetscScalar n[]);
PetscScalar PhiDer_V2_Kx(const PetscScalar n[]);
PetscScalar PhiDer_V2_Ky(const PetscScalar n[]);
PetscScalar PhiDer_V2_Kz(const PetscScalar n[]);

#endif /* __DFT_PHIDER_H */
