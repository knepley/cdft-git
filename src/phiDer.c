#include <phiDer.h>

PetscScalar PhiDer_0(const PetscScalar n[]) {
  return -log(1.0 - PetscRealPart(n[3]));
}

PetscScalar PhiDer_1(const PetscScalar n[]) {
  return PetscRealPart(n[2])/(1.0 - PetscRealPart(n[3]));
}

PetscScalar PhiDer_2(const PetscScalar n[]){
  const PetscReal magVTwo = PetscRealPart(n[7])*PetscRealPart(n[7])+PetscRealPart(n[8])*PetscRealPart(n[8])+PetscRealPart(n[9])*PetscRealPart(n[9]);
  PetscScalar ratio = ((magVTwo < 1.0e-16) && (PetscAbsScalar(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16)) ? 0.0 : (magVTwo/PetscSqr(PetscRealPart(n[2])));

  if (PetscRealPart(ratio) > 1.0) {
    if ((debug >= 7) && (magVTwo > 1.0e-7)) {
      PetscPrintf(PETSC_COMM_SELF, "n_2: %g |n_V2|^2: %g |n_V2|^2/n^2_2: %g\n", PetscRealPart(n[2]), magVTwo, PetscRealPart(ratio));
    }
    ratio = 1.0;
  }
  return (PetscRealPart(n[1])/(1 - PetscRealPart(n[3])))+((PetscSqr(PetscRealPart(n[2]))/(8*M_PI*PetscSqr(1 - PetscRealPart(n[3]))))*PetscSqr(1 - PetscRealPart(ratio))*(1 - PetscRealPart(ratio)))+((magVTwo/(4*M_PI*PetscSqr(1 - PetscRealPart(n[3]))))*PetscSqr(1 - PetscRealPart(ratio)));
}

PetscScalar PhiDer_3(const PetscScalar n[]){
  const PetscReal magVTwo = PetscRealPart(n[7])*PetscRealPart(n[7])+PetscRealPart(n[8])*PetscRealPart(n[8])+PetscRealPart(n[9])*PetscRealPart(n[9]);
  PetscScalar ratio = ((magVTwo < 1.0e-16) && (PetscAbsScalar(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16)) ? 0.0 : (magVTwo/PetscSqr(PetscRealPart(n[2])));

  if (PetscRealPart(ratio) > 1.0) {
    if ((debug >= 7) && (magVTwo > 1.0e-7)) {
      PetscPrintf(PETSC_COMM_SELF, "n_2: %g |n_V2|^2: %g |n_V2|^2/n^2_2: %g\n", PetscRealPart(n[2]), magVTwo, PetscRealPart(ratio));
    }
    ratio = 1.0;
  }
  return (PetscRealPart(n[0])/(1-PetscRealPart(n[3])))+((PetscRealPart(n[1])*PetscRealPart(n[2])-(PetscRealPart(n[4])*PetscRealPart(n[7])+PetscRealPart(n[5])*PetscRealPart(n[8])+PetscRealPart(n[6])*PetscRealPart(n[9])))/PetscSqr(1-PetscRealPart(n[3])))+((PetscRealPart(n[2])*PetscSqr(PetscRealPart(n[2]))/(12*M_PI*PetscSqr(1-PetscRealPart(n[3]))*(1-PetscRealPart(n[3]))))*(1-PetscRealPart(ratio)));
}

PetscScalar PhiDer_V1_Kx(const PetscScalar n[]){
  return (-PetscRealPart(n[7])/(1 - PetscRealPart(n[3])));
}
PetscScalar PhiDer_V1_Ky(const PetscScalar n[]){
  return (-PetscRealPart(n[8])/(1 - PetscRealPart(n[3])));
}
PetscScalar PhiDer_V1_Kz(const PetscScalar n[]){
  return (-PetscRealPart(n[9])/(1 - PetscRealPart(n[3])));
}

//Common portion of V2x,y,z:
PetscScalar PhiDer_V2Common(const PetscScalar n[]){
  const PetscReal magVTwo = PetscRealPart(n[7])*PetscRealPart(n[7])+PetscRealPart(n[8])*PetscRealPart(n[8])+PetscRealPart(n[9])*PetscRealPart(n[9]);
  PetscScalar ratio = ((magVTwo < 1.0e-16) && (PetscAbsScalar(PetscSqr(PetscRealPart(n[2]))) < 1.0e-16)) ? 0.0 : (magVTwo/PetscSqr(PetscRealPart(n[2])));	

  if (PetscRealPart(ratio) > 1.0) {
    if ((debug >= 7) && (magVTwo > 1.0e-7)) {
      PetscPrintf(PETSC_COMM_SELF, "n_2: %g |n_V2|^2: %g |n_V2|^2/n^2_2: %g\n", PetscRealPart(n[2]), magVTwo, PetscRealPart(ratio));
    }
    ratio = 1.0;
  }
  return (-PetscRealPart(n[2])/(4*M_PI*PetscSqr(1-PetscRealPart(n[3]))))*PetscSqr(1-PetscRealPart(ratio));
}

PetscScalar PhiDer_V2_Kx(const PetscScalar n[]){
  return (-PetscRealPart(n[4])/(1-PetscRealPart(n[3])))+PhiDer_V2Common(n)*PetscRealPart(n[7]);
}

PetscScalar PhiDer_V2_Ky(const PetscScalar n[]){
  return (-PetscRealPart(n[5])/(1-PetscRealPart(n[3])))+PhiDer_V2Common(n)*PetscRealPart(n[8]);
}

PetscScalar PhiDer_V2_Kz(const PetscScalar n[]){
  return (-PetscRealPart(n[6])/(1-PetscRealPart(n[3])))+PhiDer_V2Common(n)*PetscRealPart(n[9]);
}
