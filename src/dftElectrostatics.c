#include <dftElectrostatics.h>

#ifdef DFT_USE_CUDA
#include <screening.hh>
#endif

ElectrostaticsOptions *globalElectrostaticsOptions;

const char *screeningComputationTypes[8] = {"quadrature", "spectralQuadrature", "spectralQuadratureGPU", "spectralQuadratureDimReduct", "spectralQuadratureTest", "ScreeningComputationType", "what?", PETSC_NULL};

#undef __FUNCT__
#define __FUNCT__ "ForceElectroneutrality"
// Adjust the bath concentration of a given species in order to assure electroneutrality
PetscErrorCode ForceElectroneutrality(const PetscInt species, const PetscInt numSpecies, const PetscBool isConfined[], HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions)
{
  PetscReal chargeDensity = 0.0;

  PetscFunctionBegin;
  if (isConfined[species]) {SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Cannot adjust bath concentrations of confined species %d", species);}
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (isConfined[sp] || sp == species) continue;
    chargeDensity += hsOptions->rhoBath[sp]*esOptions->z[sp];
  }
  hsOptions->rhoBath[species] = -chargeDensity/esOptions->z[species];
  if (hsOptions->rhoBath[species] < 0.0) {SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Unable to make bath electroneutral using species %d", species);}
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ProcessElectrostaticsOptions"
PetscErrorCode ProcessElectrostaticsOptions(MPI_Comm comm, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options)
{
  PetscInt       n;
  PetscBool      flag;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  options->useMuES                = PETSC_FALSE;
  options->usePoisson             = PETSC_FALSE;
  options->usePoissonBoltzmann    = PETSC_FALSE;
  options->calcPoissonBoltzmannFourier = PETSC_TRUE;
  options->updateReferenceDensity = 1;
  options->screeningComputationType = SCREENING_SPECTRAL_QUADRATURE;
  options->bufferSpecies          = -1;
  options->rhoBoundary            = 0.0;
  options->dimReductErr           = 1e-6;
  options->calcRhoRefErr          = PETSC_FALSE;

  ierr = PetscOptionsBegin(comm, "", "DFT Exelectrostatics Options", "DMMG");CHKERRQ(ierr);
    ierr = PetscOptionsBool("-useMuES", "Flag for electrostatic screening", __FILE__, options->useMuES, &options->useMuES, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-usePoisson", "Flag for mean field electrostatics", __FILE__, options->usePoisson, &options->usePoisson, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-usePoissonBoltzmann", "Flag for different electrostatic splitting", __FILE__, options->usePoissonBoltzmann, &options->usePoissonBoltzmann, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsBool("-calcPoissonBoltzmannFourier", "Flag for calculating the PB solve in Fourier space", __FILE__, options->calcPoissonBoltzmannFourier, &options->calcPoissonBoltzmannFourier, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsInt("-updateReference", "Flag for updating the reference density", __FILE__, options->updateReferenceDensity, &options->updateReferenceDensity, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsEnum("-screeningComputationType", "The type of computation done to produce the reference density", __FILE__, screeningComputationTypes, (PetscEnum) options->screeningComputationType, (PetscEnum*) &options->screeningComputationType, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-rhoBoundary", "Singular charge density (Coulomb/m^2)", __FILE__, options->rhoBoundary, &options->rhoBoundary, PETSC_NULL);CHKERRQ(ierr);
    ierr = PetscOptionsReal("-dimReductErr", "The allowed L_inf tolerance on the screening", __FILE__, options->dimReductErr, &options->dimReductErr, PETSC_NULL);CHKERRQ(ierr);
    // convert to C/nm^2
    ierr = PetscOptionsBool("-rhoRefErr", "Flag for calculating the rho ref error", __FILE__, options->calcRhoRefErr, &options->calcRhoRefErr, PETSC_NULL);CHKERRQ(ierr);
    options->rhoBoundary *= 1e-18;
    ierr = PetscPrintf(PETSC_COMM_WORLD, "rhoBoundary: %g C/nm^2\n", options->rhoBoundary); CHKERRQ(ierr);

    for(PetscInt i = 0; i < geomOptions->numSpecies; ++i) {
      if(i == 0){
        options->z[i] = 2.0;
      } else{
        options->z[i] = -1.0;
      }
    }

    n = geomOptions->numSpecies;
    ierr = PetscOptionsRealArray("-z", "The valence of species i", __FILE__, options->z, &n, &flag);CHKERRQ(ierr);
    if (flag && (n != geomOptions->numSpecies)) {SETERRQ(comm, PETSC_ERR_ARG_WRONG, "Invalid number of valencies given");}
    ierr = PetscOptionsInt("-forceNeutrality", "Use the given species to enforce electroneutrality", __FILE__, options->bufferSpecies, &options->bufferSpecies, &flag);CHKERRQ(ierr);
    if (flag) {
      ierr = ForceElectroneutrality(options->bufferSpecies, geomOptions->numSpecies, geomOptions->isConfined, hsOptions, options);CHKERRQ(ierr);
    }
  ierr = PetscOptionsEnd();

#ifdef DFT_USE_CUDA
  if (options->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE_GPU || options->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE_TEST) {
    Py_Initialize();
    initscreening();
    if (PyErr_Occurred()) {
      PyErr_Print();
      SETERRQ(comm, PETSC_ERR_PLIB, "Unable to import GPU module");
    }
  }
#endif

  ierr = PetscLogEventRegister("Calc cPsiHat",   PETSC_SMALLEST_CLASSID, &options->cPsiHatEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc rho_ref",   PETSC_SMALLEST_CLASSID, &options->rhoRefEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc mu_ES",     PETSC_SMALLEST_CLASSID, &options->muESEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc phi",       PETSC_SMALLEST_CLASSID, &options->phiEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Calc PB frozen", PETSC_SMALLEST_CLASSID, &options->pbFrozenEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrintElectrostaticsModule"
PetscErrorCode PrintElectrostaticsModule(PetscViewer viewer, const char indent[], GeometryOptions *geomOpts, ElectrostaticsOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscViewerASCIIPrintf(viewer, "%sclass Electrostatics:\n", indent);CHKERRQ(ierr);
  if (options->useMuES) {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  useMuES    = True\n", indent);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  useMuES    = False\n", indent);CHKERRQ(ierr); 
  }
  if (options->usePoisson) {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  usePoisson = True\n", indent);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  usePoisson = False\n", indent);CHKERRQ(ierr); 
  }
  if (options->usePoissonBoltzmann) {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  usePoissonBoltzmann = True\n", indent);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(viewer, "%s  usePoissonBoltzmann = False\n", indent);CHKERRQ(ierr); 
  }
  ierr = PetscViewerASCIIPrintf(viewer, "%s  rhoBoundary = %g\n", indent, options->rhoBoundary);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  z          = [", indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "%g", options->z[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "]\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  bufferSpecies = %d\n", indent, options->bufferSpecies);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "%s  muESBath = {", indent);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < geomOpts->numSpecies; ++sp) {
    if (sp > 0) {ierr = PetscViewerASCIIPrintf(viewer, ", ");CHKERRQ(ierr);}
    ierr = PetscViewerASCIIPrintf(viewer, "'%s': %g", geomOpts->ionNames[sp], options->muESBath[sp]);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer, "}\n");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuESBath"
PetscErrorCode CalculateMuESBath(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal Gamma, PetscReal muESBath[])
{
  PetscReal      eta;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr  = CalculateEta(numSpecies, R, isConfined, rhoBath, z, Gamma, &eta);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    const PetscReal sigma = 2.0*R[sp];

    if (isConfined[sp]) {
      muESBath[sp] = 0.0;
    } else {
      muESBath[sp] = -e*e/(4.0*M_PI*epsilon)*
        ((Gamma*z[sp]*z[sp])/(1.0 + Gamma*sigma) + eta*sigma*((2.0*z[sp] - eta*sigma*sigma)/(1.0 + Gamma*sigma) + eta*sigma*sigma/3.0));
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateEta"
PetscErrorCode CalculateEta(const int numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal Gamma, PetscReal *eta)
{
  PetscReal xi_3  = 0.0, Delta;
  PetscReal omega = 0.0;
  PetscReal eta_t = 0.0;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    const PetscReal sigma  = 2.0*R[sp];
    const PetscReal sigma3 = PetscSqr(sigma)*sigma;

    xi_3  += (rhoBath[sp]*sigma3);
    omega += (rhoBath[sp]*sigma3)/(1.0 + Gamma*sigma);
    eta_t += (rhoBath[sp]*sigma*z[sp])/(1.0 + Gamma*sigma);
  }
  xi_3  *= M_PI/6.0;
  Delta  = 1.0 - xi_3;
  omega *= M_PI/(2.0*Delta);
  eta_t *= M_PI/(2.0*Delta);
  omega += 1.0;
  eta_t *= (1.0/omega);
  *eta   = eta_t;
  //PetscPrintf(PETSC_COMM_WORLD, "Calculating eta:\n  Gamma = %g\n", Gamma);
  //PetscPrintf(PETSC_COMM_WORLD, "  omega = %g\n  eta   = %g\n", omega, eta_t);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePressureES"
PetscErrorCode CalculatePressureES(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal Gamma, PetscReal *P)
{
  PetscReal      eta;
  PetscErrorCode ierr;

  ierr = CalculateEta(numSpecies, R, isConfined, rhoBath, z, Gamma, &eta);CHKERRQ(ierr);
  *P = (-PetscSqr(Gamma)*Gamma/(3.0*M_PI) - e*e*eta*eta/(2.0*M_PI*M_PI*kT*epsilon))*kT;
  PetscPrintf(PETSC_COMM_WORLD, "Eta: %g P_ES: %g\n", eta, *P);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Rhs_MSAScreening"
PetscErrorCode Rhs_MSAScreening(SNES snes, Vec X, Vec F, void *ctx) {
  MSAContext    *msa = (MSAContext *) ctx;
  PetscScalar    x;
  PetscReal      Gamma, eta, sum = 0.0;
  PetscInt       zero = 0;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  ierr = VecGetValues(X, 1, &zero, &x);CHKERRQ(ierr);
  Gamma = PetscRealPart(x);
  ierr = CalculateEta(msa->numSpecies, msa->R, msa->isConfined, msa->rho, msa->z, Gamma, &eta);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < msa->numSpecies; ++sp){
    PetscReal sigma = 2.0*msa->R[sp];

    sum += msa->rho[sp]*PetscSqr((msa->z[sp] - eta*PetscSqr(sigma))/(1.0 + Gamma*sigma));
  }
  ierr = VecSetValue(F, 0, 4.0*PetscSqr(Gamma) - (e*e/(kT*epsilon))*sum, INSERT_VALUES);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MSAScreeningSetup"
PetscErrorCode MSAScreeningSetup(PetscReal rho[], GeometryOptions *geomOpts, ElectrostaticsOptions *options, MSAContext *ctx) {
  MPI_Comm       comm = PETSC_COMM_SELF;
  KSP            ksp;
  PC             pc;
  Mat            J;
  Vec            r;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  // Setup context
  ctx->numSpecies = geomOpts->numSpecies;
  ctx->isConfined = geomOpts->isConfined;
  ctx->R          = geomOpts->R;
  ctx->z          = options->z;
  ctx->rho        = rho;
  // Create Objects
  ierr = SNESCreate(comm, &ctx->snes);CHKERRQ(ierr);
  ierr = SNESSetOptionsPrefix(ctx->snes, "msa_");CHKERRQ(ierr);
  ierr = PetscObjectSetTabLevel((PetscObject) ctx->snes, 3);CHKERRQ(ierr);
  ierr = VecCreate(comm, &ctx->x);CHKERRQ(ierr);
  ierr = VecSetSizes(ctx->x, PETSC_DECIDE, 1);CHKERRQ(ierr);
  ierr = VecSetFromOptions(ctx->x);CHKERRQ(ierr);
  // Set function
  ierr = VecDuplicate(ctx->x, &r);CHKERRQ(ierr);
  ierr = SNESSetFunction(ctx->snes, r, Rhs_MSAScreening, ctx);CHKERRQ(ierr);
  ierr = VecDestroy(&r);CHKERRQ(ierr);
  // Create and set matrix-free operator
  ierr = MatCreateSNESMF(ctx->snes, &J);CHKERRQ(ierr);
  ierr = MatSetFromOptions(J);CHKERRQ(ierr);
  ierr = SNESSetJacobian(ctx->snes, J, J, MatMFFDComputeJacobian, ctx);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  // Force no preconditioner
  ierr = SNESGetKSP(ctx->snes, &ksp);CHKERRQ(ierr);
  ierr = KSPGetPC(ksp, &pc);CHKERRQ(ierr);
  ierr = PCSetType(pc, PCNONE);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(ctx->snes);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateGamma"
PetscErrorCode CalculateGamma(MSAContext *ctx, PetscScalar initialGuess, PetscReal *gamma) {
  PetscScalar    x;
  PetscInt       zero = 0;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  // Set initial guess
  ierr = VecSet(ctx->x, initialGuess);CHKERRQ(ierr);
  // Solve
  ierr = SNESSolve(ctx->snes, PETSC_NULL, ctx->x);CHKERRQ(ierr);
  // Output
  ierr = VecGetValues(ctx->x, 1, &zero, &x);CHKERRQ(ierr);
  *gamma = PetscRealPart(x);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MSAScreeningCleanup"
PetscErrorCode MSAScreeningCleanup(MSAContext *ctx) {
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecDestroy(&ctx->x);CHKERRQ(ierr);
  ierr = SNESDestroy(&ctx->snes);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "cPsiHat"
PetscScalar cPsiHat(const PetscScalar x[], const PetscScalar rho[], const int spI, const int spJ) {
  const PetscReal Gamma = PetscRealPart(rho[globalGeometryOptions->numSpecies]);
  const PetscReal s     = 1.0/(2.0*Gamma);
  PetscReal       k[3];
  PetscErrorCode  ierr;

  ierr = CalculateK(x, globalGeometryOptions->h, globalGeometryOptions->dims, k);CHKERRQ(ierr);

  PetscReal Rij     = globalGeometryOptions->R[spI] + globalGeometryOptions->R[spJ];
  PetscReal K       = sqrt(PetscSqr(k[0])+PetscSqr(k[1])+PetscSqr(k[2]));
  PetscReal lambdaI = globalGeometryOptions->R[spI] + s;
  PetscReal lambdaJ = globalGeometryOptions->R[spJ] + s;
  PetscReal factor  = globalElectrostaticsOptions->z[spI]*globalElectrostaticsOptions->z[spJ]*PetscSqr(e)/(8.0*M_PI*epsilon);
  PetscReal cosKR   = cos(K*Rij);
  PetscReal sinKR   = sin(K*Rij);
  PetscReal I_m1, I_0, I_1;

  // Notice that these are 4\pi/k times the values in our paper
  if (K < 1e-10) {
    I_m1 = 2.0*M_PI*Rij*Rij;
    I_0  = 4.0*M_PI*Rij*Rij*Rij/3.0;
    I_1  = M_PI*Rij*Rij*Rij*Rij;
  } else {
    I_m1 = (4.0*M_PI/(K*K))*(1.0 - cosKR);
    I_0  = (4.0*M_PI/(K*K))*(-Rij*cosKR + sinKR/K);
    I_1  = (4.0*M_PI/(K*K))*(-Rij*Rij*cosKR + (2.0*Rij/K)*sinKR - (2.0/(K*K))*(1 - cosKR));
  }
  PetscReal termA = (1.0/(2.0*lambdaI*lambdaJ))*I_1;
  PetscReal termB = -((lambdaI + lambdaJ)/(lambdaI*lambdaJ))*I_0;
  PetscReal termC = ((PetscSqr(lambdaI - lambdaJ)/(2.0*lambdaI*lambdaJ)) + 2.0)*I_m1;
  return factor*(termA + termB + termC);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateCPsiHatFunctions"
PetscErrorCode CalculateCPsiHatFunctions(DM dm, const int numSpecies, PetscScalar (*func)(const PetscScalar *, const PetscScalar *, const int, const int), Vec v[], Rho **field[], ElectrostaticsOptions *options) {
  EvaluationOptions ctx;
  Vec               X;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->cPsiHatEvent,0,0,0,0);CHKERRQ(ierr);
  ctx.field = field;
  ctx.func2 = func;
  ierr = DMGetGlobalVector(dm, &X);CHKERRQ(ierr);
  // Can optimize by only calculating the upper triangle
  for(PetscInt spI = 0; spI < numSpecies; ++spI) {
	ctx.spI = spI;
	for(PetscInt spJ = 0; spJ < numSpecies; ++spJ) {
      ctx.spJ = spJ;
      if (v[spI*numSpecies+spJ] == PETSC_NULL) {
        ierr = DMCreateGlobalVector(dm, &v[spI*numSpecies+spJ]);CHKERRQ(ierr);
      }
      ierr = DMDAFormFunctionLocal(dm, (DMDALocalFunction1) Function_3d_TwoSpecies, X, v[spI*numSpecies+spJ], (void *) &ctx);CHKERRQ(ierr);
      ierr = PrintNorm(v[spI*numSpecies+spJ], "cPsiVecs", spI, spJ);
      ierr = VecViewCenterSingle(dm, v[spI*numSpecies+spJ], PETSC_VIEWER_DRAW_WORLD, "cPsiHat", spI, spJ);CHKERRQ(ierr);
	}
  }
  ierr = DMRestoreGlobalVector(dm, &X);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(options->cPsiHatEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CreateElectrostatics"
PetscErrorCode CreateElectrostatics(DM dm, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options)
{
  MSAContext     msa;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MSAScreeningSetup(hsOptions->rhoBath, geomOptions, options, &msa);CHKERRQ(ierr);
  ierr = CalculateGamma(&msa, 1.0, &options->GammaBath);CHKERRQ(ierr);
  ierr = MSAScreeningCleanup(&msa);CHKERRQ(ierr);
  ierr = CalculateMuESBath(geomOptions->numSpecies, geomOptions->R, geomOptions->isConfined, hsOptions->rhoBath, options->z, options->GammaBath, options->muESBath);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < PetscSqr(geomOptions->numSpecies); ++sp) {
    options->cPsiHatVecs[sp] = PETSC_NULL;
  }
  ierr = DMCreateGlobalVector(dm, &options->frozenPBVariables);CHKERRQ(ierr);
  ierr = VecSet(options->frozenPBVariables, 1.0);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(singleDA, &options->phi);CHKERRQ(ierr);
  ierr = VecSet(options->phi, 0.0);CHKERRQ(ierr);
  if (debug >= 1) {
    MPI_Comm comm;

    ierr = PetscObjectGetComm((PetscObject) dm, &comm);CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "Using the MSA Gamma = %g nm^-1 screening length %g nm\n", options->GammaBath, 1.0/(2.0*options->GammaBath));CHKERRQ(ierr);
    ierr = PetscPrintf(comm, "Calculated the electrostatic part of the chemical potential in the bath:\n");CHKERRQ(ierr); 
    for(PetscInt i = 0; i < geomOptions->numSpecies; ++i) {
      ierr = PetscPrintf(comm, "muESBath_%d: %g\n", i,options->muESBath[i]);CHKERRQ(ierr);
    }
  }
  ierr = PetscLogEventRegister("Screen Int", SNES_CLASSID, &options->screeningIntegralEvent);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("Screen Int Z", SNES_CLASSID, &options->screeningIntegralZPlaneEvent);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DestroyElectrostatics"
PetscErrorCode DestroyElectrostatics(DM dm, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < PetscSqr(geomOptions->numSpecies); ++sp) {
    ierr = VecDestroy(&options->cPsiHatVecs[sp]);CHKERRQ(ierr);
  }
  ierr = VecDestroy(&options->frozenPBVariables);CHKERRQ(ierr);
  ierr = VecDestroy(&options->phi);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePhi_Lagged"
PetscErrorCode CalculatePhi_Lagged(DMDALocalInfo *info, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions, Vec phi)
{
  PetscReal     *h = geomOptions->h;
  PetscInt       M = geomOptions->dims[0];
  PetscInt       N = geomOptions->dims[1];
  PetscInt       P = geomOptions->dims[2];
  PetscReal      scale;
  PetscScalar ***phiHatArray;
  Rho         ***rhoHatArray;
  Vec            phiHat;
  PetscReal     *z = esOptions->z;
  PetscErrorCode ierr;
	
  PetscFunctionBegin;
  scale = 1.0/((PetscReal) M*N*P);
  ierr = DMGetGlobalVector(singleDA, &phiHat);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, phiHat, &phiHatArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, hsOptions->rhoHat, &rhoHatArray);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    PetscReal kz = k <= P/2 ? 2.0*M_PI*k/(P) : -2.0*M_PI*(P-k)/(P);
    PetscScalar rhoBoundaryHat = 0.0;
    if(geomOptions->domainType == DOMAIN_WALL) {// calculate the Fourier transform of the singular charge; now valid only for DOMAIN_WALL
      // rhoBoundaryHat = exp(-i*k*r0), 
      //     where r0 = (0.0, 0.0, z0) is the position of the wall,
      //     and k = (kx/hx, ky/hy, kz/hj) = (2*PI*ii/Lx, 2*PI*jj/Ly, 2*PI*kk/Lz),
      //     where ii, jj, kk are the spatial wavenumbers varying between -M/2 to M/2, etc.
      //     Therefore, we have:
      // rhoBoundaryHat = exp(-i*kz*(z0/hz))

      // zz0 = z0/hz; techinically, it could be precomputed outside of the loop, 
      // but would cause a prolifiration of if(DOMAIN_WALL) clauses
      PetscReal zz0 = geomOptions->wallPos/geomOptions->h[2]; // zz0 = z0/hz;       
      rhoBoundaryHat = esOptions->rhoBoundary*PetscScalar(cos(kz*zz0), -sin(kz*zz0));
    }
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      PetscReal ky = j <= N/2 ? 2.0*M_PI*j/(N) : -2.0*M_PI*(N-j)/(N);

      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        PetscReal   kx  = i <= M/2 ? 2.0*M_PI*i/(M) : -2.0*M_PI*(M-i)/(M);
        PetscScalar sum = 0.0;

        for(PetscInt sp = 0; sp < geomOptions->numSpecies; ++sp) {
          sum += z[sp]*rhoHatArray[k][j][i].v[sp];
        }
        PetscReal denom = 2.0*epsilon*((1.0-cos(kx))/PetscSqr(h[0]) + (1.0-cos(ky))/PetscSqr(h[1]) + (1.0-cos(kz))/PetscSqr(h[2]));
                // Omit the zeroth moment
        if(kx == 0 && ky == 0 && kz == 0) {
          phiHatArray[k][j][i] = 0.0;
        } else {
          phiHatArray[k][j][i] = e*e*sum/denom; // Changed units by scaling by e
          if(geomOptions->domainType == DOMAIN_WALL) {// add the singular wall charge (Fourier transform thereof).
            phiHatArray[k][j][i] += rhoBoundaryHat/denom;
          }
        }
        if (PetscIsInfOrNanScalar(phiHatArray[k][j][i])) {
          SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_FP, "Nan or inf at phiHat[%d][%d][%d]: %g ", k, j, i, PetscRealPart(phiHatArray[k][j][i]));
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(info->da, hsOptions->rhoHat, &rhoHatArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(singleDA, phiHat, &phiHatArray);CHKERRQ(ierr);
  ierr = MatMultTranspose(hsOptions->F, phiHat, phi);CHKERRQ(ierr);
  ierr = VecScale(phi, scale);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &phiHat);CHKERRQ(ierr);

  // Force potential in the bath to be 0
  PetscScalar ***phiArray;
  PetscScalar    bathPotential;

  ierr = DMDAVecGetArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  bathPotential = phiArray[geomOptions->bathIndex[2]][geomOptions->bathIndex[1]][geomOptions->bathIndex[0]];
  ierr = DMDAVecRestoreArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  ierr = VecShift(phi, -bathPotential);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#define USE_PB 1

#undef __FUNCT__
#define __FUNCT__ "CalculateConfinedRhoBoltzmann_PB"
// Calculate the bath potential for confined species
PetscErrorCode CalculateConfinedRhoBoltzmann_PB(DM dm, DM singleDA, Rho ***C, PetscScalar ***phi, PetscReal rhoBoltzmann[], PBContext *options)
{
  PetscInt          numSpecies   = options->numSpecies;
  const PetscReal  *R            = options->R;
  const PetscReal  *z            = options->z;
  const PetscReal  *h            = options->h;
  const PetscBool  *isConfined   = options->isConfined;
  const PetscReal  *numParticles = options->numParticles;
  const PetscReal  *L            = options->L;
  const PetscReal   channelL     = options->channelLength;
  const PetscReal   channelR     = options->channelRadius;
  DMDALocalInfo     info;
  DM                coordDA;
  Vec               coordinates;
  DMDACoor3d     ***coords;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (!isConfined[sp]) continue;
    PetscScalar integral = 0.0;
    const PetscReal Ri = R[sp];
    const PetscReal R2 = PetscSqr(PetscMax(channelR - Ri, 0.0));

    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      const PetscReal zi = PetscRealPart(coords[k][0][0].z) - L[2]/2.0;

      if (PetscAbsScalar(zi) - channelL/2.0 + Ri > 1.0e-10) continue;
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
          if (PetscSqr(PetscRealPart(coords[k][j][i].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k][j][i].y) - L[1]/2.0) < R2) {
            integral += C[k][j][i].v[sp]*PetscExpScalar((-z[sp]*phi[k][j][i])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k][j][i+1].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k][j][i+1].y) - L[1]/2.0) < R2) {
            integral += C[k][j][i+1].v[sp]*PetscExpScalar((-z[sp]*phi[k][j][i+1])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k][j+1][i+1].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k][j+1][i+1].y) - L[1]/2.0) < R2) {
            integral += C[k][j+1][i+1].v[sp]*PetscExpScalar((-z[sp]*phi[k][j+1][i+1])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k][j+1][i].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k][j+1][i].y) - L[1]/2.0) < R2) {
            integral += C[k][j+1][i].v[sp]*PetscExpScalar((-z[sp]*phi[k][j+1][i])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k+1][j+1][i].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k+1][j+1][i].y) - L[1]/2.0) < R2) {
            integral += C[k+1][j+1][i].v[sp]*PetscExpScalar((-z[sp]*phi[k+1][j+1][i])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k+1][j+1][i+1].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k+1][j+1][i+1].y) - L[1]/2.0) < R2) {
            integral += C[k+1][j+1][i+1].v[sp]*PetscExpScalar((-z[sp]*phi[k+1][j+1][i+1])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k+1][j][i+1].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k+1][j][i+1].y) - L[1]/2.0) < R2) {
            integral += C[k+1][j][i+1].v[sp]*PetscExpScalar((-z[sp]*phi[k+1][j][i+1])/kT);
          }
          if (PetscSqr(PetscRealPart(coords[k+1][j][i].x) - L[0]/2.0) + PetscSqr(PetscRealPart(coords[k+1][j][i].y) - L[1]/2.0) < R2) {
            integral += C[k+1][j][i].v[sp]*PetscExpScalar((-z[sp]*phi[k+1][j][i])/kT);
          }
        }
      }
    }
    if (PetscIsInfOrNanScalar(integral)) {SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_FP, "Invalid integral: %g", PetscRealPart(integral));}
    rhoBoltzmann[sp] = numParticles[sp]/(0.125*h[0]*h[1]*h[2]*PetscRealPart(integral));
  }
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0); 
}

#undef __FUNCT__
#define __FUNCT__ "PoissonBoltzmann_Rhs"
// The PB equation is -Delta \phi - \sum_i z_i e e/\epsilon \rho^{Boltzmann}_i C_i e^{-\beta z_i \phi}
PetscErrorCode PoissonBoltzmann_Rhs(DMDALocalInfo *info, PetscScalar ***phi, PetscScalar ***f, void *ctx)
{
  PBContext        *options    = (PBContext *) ctx;
  Vec               C          = options->C;
  const PetscInt    numSpecies = options->numSpecies;
  const PetscInt   *bathIndex  = options->bathIndex;
  const PetscReal  *z          = options->z;
  const PetscReal  *h          = options->h;
  const PetscScalar sc         = h[0]*h[1]*h[2];
  const PetscScalar hxhzdhy    = h[0]*h[2]/h[1];
  const PetscScalar hyhzdhx    = h[1]*h[2]/h[0];
  const PetscScalar hxhydhz    = h[0]*h[1]/h[2];
  DM                dm         = options->rhoDM;
  Vec               rho        = options->rho;
  PetscReal        *rhoBoltzmann = options->rhoBoltzmann;
  Rho            ***rhoArray;
  Rho            ***CArray;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMDAVecGetArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, C,   &CArray);CHKERRQ(ierr);
  ierr = CalculateConfinedRhoBoltzmann_PB(dm, info->da, CArray, phi, rhoBoltzmann, options);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
#ifdef PB_BC
        if (k == bathIndex[2]) {
          //ierr = PetscPrintf(PETSC_COMM_WORLD, "CHECK i %d j %d k %d val %g\n", i, j , k, PetscRealPart(sc*phi[k][j][i]));CHKERRQ(ierr);
          f[k][j][i] = sc*phi[k][j][i];
        } else {
#endif
        const PetscScalar u       = phi[k][j][i];
        const PetscScalar u_east  = phi[k][j][i+1];
        const PetscScalar u_west  = phi[k][j][i-1];
        const PetscScalar u_north = phi[k][j+1][i];
        const PetscScalar u_south = phi[k][j-1][i];
        const PetscScalar u_up    = phi[k+1][j][i];
        const PetscScalar u_down  = phi[k-1][j][i];
        const PetscScalar u_xx    = (u_east  - 2.0*u + u_west)*hyhzdhx;
        const PetscScalar u_yy    = (u_north - 2.0*u + u_south)*hxhzdhy;
        const PetscScalar u_zz    = (u_up    - 2.0*u + u_down)*hxhydhz;
        f[k][j][i]  = -(u_xx + u_yy + u_zz);
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
#ifdef USE_PB
          f[k][j][i] -= sc*z[sp]*(e*e/epsilon)*rhoBoltzmann[sp]*CArray[k][j][i].v[sp]*PetscExpScalar(-z[sp]*PetscRealPart(phi[k][j][i])/kT);
#else
          f[k][j][i] -= sc*(e*e/epsilon)*z[sp]*rhoArray[k][j][i].v[sp]; // Changed units by scaling by e
#endif
        }
#ifdef PB_BC
        }
#endif
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, C,   &CArray);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PoissonBoltzmann_Jac"
PetscErrorCode PoissonBoltzmann_Jac(DMDALocalInfo *info, PetscScalar ***phi, Mat J, void *ctx)
{
  PBContext        *options    = (PBContext *) ctx;
  Vec               C          = options->C;
  const PetscInt    numSpecies = options->numSpecies;
  const PetscInt   *bathIndex  = options->bathIndex;
  const PetscReal  *z          = options->z;
  const PetscReal  *h          = options->h;
  PetscReal        *rhoBoltzmann = options->rhoBoltzmann;
  const PetscScalar sc         = h[0]*h[1]*h[2];
  const PetscScalar hxhzdhy    = h[0]*h[2]/h[1];
  const PetscScalar hyhzdhx    = h[1]*h[2]/h[0];
  const PetscScalar hxhydhz    = h[0]*h[1]/h[2];
  MatStencil        col[7], row;
  PetscScalar       v[7];
  Rho            ***CArray;
  PetscErrorCode    ierr;
  PetscFunctionBegin;
  PetscPrintf(PETSC_COMM_WORLD, "%f, %f, %f, %f, %e\n", PetscRealPart(sc), PetscRealPart(hxhzdhy), PetscRealPart(hyhzdhx), PetscRealPart(hxhydhz), epsilon);
  ierr = DMDAVecGetArray(options->rhoDM, C, &CArray);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        row.k = k; row.j = j; row.i = i;
#ifdef PB_BC
        if (k == bathIndex[2]) {
          v[0] = sc;
          ierr = MatSetValuesStencil(J, 1, &row, 1, &row, v, INSERT_VALUES);CHKERRQ(ierr);
        } else {
#endif
        v[0] = -hxhydhz; col[0].k=k-1;col[0].j=j;  col[0].i = i;
        v[1] = -hxhzdhy; col[1].k=k;  col[1].j=j-1;col[1].i = i;
        v[2] = -hyhzdhx; col[2].k=k;  col[2].j=j;  col[2].i = i-1;
        v[3] = 2.0*(hyhzdhx+hxhzdhy+hxhydhz); col[3].k=row.k;col[3].j=row.j;col[3].i=row.i;
#ifdef USE_PB
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          v[3] += sc*z[sp]*z[sp]*(1. / kT)*(e*e/epsilon)*rhoBoltzmann[sp]*CArray[k][j][i].v[sp]*PetscExpScalar(-z[sp]*PetscRealPart(phi[k][j][i])/kT);
        }
#endif
        v[4] = -hyhzdhx; col[4].k=k;  col[4].j=j;  col[4].i=i+1;
        v[5] = -hxhzdhy; col[5].k=k;  col[5].j=j+1;col[5].i=i;
        v[6] = -hxhydhz; col[6].k=k+1;col[6].j=j; col[6].i=i;
        //PetscPrintf(PETSC_COMM_WORLD, "%f, %f, %f, %f, %f, %f, %f\n", PetscRealPart(v[0]), PetscRealPart(v[2]), PetscRealPart(v[3]), PetscRealPart(v[4]), PetscRealPart(v[5]), PetscRealPart(v[6]));
        ierr = MatSetValuesStencil(J, 1, &row, 7, col, v, INSERT_VALUES);CHKERRQ(ierr);
#ifdef PB_BC
        }
#endif
      }
    }
  }
  ierr = DMDAVecRestoreArray(options->rhoDM, C, &CArray);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  //ierr = MatView(J, PETSC_VIEWER_DRAW_WORLD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CheckZeroMean"
PetscErrorCode CheckZeroMean(DMDALocalInfo *info, PetscScalar ***x, PetscScalar offset, PBContext *options) {
  const PetscInt    M          = options->dims[0];
  const PetscInt    N          = options->dims[1];
  const PetscInt    P          = options->dims[2];
  const PetscReal   scale      = 1.0/((PetscReal) M*N*P);
  PetscScalar       mean       = 0.0;

  PetscFunctionBegin;
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        mean += x[k][j][i];
      }
    }
  }
  mean *= scale;
  if (PetscAbsScalar(mean - offset) > 1.0e-8) {
    PetscErrorCode ierr;

    ierr = PetscPrintf(PETSC_COMM_WORLD, "Field has nonzero mean %g != %g (%g)\n", PetscRealPart(mean), PetscRealPart(offset), PetscAbsScalar(mean - offset));CHKERRQ(ierr);
    //SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_USER, "Field has nonzero mean %g != %g", PetscRealPart(mean), PetscRealPart(offset));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PoissonBoltzmann_Rhs_2"
// Apply the operator I - M + 1/\epsilon \Delta^{-1} \sum_i z_i e e \rho^{Boltzmann}_i C_i e^{-\beta z_i \phi}
//   where M is the mean operator M(\phi) = mean of \phi
//   Could include a line search with forced \phi to be 0 in the bath
PetscErrorCode PoissonBoltzmann_Rhs_2(DMDALocalInfo *info, PetscScalar ***phi, PetscScalar ***f, void *ctx)
{
  PBContext        *options      = (PBContext *) ctx;
  const PetscInt    numSpecies   = options->numSpecies;
  Vec               C            = options->C;
  const PetscReal  *h            = options->h;
  const PetscReal  *z            = options->z;
  const PetscInt    M            = options->dims[0];
  const PetscInt    N            = options->dims[1];
  const PetscInt    P            = options->dims[2];
  const PetscInt   *bathIndex    = options->bathIndex;
  const PetscReal   scale        = 1.0/((PetscReal) M*N*P);
  PetscReal        *rhoBoltzmann = options->rhoBoltzmann;
  DM                dm           = options->rhoDM;
  Vec               rho          = options->rho;
  PetscScalar       phiAvg       = 0.0;
  Vec               x, y;
  PetscScalar    ***xArray;
  PetscScalar    ***yArray;
  PetscScalar    ***xHatArray;
  PetscScalar    ***yHatArray;
  Rho            ***rhoArray;
  Rho            ***CArray;
  PetscErrorCode    ierr;

  PetscFunctionBegin;
  ierr = DMGetGlobalVector(info->da, &x);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(info->da, &y);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, y, &yArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, C,   &CArray);CHKERRQ(ierr);
  ierr = CalculateConfinedRhoBoltzmann_PB(dm, info->da, CArray, phi, rhoBoltzmann, options);CHKERRQ(ierr);

  // First calculate y = \mathcal{N}(\phi) = \sum_i z_i e e C_i e^{-\beta z_i \phi}
  //   In the linear case, we have y = \sum_i z_i e e \rho
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        yArray[k][j][i] = 0.0;
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
#ifdef USE_PB
          yArray[k][j][i] += z[sp]*e*e*rhoBoltzmann[sp]*CArray[k][j][i].v[sp]*PetscExpScalar((-z[sp]*PetscRealPart(phi[k][j][i]))/kT);
#else
          yArray[k][j][i] += z[sp]*e*e*rhoArray[k][j][i].v[sp];
#endif
        }
        phiAvg += phi[k][j][i];
      }
    }
  }
  phiAvg *= scale;
  ierr = CheckZeroMean(info, phi,    phiAvg, options);CHKERRQ(ierr);
  ierr = CheckZeroMean(info, yArray, 0.0,    options);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, C,   &CArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rho, &rhoArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(info->da, y, &yArray);CHKERRQ(ierr);
  // Then calculate x = -1/\epsilon \Delta^{-1} y
  Vec xHat = y;
  Vec yHat = x;
  ierr = MatMult(options->F, y, yHat);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, xHat, &xHatArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(info->da, yHat, &yHatArray);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    PetscReal kz = k <= P/2 ? 2.0*M_PI*k/(P) : -2.0*M_PI*(P-k)/(P);
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      PetscReal ky = j <= N/2 ? 2.0*M_PI*j/(N) : -2.0*M_PI*(N-j)/(N);
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        PetscReal   kx  = i <= M/2 ? 2.0*M_PI*i/(M) : -2.0*M_PI*(M-i)/(M);

        // Omit the zeroth moment
        if (kx == 0 && ky == 0 && kz == 0) {
          xHatArray[k][j][i] = 0.0;
        } else {
          PetscReal Delta = 2.0*epsilon*((1.0-cos(kx))/PetscSqr(h[0]) + (1.0-cos(ky))/PetscSqr(h[1]) + (1.0-cos(kz))/PetscSqr(h[2]));

          xHatArray[k][j][i] = yHatArray[k][j][i]/Delta; // Changed units by scaling by e
        }
        if (PetscIsInfOrNanScalar(xHatArray[k][j][i])) {
          SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_FP, "Nan or inf at xHat[%d][%d][%d]: %g ", k, j, i, PetscRealPart(xHatArray[k][j][i]));
        }
      }
    }
  }
  ierr = DMDAVecRestoreArray(info->da, yHat, &yHatArray);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(info->da, xHat, &xHatArray);CHKERRQ(ierr);
  ierr = MatMultTranspose(options->F, xHat, x);CHKERRQ(ierr);
  ierr = VecScale(x, scale);CHKERRQ(ierr);
  // Finish with I - M + x = I - M + 1/\epsilon \Delta^{-1} N
  ierr = DMDAVecGetArray(info->da, x, &xArray);CHKERRQ(ierr);
  ierr = CheckZeroMean(info, xArray, 0.0, options);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        f[k][j][i] = phi[k][j][i] - phiAvg + xArray[k][j][i];
      }
    }
  }
  ierr = DMDAVecRestoreArray(info->da, x, &xArray);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &x);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(info->da, &y);CHKERRQ(ierr);

  // Check potential in the bath
  ierr = PetscPrintf(PETSC_COMM_WORLD, "bath potential: %g\n", PetscRealPart(phi[bathIndex[2]][bathIndex[1]][bathIndex[0]]));
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PoissonBoltzmann_Jac_2"
PetscErrorCode PoissonBoltzmann_Jac_2(DMDALocalInfo *info, PetscScalar ***phi, Mat J, void *ctx)
{
  // Does nothing since we use a shell matrix
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PoissonBoltzmann_Jac_Apply"
PetscErrorCode PoissonBoltzmann_Jac_Apply(Mat J, Vec x, Vec y)
{
  // Does nothing yet
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "EquilibratedLineSearch"
PetscErrorCode EquilibratedLineSearch(SNES snes, void *lsctx, Vec x, Vec f, Vec y, PetscReal fnorm, PetscReal xnorm, Vec g, Vec w, PetscReal *ynorm, PetscReal *gnorm, PetscBool *flag)
{
  PBContext     *ctx       = (PBContext *) lsctx;
  DM             singleDA  = ctx->dm;
  PetscInt      *bathIndex = ctx->bathIndex;
  Vec            phi       = x;
  Vec            dphi      = y;
  PetscScalar ***phiArray;
  PetscScalar    bathPotential, updateBathPotential;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  // Force potential in the bath to be 0
  ierr = DMDAVecGetArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  bathPotential = phiArray[bathIndex[2]][bathIndex[1]][bathIndex[0]];
  ierr = DMDAVecRestoreArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, dphi, &phiArray);CHKERRQ(ierr);
  updateBathPotential = phiArray[bathIndex[2]][bathIndex[1]][bathIndex[0]];
  ierr = DMDAVecRestoreArray(singleDA, dphi, &phiArray);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "bath potential: %g  update bath potential: %g\n", PetscRealPart(bathPotential), PetscRealPart(updateBathPotential));
  //ierr = VecShift(dphi, -updateBathPotential);CHKERRQ(ierr);

  ierr = SNESLineSearchCubic(snes, lsctx, x, f, y, fnorm, xnorm, g, w, ynorm, gnorm, flag);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculatePhi_Boltzmann"
PetscErrorCode CalculatePhi_Boltzmann(DMDALocalInfo *info, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions, Vec rho, Vec phi)
{
  MPI_Comm       comm;
  SNES           snes;
  Mat            J;
  Vec            r;
  PBContext      ctx;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ctx.dm            = singleDA;
  ctx.numSpecies    = geomOptions->numSpecies;
  ctx.bathIndex     = geomOptions->bathIndex;
  ctx.dims          = geomOptions->dims;
  ctx.R             = geomOptions->R;
  ctx.h             = geomOptions->h;
  ctx.z             = esOptions->z;
  ctx.isConfined    = geomOptions->isConfined;
  ctx.numParticles  = geomOptions->numParticles;
  ctx.L             = geomOptions->L;
  ctx.channelLength = geomOptions->channelLength;
  ctx.channelRadius = geomOptions->channelRadius;
  ctx.rhoBoltzmann  = hsOptions->rhoBoltzmann;
  ctx.rhoDM         = info->da;
  ctx.rho           = rho;
  ctx.C             = esOptions->frozenPBVariables;
  ctx.F             = hsOptions->F;
  ierr = PetscObjectGetComm((PetscObject) singleDA, &comm);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &r);CHKERRQ(ierr);
  ierr = SNESCreate(comm, &snes);CHKERRQ(ierr);
  ierr = SNESSetOptionsPrefix(snes, "pb_");CHKERRQ(ierr);
  if (esOptions->calcPoissonBoltzmannFourier) {
    PetscInt N, n;

    ierr = VecGetLocalSize(r, &n);CHKERRQ(ierr);
    ierr = VecGetSize(r, &N);CHKERRQ(ierr);
    ierr = MatCreateShell(comm, n, n, N, N, (void *) PoissonBoltzmann_Jac_Apply, &J);CHKERRQ(ierr);
    ierr = DMDASetLocalFunction(singleDA, (DMDALocalFunction1) PoissonBoltzmann_Rhs_2);CHKERRQ(ierr);
    ierr = DMDASetLocalJacobian(singleDA, (DMDALocalFunction1) PoissonBoltzmann_Jac_2);CHKERRQ(ierr); 
  } else {
    ierr = DMGetMatrix(singleDA, MATAIJ, &J);CHKERRQ(ierr);
    ierr = DMDASetLocalFunction(singleDA, (DMDALocalFunction1) PoissonBoltzmann_Rhs);CHKERRQ(ierr);
    ierr = DMDASetLocalJacobian(singleDA, (DMDALocalFunction1) PoissonBoltzmann_Jac);CHKERRQ(ierr); 
  }
  ierr = MatSetOptionsPrefix(J, "pb_");CHKERRQ(ierr);
  ierr = SNESSetFunction(snes, r, SNESDAFormFunction, &ctx);CHKERRQ(ierr);
  ierr = SNESSetJacobian(snes, J, J, SNESDAComputeJacobian, &ctx);CHKERRQ(ierr);
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
  if (esOptions->calcPoissonBoltzmannFourier) {
    ierr = SNESLineSearchSet(snes, EquilibratedLineSearch, &ctx);CHKERRQ(ierr);
  }

#if 0
#ifndef USE_PB
  KSP          ksp;
  MatNullSpace nsp;
  ierr = MatNullSpaceCreate(comm, PETSC_TRUE, 0, PETSC_NULL, &nsp);CHKERRQ(ierr);
  ierr = SNESGetKSP(snes, &ksp);CHKERRQ(ierr);
  ierr = KSPSetNullSpace(ksp, nsp);CHKERRQ(ierr);
#endif
#endif

  ierr = SNESSolve(snes, PETSC_NULL, phi);CHKERRQ(ierr);
  ierr = SNESDestroy(&snes);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &r);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);

  // Force potential in the bath to be 0
  PetscScalar ***phiArray;
  PetscScalar    bathPotential;

  ierr = DMDAVecGetArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
  bathPotential = phiArray[geomOptions->bathIndex[2]][geomOptions->bathIndex[1]][geomOptions->bathIndex[0]];
  ierr = DMDAVecRestoreArray(singleDA, phi, &phiArray);CHKERRQ(ierr);
#ifndef USE_PB
  ierr = VecShift(phi, -bathPotential);CHKERRQ(ierr);
#else
  if (PetscAbsScalar(bathPotential) > 1.0e-3) {
    ierr = PetscPrintf(PETSC_COMM_WORLD, "WARNING: Bath potential ought to be 0.0, not %g\n", PetscAbsScalar(bathPotential));
  }
#endif
  PetscFunctionReturn(0);
}

PetscErrorCode CalculateXYStdDev(DM dm, Vec v, Vec *std);

#undef __FUNCT__
#define __FUNCT__ "CalculatePhi"
PetscErrorCode CalculatePhi(DMDALocalInfo *info, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions, Vec rho, Vec phi)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOptions->phiEvent,0,0,0,0);CHKERRQ(ierr);
  if (esOptions->usePoissonBoltzmann) {
    ierr = CalculatePhi_Boltzmann(info, singleDA, geomOptions, hsOptions, esOptions, rho, phi);CHKERRQ(ierr);
    if (0) {
      Vec       laggedPhi, stddev;
      PetscReal norm;

      ierr = VecDuplicate(phi, &laggedPhi);CHKERRQ(ierr);
      ierr = CalculatePhi_Lagged(info, singleDA, geomOptions, hsOptions, esOptions, laggedPhi);CHKERRQ(ierr);
      if (0) {
        ierr = VecViewCenterSingle_Private(singleDA, phi, PETSC_VIEWER_STDOUT_WORLD, "phi", -1, -1);CHKERRQ(ierr);
        ierr = VecViewCenterSingle_Private(singleDA, laggedPhi, PETSC_VIEWER_STDOUT_WORLD, "lagged phi", -1, -1);CHKERRQ(ierr);
        ierr = VecViewCenterSingle_Private(singleDA, phi, PETSC_VIEWER_DRAW_WORLD, "phi", -1, -1);CHKERRQ(ierr);
        ierr = VecViewCenterSingle_Private(singleDA, laggedPhi, PETSC_VIEWER_DRAW_WORLD, "lagged phi", -1, -1);CHKERRQ(ierr);
      }

      if (0) {
      ierr = CalculateXYStdDev(singleDA, laggedPhi, &stddev);CHKERRQ(ierr);
      PetscReal   s;
      PetscInt    p;
      ierr = VecMax(stddev, &p, &s);CHKERRQ(ierr);
      if (s > 1.0e-10) {
        SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_PLIB, "Homogeneity violation, std deviation %g z %d", s, p);
      } else {
        PetscPrintf(PETSC_COMM_WORLD, "Lagged Phi Homogeneity verification\n");
        PetscPrintf(PETSC_COMM_WORLD, "          std deviation   %g\n", s);
      }
      ierr = VecDestroy(&stddev);CHKERRQ(ierr);
      ierr = CalculateXYStdDev(singleDA, phi, &stddev);CHKERRQ(ierr);
      ierr = VecMax(stddev, &p, &s);CHKERRQ(ierr);
      if (s > 1.0e-10) {
        SETERRQ2(PETSC_COMM_WORLD, PETSC_ERR_PLIB, "Homogeneity violation, std deviation %g z %d", s, p);
      } else {
        PetscPrintf(PETSC_COMM_WORLD, "Phi Homogeneity verification\n");
        PetscPrintf(PETSC_COMM_WORLD, "          std deviation   %g\n", s);
      }
      ierr = VecDestroy(&stddev);CHKERRQ(ierr);

      Vec check;
      ierr = VecDuplicate(laggedPhi, &check);CHKERRQ(ierr);
      ierr = VecPointwiseDivide(check, phi, laggedPhi);CHKERRQ(ierr);
      ierr = VecViewCenterSingle_Private(singleDA, check, PETSC_VIEWER_STDOUT_WORLD, "phi/lagged phi", -1, -1);CHKERRQ(ierr);
      ierr = VecDestroy(&check);CHKERRQ(ierr);
      }

      ierr = VecAXPY(laggedPhi, -1.0, phi);CHKERRQ(ierr);
      ierr = VecNorm(laggedPhi, NORM_2, &norm);CHKERRQ(ierr);
      if (norm > 1.0e-8) {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "Invalid phi, error %g\n", norm);CHKERRQ(ierr);
        ierr = VecViewCenterSingle_Private(singleDA, laggedPhi, PETSC_VIEWER_STDOUT_WORLD, "phi error", -1, -1);CHKERRQ(ierr);
        SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_LIB, "Bad phi");
      } else {
        ierr = PetscPrintf(PETSC_COMM_WORLD, "Correct phi, error %g\n", norm);CHKERRQ(ierr);
      }
      ierr = VecDestroy(&laggedPhi);CHKERRQ(ierr);
    }
  } else {
    ierr = CalculatePhi_Lagged(info, singleDA, geomOptions, hsOptions, esOptions, phi);CHKERRQ(ierr);
  }
  ierr = PetscLogEventEnd(esOptions->phiEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateMuES"
PetscErrorCode CalculateMuES(DM dm, const int numSpecies, Vec rhoVec, DM singleDA, HardSphereOptions *hsOptions, ElectrostaticsOptions *options, Vec cPsiHatVecs[], Vec muES)
{
  Mat            F       = hsOptions->F;
  Vec            bathVec = hsOptions->rhoBathVec;
  Vec           *deltaRhoSplit, *muESSplit, product, productHat, sum, deltaRhoVec;
  PetscInt       M, N, P;
  PetscReal      scale;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(options->muESEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetInfo(dm, 0, &M,&N,&P,0,0,0,0,0,0,0,0,0);CHKERRQ(ierr);
  scale = 1.0/((PetscReal) M*N*P);
  ierr = PetscMalloc2(numSpecies+1, Vec, &deltaRhoSplit, numSpecies+1, Vec, &muESSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMGetGlobalVector(singleDA, &muESSplit[sp]);CHKERRQ(ierr);
    ierr = DMGetGlobalVector(singleDA, &deltaRhoSplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMGetGlobalVector(singleDA, &product);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &productHat);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &sum);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &deltaRhoVec);CHKERRQ(ierr);
  ierr = VecSet(muES, 0.0);CHKERRQ(ierr);
  ierr = VecSet(sum, 0.0);CHKERRQ(ierr);
  // Calculate \Delta rho = rho - rho_bath and store in deltaRhoVec:
  // OPT We can transform bathVec once, and then use the rhoHat from calculating n
  ierr = VecWAXPY(deltaRhoVec, -1.0, bathVec, rhoVec);
  ierr = VecViewCenter(dm, rhoVec,  PETSC_VIEWER_DRAW_WORLD, "rho",     -1, -1);CHKERRQ(ierr);
  ierr = VecViewCenter(dm, bathVec, PETSC_VIEWER_DRAW_WORLD, "rhoBath", -1, -1);CHKERRQ(ierr);
  ierr = VecViewCenter(dm, deltaRhoVec, PETSC_VIEWER_DRAW_WORLD, "Delta rho", -1, -1);CHKERRQ(ierr);
  // Split rhoVec (which has delta rho stored) by species
  ierr = VecStrideGatherAll(deltaRhoVec, deltaRhoSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(int spI = 0; spI < numSpecies; ++spI) {
    ierr = PrintNorm(deltaRhoSplit[spI], "deltaRhoSplit", spI, -1);
    //Compute fourier transform of deltaRho
    ierr = VecCopy(deltaRhoSplit[spI], product);CHKERRQ(ierr);
    ierr = MatMult(F, product, deltaRhoSplit[spI]);CHKERRQ(ierr);
    ierr = PrintNorm(deltaRhoSplit[spI], "deltaRhoSplitHat", spI, -1);
    ierr = VecViewCenterSingle(singleDA, deltaRhoSplit[spI], PETSC_VIEWER_DRAW_WORLD, "deltaRhoSplitHat", spI, -1);CHKERRQ(ierr);
  }
  for(int spI = 0; spI < numSpecies; ++spI) {
    for(int spJ = 0; spJ < numSpecies; ++spJ) {
      ierr = VecPointwiseMult(productHat, deltaRhoSplit[spJ], cPsiHatVecs[spI*numSpecies + spJ]);CHKERRQ(ierr);
      ierr = PrintNorm(productHat, "productHat", spI, spJ);
      // OPT We can pull this transform outside the loop
      ierr = MatMultTranspose(F, productHat, product);CHKERRQ(ierr);
      ierr = VecAXPY(sum, -1.0, product);CHKERRQ(ierr);
      ierr = PrintNorm(sum, "sum", -1, -1);
    }
    ierr = VecScale(sum, scale);CHKERRQ(ierr);
    ierr = VecCopy(sum, muESSplit[spI]);CHKERRQ(ierr);
    ierr = VecSet(sum, 0.0);
    ierr = PrintNorm(muESSplit[spI], "mu_ES", spI, -1);
  }
  // Zero out vector for Gamma component
  ierr = VecSet(muESSplit[numSpecies], 0.0);CHKERRQ(ierr);
  ierr = VecStrideScatterAll(muESSplit, muES, INSERT_VALUES);CHKERRQ(ierr);
  ierr = PrintNorm(muES, "mu_ES", -1, -1);
  ierr = VecViewCenter(dm, muES, PETSC_VIEWER_DRAW_WORLD, "mu_ES", -1, -1);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &product);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &productHat);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &sum);CHKERRQ(ierr);	
  ierr = DMRestoreGlobalVector(dm, &deltaRhoVec);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMRestoreGlobalVector(singleDA, &muESSplit[sp]);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(singleDA, &deltaRhoSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree2(deltaRhoSplit, muESSplit);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(options->muESEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateRhoBar"
PetscErrorCode CalculateRhoBar(const int numSpecies, const PetscReal z[], Rho *rho)
{
  PetscReal posAvg      = 0.0;
  PetscReal posStrength = 0.0;
  PetscReal negAvg      = 0.0;
  PetscReal negStrength = 0.0;
  PetscReal A, B;

  PetscFunctionBegin;
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (z[sp] >= 0) {
      posAvg      += z[sp]*PetscRealPart(rho->v[sp]);
      posStrength += PetscSqr(z[sp])*PetscRealPart(rho->v[sp]);
    } else {
      negAvg      -= z[sp]*PetscRealPart(rho->v[sp]);
      negStrength += PetscSqr(z[sp])*PetscRealPart(rho->v[sp]);
    }
  }
  if (posStrength + negStrength == 0.0) {
    A = B = 0.0;
  } else {
    B = posAvg/negAvg;
    A = (posStrength + negStrength)/(posStrength + B*negStrength);
  }
  //if (fabs(A - 1.0) > 1.0e-9) {SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_LIB, "A should be 1, not %g", A);}
  //if (fabs(B - 1.0) > 1.0e-9) {SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_LIB, "B should be 1, not %g", B);}
  for(PetscInt sp = 0; sp < numSpecies; ++sp) {
    if (z[sp] >= 0) {
      rho->v[sp] *= A;
    } else {
      rho->v[sp] *= A*B;
    }
  }  
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateScreeningIntegral"
/*
  Input Parameters:
  dm         - The DM supporting all ion density fields and the screeing parameter \Gamma
  singleDA   - A DM with identical layout supporting one component
  kDA        - A DM with identical layout supporting the k vector (3 component)
  numSpecies - The number of ion species
  rhoVec     - The density values for all ion species
  rhoBarVec  - The average density which is electroneutral and has the same ionic strength
  geomOpts   - The geometry options
  hsOpts     - The hard sphere options
  esOpts     - The electrostatics options

  Output Parameters:
  rhoRefVec  - The reference density obtained by smoothing the 
*/
PetscErrorCode CalculateScreeningIntegral(DMDALocalInfo *info, const int numSpecies, const PetscReal R[], const PetscReal h[], Rho ***rho, Rho ***rhoRef, ElectrostaticsOptions *esOpts)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  for(PetscInt k = info->zs; k < info->zs+info->zm; ++k) {
    for(PetscInt j = info->ys; j < info->ys+info->ym; ++j) {
      for(PetscInt i = info->xs; i < info->xs+info->xm; ++i) {
        PetscReal avgR   = 0.0;
        PetscReal avgRho = 0.0;

        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          avgR   += PetscRealPart(rho[k][j][i].v[sp])*R[sp];
          avgRho += PetscRealPart(rho[k][j][i].v[sp]);
        }
        PetscReal   Rfilter      = avgR/avgRho + 1.0/(2.0*PetscRealPart(rho[k][j][i].Gamma));
        // Rescale box to have volume equal to a sphere of the screening radius
        Rfilter *= pow(M_PI/6.0, 0.33333333);
        if (Rfilter/h[0] > info->mx/4) {Rfilter = h[0]*info->mx/4;}
        if (Rfilter/h[1] > info->my/4) {Rfilter = h[1]*info->my/4;}
        if (Rfilter/h[2] > info->mz/4) {Rfilter = h[2]*info->mz/4;}
        PetscInt    width[3]     = {(PetscInt) floor(Rfilter/h[0] + 0.5), (PetscInt) floor(Rfilter/h[1] + 0.5), (PetscInt) floor(Rfilter/h[2] + 0.5)};
        PetscReal   cellVolume   = h[0]*h[1]*h[2];
        PetscReal   filterVolume = 8.0*Rfilter*Rfilter*Rfilter;
        //PetscReal   filterVolume = (4.0/3.0)*M_PI*PetscSqr(Rfilter)*Rfilter;
        PetscScalar integral[NUM_SPECIES];

        for(PetscInt sp = 0; sp < numSpecies; ++sp) {integral[sp] = 0.0;}
        for(PetscInt kp = k-width[2]; kp <= k+width[2]; ++kp) {
          PetscScalar zWeight = 1.0;

          if (width[2] == 0) {zWeight = 2.0*Rfilter/h[2];}
          else if ((kp == k-width[2]) || (kp == k+width[2])) {zWeight = 0.5 - (width[2] - Rfilter/h[2]);}
          PetscInt kpp = kp < 0 ? info->mz+kp : (kp >= info->mz ? kp-info->mz : kp);
          for(PetscInt jp = j-width[1]; jp <= j+width[1]; ++jp) {
            PetscScalar yWeight = 1.0;

            if (width[1] == 0) {yWeight = 2.0*Rfilter/h[1];}
            else if ((jp == j-width[1]) || (jp == j+width[1])) {yWeight = 0.5 - (width[1] - Rfilter/h[1]);}
            PetscInt jpp = jp < 0 ? info->my+jp : (jp >= info->my ? jp-info->my : jp);
            for(PetscInt ip = i-width[0]; ip <= i+width[0]; ++ip) {
              PetscScalar xWeight = 1.0;

              if (width[0] == 0) {xWeight = 2.0*Rfilter/h[0];}
              else if ((ip == i-width[0]) || (ip == i+width[0])) {xWeight = 0.5 - (width[0] - Rfilter/h[0]);}
              PetscInt ipp = ip < 0 ? info->mx+ip : (ip >= info->mx ? ip-info->mx : ip);
              for(PetscInt sp = 0; sp < numSpecies; ++sp) {
                integral[sp] += rho[kpp][jpp][ipp].v[sp]*xWeight*yWeight*zWeight;
              }
            }
          }
        }
        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          rhoRef[k][j][i].v[sp] = integral[sp]*cellVolume/filterVolume;
        }
      }
    }
  }
  ierr = PetscLogEventEnd(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateScreeningIntegralFT"
/*
  Input Parameters:
  dm         - The DM supporting all ion density fields and the screeing parameter \Gamma
  singleDA   - A DM with identical layout supporting one component
  kDA        - A DM with identical layout supporting the k vector (3 component)
  numSpecies - The number of ion species
  rhoVec     - Used only for \Gamma
  rhoBarVec  - The average density which is electroneutral and has the same ionic strength
  geomOpts   - The geometry options
    - M,N,P: the size of the DM in each dimension
    - R:     the ion radii
    - h:     the grid spacing in each dimension
  hsOpts     - The hard sphere options (not used)
  esOpts     - The electrostatics options (only usedd for checking)

  Output Parameters:
  rhoRefVec  - The reference density obtained by smoothing the 
*/
PetscErrorCode CalculateScreeningIntegralFT(DM dm, DM singleDA, DM kDA, const int numSpecies, Vec rhoVec, Vec rhoBarVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOpts)
{
  PetscInt       M     = geomOpts->dims[0];
  PetscInt       N     = geomOpts->dims[1];
  PetscInt       P     = geomOpts->dims[2];
  PetscReal     *R     = geomOpts->R;
  PetscReal      scale = 1.0/((PetscReal) M*N*P);
  DMDALocalInfo  info;
  Vec            rhoBarHatVec, rhoBarHatCompVec, kVec, *rhoBarSplit;
  Rho         ***rho;
  Rho         ***rhoBar;
  Rho         ***rhoBarHat;
  Rho         ***rhoRef;
  Kvec        ***kArray;
#define NO_CHECK_SCREENING
#ifdef CHECK_SCREENING
  Vec            rhoRefCheckVec, kernelHatVec;
  Rho         ***rhoCheck;
  PetscScalar ***kernelHat;
#endif
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(kDA, &kVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(kDA, kVec, &kArray);CHKERRQ(ierr);
#ifdef CHECK_SCREENING
  ierr = DMGetGlobalVector(singleDA, &kernelHatVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, kernelHatVec, &kernelHat);CHKERRQ(ierr);
#endif
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    PetscReal kz = k <= P/2 ? 2.0*M_PI*k/(P*geomOpts->h[2]) : -2.0*M_PI*(P-k)/(P*geomOpts->h[2]);

    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      PetscReal ky = j <= N/2 ? 2.0*M_PI*j/(N*geomOpts->h[1]) : -2.0*M_PI*(N-j)/(N*geomOpts->h[1]);

      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        PetscReal kx = i <= M/2 ? 2.0*M_PI*i/(M*geomOpts->h[0]) : -2.0*M_PI*(M-i)/(M*geomOpts->h[0]);
        kArray[k][j][i].kx = kx;
        kArray[k][j][i].ky = ky;
        kArray[k][j][i].kz = kz;
        kArray[k][j][i].k  = sqrt(kx*kx + ky*ky + kz*kz);
#ifdef CHECK_SCREENING
        PetscReal Rfilter = 1.0/(2.0*esOpts->GammaBath);
        PetscReal kR = sqrt(kx*kx + ky*ky + kz*kz)*Rfilter;

        kernelHat[k][j][i] = (kx == 0 && ky == 0 && kz == 0) ? 1.0 : -(3.0*cos(kR))/(kR*kR) + (3.0*sin(kR))/(kR*kR*kR);
#endif
      }
    }
  }
  ierr = PetscLogFlops(info.zm*(4 + info.ym*(4 + info.xm*(4 + 6))));CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(kDA, kVec, &kArray);CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dm, &rhoBarHatVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &rhoBarHatCompVec);CHKERRQ(ierr);
  ierr = PetscMalloc((numSpecies+1)*sizeof(Vec), &rhoBarSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMGetGlobalVector(singleDA, &rhoBarSplit[sp]);CHKERRQ(ierr);
  }
#ifdef CHECK_SCREENING
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Starting reference density check...\n");
  ierr = DMDAVecRestoreArray(singleDA, kernelHatVec, &kernelHat);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &rhoRefCheckVec);CHKERRQ(ierr);
  ierr = VecStrideGatherAll(rhoBarVec, rhoBarSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(int sp = 0; sp < numSpecies; ++sp) {
    ierr = MatMult(hsOptions->F, rhoBarSplit[sp], rhoBarHatCompVec);CHKERRQ(ierr);
    ierr = VecPointwiseMult(rhoBarHatCompVec, rhoBarHatCompVec, kernelHatVec);CHKERRQ(ierr);
    ierr = MatMultTranspose(hsOptions->F, rhoBarHatCompVec, rhoBarSplit[sp]);CHKERRQ(ierr);
  }
  ierr = VecStrideScatterAll(rhoBarSplit, rhoRefCheckVec, INSERT_VALUES);CHKERRQ(ierr);
  ierr = VecScale(rhoRefCheckVec, scale);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Ending reference density check\n");
  ierr = VecViewCenter(dm, rhoRefCheckVec, PETSC_VIEWER_DRAW_WORLD, "RhoReferenceCheck", -1, -1);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRefCheckVec, &rhoCheck);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(singleDA, &kernelHatVec);CHKERRQ(ierr);
#endif

  ierr = PetscPrintf(PETSC_COMM_WORLD, "Starting reference density calculation...\n");
  ierr = VecStrideGatherAll(rhoBarVec, rhoBarSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(int sp = 0; sp < numSpecies; ++sp) {
    ierr = MatMult(hsOptions->F, rhoBarSplit[sp], rhoBarHatCompVec);CHKERRQ(ierr);
    ierr = VecCopy(rhoBarHatCompVec, rhoBarSplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMRestoreGlobalVector(singleDA, &rhoBarHatCompVec);CHKERRQ(ierr);
  ierr = VecStrideScatterAll(rhoBarSplit, rhoBarHatVec, INSERT_VALUES);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMRestoreGlobalVector(singleDA, &rhoBarSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree(rhoBarSplit);CHKERRQ(ierr);
  DM                coordDA;
  Vec               coordinates;
  DMDACoor3d     ***coords;
  PetscInt          outputPeriod = info.zm > 40;

  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoBarVec, &rhoBar);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoBarHatVec, &rhoBarHat);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(kDA, kVec, &kArray);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    ierr = PetscLogEventBegin(esOpts->screeningIntegralZPlaneEvent,0,0,0,0);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "  Calculating z-plane %d\n", k);
    if (outputPeriod >= 0) {
      PetscViewer viewer;
      char        date[64];

      ierr = PetscViewerCreate(PETSC_COMM_WORLD, &viewer);CHKERRQ(ierr);
      ierr = PetscViewerSetType(viewer, PETSCVIEWERASCII);CHKERRQ(ierr);
      ierr = PetscViewerFileSetMode(viewer, FILE_MODE_APPEND);CHKERRQ(ierr);
      ierr = PetscViewerFileSetName(viewer, "convergence.txt");CHKERRQ(ierr);
      ierr = PetscGetDate(date, 64);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer, "  Calculating z-plane %d: %s\n", k, date);CHKERRQ(ierr);
      ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
    }
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        PetscReal z = PetscRealPart(coords[k][j][i].z);
        PetscReal y = PetscRealPart(coords[k][j][i].y);
        PetscReal x = PetscRealPart(coords[k][j][i].x);
        PetscReal avgR   = 0.0;
        PetscReal avgRho = 0.0;

        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          avgR   += PetscRealPart(rhoBar[k][j][i].v[sp])*R[sp];
          avgRho += PetscRealPart(rhoBar[k][j][i].v[sp]);
          rhoRef[k][j][i].v[sp] = 0.0;
        }
#ifndef CHECK_SCREENING
        PetscReal Rfilter = avgR/avgRho + 1.0/(2.0*PetscRealPart(rho[k][j][i].Gamma));
#ifdef PETSC_USE_DEBUG
        if (PetscIsInfOrNanScalar(Rfilter)) {
          SETERRQ6(PETSC_COMM_SELF, PETSC_ERR_FP, "Nan or inf for Rfilter at [%d][%d][%d]: %g %g %g", k, j, i, avgR, avgRho, PetscRealPart(rho[k][j][i].Gamma));
        }
#endif
#else
        PetscReal Rfilter = 1.0/(2.0*esOpts->GammaBath);
#endif

        for(PetscInt kp = info.zs; kp < info.zs+info.zm; ++kp) {
          for(PetscInt jp = info.ys; jp < info.ys+info.ym; ++jp) {
            for(PetscInt ip = info.xs; ip < info.xs+info.xm; ++ip) {
              PetscReal kx = PetscRealPart(kArray[kp][jp][ip].kx);
              PetscReal ky = PetscRealPart(kArray[kp][jp][ip].ky);
              PetscReal kz = PetscRealPart(kArray[kp][jp][ip].kz);
	      if (kp == 3 && jp == 2 && ip == 3 && i == 3 && j == 2 && k == 3) {
		  ierr = PetscPrintf(PETSC_COMM_WORLD, "PETSC coords: %f, %f, %f, %f, %f, %f \n", kx, ky, kz, x, y, z);
		  ierr = PetscPrintf(PETSC_COMM_WORLD, "PETSC Rfilter: %f\n", Rfilter);
	      }
              PetscReal kR = PetscRealPart(kArray[kp][jp][ip].k)*Rfilter;
              PetscReal K  = (kx == 0 && ky == 0 && kz == 0) ? 1.0 : 3.0*(sin(kR)/kR - cos(kR))/(kR*kR);
              PetscReal k_x = kx*x + ky*y + kz*z;
              PetscScalar product = K*(cos(k_x) + PETSC_i*sin(k_x));
              //PetscReal kR = sqrt(kx*kx + ky*ky + kz*kz)*Rfilter;
              //PetscReal K  = (kx == 0 && ky == 0 && kz == 0) ? 1.0 : -(3.0*cos(kR))/(kR*kR) + (3.0*sin(kR))/(kR*kR*kR);
              //PetscScalar product = K*PetscExpScalar(PETSC_i*(kx*x + ky*y + kz*z));
#ifdef PETSC_USE_DEBUG
              if (PetscIsInfOrNanScalar(product)) {
                SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_FP, "Nan or inf at [%d][%d][%d]: %g %g", kp, jp, ip, K, PetscRealPart(PetscExpScalar(PETSC_i*(kx*x + ky*y + kz*z))));
              }
#endif
              for(int sp = 0; sp < numSpecies; ++sp) {
		if (kp == 3 && jp == 2 && ip == 3 && i == 3 && j == 2 && k == 3) {
		  ierr = PetscPrintf(PETSC_COMM_WORLD, "PETSC Answer: %e\n", PetscRealPart(product*rhoBarHat[kp][jp][ip].v[sp]));
		  ierr = PetscPrintf(PETSC_COMM_WORLD, "PETSC Rhohat: %e\n", PetscRealPart(rhoBarHat[kp][jp][ip].v[sp]));
		  ierr = PetscPrintf(PETSC_COMM_WORLD, "PETSC Kernel: %e\n", K);
		}
                rhoRef[k][j][i].v[sp] += product * rhoBarHat[kp][jp][ip].v[sp];
              }
            }
          }
        }
#ifdef PETSC_USE_DEBUG
        for(int sp = 0; sp < numSpecies; ++sp) {
          if (PetscIsInfOrNanScalar(rhoRef[k][j][i].v[sp])) {
            SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_FP, "Nan or inf at rhoRef[%d][%d][%d][%d]: %g ", k, j, i, sp, PetscRealPart(rhoRef[k][j][i].v[sp]));
          }
          if (PetscImaginaryPart(rhoRef[k][j][i].v[sp]) > 1.0e-9) {
            SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_LIB, "Invalid ref density at (%d,%d,%d): %g + I %g", k, j, i, PetscRealPart(rhoRef[k][j][i].v[sp])*scale, PetscImaginaryPart(rhoRef[k][j][i].v[sp])*scale);
          }
#ifdef CHECK_SCREENING
          if (fabs(PetscRealPart(rhoRef[k][j][i].v[sp])*scale - PetscRealPart(rhoCheck[k][j][i].v[sp])) > 1.0e-9) {
            SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_LIB, "Invalid ref density at (%d,%d,%d): %g != %g", k, j, i, PetscRealPart(rhoRef[k][j][i].v[sp])*scale, PetscRealPart(rhoCheck[k][j][i].v[sp]));
          }
#endif
        }
#endif
      }
    }
    ierr = PetscLogEventEnd(esOpts->screeningIntegralZPlaneEvent,0,0,0,0);CHKERRQ(ierr);
  }
  ierr = PetscLogFlops(info.zm*info.ym*info.xm*(numSpecies*3 + 4));CHKERRQ(ierr);
  //ierr = PetscLogFlops(info.zm*info.ym*info.xm*info.zm*info.ym*info.xm*(numSpecies*2 + 7 + 10 + 8));CHKERRQ(ierr);
  ///ierr = PetscLogFlops(info.zm*info.ym*info.xm*info.zm*info.ym*info.xm*(numSpecies*2 + 1 + 7 + 8));CHKERRQ(ierr);
  ierr = PetscLogFlops(info.zm*info.ym*info.xm*info.zm*info.ym*info.xm*(numSpecies*2 + 1 + 7 + 10));CHKERRQ(ierr);
#ifdef CHECK_SCREENING
  ierr = DAVecRestoreArray(da, rhoRefCheckVec, &rhoCheck);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(da, &rhoRefCheckVec);CHKERRQ(ierr);
#endif
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoBarVec, &rhoBar);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoBarHatVec, &rhoBarHat);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(kDA, kVec, &kArray);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(kDA, &kVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoBarHatVec);CHKERRQ(ierr);
  ierr = VecScale(rhoRefVec, scale);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD, "Ending reference density calculation\n");
  ierr = VecViewCenter(dm, rhoRefVec, PETSC_VIEWER_DRAW_WORLD, "RhoReference", -1, -1);CHKERRQ(ierr);

  // Another possible check:
  //   Pick a grid point p randomly
  //   Set Gamma_fixed = Gamma(p)
  //   Use convolution to generate rhoRef_check from Gamma_fixed
  //   Check that rhoRef_check(p) == rhoRef(p)

  ierr = PetscLogEventEnd(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "GradRhoBarNorm"

//this computes the L_2, L_1, or L_infinity norm of grad(rhoBar) as:
// \sum_{species}\sqrt{\int_{\Omega}} 
// = \sum_{species}\sqrt{\sum_{(i, j, k)}vol_h*( (du / dx)^2 + (du / dy)^2 + (du / dz)^2)}

PetscErrorCode GradRhoBarNorm(DM dm, Vec rhoBarVec, NormType type, PetscReal *val, GeometryOptions * options) {
  PetscFunctionBegin;
  PetscErrorCode    ierr;
  PetscInt          M     = options->dims[0];
  PetscInt          N     = options->dims[1];
  PetscInt          P     = options->dims[2];
  const PetscInt   *bathIndex  = options->bathIndex;
  const PetscInt    numSpecies = options->numSpecies;
  const PetscReal  *h          = options->h;
  const PetscReal invhx    = 1./(2.*h[0]);
  const PetscReal invhy    = 1./(2.*h[1]);
  const PetscReal invhz    = 1./(2.*h[2]);
  DMDALocalInfo     info;
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  Rho            ***rhoArray;
  
  PetscReal norm_2 = 0.;
  PetscReal norm_1 = 0.;
  PetscReal norm_inf = 0.;
  ierr = DMDAVecGetArray(dm, rhoBarVec, &rhoArray);CHKERRQ(ierr);
  for (PetscInt sp = 0; sp < numSpecies; ++sp) {
    PetscReal sp_2_norm = 0.;
    PetscReal sp_1_norm = 0.;
    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
	for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
	  if ((k < 1) || (j < 1) || (i < 1) || (k >= P - 1) || (j >= N - 1) || (i >= M - 1)) {
	  } else {
	    //const PetscScalar u       = rho[k][j][i];
	    //PetscPrintf(PETSC_COMM_WORLD, "%d %d %d, %f\n", k, j, i, PetscRealPart(rhoArray[k][j][i].v[sp]));
	    const PetscReal u_east  = PetscRealPart(rhoArray[k][j][i+1].v[sp]);
	    const PetscReal u_west  = PetscRealPart(rhoArray[k][j][i-1].v[sp]);
	    const PetscReal u_north = PetscRealPart(rhoArray[k][j+1][i].v[sp]);
	    const PetscReal u_south = PetscRealPart(rhoArray[k][j-1][i].v[sp]);
	    const PetscReal u_up    = PetscRealPart(rhoArray[k+1][j][i].v[sp]);
	    const PetscReal u_down  = PetscRealPart(rhoArray[k-1][j][i].v[sp]);
	    const PetscReal u_x    = (u_east  - u_west)*invhx;
	    const PetscReal u_y    = (u_north - u_south)*invhy;
	    const PetscReal u_z    = (u_up    - u_down)*invhz;
	    PetscReal point_inf_norm = fabs(u_x);
	    if (u_y > point_inf_norm) point_inf_norm = u_y;
	    if (u_z > point_inf_norm) point_inf_norm = u_z;
	    if (norm_inf < point_inf_norm) norm_inf = point_inf_norm; 
	    sp_1_norm += fabs(u_x) + fabs(u_y) + fabs(u_z);
	    sp_2_norm += u_x*u_x + u_y*u_y + u_z*u_z;
	  }
	} 
      }
    }
    //the norm for this particular species
    
    //PetscPrintf(PETSC_COMM_WORLD, "||GradRhoBar||^i_2 = %f\n", sp, sp_2_norm);
    norm_2 += sqrt(sp_2_norm);
    norm_1 += sp_1_norm;
  }
  if (type == NORM_1) {
    *val = norm_1;
  } else if (type == NORM_2) {
    *val = norm_2;
  } else if (type == NORM_INFINITY) {
    *val = norm_inf;
  }

  //PetscPrintf(PETSC_COMM_WORLD, "||GradRhoBar||_2 = %f\n", norm);
  ierr = DMDAVecRestoreArray(dm, rhoBarVec, &rhoArray);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "CalculateScreeningIntegralDimReduct"
/*
  Input Parameters:
  dm         - The DM supporting all ion density fields and the screeing parameter \Gamma
  singleDA   - A DM with identical layout supporting one component
  kDA        - A DM with identical layout supporting the k vector (3 component)
  numSpecies - The number of ion species
  rhoVec     - Used only for \Gamma
  rhoBarVec  - The average density which is electroneutral and has the same ionic strength
  geomOpts   - The geometry options
    - M,N,P: the size of the DM in each dimension
    - R:     the ion radii
    - h:     the grid spacing in each dimension
  hsOpts     - The hard sphere options (not used)
  esOpts     - The electrostatics options (only usedd for checking)

  Output Parameters:
  rhoRefVec  - The reference density obtained by smoothing the 
*/
PetscErrorCode CalculateScreeningIntegralDimReduct(DM dm, DM singleDA, DM kDA, const int numSpecies, Vec rhoVec, Vec rhoBarVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOpts)
{
  PetscInt       M     = geomOpts->dims[0];
  PetscInt       N     = geomOpts->dims[1];
  PetscInt       P     = geomOpts->dims[2];
  PetscReal     *R     = geomOpts->R;
  PetscReal      scale = 1.0/((PetscReal) M*N*P);
  DMDALocalInfo  info;
  Vec            rhoBarHatVec, rhoBarHatCompVec, kVec, *rhoBarSplit, RfilterVec;
  Rho         ***rho;
  Rho         ***rhoBar;
  Rho         ***rhoBarHat;
  Rho         ***rhoRef;
  Kvec        ***kArray;

  PetscScalar ***Rfilter;

  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(kDA, &kVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &RfilterVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(kDA, kVec, &kArray);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(singleDA, RfilterVec, &Rfilter);CHKERRQ(ierr);

  /*
  Precomputation of frequency-space coordinates and radii
   */
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    PetscReal kz = k <= P/2 ? 2.0*M_PI*k/(P*geomOpts->h[2]) : -2.0*M_PI*(P-k)/(P*geomOpts->h[2]);

    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      PetscReal ky = j <= N/2 ? 2.0*M_PI*j/(N*geomOpts->h[1]) : -2.0*M_PI*(N-j)/(N*geomOpts->h[1]);

      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        PetscReal kx = i <= M/2 ? 2.0*M_PI*i/(M*geomOpts->h[0]) : -2.0*M_PI*(M-i)/(M*geomOpts->h[0]);
        kArray[k][j][i].kx = kx;
        kArray[k][j][i].ky = ky;
        kArray[k][j][i].kz = kz;
        kArray[k][j][i].k  = sqrt(kx*kx + ky*ky + kz*kz);
      }
    }
  }
  ierr = PetscLogFlops(info.zm*(4 + info.ym*(4 + info.xm*(4 + 6))));CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(kDA, kVec, &kArray);CHKERRQ(ierr);

  ierr = DMGetGlobalVector(dm, &rhoBarHatVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &rhoBarHatCompVec);CHKERRQ(ierr);
  ierr = PetscMalloc((numSpecies+1)*sizeof(Vec), &rhoBarSplit);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMGetGlobalVector(singleDA, &rhoBarSplit[sp]);CHKERRQ(ierr);
  }

  ierr = PetscPrintf(PETSC_COMM_WORLD, "Starting reference density calculation...\n");
  ierr = VecStrideGatherAll(rhoBarVec, rhoBarSplit, INSERT_VALUES);CHKERRQ(ierr);
  for(int sp = 0; sp < numSpecies; ++sp) {
    ierr = MatMult(hsOptions->F, rhoBarSplit[sp], rhoBarHatCompVec);CHKERRQ(ierr);
    ierr = VecCopy(rhoBarHatCompVec, rhoBarSplit[sp]);CHKERRQ(ierr);
  }
  ierr = DMRestoreGlobalVector(singleDA, &rhoBarHatCompVec);CHKERRQ(ierr);
  ierr = VecStrideScatterAll(rhoBarSplit, rhoBarHatVec, INSERT_VALUES);CHKERRQ(ierr);
  //ierr = VecScale(rhoBarHatVec, scale);CHKERRQ(ierr);
  for(PetscInt sp = 0; sp < numSpecies+1; ++sp){
    ierr = DMRestoreGlobalVector(singleDA, &rhoBarSplit[sp]);CHKERRQ(ierr);
  }
  ierr = PetscFree(rhoBarSplit);CHKERRQ(ierr);
  DM                coordDA;
  Vec               coordinates;
  DMDACoor3d     ***coords;

  ierr = DMDAGetCoordinateDA(dm, &coordDA);CHKERRQ(ierr);
  ierr = DMDAGetCoordinates(dm, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoBarVec, &rhoBar);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoBarHatVec, &rhoBarHat);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(kDA, kVec, &kArray);CHKERRQ(ierr);
  /*
    Calculate RFilter at every position in the domain
  */

  //ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculating Rfilter..\n");
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        PetscReal avgR   = 0.0;
        PetscReal avgRho = 0.0;

        for(PetscInt sp = 0; sp < numSpecies; ++sp) {
          avgR   += PetscRealPart(rhoBar[k][j][i].v[sp])*R[sp];
          avgRho += PetscRealPart(rhoBar[k][j][i].v[sp]);
          rhoRef[k][j][i].v[sp] = 0.0;
        }
        Rfilter[k][j][i] = avgR/avgRho + 1.0/(2.0*PetscRealPart(rho[k][j][i].Gamma));
      }
    }
  }

  ierr = DMDAVecRestoreArray(singleDA, RfilterVec, &Rfilter);CHKERRQ(ierr);

  /*
  Calculate the max/min(Rfilter), and the multiplicative factor for each step in R
   */

  PetscReal Rfilter_min;
  PetscReal Rfilter_max;
  PetscReal rho_norm, rho_grad_norm;

  ierr = VecNorm(rhoBarVec, NORM_INFINITY, &rho_norm);CHKERRQ(ierr);
  ierr = GradRhoBarNorm(dm, rhoBarVec, NORM_INFINITY, &rho_grad_norm, geomOpts);CHKERRQ(ierr);

  //scale by the size of the domain and divide by the number of species
  ierr = VecMin(RfilterVec, &Rfilter_min);CHKERRQ(ierr);
  ierr = VecMax(RfilterVec, &Rfilter_max);CHKERRQ(ierr);
  PetscReal R_1 = 0., R_0 = Rfilter_min;

  Rho *** rhoRef_R0;
  Rho *** rhoRef_R1;
  PetscScalar ***rhoRef_RHat;


  Vec rhoRef_RHatVec, rhoRef_R_splitVec[numSpecies+1], rhoRef_R_0Vec, rhoRef_R_1Vec;

  /* Initialize the vectors */
  ierr = DMDAVecGetArray(singleDA, RfilterVec, &Rfilter);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(singleDA, &rhoRef_RHatVec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &rhoRef_R_0Vec);CHKERRQ(ierr);
  ierr = DMGetGlobalVector(dm, &rhoRef_R_1Vec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRef_R_0Vec, &rhoRef_R0);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(dm, rhoRef_R_1Vec, &rhoRef_R1);CHKERRQ(ierr);
  for (int sp = 0; sp < numSpecies+1; ++sp) {
    ierr = DMGetGlobalVector(singleDA, &rhoRef_R_splitVec[sp]);CHKERRQ(ierr);
  }
 
  PetscReal debug_max;

  ierr = VecMax(rhoBarHatVec, &debug_max);
  //ierr = PetscPrintf(PETSC_COMM_WORLD, "Rhobarhat max %e\n", debug_max);CHKERRQ(ierr);

  /* Calculate the first one and set it as R0 */
  //ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculating R_0 = %f\n", R_0);
  for (int sp = 0; sp < numSpecies; ++sp) {
    ierr = DMDAVecGetArray(singleDA, rhoRef_RHatVec, &rhoRef_RHat);CHKERRQ(ierr);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "Species %d\n", sp);
    //Initialize the vector for this purpose
    for(PetscInt kp = info.zs; kp < info.zs+info.zm; ++kp) {
      for(PetscInt jp = info.ys; jp < info.ys+info.ym; ++jp) {
	for(PetscInt ip = info.xs; ip < info.xs+info.xm; ++ip) {
	  PetscReal kx = PetscRealPart(kArray[kp][jp][ip].kx);
	  PetscReal ky = PetscRealPart(kArray[kp][jp][ip].ky);
	  PetscReal kz = PetscRealPart(kArray[kp][jp][ip].kz);
	  PetscReal kR = PetscRealPart(kArray[kp][jp][ip].k)*R_0;
	  PetscReal K  = (kx == 0 && ky == 0 && kz == 0) ? 1.0 : 3.0*(sin(kR)/kR - cos(kR))/(kR*kR);
	  rhoRef_RHat[kp][jp][ip] = K*rhoBarHat[kp][jp][ip].v[sp];
	}
      }
    }

    ierr = DMDAVecRestoreArray(singleDA, rhoRef_RHatVec, &rhoRef_RHat);CHKERRQ(ierr);

    //ierr = VecMax(rhoRef_RHatVec, &debug_max);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef_RHat max %e\n", debug_max);CHKERRQ(ierr);

    ierr = MatMultTranspose(hsOptions->F, rhoRef_RHatVec, rhoRef_R_splitVec[sp]);CHKERRQ(ierr);
    //ierr = DMRestoreGlobalVector(singleDA, &rhoRef_R_splitVec[sp]);CHKERRQ(ierr);

    //ierr = VecMax(rhoRef_R_splitVec[sp], &debug_max);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef_RSplit max %e\n", debug_max);CHKERRQ(ierr);

  }
  ierr = VecStrideScatterAll(rhoRef_R_splitVec, rhoRef_R_0Vec, INSERT_VALUES);CHKERRQ(ierr); 
  //ierr = DMRestoreGlobalVector(dm, &rhoRef_R_0Vec);CHKERRQ(ierr);

  ierr = DMDAVecGetArray(dm, rhoRef_R_0Vec, &rhoRef_R0);CHKERRQ(ierr);

  //ierr = VecMax(rhoRef_R_0Vec, &debug_max);
  //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef_R0 max %e\n", debug_max);CHKERRQ(ierr);

  //PetscFunctionReturn(0);

  PetscInt n_R = 0;
  R_1 = 0.;
  while (R_1 < Rfilter_max) {
    n_R++;
    //PetscReal R_factor = 1. + pow(esOpts->dimReductErr / 2., 0.5);
    PetscReal R_factor = 1. + sqrt(8.*(esOpts->dimReductErr*rho_norm) / ((R_0*rho_grad_norm + 10.*rho_norm)));
    
    R_1 = R_0*R_factor;

    //ierr = PetscPrintf(PETSC_COMM_WORLD, "R_0: %f, R_1: %f, R_factor: %f\n",R_0, R_1, R_factor);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "Calculating R_0 = %f -> R_1 = %f...\n", R_0, R_1);
    /*
    Compute the convolution at this R
     */

    for (int sp = 0; sp < numSpecies; ++sp) {
      ierr = DMDAVecGetArray(singleDA, rhoRef_RHatVec, &rhoRef_RHat);CHKERRQ(ierr);
      //ierr = PetscPrintf(PETSC_COMM_WORLD, "Species %d\n", sp);
      //Initialize the vector for this purpose
      for(PetscInt kp = info.zs; kp < info.zs+info.zm; ++kp) {
	for(PetscInt jp = info.ys; jp < info.ys+info.ym; ++jp) {
	  for(PetscInt ip = info.xs; ip < info.xs+info.xm; ++ip) {
	    PetscReal kx = PetscRealPart(kArray[kp][jp][ip].kx);
	    PetscReal ky = PetscRealPart(kArray[kp][jp][ip].ky);
	    PetscReal kz = PetscRealPart(kArray[kp][jp][ip].kz);
	    PetscReal kR = PetscRealPart(kArray[kp][jp][ip].k)*R_1;
	    PetscReal K  = (kx == 0 && ky == 0 && kz == 0) ? 1.0 : 3.0*(sin(kR)/kR - cos(kR))/(kR*kR);
	    rhoRef_RHat[kp][jp][ip] = K*rhoBarHat[kp][jp][ip].v[sp];
	  }
	}
      }
      //ierr = PetscPrintf(PETSC_COMM_WORLD, "Restoring Array: %f", rhoRef_RHat[1][1][1]);
      ierr = DMDAVecRestoreArray(singleDA, rhoRef_RHatVec, &rhoRef_RHat);CHKERRQ(ierr);
      ierr = MatMultTranspose(hsOptions->F, rhoRef_RHatVec, rhoRef_R_splitVec[sp]);CHKERRQ(ierr);
    }
    ierr = VecStrideScatterAll(rhoRef_R_splitVec, rhoRef_R_1Vec, INSERT_VALUES);CHKERRQ(ierr) ;
    //ierr = DMRestoreGlobalVector(dm, &rhoRef_R_1Vec);CHKERRQ(ierr);

    //ierr = VecMax(rhoRef_R_0Vec, &debug_max);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef_R0 max %e\n", debug_max);CHKERRQ(ierr);

    //ierr = VecMax(rhoRef_R_1Vec, &debug_max);
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef_R1 max %e\n", debug_max);CHKERRQ(ierr);

    ierr = DMDAVecGetArray(dm, rhoRef_R_1Vec, &rhoRef_R1);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, rhoRef_R_0Vec, &rhoRef_R0);CHKERRQ(ierr);

    /*
    Set rhoRef[i,j,k] = Interpolation(rhoRef_R0[i, j, k], rhoRef_R1[i, j, k]
     */

    //ierr = PetscPrintf(PETSC_COMM_WORLD, "Setting the values..\n");

    for (PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      for (PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
	for (PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
	  PetscReal R = PetscRealPart(Rfilter[k][j][i]);
	  if (R >= R_0 && R <= R_1) {
	    PetscReal Rho_1_Const = double(R - R_0) / (R_1 - R_0);
	    PetscReal Rho_0_Const = 1. - Rho_1_Const;
	    for (PetscInt sp = 0; sp < numSpecies; ++sp) {
	      rhoRef[k][j][i].v[sp] = rhoRef_R1[k][j][i].v[sp]*Rho_1_Const + rhoRef_R0[k][j][i].v[sp]*Rho_0_Const;
	      //ierr = PetscPrintf(PETSC_COMM_WORLD, "%d %d %d %f \n", i, j, k, PetscRealPart(rhoRef_R0[k][j][i].v[sp]));CHKERRQ(ierr);
	    }
	  }
	}
      }
    }

    ierr = DMDAVecRestoreArray(dm, rhoRef_R_1Vec, &rhoRef_R1);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, rhoRef_R_0Vec, &rhoRef_R0);CHKERRQ(ierr);

    //Shift the window over one
    ierr = VecCopy(rhoRef_R_1Vec, rhoRef_R_0Vec);CHKERRQ(ierr);
    //ierr = DMRestoreGlobalVector(dm, &rhoRef_R_0Vec);CHKERRQ(ierr);

    R_0 = R_1;
    //ierr = PetscPrintf(PETSC_COMM_WORLD, "Next..\n");
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD, "n_R = %d [%g, %g] (%g)\n", n_R, Rfilter_min, Rfilter_max, Rfilter_max - Rfilter_min);
  //ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);

  //consistency check rhoRef
  ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  ierr = VecScale(rhoRefVec, scale);CHKERRQ(ierr);

  ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
  for (PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for (PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for (PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
	//PetscReal R = PetscRealPart(Rfilter[k][j][i]);
	for (PetscInt sp = 0; sp < numSpecies; ++sp) {
	  //if (PetscRealPart(rhoRef[k][j][i].v[sp]) <= 0.) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_ARG_WRONG, "Bad reference value %f", PetscRealPart(rhoRef[k][j][i].v[sp]));
	  //ierr = PetscPrintf(PETSC_COMM_WORLD, "%d %d %d %f\n", i, j, k, PetscRealPart(rhoRef[k][j][i].v[sp]));CHKERRQ(ierr);
	}
      }
    }
  }
  ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);

  //  Vec            rhoBarHatVec, rhoBarHatCompVec, kVec, *rhoBarSplit, RfilterVec;
  //  Vec rhoRef_RHatVec, rhoRef_R_splitVec[numSpecies+1], rhoRef_R_0Vec, rhoRef_R_1Vec;

  for (int sp = 0; sp < numSpecies + 1; ++sp) {
    ierr = DMRestoreGlobalVector(singleDA, &rhoRef_R_splitVec[sp]);CHKERRQ(ierr);
  }

  ierr = DMRestoreGlobalVector(singleDA, &rhoRef_RHatVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoRef_R_0Vec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoRef_R_1Vec);CHKERRQ(ierr);

  ierr = PetscLogFlops(info.zm*info.ym*info.xm*(numSpecies*3 + 4));CHKERRQ(ierr);
  ierr = PetscLogFlops(info.zm*info.ym*info.xm*info.zm*info.ym*info.xm*(numSpecies*2 + 1 + 7 + 10));CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoBarVec, &rhoBar);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(dm, rhoBarHatVec, &rhoBarHat);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(kDA, kVec, &kArray);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(kDA, &kVec);CHKERRQ(ierr);
  ierr = DMRestoreGlobalVector(dm, &rhoBarHatVec);CHKERRQ(ierr);

  ierr = VecMax(rhoRefVec, &debug_max);
  //ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRefVec max %e\n", debug_max);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD, "Ending reference density calculation\n");
  ierr = VecViewCenter(dm, rhoRefVec, PETSC_VIEWER_DRAW_WORLD, "RhoReference", -1, -1);CHKERRQ(ierr);

  // Another possible check:
  //   Pick a grid point p randomly
  //   Set Gamma_fixed = Gamma(p)
  //   Use convolution to generate rhoRef_check from Gamma_fixed
  //   Check that rhoRef_check(p) == rhoRef(p)

  ierr = PetscLogEventEnd(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#ifdef DFT_USE_CUDA
#undef __FUNCT__
#define __FUNCT__ "CalculateScreeningIntegralGPU"
/*
  Input Parameters:
  da         - The DA supporting all ion density fields and the screeing parameter \Gamma
  singleDA   - A DA with identical layout supporting one component
  kDA        - A DA with identical layout supporting the k vector (3 component)
  numSpecies - The number of ion species
  rhoVec     - Used only for \Gamma
  rhoBarVec  - The average density which is electroneutral and has the same ionic strength
  geomOpts   - The geometry options
    - M,N,P: the size of the DA in each dimension
    - R:     the ion radii
    - h:     the grid spacing in each dimension
  hsOpts     - The hard sphere options (not used)
  esOpts     - The electrostatics options (only usedd for checking)

  Output Parameters:
  rhoRefVec  - The reference density obtained by smoothing the 
*/
PetscErrorCode CalculateScreeningIntegralGPU(DA da, DA singleDA, DA kDA, const int numSpecies, Vec rhoVec, Vec rhoBarVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOpts)
{
  DALocalInfo    info;
  Vec            gammaVec;
  PetscScalar ***gamma;
  Rho         ***rho;
  PetscScalar   *gamma1D;
  PetscScalar   *rhoBar1D;
  PetscScalar   *rhoRef1D;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  // Make Gamma vector
  ierr = DAGetLocalInfo(da, &info);CHKERRQ(ierr);
  ierr = DAGetGlobalVector(singleDA, &gammaVec);CHKERRQ(ierr);
  ierr = DAVecGetArray(singleDA, gammaVec, &gamma);CHKERRQ(ierr);
  ierr = DAVecGetArray(da, rhoVec, &rho);CHKERRQ(ierr);
  for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
    for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
      for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
        gamma[k][j][i] = rho[k][j][i].Gamma;
      }
    }
  }
  ierr = DAVecRestoreArray(da, rhoVec, &rho);CHKERRQ(ierr);
  ierr = DAVecRestoreArray(singleDA, gammaVec, &gamma);CHKERRQ(ierr);
  // Call GPU version (should pass coordinates eventually?)
  ierr = VecGetArray(gammaVec, &gamma1D);CHKERRQ(ierr);
  ierr = VecGetArray(rhoBarVec, &rhoBar1D);CHKERRQ(ierr);
  ierr = VecGetArray(rhoRefVec, &rhoRef1D);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Starting reference density calculation...\n");CHKERRQ(ierr);
  
  {
    PetscInt N, M;
    double  *gamma1DReal, *rhoBar1DReal, *rhoRef1DReal;

    ierr = VecGetSize(gammaVec, &N);CHKERRQ(ierr);
    ierr = VecGetSize(rhoBarVec, &M);CHKERRQ(ierr);
    ierr = PetscMalloc3(N,double,&gamma1DReal,M,double,&rhoBar1DReal,M,double,&rhoRef1DReal);CHKERRQ(ierr);
    for(PetscInt i = 0; i < N; ++i) {
      gamma1DReal[i]  = PetscRealPart(gamma1D[i]);
    }
    for(PetscInt i = 0; i < M; ++i) {
      rhoBar1DReal[i] = PetscRealPart(rhoBar1D[i]);
    }
    screening(geomOpts->dims, geomOpts->numSpecies, geomOpts->h, geomOpts->R, gamma1DReal, rhoBar1DReal, rhoRef1DReal);
    for(PetscInt i = 0; i < M; ++i) {
      rhoRef1D[i] = rhoRef1DReal[i];
    }
    ierr = PetscFree3(gamma1DReal,rhoBar1DReal,rhoRef1DReal);CHKERRQ(ierr);
  }
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Ending reference density calculation\n");CHKERRQ(ierr);
  // Cleanup
  ierr = VecRestoreArray(rhoRefVec, &rhoRef1D);CHKERRQ(ierr);
  ierr = VecRestoreArray(rhoBarVec, &rhoBar1D);CHKERRQ(ierr);
  ierr = VecRestoreArray(gammaVec, &gamma1D);CHKERRQ(ierr);
  ierr = DARestoreGlobalVector(singleDA, &gammaVec);CHKERRQ(ierr);
  ierr = VecViewCenter(da, rhoRefVec, PETSC_VIEWER_DRAW_WORLD, "RhoReference", -1, -1);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(esOpts->screeningIntegralEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
#endif

#undef __FUNCT__
#define __FUNCT__ "CalculateRhoReference"
PetscErrorCode CalculateRhoReference(DMMG *dmmg, DM dm, DM singleDA, DM kDA, Vec rhoVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOpts, ElectrostaticsOptions *esOpts)
{
  DMDALocalInfo    info;
  Rho           ***rho;
  Rho           ***rhoRef;
  PetscInt         numSpecies = geomOpts->numSpecies;
  const PetscReal *z          = esOpts->z;
  PetscInt         iter;
  PetscErrorCode   ierr;

  PetscInt       M = geomOpts->dims[0];
  PetscInt       N = geomOpts->dims[1];
  PetscInt       P = geomOpts->dims[2];
  PetscReal      scale = M*N*P;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(esOpts->rhoRefEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = SNESGetIterationNumber(DMMGGetSNES(dmmg), &iter);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(dm, &info);CHKERRQ(ierr);
  //ierr = VecSet(rhoVec, 1.0);CHKERRQ(ierr);
  ierr = VecViewCenter(dm, rhoVec, PETSC_VIEWER_DRAW_WORLD, "Rho", -1, -1);CHKERRQ(ierr);
  if (esOpts->updateReferenceDensity && !(iter%esOpts->updateReferenceDensity)) {
    Vec rhoBarVec;

    ierr = DMGetGlobalVector(dm, &rhoBarVec);CHKERRQ(ierr);
    ierr = VecCopy(rhoVec, rhoBarVec);
    ierr = DMDAVecGetArray(dm, rhoBarVec, &rho);CHKERRQ(ierr);
    ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
    for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
      for(PetscInt j = info.ys; j < info.ys+info.ym; ++j) {
        for(PetscInt i = info.xs; i < info.xs+info.xm; ++i) {
          ierr = CalculateRhoBar(numSpecies, z, &rho[k][j][i]);CHKERRQ(ierr);
        }
      }
    }
    ierr = VecViewCenter(dm, rhoBarVec, PETSC_VIEWER_DRAW_WORLD, "RhoBar", -1, -1);CHKERRQ(ierr);
    if (esOpts->screeningComputationType == SCREENING_QUADRATURE) {
      const PetscReal *R = geomOpts->R;
      const PetscReal *h = geomOpts->h;

      ierr = CalculateScreeningIntegral(&info, numSpecies, R, h, rho, rhoRef, esOpts);CHKERRQ(ierr);
    } else {
      ierr = DMDAVecRestoreArray(dm, rhoBarVec, &rho);CHKERRQ(ierr);
      ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
      if (esOpts->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE) {
        ierr = CalculateScreeningIntegralFT(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
#ifdef DFT_USE_CUDA
      } else if (esOpts->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE_GPU) {
        ierr = CalculateScreeningIntegralGPU(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
#endif
      } else if (esOpts->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE_DIMREDUCT) {
        ierr = CalculateScreeningIntegralDimReduct(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
	if (esOpts->calcRhoRefErr) {
	  //calculate the error by seeing what the difference between this and refined is
	  Vec       rhoRefCheckVec, rhoRefDiffVec;
	  PetscReal errorNorm, refNorm;
	  
	  ierr = DMGetGlobalVector(dm, &rhoRefCheckVec);CHKERRQ(ierr);
	  ierr = VecSet(rhoRefCheckVec, 0.);CHKERRQ(ierr);
	  //ierr = VecSet(rhoRefVec, 0.);CHKERRQ(ierr);
	  esOpts->dimReductErr *= 0.01;
	  ierr = CalculateScreeningIntegralDimReduct(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefCheckVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
	  esOpts->dimReductErr *= 100.;
	  ierr = VecDuplicate(rhoRefCheckVec, &rhoRefDiffVec);CHKERRQ(ierr);
	  ierr = VecCopy(rhoRefCheckVec, rhoRefDiffVec);CHKERRQ(ierr);
	  ierr = VecAXPY(rhoRefDiffVec, -1.0, rhoRefVec);CHKERRQ(ierr);
	  //ierr = VecNorm(rhoRefDiffVec, NORM_1, &errorNorm);CHKERRQ(ierr);
	  ierr = VecNorm(rhoRefDiffVec, NORM_INFINITY, &errorNorm);CHKERRQ(ierr);
	  ierr = VecNorm(rhoBarVec,      NORM_INFINITY, &refNorm);CHKERRQ(ierr);
	  ierr = PetscPrintf(PETSC_COMM_WORLD, "RhoRef Error (%f): %g = %g / %g\n", esOpts->dimReductErr, errorNorm / (refNorm), errorNorm, refNorm);CHKERRQ(ierr);
	  ierr = VecDestroy(&rhoRefCheckVec);CHKERRQ(ierr);
	  ierr = VecDestroy(&rhoRefDiffVec);CHKERRQ(ierr);
	}
#ifdef DFT_USE_CUDA
      } else if (esOpts->screeningComputationType == SCREENING_SPECTRAL_QUADRATURE_TEST) {
        Vec       rhoRefCheckVec, rhoRefDiffVec;
        PetscReal errorNorm, refNorm;

        ierr = DMGetGlobalVector(dm, &rhoRefCheckVec);CHKERRQ(ierr);
        ierr = CalculateScreeningIntegralFT(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
        ierr = CalculateScreeningIntegralGPU(dm, singleDA, kDA, numSpecies, rhoVec, rhoBarVec, rhoRefCheckVec, geomOpts, hsOpts, esOpts);CHKERRQ(ierr);
        ierr = VecDuplicate(rhoRefCheckVec, &rhoRefDiffVec);CHKERRQ(ierr);
        ierr = VecCopy(rhoRefCheckVec, rhoRefDiffVec);CHKERRQ(ierr);
        ierr = VecAXPY(rhoRefDiffVec, -1.0, rhoRefVec);CHKERRQ(ierr);
        ierr = VecNorm(rhoRefDiffVec, NORM_2, &errorNorm);CHKERRQ(ierr);
        ierr = VecNorm(rhoRefVec,      NORM_2, &refNorm);CHKERRQ(ierr);
        ierr = PetscPrintf(PETSC_COMM_WORLD, "GPU vs. CPU reference density error: %g relative error %g\n", errorNorm, errorNorm / refNorm);CHKERRQ(ierr);
        ierr = VecViewCenter(dm, rhoRefCheckVec, PETSC_VIEWER_DRAW_WORLD, "RhoReferenceGPU", -1, -1);CHKERRQ(ierr);
        ierr = VecViewCenter(dm, rhoRefVec, PETSC_VIEWER_DRAW_WORLD, "RhoReferenceCPU", -1, -1);CHKERRQ(ierr);
        //Rho           ***rhoRefCpu;
        //Rho           ***rhoRefGpu;
        //ierr = DMDAVecGetArray(dm, rhoRefCheckVec, &rhoRefGpu);CHKERRQ(ierr);
        //ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRefCpu);CHKERRQ(ierr);
        //for(PetscInt k = info.zs; k < info.zs+info.zm; ++k) {
        //  int i = (info.xs + info.xm) / 2;
        //  int j = (info.ys + info.ym) / 2;
        //  PetscPrintf(PETSC_COMM_WORLD, "CPU: %e GPU: %e\n", rhoRefCpu[k][j][i].v[0], rhoRefGpu[k][j][i].v[0]);
        //}
        //if (errorNorm / refNorm > 0.0001) {
        //  ierr = VecView(rhoRefCheckVec, PETSC_NULL);CHKERRQ(ierr);
        //  ierr = VecView(rhoRefVec, PETSC_NULL);CHKERRQ(ierr);
        //}
        ierr = DMRestoreGlobalVector(dm, &rhoRefCheckVec);CHKERRQ(ierr);
#endif
      } else {
        SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "Unknown screening computation type: %d", esOpts->screeningComputationType);
      }
      ierr = DMDAVecGetArray(dm, rhoBarVec, &rho);CHKERRQ(ierr);
      ierr = DMDAVecGetArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
    }
    ierr = DMDAVecRestoreArray(dm, rhoBarVec, &rho);CHKERRQ(ierr);
    ierr = DMDAVecRestoreArray(dm, rhoRefVec, &rhoRef);CHKERRQ(ierr);
    ierr = DMRestoreGlobalVector(dm, &rhoBarVec);CHKERRQ(ierr);
  }
  ierr = VecViewCenter(dm, rhoRefVec, PETSC_VIEWER_DRAW_WORLD, "RhoReference", -1, -1);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(esOpts->rhoRefEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
