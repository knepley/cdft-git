#ifndef __DFT_WINDOWFUNCTIONS_H
#define __DFT_WINDOWFUNCTIONS_H

#include <dftUtils.h>

extern PetscScalar OmegaHat_0(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_1(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_2(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_3(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V1_Kx(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V1_Ky(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V1_Kz(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V2_Kx(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V2_Ky(const PetscScalar x[], const int species);
extern PetscScalar OmegaHat_V2_Kz(const PetscScalar x[], const int species);

#endif /* __DFT_WINDOWFUNCTIONS_H */
