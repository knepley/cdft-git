#ifndef __DFT_VERIFICATION_H
#define __DFT_VERIFICATION_H

#include<dftHardSphere.h>
#include<dftElectrostatics.h>
#include<dftOutput.h>

typedef struct {
  PetscReal nCheckTol;  // Tolerance for n convolution check (should be calculated from h)
  PetscReal volFracTol; // Tolerance for volume fraction check (should be calculated from h)
  PetscReal homoTol;    // Tolerance for homogeneity
} VerificationOptions;

extern PetscErrorCode ProcessVerificationOptions(MPI_Comm comm, VerificationOptions *options);
extern PetscErrorCode CalculateXYAverage(DM dm, const int numSpecies, Vec v, Vec **avg);
extern PetscErrorCode CalculateXYStdDev(DM dm, const int numSpecies, Vec v, Vec **std);
extern PetscErrorCode CheckN(DM dm, DM nDA, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, VerificationOptions *options);
extern PetscErrorCode CheckScreeningIntegral(DM dm, GeometryOptions *geomOptions, VerificationOptions *options);
extern PetscErrorCode CheckPotential(DM dm, Vec rhoVec, DM singleDA, Vec phi, const PetscReal z[], GeometryOptions *geomOptions);
extern PetscErrorCode CheckVolumeFraction(DMMG *dmmg, DM nDA, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, VerificationOptions *options);
extern PetscErrorCode CheckElectroneutrality(const PetscInt numSpecies, const PetscBool isConfined[], HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions);
extern PetscErrorCode CheckHomogeneity(DMMG *dmmg, Vec x, GeometryOptions *geomOptions, VerificationOptions *options);
extern PetscErrorCode CheckSumRule(DMMG *dmmg, const PetscReal rhoBath[], DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions);
extern PetscErrorCode CheckInitialGuess(DMMG *dmmg, DM singleDA, Mat F, Vec x, GeometryOptions *geomOptions);

#endif /* __DFT_VERIFICATION_H */
