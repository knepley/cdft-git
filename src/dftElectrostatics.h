#ifndef __DFT_ELECTROSTATICS_H
#define __DFT_ELECTROSTATICS_H

#include<dftHardSphere.h>
#include<dftOutput.h>

typedef struct {
  SNES        snes;
  Vec         x;
  int         numSpecies;
  PetscBool  *isConfined;
  PetscReal  *R;
  PetscReal  *z;
  PetscReal  *rho;
} MSAContext;

typedef struct {
  DM          dm;
  PetscInt    numSpecies;
  PetscInt   *bathIndex;
  PetscInt   *dims;
  PetscReal  *R;
  PetscReal  *h;
  PetscReal  *z;
  PetscBool  *isConfined;
  PetscReal  *numParticles;
  PetscReal  *L;
  PetscReal   channelLength;
  PetscReal   channelRadius;
  PetscReal  *rhoBoltzmann;
  DM          rhoDM;
  Vec         rho;
  Vec         C;
  Mat         F;
} PBContext;

typedef enum {SCREENING_QUADRATURE, SCREENING_SPECTRAL_QUADRATURE, SCREENING_SPECTRAL_QUADRATURE_GPU, SCREENING_SPECTRAL_QUADRATURE_DIMREDUCT, SCREENING_SPECTRAL_QUADRATURE_TEST} ScreeningComputationType;

typedef struct {
  PetscBool  useMuES;                              // Flag for electrostatic screening
  PetscBool  usePoisson;                           // Flag for mean field electrostatics
  PetscBool  usePoissonBoltzmann;                  // Flag for improved Poisson solves (different splitting)
  PetscBool  calcPoissonBoltzmannFourier;          // Flag for calculating the PB solve in Fourier space
  PetscInt   updateReferenceDensity;               // Period for recalculation of reference density for electrostatics
  ScreeningComputationType screeningComputationType; // The type of computation done to produce the reference density
  PetscReal	 z[NUM_SPECIES];                       // Valence of species i (fractional charges are possible for confined species in the channel)
  PetscReal  dimReductErr;                         // The tolerance for the a priori spacing in the dimensionality-reduced screening model
  PetscBool  calcRhoRefErr;                        // Calculate the error of the rhoref calc.
  PetscReal  GammaBath;                            // The inverse screening length Gamma for bath concentrations
  PetscReal  muESBath[NUM_SPECIES];                // Chemical potential of species [kg nm^2 / s^2]
  PetscInt   bufferSpecies;                        // Use this species to enforce electroneutrality
  Vec	     cPsiHatVecs[NUM_SPECIES*NUM_SPECIES]; // Fourier transforms of MuES functions, (kT c_ij + psi_ij)
  PetscReal  rhoBoundary;                          // Singular charge density (Coulombs/nm^2); only works for DOMAIN_WALL and places the charge on the wall
  Vec        frozenPBVariables;                    // The part of the density not dependent on phi
  Vec        phi;                                  // The mean-field electrostatic potential \phi
  // Logging
  PetscLogEvent screeningIntegralEvent;
  PetscLogEvent screeningIntegralZPlaneEvent;
  PetscLogEvent cPsiHatEvent, rhoRefEvent, muESEvent, phiEvent, pbFrozenEvent;
} ElectrostaticsOptions;

extern ElectrostaticsOptions *globalElectrostaticsOptions;

extern PetscErrorCode ProcessElectrostaticsOptions(MPI_Comm comm, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options);
extern PetscErrorCode PrintElectrostaticsModule(PetscViewer viewer, const char indent[], GeometryOptions *geomOpts, ElectrostaticsOptions *options);
extern PetscErrorCode ForceElectroneutrality(const PetscInt species, const PetscInt numSpecies, const PetscBool isConfined[], HardSphereOptions *hsOptions, ElectrostaticsOptions *esOptions);
extern PetscErrorCode CalculateMuESBath(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal Gamma, PetscReal muESBath[]);
extern PetscErrorCode CalculateEta(const int numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal x, PetscReal *eta);
extern PetscErrorCode CalculatePressureES(const PetscInt numSpecies, const PetscReal R[], const PetscBool isConfined[], const PetscReal rhoBath[], const PetscReal z[], PetscReal Gamma, PetscReal *P);
extern PetscErrorCode MSAScreeningSetup(PetscReal rho[], GeometryOptions *geomOpts, ElectrostaticsOptions *options, MSAContext *ctx);
extern PetscErrorCode CalculateGamma(MSAContext *ctx, PetscScalar initialGuess, PetscReal *gamma);
extern PetscErrorCode MSAScreeningCleanup(MSAContext *ctx);
extern PetscErrorCode CalculateCPsiHatFunctions(DM dm, const int numSpecies, PetscScalar (*func)(const PetscScalar *, const PetscScalar *, const int, const int), Vec v[], Rho **[], ElectrostaticsOptions *options);
extern PetscErrorCode CreateElectrostatics(DM dm, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options);
extern PetscErrorCode DestroyElectrostatics(DM dm, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options);
extern PetscErrorCode CalculatePhi(DMDALocalInfo *info, DM singleDA, GeometryOptions *geomOptions, HardSphereOptions *hsOptions, ElectrostaticsOptions *options, Vec rho, Vec phi);
extern PetscErrorCode CalculateMuES(DM dm, const int numSpecies, Vec rhoVec, DM singleDA, HardSphereOptions *hsOptions, ElectrostaticsOptions *options, Vec cPsiHatVecs[], Vec muES);
extern PetscErrorCode CalculateScreeningIntegral(DMDALocalInfo *info, const int numSpecies, const PetscReal R[], const PetscReal h[], Rho ***rho, Rho ***rhoRef, ElectrostaticsOptions *esOpts);
extern PetscErrorCode CalculateScreeningIntegralFT(DM dm, DM singleDA, DM kDA, const int numSpecies, Vec rhoVec, Vec rhoBarVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOpts);
extern PetscErrorCode CalculateRhoReference(DMMG *dmmg, DM dm, DM singleDA, DM kDA, Vec rhoVec, Vec rhoRefVec, GeometryOptions *geomOpts, HardSphereOptions *hsOptions, ElectrostaticsOptions *esOpts);

extern PetscScalar cPsiHat(const PetscScalar x[], const PetscScalar field[], const int spI, const int spJ);

#endif /* __DFT_ELECTROSTATICS_H */
