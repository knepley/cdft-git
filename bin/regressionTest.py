#!/usr/bin/env python
# Dirk's answers
#
# 1) Run Channel for 2 Oxygen
# 2) Run Channel for 4 Oxygen with continuation
# 3) Email Deszo for runs at 2,4,6,8 Oxygens (give all specifications in the channel), ask for data format
import os,sys
sys.path.insert(0, os.path.join(os.environ['PETSC_DIR'], 'config', 'BuildSystem'))

import unittest
import script
import daemon
import nargs
from dftRunner import kT, DFT
from dftData   import DirkData, PetDFTData, EquilibriumHardSphere, EquilbilrumElectrostatics

class TestDFT(unittest.TestCase, script.Script):
  def __init__(self, name):
    import RDict
    script.Script.__init__(self, argDB = RDict.RDict())
    unittest.TestCase.__init__(self, name)
    self.runner = DFT(self.argDB)
    self.setup()
    return

  def setupHelp(self, help):
    import nargs

    script.Script.setupHelp(self, help)
    help.addArgument('DFT', '-dft_debug=<int>', nargs.ArgInt(None, 0, 'Debugging flag', min = 0, isTemporary = True))
    return

  def setup(self):
    return

  def processOutput(self, output):
    import re

    data      = {}
    sumRule   = False
    parameter = re.compile(r'(?P<quantity>\w+) = (?P<value>-?\d\.\d+(e[+-]\d\d)?)')
    for line in output.split('\n'):
      # Sum rule parse
      if line == 'Sum rule verification':
        sumRule = True
      if sumRule:
        l = line.strip().split(' ')
        if   l[0] == 'P':  pass
        elif l[0] == 'kT': pass
        elif l[0] == 'relative':
          sumRule = False
          data['sum rule'] = float(l[-1])
      # Parameter parse
      match = parameter.match(line)
      if match:
        data[match.group('quantity')] = float(match.group('value'))
    return data

  def dftTest(self, opts, checks, bounds = []):
    self.runner.buildExecutable()
    output = self.runner.runSimulation(opts)
    data   = self.processOutput(output)
    # TODO: Note that this kills the log files prematurely
    self.runner.storeOutput(4)
    for quantity in checks:
      self.assertAlmostEqual(checks[quantity], data[quantity], 4, 'Quantity '+quantity+'('+str(data[quantity])+') does not match ('+str(checks[quantity])+')')
    for quantity in bounds:
      self.failUnless(bounds[quantity] > data[quantity], 'Quantity '+quantity+'('+str(data[quantity])+') exceeds bound ('+str(bounds[quantity])+')')
    return

  def dftTest2(self, opts, checks, bounds = []):
    dataDir       = os.path.join('.', 'data')
    numSpecies    = 2
    speciesName   = ['Cation', 'Anion']
    z,   rho,   phi,   rhoRef   = PetDFTData().readData(numSpecies, self.argDB['domainLength'])
    zBF, rhoBF, phiBF           = DirkData().readFile(os.path.join(dataDir, 'problem2', 'Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0_Rosen.txt'), numSpecies)
    zRF, rhoRF, phiRF, rhoRefRF = DirkData().readFile(os.path.join(dataDir, 'problem2', 'Sim02_z_1_1_d_2.0_4.25_sc_0_c_1.0.txt'), numSpecies)
    print z
    print zBF
    print zRF
    return

  def OFFtestHardSphereBath(self):
    self.argDB['domainLength'] = 6.0
    self.argDB['ion_species']  = ['Test0', 'Test1']
    self.argDB['useES']        = False
    rhoM                       = [1.0, 1.0]
    radii                      = [self.runner.ionRadii[sp] for sp in self.argDB['ion_species']]
    eqHS     = EquilibriumHardSphere(rhoM, radii)
    muHSBath = eqHS.muHSBath()
    print 
    checks   = {'muHS_bath_0': muHSBath[0],
                'muHS_bath_1': muHSBath[1]}
    bounds   = {'sum rule':    0.0015}
    geomOpts = self.runner.getBaseGeometryOptions()
    discOpts = self.runner.getGridOptions(4)
    physOpts = self.runner.getPhysicsOptions(None)+['-rhoBathM '+','.join([str(r) for r in rhoM])]
    outOpts  = self.runner.getOutputOptions(False, False)
    return self.dftTest(geomOpts+discOpts+physOpts+outOpts, checks, bounds)

  def runProblem2(self, usePB, useRFD):
    self.argDB['domainLength']     = 6.0
    self.argDB['ion_species']      = ['Test0', 'Test2']
    self.argDB['usePB']            = usePB
    self.argDB['update_reference'] = useRFD
    rhoM                           = [1.0, 1.0]
    radii                          = [self.runner.ionRadii[sp] for sp in self.argDB['ion_species']]
    z                              = [self.runner.ionCharges[sp] for sp in self.argDB['ion_species']]
    eqHS = EquilibriumHardSphere(rhoM, radii)
    eqES = EquilbilrumElectrostatics(rhoM, radii, z)
    muHSBath = eqHS.muHSBath()
    Gamma    = eqES.Gamma(1.0)
    muESBath = eqES.muESBath(Gamma)
    checks = {'Gamma':       Gamma,
              'muHS_bath_0': muHSBath[0],
              'muHS_bath_1': muHSBath[1],
              'muES_bath_0': muESBath[0],
              'muES_bath_1': muESBath[1]}
    geomOpts = self.runner.getBaseGeometryOptions()
    discOpts = self.runner.getGridOptions(11)
    physOpts = self.runner.getPhysicsOptions(None)+['-rhoBathM '+','.join([str(r) for r in rhoM])]
    outOpts  = self.runner.getOutputOptions(False, True)
    self.dftTest(geomOpts+discOpts+physOpts+outOpts, checks)
    return self.dftTest2(geomOpts+discOpts+physOpts+outOpts, checks)

  def testProblem2PoissonBulkFluid(self):
    self.argDB['dataDir'] = 'testProblem2PoissonBulkFluid'
    return self.runProblem2(False, False)

  def testProblem2PoissonReferenceFluid(self):
    self.argDB['dataDir'] = 'testProblem2PoissonReferenceFluid'
    return self.runProblem2(False, True)

  def testProblem2PoissonBoltzmannBulkFluid(self):
    self.argDB['dataDir'] = 'testProblem2PoissonBoltzmannBulkFluid'
    return self.runProblem2(True, False)

  def testProblem2PoissonBoltzmannReferenceFluid(self):
    self.argDB['dataDir'] = 'testProblem2PoissonBoltzmannReferenceFluid'
    return self.runProblem2(True, True)

  def testCalciumChannel(self):
    return

if __name__ == '__main__':
  if nargs.Arg.findArgument('daemonize', sys.argv[1:]):
    print 'Starting daemon'
    daemon.createDaemon('.')
  module = __import__('__main__')
  test = unittest.TestLoader().loadTestsFromModule(module)
  testRunner = unittest.TextTestRunner(verbosity = 2)
  result = testRunner.run(test)
  sys.exit(not result.wasSuccessful())
